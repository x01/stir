#!/usr/bin/perl

use File::Copy;
use File::Path;
use File::Basename;

my $os = $^O;
print ("os= '$os'\n");

my $use_mechan=0;
BEGIN {
eval { require WWW::Mechanize; };
if (!$@) {
	print "Using mechanize!\n";
	$use_mechan=1;
}
}

use Archive::Tar;

sub run
{
	@args = @_;
#	print "system: ";
#	print "$_ " foreach (@args);
#	print "\n";
	system @args;
	return @?;
}

sub clean_target
{
	my ($dir) = @_;
	rmtree ($dir);
	return @?;
}

sub download
{
	my ($name, $url) = @_;
	print "  (o) Downloading $name...\n";

	if ($use_mechan) { 	
		my $mech = WWW::Mechanize->new();
		my $response = $mech->get( $url );
		
		my $filename = basename ($url);	
	#	sysopen local $fh, $filename, O_CREAT|O_WRONLY|O_TRUNC or die "Failed to open '$filename' to save data to\n";
	#	binmode $fh;

		open local $fh, ">", $filename or die "Failed to open '$filename' to save data to\n";
		binmode $fh;
		
		if ($response->is_success) {
			print $fh $response->content;
		}
		else {
			print STDERR $response->status_line, "\n";
		}
		
		close $fh or die "Failed to close '$filename'\n";
	} else {

		run ("curl", "-L", "-C","-", "-O", "$url") == 0
			or die ("  (x) Failed to download $name\n");
	}
}

sub untar
{
	my ($file, $final) = @_;
	my $extracted, $archive;

#	print "args: file: $file, final: $final\n";

	$file =~ /((\w+)-.*)\.tar\.(.+)/;
	if ($final eq "") {
		($extracted, $final, $archive) = ($1, $2, $3);
	}

	clean_target ($final);

#	print "archive: $archive\n";

	my $flags = "-x";
	if ($archive eq "gz") {
		$flags .= "z";
	} elsif ($archive eq "bz2") {
		$flags .= "j";
	}
	$flags .= "f";

#	print "1: $1, 2: $2, $3: $3\n";

#	print "args: file: $file, final: $final, flags: $flags\n";
	print "  (.) Extracting $file...\n";

#	run ("tar", "$flags", "$file") == 0
	Archive::Tar->extract_archive ($file, $gzip) != false
		or die ("Failed to extract $name\n");

	print "  (.) Renaming $extracted -> $final\n";
#	run ("mv", ) == 0
	move ($extracted, $final) 
		or die ("Failed to move '$extracted' to '$final'\n");
}

sub download_ne
{
	my ($name, $url) = @_;
	
	my $filename = basename ($url);	

	print ("- $name\n");
	
	if (-e $filename) {
		print "  (.) exists, not downloading\n";
	} else {
		download ($name, $url);
	}
}

download_ne ("zlib 1.2.3", "http://www.zlib.net/zlib-1.2.3.tar.gz");
untar ("zlib-1.2.3.tar.gz");

#download_ne ("physFS 2.0.0", "http://offload2.icculus.org:9090/physfs/downloads/physfs-2.0.0.tar.gz");
download_ne ("physFS 2.0.2", "http://icculus.org/physfs/downloads/physfs-2.0.2.tar.gz");
untar ("physfs-2.0.2.tar.gz");

download_ne ("ogg 1.1.3", "http://downloads.xiph.org/releases/ogg/libogg-1.1.3.tar.gz");
untar ("libogg-1.1.3.tar.gz");

download_ne ("freetype-2.3.9","http://downloads.sourceforge.net/project/freetype/freetype2/2.3.9/freetype-2.3.9.tar.gz");
untar ("freetype-2.3.9.tar.gz");

download_ne ("lua 5.1.4", "http://www.lua.org/ftp/lua-5.1.4.tar.gz");
untar ("lua-5.1.4.tar.gz");

download_ne ("vorbis 1.2.0", "http://downloads.xiph.org/releases/vorbis/libvorbis-1.2.0.tar.gz");
untar ("libvorbis-1.2.0.tar.gz");

download_ne ("theora", "http://downloads.xiph.org/releases/theora/libtheora-1.0.tar.gz");
untar ("libtheora-1.0.tar.gz");

download_ne ("luadbind 0.8", "http://garr.dl.sourceforge.net/sourceforge/luabind/luabind-0.8.tar.gz");
untar ("luabind-0.8.tar.gz");

download_ne ("UTF8 CPP 2.1", "http://sunet.dl.sourceforge.net/sourceforge/utfcpp/utf8_v2_1.zip");
system ("unzip", "-o", "-d", "utf8_v2_1", "-x", "utf8_v2_1.zip"); 
system ("svn", "co", "http://bullet.googlecode.com/svn/tags/bullet-2.77/", "bullet");
#system ("svn", "co", "http://bullet.googlecode.com/svn/tags/bullet-2.73/", "bullet");
#system ("svn", "co", "https://utfcpp.svn.sourceforge.net/svnroot/utfcpp", "utfcpp");
#system ("svn", "co", ""

download_ne ("libpng", "http://sunet.dl.sourceforge.net/project/libpng/02-libpng-devel/1.5.0beta53/libpng-1.5.0beta53.tar.gz");
untar ("libpng-1.5.0beta53.tar.gz");

download_ne ("BZip2", "http://www.bzip.org/1.0.6/bzip2-1.0.6.tar.gz");
untar ("bzip2-1.0.6.tar.gz");

download_ne ("CELT", "http://downloads.xiph.org/releases/celt/celt-0.11.1.tar.gz");
untar ("celt-0.11.1.tar.gz");

print " (o) Finish...\n"
