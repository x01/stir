/* Copyright (C) Dante Treglia II and Mark A. DeLoura, 2000. 
 * All rights reserved worldwide.
 *
 * This software is provided "as is" without express or implied
 * warranties. You may freely copy and compile this source into
 * applications you distribute provided that the copyright text
 * below is included in the resulting source code, for example:
 * "Portions Copyright (C) Dante Treglia II and Mark A. DeLoura, 2000"
 */
//==========================================================
// C++ Matrix Library
// Version: 2.5
// Date: April 22, 2000
// Authors: Dante Treglia II and Mark A. DeLoura
// Thanks to: Miguel Gomez, Stan Melax, Pete Isensee, 
//   Gabor Nagy, Scott Bilas, James Boer, Eric Lengyel
//==========================================================

#ifndef _MTXLIB_H
#define _MTXLIB_H

#include "stdio.h"

namespace mtxlib {

#if !defined(MTXLIB_DLL)
#define MTXLIB_DLL
//#pragma message ("MTXLIB_DLL=empty")
#else
#define MTXLIB_DLL_aux2(x) #x
#define MTXLIB_DLL_aux1(x) "" MTXLIB_DLL_aux2(x)
//#pragma message ("MTXLIB_DLL=" MTXLIB_DLL_aux1(MTXLIB_DLL) )
#endif

static inline float DegToRad(float a) { return a*0.01745329252f; };
static inline float RadToDeg(float a) { return a*57.29577951f; };

class vector2;
class vector3;
class vector4;
class matrix33;
class matrix44;
class quaternion;

////////////////////////////////////////////////////////////
// vector2 class
//

class MTXLIB_DLL vector2 
{
 public:
  // Members
  float x, y;

 public:
  // Constructors
  // vector2(): x(0), y(0) {};
  vector2() {};
  vector2(float inX, float inY);
  vector2(const vector2 &v);
  
 public:
  // Operators
  float                 &operator [] (unsigned int index);
  const float           &operator [] (unsigned int index) const;
  vector2               &operator =  (const vector2 &v);
  vector2               &operator += (const vector2 &v);
  vector2               &operator -= (const vector2 &v);
  vector2               &operator *= (float f);
  vector2               &operator /= (float f);
  friend bool           operator == (const vector2 &a, const vector2 &b);
  friend bool           operator != (const vector2 &a, const vector2 &b);
  friend vector2        operator - (const vector2 &a);
  friend vector2        operator + (const vector2 &a, const vector2 &b);
  friend vector2        operator - (const vector2 &a, const vector2 &b);
  friend vector2        operator * (const vector2 &v, float f);
  friend vector2        operator * (float f, const vector2 &v);
  friend vector2        operator / (const vector2 &v, float f);
  
  //operator float*() {return &x;};

 public:
  // Methods
  void                  set(float xIn, float yIn);
  float                 length() const;
  float                 lengthSqr() const;
  bool                  isZero() const;
  vector2               &normalize();
  
  // Debug
  void                  fprint(FILE* file, char* str) const;
};


////////////////////////////////////////////////////////////
// vector3 class
//

class MTXLIB_DLL vector3 
{
 public:
  // Members
  float x, y, z;

 public:
  // Constructors
  // vector3(): x(0), y(0), z(0) {};
  vector3() {};
  vector3(float inX, float inY, float inZ);
  vector3(const vector3 &v);
  explicit vector3(const vector2 &v);
  explicit vector3(const vector4 &v);
  
 public:
  // Operators
  float                 &operator [] (unsigned int index);
  const float           &operator [] (unsigned int index) const;
  vector3               &operator =  (const vector3 &v);
  vector3               &operator =  (const vector2 &v);
  vector3               &operator += (const vector3 &v);
  vector3               &operator -= (const vector3 &v);
  vector3               &operator *= (float f);
  vector3               &operator /= (float f);
  friend bool           operator == (const vector3 &a, const vector3 &b);
  friend bool           operator != (const vector3 &a, const vector3 &b);
  friend vector3        operator - (const vector3 &a);
  friend vector3        operator + (const vector3 &a, const vector3 &b);
  friend vector3        operator - (const vector3 &a, const vector3 &b);
  friend vector3        operator * (const vector3 &v, float f);
  friend vector3        operator * (float f, const vector3 &v);
  friend vector3        operator / (const vector3 &v, float f);

  //operator float*() {return &x;};

 public:
  // Methods
  void                  set(float xIn, float yIn, float zIn);
  float                 length() const;
  float                 lengthSqr() const;
  bool                  isZero() const;
  vector3               &normalize();
  
  // Debug
  void                  fprint(FILE* file, char* str) const;
};


////////////////////////////////////////////////////////////
// vector4 class
//

class MTXLIB_DLL vector4 
{
 public:
  // Members
  float x, y, z, w;
  
 public:
  // Constructors
  // vector4(): x(0), y(0), z(0), w(0) {};
  vector4() {};
  vector4(float inX, float inY, float inZ, float inW);
  vector4(const vector4 &v);
  explicit vector4(const vector3 &v);
  explicit vector4(const vector2 &v);
  
 public:
  // Operators
  float                 &operator [] (size_t index);
  const float           &operator [] (size_t index) const;
  vector4               &operator =  (const vector4 &v);
  vector4               &operator =  (const vector3 &v);
  vector4               &operator =  (const vector2 &v);
  vector4               &operator += (const vector4 &v);
  vector4               &operator -= (const vector4 &v);
  vector4               &operator *= (float f);
  vector4               &operator /= (float f);
  friend bool           operator == (const vector4 &a, const vector4 &b);
  friend bool           operator != (const vector4 &a, const vector4 &b);
  friend vector4        operator - (const vector4 &a);
  friend vector4        operator + (const vector4 &a, const vector4 &b);
  friend vector4        operator - (const vector4 &a, const vector4 &b);
  friend vector4        operator * (const vector4 &v, float f);
  friend vector4        operator * (float f, const vector4 &v);
  friend vector4        operator / (const vector4 &v, float f);

  //operator float*() {return &x;};

 public:
  // Methods
  void                  set(float xIn, float yIn, float zIn, float wIn);
  float                 length() const;
  float                 lengthSqr() const;
  bool                  isZero() const;
  vector4               &normalize();
  
  // Debug
  void                  fprint(FILE* file, char* str) const;
};

class MTXLIB_DLL quaternion
{
 public:
  // Members
  float x, y, z, w;
  
 public:
  // Constructors
  // vector4(): x(0), y(0), z(0), w(0) {};
  quaternion() {}
  quaternion(float inX, float inY, float inZ, float inW);
  quaternion(const quaternion &v);
  explicit quaternion(const vector3 &v);
  explicit quaternion(const vector2 &v);
  
 public:
  // Operators
  float                 &operator [] (unsigned int index);
  const float           &operator [] (unsigned int index) const;
  quaternion               &operator =  (const quaternion &v);
  quaternion               &operator =  (const vector3 &v);
  quaternion               &operator =  (const vector2 &v);
  quaternion               &operator += (const quaternion &v);
  quaternion               &operator -= (const quaternion &v);
  quaternion               &operator *= (float f);
  quaternion               &operator /= (float f);
  friend bool           operator == (const quaternion &a, const quaternion &b);
  friend bool           operator != (const quaternion &a, const quaternion &b);
  friend quaternion        operator - (const quaternion &a);
  friend quaternion        operator + (const quaternion &a, const quaternion &b);
  friend quaternion        operator - (const quaternion &a, const quaternion &b);
  friend quaternion        operator * (const quaternion &v, float f);
  friend quaternion        operator * (float f, const quaternion &v);
  friend quaternion        operator / (const quaternion &v, float f);

  //operator float*() {return &x;};

 public:
  // Methods
  void                  set(float xIn, float yIn, float zIn, float wIn);
  float                 length() const;
  float                 lengthSqr() const;
  bool                  isZero() const;
  quaternion               &normalize();
  
  // Debug
  void                  fprint(FILE* file, char* str) const;
};

inline bool           operator == (const quaternion &a, const quaternion &b) { return a.x == b.x && a.y == b.y && a.z == b.z && a.w == b.w; }
inline bool           operator != (const quaternion &a, const quaternion &b) { return a.x != b.x || a.y != b.y || a.z != b.z || a.w != b.w; }
inline quaternion        operator - (const quaternion &a) { return quaternion (-a.x, -a.y, -a.z, -a.w); }
inline quaternion        operator + (const quaternion &a, const quaternion &b);
inline quaternion        operator - (const quaternion &a, const quaternion &b)
{
	return quaternion (a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w);
}
inline quaternion        operator * (const quaternion &v, float f);
inline quaternion        operator * (float f, const quaternion &v);
inline quaternion operator * (quaternion const& a, quaternion const& b) {
	return quaternion(
		a.x*b.y + a.y*b.x + a.z*b.w - a.w*b.z,
		a.x*b.z - a.y*b.w + a.z*b.x + a.z*b.y,
		a.x*b.w + a.y*b.z - a.z*b.y + a.w*b.x,
		a.x*b.x - a.y*b.y - a.z*b.z - a.w*b.w);  
}

inline quaternion        operator / (const quaternion &v, float f)
{
	return quaternion (v.x/f, v.y/f, v.z/f, v.w/f);
}

////////////////////////////////////////////////////////////
// Miscellaneous vector functions
//

vector2 MTXLIB_DLL Normalized(const vector2 &a);
vector3 MTXLIB_DLL Normalized(const vector3 &a);
vector4 MTXLIB_DLL Normalized(const vector4 &a);
float MTXLIB_DLL DotProduct(const vector2 &a, const vector2 &b);
float MTXLIB_DLL DotProduct(const vector3 &a, const vector3 &b);
float MTXLIB_DLL DotProduct(const vector4 &a, const vector4 &b);
void MTXLIB_DLL SwapVec(vector2 &a, vector2 &b);
void MTXLIB_DLL SwapVec(vector3 &a, vector3 &b);
void MTXLIB_DLL SwapVec(vector4 &a, vector4 &b);
vector3 MTXLIB_DLL CrossProduct(const vector3 &a, const vector3 &b);
bool MTXLIB_DLL NearlyEquals(const vector2 &a, const vector2 &b, float r);
bool MTXLIB_DLL NearlyEquals(const vector3 &a, const vector3 &b, float r);
bool MTXLIB_DLL NearlyEquals(const vector4 &a, const vector4 &b, float r);


////////////////////////////////////////////////////////////
// matrix33 class
//

class MTXLIB_DLL matrix33 
{
 public:
  // Members
  vector3       col[3];
  
 public:
  // Constructors
  matrix33() {};
  matrix33(float v);
  matrix33(const matrix33 &m);
  matrix33(const vector3 &v0, const vector3 &v1, const vector3 &v2);
  
 public:
  // Operators
  vector3		&operator [] (unsigned int i);
  const vector3		&operator [] (unsigned int i) const;
  matrix33		&operator =  (const matrix33 &m);
  matrix33		&operator += (const matrix33 &m);
  matrix33		&operator -= (const matrix33 &m);
  matrix33		&operator *= (const matrix33 &m);
  matrix33		&operator *= (float f);
  matrix33		&operator /= (float f);
  friend bool		operator == (const matrix33 &a, const matrix33 &b);
  friend bool		operator != (const matrix33 &a, const matrix33 &b);
  friend matrix33	operator + (const matrix33 &a, const matrix33 &b);
  friend matrix33	operator - (const matrix33 &a, const matrix33 &b);
  friend matrix33	operator * (const matrix33 &a, const matrix33 &b);
  friend vector3 	operator * (const matrix33 &m, const vector3 &v);
  friend vector3 	operator * (const vector3 &v, const matrix33 &m);
  friend matrix33	operator * (const matrix33 &m, float f);
  friend matrix33	operator * (float f, const matrix33 &m);
  friend matrix33	operator / (const matrix33 &m, float f);
  
  //operator float*() {return (float*)col[0]; };

 public:
  // Methods
  matrix33		&identity();
  matrix33		&transpose();
  matrix33		&invert();

  // Debug
  void			fprint(FILE* file, char* str) const;
};

matrix33	MTXLIB_DLL IdentityMatrix33();
matrix33	MTXLIB_DLL TransposeMatrix33(const matrix33 &m);
matrix33	MTXLIB_DLL InvertMatrix33(const matrix33 &m);
matrix33	MTXLIB_DLL RotateRadMatrix33(float rad);
matrix33	MTXLIB_DLL TranslateMatrix33(float x, float y);
matrix33	MTXLIB_DLL ScaleMatrix33(float x, float y, float z = 1.0);


////////////////////////////////////////////////////////////
// matrix44 class
//

class MTXLIB_DLL matrix44
{
 public:
  // Members
  vector4	col[4];

 public:
  // Constructors
  matrix44() {};
  matrix44(float v);
  matrix44(const matrix44 &m);
  matrix44(const vector4 &v0, const vector4 &v1, 
	   const vector4 &v2, const vector4 &v3);
  explicit matrix44(const matrix33 &m);

 public:
  // Operators
  float& operator()(size_t i, size_t j) { return col[j][i]; }
  float operator()(size_t i, size_t j) const { return col[j][i]; }

  vector4		&operator [] (unsigned int i);
  const vector4		&operator [] (unsigned int i) const;
  matrix44		&operator =  (const matrix44 &m);
  matrix44		&operator =  (const matrix33 &m);
  matrix44		&operator += (const matrix44 &m);
  matrix44		&operator -= (const matrix44 &m);
  matrix44		&operator *= (const matrix44 &m);
  matrix44		&operator *= (float f);
  matrix44		&operator /= (float f);
  friend bool		operator == (const matrix44 &a, const matrix44 &b);
  friend bool		operator != (const matrix44 &a, const matrix44 &b);
  friend matrix44	operator + (const matrix44 &a, const matrix44 &b);
  friend matrix44	operator - (const matrix44 &a, const matrix44 &b);
  friend matrix44	operator * (const matrix44 &a, const matrix44 &b);
  friend vector3 	operator * (const matrix44 &m, const vector3 &v);
  friend vector3 	operator * (const vector3 &v, const matrix44 &m);
  friend vector4 	operator * (const matrix44 &m, const vector4 &v);
  friend vector4 	operator * (const vector4 &v, const matrix44 &m);
  friend matrix44	operator * (const matrix44 &m, float f);
  friend matrix44	operator * (float f, const matrix44 &m);
  friend matrix44	operator / (const matrix44 &m, float f);

  //operator float*() {return (float*)col[0]; };

 public:
  // Methods
  matrix44		&identity();
  matrix44		&transpose();
  matrix44		&invert();

  // Debug
  void			fprint(FILE* file, char* str) const;
};

matrix44	MTXLIB_DLL IdentityMatrix44();
matrix44	MTXLIB_DLL TransposeMatrix44(const matrix44 &m);
matrix44	MTXLIB_DLL InvertMatrix44(const matrix44 &m);
matrix44	MTXLIB_DLL RotateRadMatrix44(char axis, float rad);
matrix44	MTXLIB_DLL RotateRadMatrix44(const vector3 &axis, float rad);
matrix44	MTXLIB_DLL TranslateMatrix44(float x, float y, float z);
matrix44	MTXLIB_DLL ScaleMatrix44(float x, float y, float z, float w = 1.0);
matrix44	MTXLIB_DLL LookAtMatrix44(const vector3 &camPos, const vector3 &camUp, 
			    const vector3 &target );
matrix44	MTXLIB_DLL FrustumMatrix44(float l, float r, float b, float t, 
				float n, float f);
matrix44	MTXLIB_DLL PerspectiveMatrix44(float fovY, float aspect, 
				float n, float f);
matrix44	MTXLIB_DLL OrthoMatrix44(float l, float r, float b, float t, 
			    float n, float f);
matrix44	MTXLIB_DLL OrthoNormalMatrix44(const vector3 &xdir, 
				const vector3 &ydir, const vector3 &zdir);

}

#endif
