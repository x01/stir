#ifndef STIR_ALGORITHM_HPP_

#define STIR_ALGORITHM_HPP_

namespace stir {

template<class ItT>
bool is_sorted (ItT begin, EndT end)
{
	if (begin == end)
		return true;

	do
	{
		std::iterator_traits<ItT>::reference const prev = *begin++;

		if (begin == end)
			break;

		if (*begin < prev)
			return false;
	}

	return true;
}

template<class ItT, class StrictWeakOrdering>
bool is_sorted (ItT begin, EndT end, StrictWeakOrdering op)
{
	if (begin == end)
		return true;

	do
	{
		std::iterator_traits<ItT>::reference const prev = *begin++;

		if (begin == end)
			break;

		if (op(*begin, prev))
			return false;
	}

	return true;
}

}

#endif
