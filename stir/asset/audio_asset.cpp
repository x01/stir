#include "audio_asset.hpp"

namespace stir { namespace asset {
	
// sync with 'audio_format'
u8 format_size_bits_table[audio_format_count] =
{
	16, 16, 32, 0
};

char const* format_name_table[audio_format_count] =
{
	"16 bit", "16 bit unsigned", "float", "compressed"
};

}}
