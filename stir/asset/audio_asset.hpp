#ifndef STIR_AUDIO_ASSET_HPP_
#define STIR_AUDIO_ASSET_HPP_

#include <cassert>
#include <stir/dynamic_array.hpp>
#include <stir/basic_types.hpp>

#include <pulmotor/archive.hpp>

namespace stir { namespace asset {

	// sync with format_size_bits_table
enum audio_format
{
	wave_s16 = 0,
	wave_u16 = 1,
	wave_float = 2,
	wave_compressed = 3,
	
	audio_format_count
};
	
enum audio_flags
{
	audio_flag_channels_interleaved = 0x01
};
	
inline size_t format_size_bits (audio_format format)
{
	extern u8 format_size_bits_table[audio_format_count];
	return format_size_bits_table [format];
}

inline char const* format_name (audio_format format)
{
	extern char const* format_name_table[audio_format_count];
	return format_name_table [format];
}
	
enum audio_compression
{
	audio_compression_none = 0,
	audio_compression_celt = 1
};

struct audio_channel
{
	u64 duration; // in samples
	dynamic_array<u8> data;
	
	template<class ArchiveT>
	void archive (ArchiveT& ar, unsigned aver)
	{
		ar	& nv(duration) & nv(data);
	}
};
	
struct audio_asset
{
	u8  version, format, compression, channel_count;
	u8	flags, sample_size, reserved[2];
	int sampling_rate;
	dynamic_array<audio_channel> channel_data;

	// true if all contained channels have audio of the same length
	bool same_length () const {
		if (channel_data.empty ())
			return true;

		u64 l = channel_data[0].duration; 
		for (size_t i=1; i<channel_data.size (); ++i)
			if (l != channel_data[i].duration)
				return false;
		return true;
	}

	float seconds () const {
		assert (same_length ());

		if (channel_data.empty ())
			return 0;

		return (float)channel_data[0].duration / (float)sampling_rate;
	}
	
	template<class ArchiveT>
	void archive (ArchiveT& ar, unsigned aver)
	{
		ar	& nv(version) & nv(format) & nv(compression) & nv(channel_count)
			& nv(flags) & nv(sample_size) & reserved
			& nv(sampling_rate) & nv(channel_data)
		;
	}
	
};

template<class ArchiveT>
void serialize (ArchiveT& ar, audio_channel& chan, unsigned version)
{
	ar	| chan.data | chan.duration;
}
	
template<class ArchiveT>
void serialize (ArchiveT& ar, audio_asset& ass, unsigned version)
{
	ar	| ass.version | ass.format | ass.compression | ass.channel_count
		| ass.flags
		| ass.sampling_rate
		| ass.channel_data;
}
	
}}

#endif
