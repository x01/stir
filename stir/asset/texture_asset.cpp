#include "texture_asset.hpp"

namespace stir { namespace asset {

u8 pixel_size_table[7] = { 8, 16, 24, 32, 4, 8, 8 };
	
bool texture_builder::finish()
{
	std::sort(m_imgs.begin (), m_imgs.end (), [](image_ref& a, image_ref& b)
		{
			return a.width == b.width ? a.height < b.height : a.width < b.width;
		});
	
	int w = m_asset.width, h = m_asset.height;
	
	size_t wholeSize = 0;
	for (auto img : m_imgs)
	{
		if (img.width != w || img.height != h)
			return false;
		
		if (m_align != 0)
			stir::align(wholeSize, m_align);
		
		wholeSize += img.size_bytes();
		
		if (w > 1) w >>= 1;
		if (h > 1) h >>= 1;
	}
	
	m_asset.pixels.resize (wholeSize);
	m_asset.levels.resize (m_imgs.size ());
	
	w = m_asset.width, h = m_asset.height;
	size_t offset = 0;
	for (size_t i=0; i<m_imgs.size (); ++i)
	{
		image_ref& img = m_imgs[i];
		if (img.width != w || img.height != h)
			return false;
		
		if (m_align != 0)
			stir::align(offset, m_align);

		u8* pix = m_asset.pixels.data() + offset;
		
		image_ref dest (w, h, pix, pixel_size((texture_format)m_asset.format) >> 3);
		dest.put(0, 0, m_imgs[i]);
		m_asset.levels[i] = dest;
		
		offset += m_imgs[i].size_bytes();
		
		if (w > 1) w >>= 1;
		if (h > 1) h >>= 1;
	}
	
	return true;
}

void texture_builder::add_level (image_ref const& img)
{
	m_imgs.push_back(img);
}
	


}}
