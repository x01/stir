#ifndef STIR_TEXTURE_ASSET_HPP_
#define STIR_TEXTURE_ASSET_HPP_

#include <pulmotor/pulmotor_fwd.hpp>
#include <pulmotor/archive.hpp>
#include <stir/basic_types.hpp>
#include <stir/dynamic_array.hpp>
#include <stir/image.hpp>

namespace stir { namespace asset {
	
enum texture_format
{
	i8 = 0,
	//  r5g6b5 = 1,
	//  rgb8 = 2,
	rgba8 = 3,
	
	//  dxt1 = 0x10,
	//  dxt3 = 0x11,
	//  dxt5 = 0x12,

	texture_count = 2
};

inline size_t pixel_size (texture_format fmt) {
	extern u8 pixel_size_table[7];
	return pixel_size_table[fmt];
}

enum asset_compression {
	compress_none = 0,
	//	compress_zip = 1,
	//	compress_lzma = 2
};

struct texture_asset
{
	u8  version, format, compression, reserved;
	u32 width, height;
	
	dynamic_array<u8> pixels;
	dynamic_array<image_ref> levels;

	static const size_t MAX_LEVELS = 16;
	
	PULMOTOR_ARCHIVE()
	{
		ar & nv(version) & nv(format) & nv(compression) & nv(reserved) & nv(width) & nv(height);
		ar & nv(pixels);
		ar & pulmotor::with_ctx (levels, pixels.data ());
	}
};
	
class texture_builder
{
	size_t m_align;
	texture_asset& m_asset;
	std::vector<image_ref> m_imgs;
	std::list<image> m_temps;
	
public:
	texture_builder(texture_asset& ass, int w, int h, texture_format fmt) : m_asset(ass), m_align(0)
	{
		m_asset.version = 0;
		m_asset.reserved = 0;
		m_asset.compression = asset_compression::compress_none;
		m_asset.width = w;
		m_asset.height = h;
		m_asset.format = fmt;
	}
	
	~texture_builder()
	{}
	
	void align(size_t a) { m_align = a; }
	void compression (asset_compression comp) { m_asset.compression = comp; }

	bool finish();
	void add_level (image_ref const& img);
};

template<class ArchiveT>
void serialize (ArchiveT& ar, texture_asset& ass, unsigned version)
{
	ar  | ass.version | ass.format | ass.compression | ass.reserved
		| ass.width | ass.height
		| ass.levels;
}
	
}}

#endif // STIR_TEXTURE_ASSET_HPP_
