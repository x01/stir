#include "atlas_common.hpp"
#include "../log.hpp"

namespace stir {

STIR_DEFINE_LOG_INTERNAL (atlas_info, "atlas_info");
STIR_DEFINE_LOG_INTERNAL (atlas_error, "atlas_error");

}
