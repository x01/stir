#ifndef STIR_ATLAS_COMMON_HPP_
#define STIR_ATLAS_COMMON_HPP_

#include "../log.hpp"

namespace stir {

namespace atlas_detail {
	typedef unsigned int u32;
	typedef unsigned char u8;
}

STIR_DECLARE_LOG_INTERNAL (atlas_info);
STIR_DECLARE_LOG_INTERNAL (atlas_error);

}

#endif // STIR_ATLAS_COMMON_HPP_
