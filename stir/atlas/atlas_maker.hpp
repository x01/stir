#ifndef STIR_ATLAS_MAKE_HPP_
#define STIR_ATLAS_MAKE_HPP_

#include <functional>
#include <stack>
#include <cstddef>

namespace stir {

class atlas_maker
{
public:
	struct vector2
	{
		vector2() {}
		vector2(int xx, int yy) : x(xx), y(yy) {}
		int x, y;
	};

	struct rect
	{
		rect() {}
		rect (int xx, int yy, int ww, int hh)
			:	x(xx), y(yy), w(ww), h(hh) {}

		bool fits (int ww, int hh) const			{ return ww<=w && hh<=h;}
		bool fits_exactly (int ww, int hh) const	{ return ww==w && hh==h;}

		int x, y, w, h;
	};

	struct node
	{
		node*		child[2];
		rect		r;
		uintptr_t	load;
		bool		used; // marks an area used, so it can't be subdivided.

		bool is_used () const { return used; }
		bool is_leaf () const { return child[0] == 0 && child[1] == 0; }
		bool has_load () const { return load != 0; }
		void set_load(uintptr_t l) { load = l; }
		template<class T>
		T get_load() const { return (T)load; }

		node()
			:	load(0), used(false)
		{
			child[0] = 0;
			child[1] = 0;
		}
	};

	void clean_up (node& n)
	{
		if (n.child[0]) {
			clean_up (*n.child[0]);
			delete n.child[0];
			n.child[0] = 0;
		}

		if (n.child[1]) {
			clean_up (*n.child[1]);
			delete n.child[1];
			n.child[1] = 0;
		}
	}

	struct iterator
	{
		typedef std::forward_iterator_tag iterator_category;
		typedef node* value_type;
		typedef std::ptrdiff_t difference_type;
		typedef value_type* pointer;
		typedef value_type& reference;

		std::stack<node*>	m_parents;
		node*				m_ptr;

		iterator (node*	root)
			:	m_ptr (root)
		{}

		node& operator*() { return *m_ptr; }
		node* operator->() { return m_ptr; }
		bool operator==(iterator const& r) { return m_ptr == r.m_ptr; }
		bool operator!=(iterator const& r) { return m_ptr != r.m_ptr; }
		iterator operator++() { increment(); return *this; }
		iterator operator++(int) { increment(); return *this; }

		template<class T>
		T get_load () const { return m_ptr->get_load<T> (); } 

		void increment()
		{
			assert (m_ptr);
			if (m_ptr->child[0]) {
				m_parents.push (m_ptr);
				m_ptr = m_ptr->child[0];
				return;
			} else if (m_ptr->child[1]) {
				m_ptr = m_ptr->child[1];
				return;
			} else {
				while (!m_parents.empty())
				{
					m_ptr = m_parents.top()->child[1];
					m_parents.pop();
					if (m_ptr)
						return;
				}
				m_ptr = 0;
			}
		}
	};

	atlas_maker (int w, int h)
		:	m_pixel_budget(w*h), m_used_pixels(0)
	{
		m_root.r = rect(0,0,w,h);
	}

	~atlas_maker()
	{
		clean_up (m_root);
	}

	node* insert (int w, int h)
	{
		node* newnode = buy_node (m_root,w,h);
		if (!newnode)
			return 0;

		m_used_pixels += w * h;

		return newnode;
	}

	std::pair<int,int> calc_size()
	{
		int y = 0, x = 0;
		for (iterator it = begin(), endIt = end(); it != endIt; ++it)
		{
			if (!it->has_load())
				continue;

			int xmax = it->r.x + it->r.w;
			if (x < xmax)
				x = xmax;

			int ymax = it->r.y + it->r.h;
			if (y < ymax)
				y = ymax;
		}

		return std::make_pair(x,y);
	}

	unsigned width() const { return m_root.r.w; }
	unsigned height() const { return m_root.r.h; }

	unsigned total_pixels() const { return m_pixel_budget; }
	unsigned used_pixels() const { return m_used_pixels; }
	unsigned free_pixels () const { return m_pixel_budget - m_used_pixels; }

	size_t image_count () {
		size_t count = 0;
		for (iterator it = begin (), e = end (); it != e; ++it)
			if (it->is_leaf ())
				++count;

		return count;
	}

	iterator begin() { return iterator(&m_root); }
	iterator end() { return iterator(0); }

	void swap (atlas_maker& a)
	{
		std::swap (m_pixel_budget, a.m_pixel_budget);
		std::swap (m_used_pixels, a.m_used_pixels);
		std::swap (m_root, a.m_root);
	}

private:
	node* buy_node (node& n, int width, int height)
	{
		if (!n.is_leaf())
		{
			node* newnode = buy_node (*n.child[0], width, height);

			// found/created a node on the right
			if (newnode)
				return newnode;

			newnode = buy_node (*n.child[1], width, height);
			return newnode;
		}
		else
		{
			if (n.has_load() || n.is_used())
				return 0;

			if (!n.r.fits (width, height))
				return 0;

			if (n.r.fits_exactly (width, height)) {
				n.used = true;
				return &n;
			}

			n.child[0] = new node;
			n.child[1] = new node;

			int dw = n.r.w - width;
			int dh = n.r.h - height;
	        
			if (dw > dh)
			{
				n.child[0]->r = rect(n.r.x, n.r.y, width, n.r.h);
				n.child[1]->r = rect(n.r.x+width, n.r.y, n.r.w-width, n.r.h);
			}
			else
			{
				n.child[0]->r = rect(n.r.x, n.r.y, n.r.w, height);
				n.child[1]->r = rect(n.r.x, n.r.y+height, n.r.w, n.r.h-height);
			}

			return buy_node (*n.child[0], width, height);
		}
	}

	node		m_root;
	unsigned	m_pixel_budget, m_used_pixels;
};

}

#endif
