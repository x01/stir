#ifndef STIR_ATLAS_READER_HPP_
#define STIR_ATLAS_READER_HPP_

#include <vector>
#include <string>
#include <sstream>
#include <iostream>

namespace stir { namespace font_detail {

/*
struct glyph_info
{
	unsigned short page_image;
	unsigned short char_code;
	int	advance, beary, bearx;
	
	int x, y;
	int	width, height;
};

struct page_info
{
};

struct page
{
	texture	tex;
};
*/
class font_info
{
public:
	struct glyph {
		short int code;
		short int advance, width, height, beary, bearx;
		unsigned short int tex_x, tex_y;
		unsigned short int tex_width, tex_height;
		unsigned short int page_index;
		unsigned short int group_index;
	};
	
	struct kern_pair {
		int left, right, amount;
	};
	
	struct page {
		std::string		group_name;
		int				first_code_offset;
	};
		
	typedef std::vector<glyph> glyph_container;
	typedef std::vector<page> page_container;
	typedef std::vector<kern_pair> kerning_container;
	
	stir::string		m_atlas_name;
	page_container		m_pages;
	glyph_container		m_glyphs;
	kerning_container	m_kerning;

	font_info () {
	}
	
	void read_from_text (std::string const& str)
	{
		// format
		enum { k_directive, k_group, k_kerning, k_glyphs };
		
		std::stringstream in (str);
		std::string line;
		
//		kern_pair kp;
		
		int linenumber = 0;
		int phase = k_directive;
		while (std::getline(in, line))
		{
			// increase in the beginning, as linenumber was 0
			linenumber ++;
			
			if (line.empty())
				continue;
				
			line.erase (0, line.find_first_not_of(" \t"));
			
			// do we need to switch reading phase
			if (line[0]==':')
			{
				line.erase (0,1);
				std::size_t e = line.find_first_of (" \n");
				if (line.compare (0, 6, "groups") == 0) {
					phase = k_group;
				} else if (line.compare (0, 7, "kerning") == 0) {
					phase = k_kerning;
				} else if (line.compare (0, 6, "glyphs") == 0) {
					phase = k_glyphs;
				}
				
				continue;
			} else if (line[0]=='#')
				continue;
				
			std::stringstream i (line);

			switch (phase)
			{
				case k_directive:
				break;
				
				case k_group:
				{
					page pg;
					
					i >> pg.group_name >> pg.first_code_offset;
					m_pages.push_back (pg);
				}
				break;
				
				case k_kerning:
				{
					kern_pair kp;
					i >> kp.left >> kp.right >> kp.amount;
					m_kerning.push_back (kp);
				}
				break;
				
				case k_glyphs:
				{
					glyph g;
					i >> g.group_index >> g.page_index;
					i >> g.code >> g.advance >> g.width >> g.height >> g.beary >> g.bearx;
					i >> g.tex_x >> g.tex_y >> g.tex_width >> g.tex_height;
					
					m_glyphs.push_back (g);
				}
				break;
			}
		}
		
		
		// pages : [ page1, page2, ..., pageN ]
		// kerning : [
		//	[ "code", "code", value ],
		//	[ "code", "code", value ]
		// ]
		// symbols : [
		//	
		// ]
		
		// :pages
		// moo
		// aso
		// :kerning
		// A B nnn
		// :glyphs
		// A x y w h 
	}

//	void add_glyph (page_hint const& hint, glyph_info const& gi) {
//	}
	
//	void add_page (page_info const& p) {
//	}
	
/*	bool is_valid (wchar_t const code) const {
		page const& p = m_pages [((unsigned int)code&0xff00) >> 8];
		int ch = code & 0xff;
		return ch >= p.first_code_offset && ch < (p.size() - p.first_code_offset);
	}*/
	
	/*glyph_info const& get_info (wchar_t const code) const {
	
		assert (is_valid(code));
		
		page const& p = m_pages [((unsigned int)code&0xff00) >> 8];
		int ch = code & 0xff;
		
		if (ch >= p.first_code_offst && ch < (p.size() - p.first_code_offst))
		{
			// if symbol exists
			return p [ch];			
		}
		else
		{
			return m_invalid_glyph;
		}
	}*/
	
	template<class ArchiveT>
	void serialize (ArchiveT& ar, unsigned version) {
		ar & m_pages & m_invalid_glyph;
	}
};

/*void parse_font_definition (char const* config, size_t length)
{	
}*/


}} // namespace font_detail


#endif // STIR_ATLAS_READER_HPP_
