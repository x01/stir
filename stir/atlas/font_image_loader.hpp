#ifndef STIR_ATLAS_FT_IMAGE_LOADER_HPP_
#define STIR_ATLAS_FT_IMAGE_LOADER_HPP_

#include <ft2build.h>
#include FT_FREETYPE_H

#include "image_loader.hpp"
#include <stir/flat_types.hpp>

namespace stir { namespace atlas_detail { namespace ftloader {

struct glyph
{
	int			char_code;
	int			width, height, advance, bearingx, bearingy;
	int			bitmaptop, bitmapleft;
	image_ptr	image;
};

struct font_info
{
	int text_height, max_advance, ascender, descender;

//	int max_width, max_height;
//	int text_height, underline_position, underline_thickness;
};

struct font_image_generator
{
	font_image_generator (FT_Library library, char const* fontName, int heightInPixels)
		:	m_library (library)
		,	m_face (0)
	{
		FT_Error error;

		error = FT_New_Face (m_library, fontName, 0, &m_face );
		if (error == FT_Err_Unknown_File_Format)
			throw ft_error (error, "Font file seems to be unsupported");
		else if (error)
			throw ft_error (error, "Failed to load font file");

		error = FT_Set_Pixel_Sizes (m_face, 0, heightInPixels);
		if (error)
			throw ft_error (error, "Failed to set font size");
	}

	void load_glyph (glyph& g, wchar_t charcode)
	{
		int glyph_index = FT_Get_Char_Index( m_face, charcode );

		FT_Error error;
		error = FT_Load_Glyph( m_face, glyph_index, FT_LOAD_DEFAULT );
		if (error)
			throw ft_error (error, "Failed to load character glyph");

		error = FT_Render_Glyph( m_face->glyph, FT_RENDER_MODE_NORMAL );
		if (error)
			throw ft_error (error, "Failed to render glyph");

		image_ptr destimg (
			new image(
				m_face->glyph->bitmap.width,
				m_face->glyph->bitmap.rows,
				1
			)
		);
		destimg->set_from_greyscale (
			m_face->glyph->bitmap.buffer,
			m_face->glyph->bitmap.width
		);

		g.char_code= charcode;

		g.image		= destimg;
		g.width		= m_face->glyph->metrics.width;
		g.height	= m_face->glyph->metrics.height;

		g.bearingx	= m_face->glyph->metrics.horiBearingX;
		g.bearingy	= m_face->glyph->metrics.horiBearingY;
		g.advance	= m_face->glyph->metrics.horiAdvance;

		g.bitmaptop	= m_face->glyph->bitmap_top;
		g.bitmapleft= m_face->glyph->bitmap_left;
	}

	void get_font_info (font_info& fi)
	{
		fi.text_height = m_face->size->metrics.height;
		fi.max_advance = m_face->size->metrics.max_advance;
		fi.ascender = m_face->size->metrics.ascender;
		fi.descender= m_face->size->metrics.descender;
	}

	vector2i get_kerning (int a, int b)
	{
		FT_Vector k;
		FT_Get_Kerning (m_face, a, b, FT_KERNING_DEFAULT, &k);

		return vector2i (k.x, k.y);
	}

private:
	FT_Library	m_library;
	FT_Face		m_face;
};

}}}

#endif // STIR_ATLAS_FT_IMAGE_LOADER_HPP_
