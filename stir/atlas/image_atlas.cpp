#include <stir/platform.hpp>
#include <stir/basic_util.hpp>
#include <algorithm>

#include "image_atlas.hpp"
#include "../bits.hpp"

namespace stir {

using atlas_detail::u8;
using atlas_detail::u32;

image_atlas::image_atlas (int maxw, int maxh, int border)
	:	m_max_width(maxw), m_max_height(maxh), m_border(border)
	,	m_page_fence(0)
{
}

image_atlas::~image_atlas()
{
}

void image_atlas::add_image (int group, int w, int h, void* pix, bool grey, boost::any attr)
{
	// check if image can fit at all
	if ( w != m_max_width && m_max_width < w + 2 * m_border)
		return;
	if ( h != m_max_height && m_max_height < h + 2 * m_border)
		return;

	image_info* ii = new image_info();

	size_t imgsize = w * h * (grey ? 1 : 4);

	ii->group_id	= group;
	if (pix) {
		ii->pixels		= new unsigned char [imgsize];
		memcpy (ii->pixels, pix, imgsize);
	} else
		ii->pixels = pix;
	ii->format		= grey ? image_info::kGreyscale : image_info::kABGR;
	ii->width		= w;
	ii->height		= h;
	ii->attributes	= attr;
	
#if 1
	STIR_LOG(atlas_info).printf ("add_image: group:%d, img: w:%d, h:%d\n", group, w, h);	
#endif

	m_images.push_back (ii);
}

void image_atlas::fit_images (image_atlas::page_container& pages, image_atlas::image_info_container::iterator begin, image_atlas::image_info_container::iterator end)
{
	for (image_info_container::iterator it = begin; it != end; ++it)
	{
		// we have a atlas-page (atlas_maker) container for each group
		// if this container is empty, add new atlas_maker
		if (pages.empty())
			pages.push_back (new atlas_maker(m_max_width, m_max_height));

		image_info* img = *it;

		int atlasw = img->width +
			(img->width == m_max_width ? 0 : 2 * m_border);
		int atlash = img->height +
			(img->height == m_max_height ? 0 : 2 * m_border);

		// try to fit image into atlas_maker
		for (unsigned i=0; i<pages.size(); ++i)
		{
			atlas_maker& am = *pages [i];
			atlas_maker::node* newNode = am.insert (atlasw, atlash);
			if (!newNode)
			{
				// if inserting failed, move to another atlas_maker
				// if there is no more, add new page
				if (i == pages.size() - 1)
				{
					// add new page
					atlas_maker* newpage = new atlas_maker(m_max_width, m_max_height);
					newNode = newpage->insert (atlasw, atlash);
					if (!newNode)
					{
						delete newpage;
						continue;
					}
					pages.push_back(newpage);
				}
				else // next atlas_maker
					continue;
			}

			img->atlas_page_index = pages.size() - 1;

			// node found, set node's load and process to next image
			newNode->set_load ((uintptr_t)img);
			break;
		}
	}
}

void image_atlas::order_atlases (image_atlas::group_container& groups)
{
	if (m_images.empty())
		return;

	// sort images by group and area
	image_info_container images = m_images;
	std::sort (images.begin(), images.end(),
			ptr_argument_dereference_adapter<
			image_info_greater
		>()
	);

//	std::reverse (images.begin(), images.end());

	//
	image_info_container::iterator curIt = images.begin();
	image_info_container::iterator totalEndIt = images.end();
	while (curIt != totalEndIt)
	{
		unsigned current_groupid = (*curIt)->group_id;

		page_container& pages = groups [current_groupid];

		image_info_container::iterator endIt =
			std::upper_bound(curIt, totalEndIt, current_groupid, compare_group_id());

		fit_images (pages, curIt, endIt);

		// move to next group
		curIt = std::upper_bound (curIt, totalEndIt, current_groupid, compare_group_id() );
	}

#if 1
	// images positioned
	// for now, print out the position information
	for (group_container::iterator it = groups.begin();
		it != groups.end(); ++it)
	{
		STIR_LOG(atlas_info).printf ("group %d, %d pages:\n", it->first, it->second.size());
		
		page_container const& pages = it->second;
		for (unsigned i=0; i<pages.size(); ++i)
		{
			atlas_maker& am = *pages[i];

			for (atlas_maker::iterator it=am.begin(); it != am.end(); ++it )
			{
				if (!it->has_load())
					continue;
				STIR_LOG(atlas_info).printf ("  (page:%d, node:%d): (%d,%d,%d,%d)\n", i,
					std::distance (am.begin(), it),
					it->r.x, it->r.y, it->r.w, it->r.h
				);
			}
		}
	}
#endif
}

std::pair<int,int> image_atlas::optimize_atlas (atlas_maker& a, int iw, int ih)
{
	int w = iw, h = ih;
	int phase = 0;

	// calculate the size of atlas
	int maximgw = 0, maximgh = 0;
	for (atlas_maker::iterator it = a.begin(); it!=a.end(); ++it)
	{
		if (!it->has_load())
			continue;

		if (maximgw < it->r.w)
			maximgw = it->r.w;
		if (maximgh < it->r.h)
			maximgh = it->r.h;
	}

	// make atlas smaller by half and try to fit, if images do not fit
	// return currently sized atlas; otherwise, if images fit, repeat:
	// make atlas smaller and fit again
	while (true)
	{
		// make smaller and check if it is possible for all images to fit
		int lastw = w, lasth = h;
		if (phase && w / 2 >= maximgw)
			w >>= 1;
		else if (h / 2 >= maximgh)
			h >>= 1;
		else // some of the images will not fit
			return std::make_pair (w,h);

		phase = 1-phase;

		image_info_container images;
		atlas_maker am (w,h);
		
		// gather images from initial atlas
		for (atlas_maker::iterator it = a.begin(); it!=a.end(); ++it)
		{
			if (!it->has_load())
				continue;
			images.push_back ((image_info*)(it->load));
		}

		// sort by size
		std::sort (images.begin(), images.end(),
				ptr_argument_dereference_adapter<image_info_greater>()
		);

		// put images to atlas
		for (image_info_container::iterator it = images.begin(); it != images.end(); ++it)
		{
			// account for border
			int atlasw = (*it)->width +
				((*it)->width == m_max_width ? 0 : m_border*2);
			int atlash = (*it)->height +
				((*it)->height == m_max_height ? 0 : m_border*2);

			// add image, if it doesn't fit, return last good atlas and its size
			atlas_maker::node* node = am.insert (atlasw, atlash);
			if (!node)
				return std::make_pair (lastw,lasth);
			else
			{
				node->set_load ((uintptr_t)*it);
			}
		}
		
		STIR_LOG (atlas_info).printf ("  atlas size: %d x %d\n", w, h);

		// images did fit into smaller atlas, repeat fitting into even smaller one
		a.swap (am);
	}
}

void image_atlas::optimize_pages (image_atlas::page_container::iterator begin, image_atlas::page_container::iterator end)
{
	for( page_container::iterator it = begin; it != end; ++it )
	{
		atlas_maker& a = **it;
		if (a.used_pixels() <= a.total_pixels() / 2 )
		{
			STIR_LOG(atlas_info).printf ("Optimizing atlas: %d, %4.2f %% usage\n", it - begin,
				(float)a.used_pixels() / (float)a.total_pixels() * 100.0f );

			std::pair<int,int> newsize = optimize_atlas (**it, m_max_width, m_max_height);

			STIR_LOG(atlas_info).printf ("  Optimized atlas: %d, size: %dx%d, %4.2f %s usage\n", it - begin,
				newsize.first, newsize.second,
				(float)a.used_pixels() / (float)a.total_pixels() * 100.0f, "%" );
		}
	}
}

void image_atlas::optimize_atlas (image_atlas::group_container& groups)
{
	for (image_atlas::group_container::iterator it = groups.begin(); it != groups.end(); ++it)
	{
		STIR_LOG(atlas_info).printf ("group %d, %d pages:\n", it->first, it->second.size());
		image_atlas::page_container& pages = it->second;
		for (unsigned i=0; i<pages.size(); ++i)
		{
			STIR_LOG(atlas_info).printf ("page %d, %1.2f utilization\n", i, float(pages[i]->used_pixels())/pages[i]->total_pixels() );
		}

		STIR_LOG(atlas_info).printf ("optimizing page\n");
		optimize_pages (pages.begin(), pages.end());
	}
}

image_atlas::page_info::page_info (int ww, int hh, int comp)
	:	width(ww), height(hh), components(comp)
{
	data = new unsigned char [width*height*comp];
}

image_atlas::page_info::~page_info()
{
	delete[] (unsigned char*)data;
}

void image_atlas::page_info::put1 (int x, int y, void* src, int w, int h)
{
	for (int i=0; i<h; ++i) {
		void* d = (u8*)data + (width * (y+i) + x) * components;
		void* s = (u8*)src + w * i;

		if (components==1)
			std::copy ((u8*)s, (u8*)s + w, (u8*)d);
		else if (components==4)
			std::transform ((u8*)s, (u8*)s + w, (u32*)d, atlas_detail::expand_u8_to_u32alpha() );
		else
			utility::stir_fatal ("image_atlas::page_info::put1: unsupported number of components");
	}
}

void image_atlas::page_info::put4 (int x, int y, void* src, int w, int h)
{
	for (int i=0; i<h; ++i) {
		void* d = (u8*)data + (width * (y+i) + x) * components;
		void* s = (u32*)src + w * i;

		if (components==1)
			std::transform ((u32*)s, (u32*)s + w, (u8*)d, atlas_detail::convert_u32alpha_to_u8() );
		else if (components==4)
			std::copy ((u32*)s, (u32*)s + w, (u32*)d);
		else
			utility::stir_fatal("image_atlas::page_info::put4: unsupported number of components");
	}
}

void image_atlas::page_info::duplicate (int tox, int toy, int w, int h, int fromx, int fromy)
{
	assert (tox >= 0);
	assert (tox < width);
	assert (toy >= 0);
	assert (toy < height);
	
	assert (fromx >= 0);
	assert (fromx < width);
	assert (fromy >= 0);
	assert (fromy < height);
	
	assert (tox + w <= width);
	assert (toy + h <= height);
	
	assert (fromx + w <= width);
	assert (fromy + h <= height);
	
	for (int i=0; i<h; ++i) {
		
		void* d = (u8*)data + (width * (toy+i) + tox) * components;
		void* s = (u8*)data + (width * (fromy+i) + fromx) * components;

		if (components==1)
			for (u8* sp = (u8*)s, *end = (u8*)s + w, *dp = (u8*)d; sp != end; ++sp, ++dp)
				*dp = *sp;

//			for (int j=0; j<w; ++j) {
//				*((u8*)d+j) = *((u8*)s+j);
//			}
		else if (components==4)
			for (u32* sp = (u32*)s, *end = (u32*)s + w, *dp = (u32*)d; sp != end; ++sp, ++dp)
				*dp = *sp;
				
//			for (int j=0; j<w; ++j) {
//				*((u32*)d+j) = *((u32*)s+j);
//			}
		else
			utility::stir_fatal("image_atlas::page_info::duplicate: unsupported number of components");
	}
}

void image_atlas::page_info::fill_rect (int tox, int toy, int w, int h, int fromx, int fromy)
{
	for (int i=0; i<h; ++i) {
		void* d = (u8*)data + (width * (toy+i) + tox) * components;
		void* s = (u8*)data + (width * (fromy+i) + fromx) * components;

		if (components==1) {
			u8 pix = *(u8*)s;
			for (int j=0; j<w; ++j) {
				*((u8*)d+j) = pix;
			}
		} else if (components==4) {
			u32 pix = *(u32*)s;
			for (int j=0; j<w; ++j) {
				*((u32*)d+j) = pix;
			}
		} else
			utility::stir_fatal ("image_atlas::page_info::fill_rect: unsupported number of components");
	}
}

void image_atlas::page_info::fill_rect (int tox, int toy, int w, int h, color8 color)
{
	for (int i=0; i<h; ++i) {
		void* d = (u8*)data + (width * (toy+i) + tox) * components;

		if (components==1) {
			for (int j=0; j<w; ++j) {
				*((u8*)d+j) = (color&0xff000000) >> 24;
			}
		} else if (components==4) {
			for (int j=0; j<w; ++j) {
				*((u32*)d+j) = color;
			}
		} else
			utility::stir_fatal ("image_atlas::page_info::fill_rect: unsupported number of components");
	}
}

std::auto_ptr<image_atlas::page_info> image_atlas::build_page (atlas_maker& am, int components, bool allownonpow, bool borderDuplicate,  color8 borderFillColor)
{
	std::pair<int,int> size = am.calc_size();
	if (size.first == 0 && size.second == 0)
		return std::auto_ptr<page_info>();

	if (!allownonpow) {
		size.first = bits::next_power_of_two<unsigned short> (size.first);
		size.second = bits::next_power_of_two<unsigned short> (size.second);
	}

	std::auto_ptr<page_info> pageinfo (new page_info (size.first, size.second, components));
	memset (pageinfo->data, 0, components * size.first * size.second);

	for (atlas_maker::iterator it = am.begin(); it != am.end(); ++it )
	{
		atlas_maker::node& node = *it;
		if (!node.has_load())
			continue;
		
		pageinfo->nodes_.push_back (&node);

		image_info& ii = *node.get_load<image_info*>();
		int diffx = node.r.w - ii.width;
		int borderx = diffx >> 1;

		int diffy = node.r.h - ii.height;
		int bordery = diffy >> 1;
		
		if (ii.format == image_info::kGreyscale)
		{
			pageinfo->put1 (
				node.r.x + borderx, node.r.y + bordery,
				ii.pixels,
				ii.width, ii.height
			);
		} else if (ii.format == image_info::kABGR) {
			pageinfo->put4 (
				node.r.x + borderx, node.r.y + bordery,
				ii.pixels,
				ii.width, ii.height
			);
		} else
			utility::stir_fatal ("image_atlas::build_page: unknown image format");

		// add borders if such were specified
		if (borderDuplicate)
		{
			if (borderx != 0)
			{
				for (int i=0; i<borderx; ++i)
				{
					pageinfo->duplicate (
						node.r.x + i,				node.r.y + bordery,
						1,							ii.height,
						node.r.x + borderx,			node.r.y + bordery
					);
					pageinfo->duplicate (
						node.r.x + borderx + ii.width + i,	node.r.y + bordery,
						1,									ii.height,
						node.r.x + borderx + ii.width - 1,	node.r.y + bordery
					);
				}
			}

			if (bordery != 0)
			{
				for (int i=0; i<bordery; ++i)
				{
					pageinfo->duplicate (
						node.r.x + borderx,	node.r.y + i,
						ii.width,			1,
						node.r.x + borderx,	node.r.y + bordery
					);
					pageinfo->duplicate (
						node.r.x + borderx,	node.r.y + bordery + ii.height + i,
						ii.width,			1,
						node.r.x + borderx,	node.r.y + bordery + ii.height - 1
					);
				}
			}

			// add corners
			if (borderx != 0 || bordery != 0)
			{
				// top-left
				pageinfo->fill_rect (
					node.r.x, node.r.y,
					borderx, bordery,
					node.r.x + borderx, node.r.y + bordery
				);

				// top-right
				pageinfo->fill_rect (
					node.r.x + borderx + ii.width, node.r.y,
					borderx, bordery,
					node.r.x + borderx + ii.width - 1, node.r.y + bordery
				);

				// bottom-left
				pageinfo->fill_rect (
					node.r.x, node.r.y + bordery + ii.height,
					borderx, bordery,
					node.r.x + borderx, node.r.y + bordery + ii.height - 1
				);

				// bottom-right
				pageinfo->fill_rect (
					node.r.x + borderx + ii.width, node.r.y + bordery + ii.height,
					borderx, bordery,
					node.r.x + borderx + ii.width - 1, node.r.y + bordery + ii.height - 1
				);
			}
		} else {
			pageinfo->fill_rect (node.r.x, node.r.y, borderx, node.r.h, borderFillColor);
			pageinfo->fill_rect (node.r.x + ii.width + borderx, node.r.y, borderx, node.r.h, borderFillColor);

			pageinfo->fill_rect (node.r.x + borderx, node.r.y, ii.width, bordery, borderFillColor);
			pageinfo->fill_rect (node.r.x + borderx, node.r.y + ii.height + bordery, ii.width, bordery, borderFillColor);
		}
	}

	return pageinfo;
}

void image_atlas::build_atlas_images (image_atlas::atlas_images_container& atlasimages, image_atlas::group_container& groups, int components, bool allownonpot, bool borderDuplicate)
{
	for (group_container::const_iterator grIt = groups.begin(); grIt != groups.end(); ++grIt)
	{
		// iterate through all atlas pages and build page_info
		for (page_container::const_iterator it = grIt->second.begin(), endIt = grIt->second.end(); it != endIt; ++it)
		{
			std::auto_ptr<page_info> page =
				build_page (**it, components, allownonpot, borderDuplicate, 0x00000000);

			if (!page.get()) {
				STIR_LOG(atlas_error).printf ("Failed to build page %d, group %d\n",
					std::distance(grIt->second.begin(), it), grIt->first);
			} else {
				atlas_image ai (grIt->first, page);
				atlasimages.push_back (ai);
			}
		}
	}
}

}
