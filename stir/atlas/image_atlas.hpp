#ifndef STIR_ATLAS_IMAGE_ATLAS_HPP_
#define STIR_ATLAS_IMAGE_ATLAS_HPP_

#include <stir/platform.hpp>
#include <stir/math_types.hpp>

#include <stdexcept>
#include <map>
#include <vector>
#include <functional>
#include <cassert>

#include <boost/tr1/memory.hpp>
#include <boost/any.hpp>
#include <boost/noncopyable.hpp>

#include "atlas_maker.hpp"
#include "atlas_common.hpp"

namespace stir {

namespace atlas_detail {

struct expand_u8_to_u32alpha {
	u32 operator()( u8 a ) const {
		return ((u32)a << 24) | 0xffffff;
	}
};

struct convert_u32alpha_to_u8 {
	u8 operator()( u32 a ) const {
		return (u8)((a >> 24)&0xff);
	}
};

}

struct STIR_ATTR_DLL image_atlas : boost::noncopyable
{
public:
	struct image_info
	{
		enum PixelFormat { kGreyscale, kABGR };

		int			group_id;

		int			atlas_page_index;
		void*		pixels;
		PixelFormat	format;
		int			width, height;

		int offset_border_x (int rectw) const { return rectw - width >> 1; } 
		int offset_border_y	(int recth) const { return recth - height >> 1; } 

		boost::any	attributes;
	};

	struct image_info_greater : public std::binary_function<image_info const, image_info const, bool>
	{
		bool operator()(image_info const& a, image_info const& b) const
		{
			if (a.group_id == b.group_id)
			{
				int a_area = a.width * a.height;
				int b_area = b.width * b.height;

				return a_area > b_area;
			}
			else
			{
				return a.group_id < b.group_id;
			}
		}
	};

	template<class Func>
	struct ptr_argument_dereference_adapter : Func
	{
		ptr_argument_dereference_adapter (Func f = Func()) : Func(f) {}

		template<class T>
		bool operator()(T const* a, T const* b) const
		{	return Func::operator() (*a, *b); }
	};

	template<class Func, class Arg>
	struct dereference_and_forward : Func
	{
		template<class T>
		bool operator ()(Arg a, T const* b) const
		{	return Func::operator() (a,*b); }

		template<class T>
		bool operator ()(T const* b, Arg a) const
		{	return Func::operator() (*b,a); }
	};

	struct compare_group_id
	{
		bool operator() (int a, image_info const* b) const
		{	return a < b->group_id; }
		bool operator() (image_info const* a, int b) const
		{	return a->group_id < b; }
		bool operator() (image_info const* a, image_info const* b) const
		{	return a->group_id < b->group_id; }
	};

	typedef std::vector<image_info*>		image_info_container;

	typedef std::vector<atlas_maker*>		page_container;
	typedef std::map<int,page_container>	group_container;

	int				m_max_width, m_max_height, m_border;
	int				m_page_fence;

	image_info_container	m_images;

	image_atlas (int maxw, int maxh, int border);
	~image_atlas();

	// registers image for atlas
	void add_image (int group, int w, int h, void* pix, bool grey, boost::any attr);
	size_t image_count () const { return m_images.size (); }

	// iterates on added images and puts image rects into atlas pages,
	// separated into groupes by group_id
	void order_atlases (group_container& groups);

	// optimize a range of atlas pages
	void optimize_pages(page_container::iterator begin, page_container::iterator end);
	// optimize whole atlas
	void optimize_atlas (group_container& groups);

private:
	// places range of images into atlas page creating new if necessary
	void fit_images (page_container& pages, image_info_container::iterator begin, image_info_container::iterator end);
	// shrinks atlas_maker area making it no bigger than iw x ih and returns new size
	std::pair<int,int> optimize_atlas (atlas_maker& a, int iw, int ih);

public:
	struct page_info
	{
		page_info (int ww, int hh, int comp);
		~page_info();

		void put1 (int x, int y, void* src, int w, int h);
		void put4 (int x, int y, void* src, int w, int h);

		// duplicates pixel rect of size (w,h)
		void duplicate (int tox, int toy, int w, int h, int fromx, int fromy);

		// fills rect of size (w,h) by pixel color from (fromx,fromy) position
		void fill_rect (int tox, int toy, int w, int h, int fromx, int fromy);
		// fill a rect with specified color. alpha is used if data is 8bit
		void fill_rect (int tox, int toy, int w, int h, color8 color);

		int		width, height;
		int		components;
		void*	data;
		
		typedef std::vector<atlas_maker::node const*> node_container_t;
		node_container_t	nodes_;

	private:
		page_info( page_info const&);
		page_info& operator=( page_info const& a );
	};

	std::auto_ptr<page_info>
		build_page (atlas_maker& am, int components, bool allownonpow, bool borderDuplicate, color8 borderFillColor);

public:
	struct atlas_image
	{
		typedef std::tr1::shared_ptr<page_info> page_info_ptr;

		struct sort_by_group {
			typedef bool result_type;
			bool operator()( atlas_image const& a, atlas_image const& b) const {
				return a.group < b.group;
			}
		};

		page_info_ptr		page;
		int					group;		

		atlas_image (int gr, std::auto_ptr<page_info>& p)
			:	page (p), group (gr) {}
	};

	typedef std::vector<atlas_image> atlas_images_container;
	// borderDuplicate - whether to (false) fill with border color (currenly #000000) or (true) with border data
	void build_atlas_images (atlas_images_container& atlasimages, group_container& groups, int components, bool allownonpot, bool borderDuplicate);

	/*void fill_rect (atlas_maker::rect const& r, uintptr_t col)
	{
		for (int i=0; i<r.h; ++i)
		{
			unsigned int* p = m_img + (r.y+i) * m_page.m_options.max_page_size.x + r.x;
			std::fill_n (p, r.w, col);
		}
	}*/
};

}

#endif // STIR_ATLAS_IMAGE_ATLAS_HPP_
