#ifndef STIR_ATLAS_IMAGE_LOADER_HPP_
#define STIR_ATLAS_IMAGE_LOADER_HPP_

#include <stdexcept>

#include "atlas_common.hpp"

namespace stir { namespace atlas_detail {

struct ft_error : public std::runtime_error {
	ft_error (int err, std::string const& str)
		:	std::runtime_error (str), error (err) {}
		
	int error;
};

struct image {
	atlas_detail::u8* pixels;
	int width, height, components;

	image(int w, int h, int comp)
		:	width (w), height(h), components(comp)
	{
		pixels = new atlas_detail::u8 [w * h * components];
	}

	~image()
	{
		delete[] pixels;
	}

	void set_from_greyscale (void* src, int stride) {
		for (int i=0; i<height; ++i)
		{
			void* s = (atlas_detail::u8*)src + (i * stride); 
			void* d = pixels + (i * width) * components;

			if (components == 4)
				std::transform ((atlas_detail::u32*)s, (atlas_detail::u32*)s + width, (atlas_detail::u8*)d, convert_u32alpha_to_u8() );
			else if (components == 1)
				std::copy ((atlas_detail::u8*)s, (atlas_detail::u8*)s + width, (atlas_detail::u8*)d);
		}
	}
};

typedef std::tr1::shared_ptr<image> image_ptr;

struct image_loader
{
	typedef std::vector<image_ptr> image_container;

	virtual ~image_loader() = 0;

	virtual void get (image_container& output) = 0;
};

image_loader::~image_loader()
{
}

/* move gdiplus loaded to another file
//
Gdiplus::GdiplusStartupInput	gdiplusStartupInput;
ULONG_PTR						gdiplusToken;

struct gdiplus_file_image_loader : public image_loader
{
	gdiplus_file_image_loader()
	{
	}

	void load_disk_image (std::wstring const& a)
	{
		// try stb_image first, if it doesn't work, fallback to gdiplus
		GdiplusStartup (&gdiplusToken, &gdiplusStartupInput, NULL);

		Gdiplus::Bitmap img (a.c_str(), false);

		Gdiplus::BitmapData bitmapData;
		Gdiplus::Rect rect(0, 0, img.GetWidth(), img.GetHeight());

		img.LockBits (&rect, Gdiplus::ImageLockModeRead, PixelFormat32bppARGB, &bitmapData);

		image_ptr destimg (new image(bitmapData.Width, bitmapData.Height,4));
		destimg->set (bitmapData.Scan0, bitmapData.Width, bitmapData.Height, bitmapData.Stride);

		img.UnlockBits (&bitmapData);
	}

	virtual void get (image_container& output)
	{
		output.reserve (m_images.size());
		std::copy (m_images.begin(),m_images.end(), std::back_inserter(output));
	}

private:
	image_container		m_images;
};*/

} } // namespace stir :: atlas_detail

#endif // STIR_ATLAS_IMAGE_LOADER_HPP_
