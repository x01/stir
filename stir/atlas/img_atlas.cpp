#include <stir/platform.hpp>
#include <stir/basic_util.hpp>
#include <algorithm>

#include "img_atlas.hpp"
#include "../bits.hpp"

namespace stir {


// 
atlas_page::atlas_page (int idx, int ww, int hh)
:	index (idx)
,	img(NULL)
,	maker (new atlas_maker (ww, hh))
{
}

atlas_page::~atlas_page()
{
	delete maker;
	delete img;
}
void atlas_page::build_image (int flags, color8 borderFillColor)
{
	std::pair<int,int> size = maker->calc_size ();

	int imgW = size.first;
	int imgH = size.second;

	if (flags & pot_only) {
		imgW = bits::next_power_of_two<unsigned short> (imgW);
		imgH = bits::next_power_of_two<unsigned short> (imgH);
	}

	// TODO: do not create image if no actual images are in the page
	img = new image (imgW, imgH, 4);

	for (auto node : *maker)
	{
		image_info const& ii = *node.get_load<image_info const*>();
		if (!node.has_load ())
			continue;

		img->put (ii.actualX, ii.actualY, ii.img);

		int diffx = node.r.w - ii.img.width;
		int borderx = diffx >> 1;

		int diffy = node.r.h - ii.img.height;
		int bordery = diffy >> 1;
		
		// add borders if such were specified
		if (flags & border_dup)
		{
			if (borderx != 0)
			{
				for (int i=0; i<borderx; ++i)
				{
					img->duplicate (
						node.r.x + i,				node.r.y + bordery,
						1,							ii.img.height,
						node.r.x + borderx,			node.r.y + bordery
					);
					img->duplicate (
						node.r.x + borderx + ii.img.width + i,	node.r.y + bordery,
						1,										ii.img.height,
						node.r.x + borderx + ii.img.width - 1,	node.r.y + bordery
					);
				}
			}

			if (bordery != 0)
			{
				for (int i=0; i<bordery; ++i)
				{
					img->duplicate (
						node.r.x + borderx,	node.r.y + i,
						ii.img.width,			1,
						node.r.x + borderx,	node.r.y + bordery
					);
					img->duplicate (
						node.r.x + borderx,	node.r.y + bordery + ii.img.height + i,
						ii.img.width,			1,
						node.r.x + borderx,	node.r.y + bordery + ii.img.height - 1
					);
				}
			}

			// add corners
			if (borderx != 0 || bordery != 0)
			{
				// top-left
				img->fill_rect (
					node.r.x, node.r.y,
					borderx, bordery,
					node.r.x + borderx, node.r.y + bordery
				);

				// top-right
				img->fill_rect (
					node.r.x + borderx + ii.img.width, node.r.y,
					borderx, bordery,
					node.r.x + borderx + ii.img.width - 1, node.r.y + bordery
				);

				// bottom-left
				img->fill_rect (
					node.r.x, node.r.y + bordery + ii.img.height,
					borderx, bordery,
					node.r.x + borderx, node.r.y + bordery + ii.img.height - 1
				);

				// bottom-right
				img->fill_rect (
					node.r.x + borderx + ii.img.width, node.r.y + bordery + ii.img.height,
					borderx, bordery,
					node.r.x + borderx + ii.img.width - 1, node.r.y + bordery + ii.img.height - 1
				);
			}
		} else if (flags & border_fill) {
			img->fill_rect (node.r.x, node.r.y, borderx, node.r.h, borderFillColor);
			img->fill_rect (node.r.x + ii.img.width + borderx, node.r.y, borderx, node.r.h, borderFillColor);

			img->fill_rect (node.r.x + borderx, node.r.y, ii.img.width, bordery, borderFillColor);
			img->fill_rect (node.r.x + borderx, node.r.y + ii.img.height + bordery, ii.img.width, bordery, borderFillColor);
		}
	}
}

//
image_atlas::image_atlas (int maxw, int maxh, int border)
	:	m_xw(maxw), m_xh(maxh), m_border(border)
{
}

image_atlas::~image_atlas ()
{
	for (auto ii : m_images)
		delete ii;
}

void image_atlas::add (image_ref const& img, boost::any attr)
{
	image_info* ii = new image_info (img, attr);
	m_images.push_back (ii);
}

image_atlas::node_t* image_atlas::insert (atlas_page& page, image_info& ii)
{
	int extW = extend (ii.img.width);
	int extH = extend (ii.img.height);

	if (node_t* node = page.maker->insert (extW, extH)) {
		node->set_load ((uintptr_t)&ii);
		ii.node = node;
		ii.page_index = page.index;
		ii.actualX = node->r.x + (node->r.w - ii.img.width) / 2;
		ii.actualY = node->r.y + (node->r.h - ii.img.height) / 2;
		return node;
	}

	return 0;
}

void image_atlas::build_atlas (int flags, color8 borderFillColor)
{
	// sort by area
	// std::sort (begin(m_pages), end(m_pages), image_info::area_greater ());

	for (auto ii : m_images) {

		// We want to create a page only if there are images.
		atlas_page* page;
		if (pages.empty ()) {
			page = new atlas_page (0, m_xw, m_xh);
			pages.push_back (page);
		}

		// Try to fit the image into each page
		for (int i=0; i<pages.size(); ++i) {
			page = pages[i];
			if (insert (*page, *ii) == NULL) {

				if (i != pages.size () - 1)
					continue;

				// Failed to insert into existing pages, create new one
				page = new atlas_page ((int)pages.size(), m_xw, m_xh);
				if (insert (*page, *ii) == NULL)
					// Can't fit into a newly created one. Image is larger than the atlas.
					delete page;
				else {
					pages.push_back (page);
					break;
				}
			}
		}
	}

	// optimize atlas page and 
	for (auto page : pages)
	{
		optimize_page (*page);
		page->build_image(flags, borderFillColor);
	}
}

void image_atlas::optimize_page (atlas_page& page)
{
	// extrace image infos
	std::vector<image_info*> iis;
	for (auto node : *page.maker)
		if (node.has_load())
			iis.push_back (node.get_load<image_info*> ());

	// sort by area
	std::sort (iis.begin (), iis.end(), image_info::area_greater ());

	// atlas into smaller one
	int mw = page.maker->width ();
	int mh = page.maker->height ();
	bool shrinkw = true, shrinkh = true, horizontal = true;
	while (true)
	{
		int w = mw;
		int h = mh;
		
		if (horizontal) {
		   if (shrinkw) w /= 2;
		} else {
		   if (shrinkh)	h /= 2;
		}

		// can't shrink anymore, return
		if (w == mw && h == mh)
		{
			atlas_page smaller (page.index, w, h);
			for (auto ii : iis)
				if (!insert (smaller, *ii))
					utility::stir_fatal ("An image should fit, but it doesn't. ");

			page.swap (smaller);
			return;
		}
		
		bool canFit = true;
		atlas_maker tempAtlas (w, h);
		for (auto ii : iis)
		{
			int extW = extend (ii->img.width);
			int extH = extend (ii->img.height);
			if (!tempAtlas.insert (extW, extH))
			{
				canFit = false;
				break;
			}
		}

		if (canFit == false) {
			if (horizontal) shrinkw = false; else shrinkh = false;
		} else {
			mw = w;
			mh = h;
		}

		horizontal = !horizontal;
	}
}


}

