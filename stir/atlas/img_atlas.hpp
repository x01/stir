#include <stir/platform.hpp>
#include <stir/math_types.hpp>
#include <stir/flat_types.hpp>
#include <stir/basic_util.hpp>
#include <stir/image.hpp>

#include <stdexcept>
#include <map>
#include <vector>
#include <functional>
#include <cassert>

//#include <boost/tr1/memory.hpp>
#include <boost/any.hpp>
#include <boost/noncopyable.hpp>

#include "atlas_maker.hpp"
#include "atlas_common.hpp"

namespace stir {

struct image_info
{
	struct area_greater// : public std::binary_function<image_info const, image_info const, bool>
	{
		bool operator()(image_info const* a, image_info const* b) const {
			assert (a != NULL); assert (b != NULL);
		   	return operator()(*a, *b);
	   	}
		bool operator()(image_info const& a, image_info const& b) const
		{
			int a_area = a.img.width * a.img.height;
			int b_area = b.img.width * b.img.height;
			return a_area > b_area;
		}
	};

	image_info (image_ref const& image, boost::any attribute)
		:	img(image), page_index(-1), actualX(-1), actualY(-1), attr(attribute), node(NULL)
	{}
	
	areaf area (float scaleX, float scaleY) const {
		float u0 = actualX * scaleX,
			  v0 = actualY * scaleY,
			  u1 = u0 + img.width * scaleX,
			  v1 = v0 + img.height * scaleY;

		return areaf (u0, v0, u1, v1);
	}

	image_ref img; // the source image that will be included into an atlas
	int page_index;
	int actualX, actualY; // location of actual image, (borders ignored, if present)
	boost::any attr;
	atlas_maker::node* node;
};

struct atlas_page
{
	enum { pot_only = 0x01, border_dup = 0x02, border_fill = 0x04 };
	atlas_page (int idx, int ww, int hh);
	~atlas_page();

	void build_image (int flags = pot_only|border_dup, color8 borderFillColor = color8::black(0x00));

	int index; // Must be equal to the index in its container
	atlas_maker* maker;
	image* img;
	
	void swap (atlas_page& a) {
		std::swap (index, a.index);
		std::swap (maker, a.maker);
		std::swap (img, a.img);
	}

private:
	atlas_page( atlas_page const&);
	atlas_page& operator=( atlas_page const& a );
};

class image_atlas
{
	typedef atlas_maker::node node_t;

	int m_xw, m_xh, m_border;
	std::vector<image_info*> m_images;

	int extend(int size) {
		return size + m_border * 2;
	}

public:
	std::vector<atlas_page*> pages;

	image_atlas (int maxw, int maxh, int border);
	~image_atlas ();

	void add (image_ref const& img, boost::any attr);
	void build_atlas (int flags = atlas_page::pot_only|atlas_page::border_dup, color8 borderFillColor = color8::black(0x00));

private:
	node_t* insert (atlas_page& page, image_info& ii);
	void optimize_page (atlas_page& page);
};

} // stir
