#include "img_atlas.hpp"
#include <stb_image.h>
#include <boost/lexical_cast.hpp>

stir::image_ref load_image(std::string const& name)
{
	int w, h, c;
	stbi_uc* img = stbi_load (name.c_str(), &w, &h, &c, 4);
	if (!img) {
		printf("Failed to load %s\n", name.c_str());
		return stir::image_ref();
	}

	return stir::image_ref (w, h, img, 4);
}

struct image_info
{
	std::string name;
	stbi_uc* pix;
};

int main (int narg, char** parg)
{
	if (narg < 2) return 1;

	stir::image_atlas atlas (1024, 1024, 4);
	for (int i=1; i<narg; ++i)
	{
		std::string in = parg[i];
		stir::image_ref img = load_image (in);
		if (img.pixels)
			atlas.add (img, boost::any (image_info {in, img.pixels}));
	}

	atlas.build_atlas();

	//
	std::string outname = "atlas";
	for (stir::atlas_page* page : atlas.pages)
	{
		std::string out = outname + boost::lexical_cast<std::string> (page->index) + ".png";

		if (stir::image_ref const* img = page->img) {
			stbi_write_tga (out.c_str(), img->width, img->height, img->comp, img->pixels);
		}
	}

	return 0;
}
