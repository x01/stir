#ifndef STIR_STORAGE_IMAGE_ATLAS_HPP_
#define STIR_STORAGE_IMAGE_ATLAS_HPP_

#include <stir/dynamic_array.hpp>
#include <stir/flat_types.hpp>
#include <stir/basic_cstring.hpp>
#include <stir/basic_util.hpp>
#include <stir/limit_string.hpp>
//#include <stir/ser/stir.hpp>
#include <stir/asset/texture_asset.hpp>
#include <pulmotor/archive_std.hpp>

namespace stir { namespace storage { namespace image_atlas {
	
//struct pixel_section
//{
//	recti16		rect;
//	u16			page_index, group_index;
//	castring	original_name;
//	
//	template<class ArchiveT>
//	void serialize (ArchiveT& ar, unsigned version)
//	{
//		ar | rect | page_index | group_index | original_name;
//	}
//};
		
struct section
{
	areaf		area; // normalized coordinates (uv)
	u16			page_index, group_index;
	castring	original_name;
	
	template<class ArchiveT>
	void serialize (ArchiveT& ar, unsigned version)
	{
		ar | area | page_index | group_index | original_name;
	}
};

struct sprite_frame
{
	areaf		area; // normalized coordinates (uv)
	u16			page_index, group_index;
	
	template<class ArchiveT>
	void serialize (ArchiveT& ar, unsigned version)
	{
		ar | area | page_index | group_index;
	}
};

struct sprite_animation
{
	stir::limit_string<26, char> name;
	u16 start, count;

	struct sort_by_name { bool operator() (sprite_animation const& a, sprite_animation const& b) const {
		return strcmp (a.name.c_str(), b.name.c_str()) < 0; } };
	struct find_by_name {
		bool operator() (sprite_animation const& a, char const* name) const { return strcmp (a.name.c_str(), name) < 0; }
		bool operator() (char const* name, sprite_animation const& b) const { return strcmp (name, b.name.c_str()) < 0; }
   	};
	
	template<class ArchiveT>
	void serialize (ArchiveT& ar, unsigned version)
	{
		ar | name | start | count;
	}
};

struct sprite
{
	std::vector<sprite_frame> frames;
	std::vector<sprite_animation> anims;
//	stir::dynamic_array<sprite_frame> frames;
//	stir::dynamic_array<sprite_animation> anims;
	stir::limit_string<16, char> name;
	
	sprite () {}
	sprite (std::string const& n) : name(n) {}

	struct sort_by_name {
		bool operator()(sprite const& a, sprite const& b) const { return strcmp (a.name.c_str(), b.name.c_str()) < 0; }
	};

	struct find_by_name {
		bool operator()(sprite const& a, char const* name) const { return strcmp (a.name.c_str(), name) < 0; }
		bool operator()(char const* name, sprite const& b) const { return strcmp (name, b.name.c_str()) < 0; }
	};

	sprite_animation const* anim (char const* nm) const {
		auto it = utility::binary_find (anims.begin (), anims.end (), nm, sprite_animation::find_by_name ());
	   //[](sprite_animation const& a, char const* i) { return strcmp (a.name.c_str(), i) < 0; }
		return it == anims.end () ? NULL : &*it;
	}

	sprite_frame const& operator[] (size_t index) const {
		return frames[index];
	}
	
	sprite_animation const& operator[] (char const* nm) const {
		if (sprite_animation const* a = anim (nm)) return *a;
		//stir_fatal ("Sprite animation '%s' could not be found in sprite '%s'\n", nm, name.c_str());
		return anims[0];
	}
	
	template<class ArchiveT>
	void serialize (ArchiveT& ar, unsigned version)
	{
		ar | name | frames | anims;
	}
};

struct page
{
	castring	name;
	vector2f	pixel_size;
	asset::texture_asset* texture;
	
	page () : texture(nullptr) {}
	page(page const&) = delete;
	page& operator=(page const&) = delete;
	~page () { delete texture; }
	
	template<class ArchiveT>
	void serialize (ArchiveT& ar, unsigned version)
	{
		ar | name | pixel_size | texture;
	}
};

//struct pixel_group
//{
//	dynamic_array<pixel_section>	sections;
//	dynamic_array<castring>			atlas_page_names;
//	u16								section_begin, section_end;
//		
//	template<class ArchiveT>
//	void serialize (ArchiveT& ar, unsigned version)
//	{
//		ar | sections | atlas_page_names | section_begin, section_end;
//	}	
//};

//struct group
//{
//	u16							section_begin, section_end;
//	
//	template<class ArchiveT>
//	void serialize (ArchiveT& ar, unsigned version)
//	{
//		ar | sections | atlas_page_names | section_begin, section_end;
//	}	
//};



struct atlas
{
	stir::dynamic_array<page*>		pages;
	stir::dynamic_array<section>	sections;
	
	atlas() {}
	atlas (atlas const&) = delete;
	atlas& operator=(atlas const&) = delete;
	~atlas() {
		for (auto pp : pages) delete pp;
	}
	
	
	typedef stir::dynamic_array<section>::iterator section_iterator;
	typedef stir::dynamic_array<section>::const_iterator const_section_iterator;
	
	template<class ArchiveT>
	void serialize (ArchiveT& ar, unsigned version)
	{
		ar | pages | sections;
	}

	template<class ArchiveT>
	void archive (ArchiveT& ar, unsigned version)
	{
		ar & pages & sections;
	}
};

struct sprite_sheet
{
	std::vector<page*>	pages;
	std::vector<sprite>	sprites;
	
	sprite_sheet() {}
	sprite_sheet (sprite_sheet const&) = delete;
	sprite_sheet& operator=(sprite_sheet const&) = delete;
	~sprite_sheet() {
		for (auto pp : pages) delete pp;
	}
	
	typedef std::vector<sprite>::iterator sprite_iterator;
	typedef std::vector<sprite>::const_iterator const_sprite_iterator;

	sprite const* operator[] (char const* name) const {
		auto it = utility::binary_find (sprites.begin (), sprites.end (), name, sprite::find_by_name ());
		if (it == sprites.end ())
			return NULL;
		return &*it;
	}
	
	template<class ArchiveT>
	void serialize (ArchiveT& ar, unsigned version) {
		ar | pages | sprites;
	}
};

}}}

#endif
