#ifndef STIR_STORAGE_IMAGE_ATLAS_UTIL_HPP_
#define STIR_STORAGE_IMAGE_ATLAS_UTIL_HPP_

#include "storage-atlas.hpp"
#include "storage_access-atlas.hpp"
#include "../basic_util.hpp"
#include <algorithm>
#include <cstdio>
#include <string>

namespace stir { namespace storage { namespace image_atlas {

/*
inline int fill_pages (dynamic_array<page>& pages, char const* pageName, struct atlas& in)
{
	size_t pageOffset = pages.size ();
	pages.resize (pages.size () + in.pages.size ());

	// Create page info first, so we can reference it afterwards
	for (int i=0; i<in.pages.size(); ++i)
	{
		image_atlas::page p;
		atlas_page const* pg = in.pages[i];
		p.name = image_atlas::generate_name (pageName, pg->index).c_str();
		p.pixel_size = stir::vector2f (1.0f / pg->img->width, 1.0f / pg->img->height);
		pages[pageOffset + i] = p;
	}

	return pageOffset;
}

inline void fill (atlas& storage, char const* pageName, struct image_atlas& in, int group_index, std::string (*extract_name) (image_info const&))
{
	size_t pageOffset = fill_pages (storage.pages, pageName, in);

	// Fill temporary container with sections first, copy to storage later
	std::vector<storage::image_atlas::section> tempSections;

	for (int i=0; i<in.pages.size(); ++i)
	{
		atlas_page* pg = in.pages[i];

		for (auto const& node : *pg->maker)
		{
			if (!node.is_used () || !node.has_load ())
				continue;

			image_atlas::section sec;
			image_atlas::page const* p = &storage.pages[pageOffset + i];
			image_info const* ii = node.get_load<image_info const*> ();	

		 	sec.area = ii->area (p->pixel_size.x, p->pixel_size.y);
			sec.page_index = pageOffset + ii->page_index;
			sec.group_index = group_index;
			sec.original_name = extract_name (*ii).c_str();
			tempSections.push_back (sec);
		}
	}
	
	size_t sectionOffset = storage.sections.size ();
	storage.sections.resize (storage.sections.size () + tempSections.size ());

	std::copy (tempSections.begin (), tempSections.end (), storage.sections.begin () + sectionOffset);

	std::sort (storage.sections.begin (), storage.sections.end (), less_by_name_and_group ());
}


inline void fill (sprite_sheet& storage, char const* sheetName, struct image_atlas& in, int group_index,
	std::string (*extract_sprite_name) (image_info const&),
	int (*extract_sprite_frame) (image_info const&))
{
	size_t pageOffset = fill_pages (storage.pages, sheetName, in);

	// Fill temporary container with sections first, copy to storage later
	std::map<std::string, std::map<int, storage::image_atlas::sprite_frame> > tempSprites; // < name : <frame : data> >
	std::vector<storage::image_atlas::section> tempSections;

	for (int i=0; i<in.pages.size(); ++i)
	{
		atlas_page* pg = in.pages[i];

		for (auto const& node : *pg->maker)
		{
			if (!node.is_used () || !node.has_load ())
				continue;

			image_atlas::sprite_frame frame;
			image_atlas::page const* p = &storage.pages[pageOffset + i];
			image_info const* ii = node.get_load<image_info const*> ();	

		 	frame.area = ii->area (p->pixel_size.x, p->pixel_size.y);
			frame.page_index = pageOffset + ii->page_index;
			frame.group_index = group_index;
			std::string spriteName = extract_sprite_name (*ii);
			int spriteFrame = extract_sprite_frame (*ii);
			tempSprites[spriteName][spriteFrame] = frame;
		}
	}
	
	// 
	size_t spriteOffset = storage.sprites.size ();
	storage.sprites.resize (storage.sprites.size () + tempSprites.size ());
	
	int sprI = spriteOffset;
	for (auto const& spr : tempSprites)
	{
		storage::image_atlas::sprite& o_sprite = storage.sprites[sprI++];
		o_sprite.name = spr.first.c_str();
		o_sprite.frames.resize (spr.second.size ());

		int frmI = 0;
		for (auto const& frm : spr.second)
			o_sprite.frames[frmI++] = frm.second;
	}

	std::sort (storage.sprites.begin (), storage.sprites.end (), sprite::sort_by_name ());
}
*/
}}}

#endif
