#ifndef STIR_STORAGE_ACCESS_IMAGE_ATLAS_HPP_
#define STIR_STORAGE_ACCESS_IMAGE_ATLAS_HPP_

#include "storage-atlas.hpp"
#include "../basic_util.hpp"
#include <algorithm>
#include <cstdio>
#include <string>

namespace stir { namespace storage { namespace image_atlas {

// interface
struct less_by_name_and_group
{
	bool operator()(section const& a, section const& b) const
	{
		int cmp = strcmp (a.original_name.c_str (), b.original_name.c_str());
		if (cmp == 0)
			return a.group_index < b.group_index;
		else
			return cmp < 0;
	}

	bool operator()(section const& sect, std::pair<char const*, unsigned> const& info) const
	{
		int cmp = strcmp (sect.original_name.c_str (), info.first);
		if (cmp == 0)
			return sect.group_index < info.second;
		else
			return cmp < 0;
	}

	bool operator()(std::pair<char const*, unsigned> const& info, section const& sect) const
	{
		int cmp = strcmp (info.first, sect.original_name.c_str ());
		if (cmp == 0)
			return info.second < sect.group_index;
		else
			return cmp < 0;
	}
};

inline atlas::const_section_iterator find_section (atlas const& atlas, unsigned group, char const* section_name)
{
	std::pair<char const*, unsigned> key = std::make_pair (section_name, group);
	return utility::binary_find (atlas.sections.begin (), atlas.sections.end (), key, less_by_name_and_group());
}
	
inline section const* get_section (atlas const& atlas, char const* patch_name, int group = 0)
{
	atlas::const_section_iterator it = find_section (atlas, group, patch_name);
	return it == atlas.sections.end () ? NULL : &*it;
}	
	
inline stir::rectf get_pixel_rect (image_atlas::page const& page, areaf const& uv)
{
	float rx = 1.0f / page.pixel_size.x;
	float ry = 1.0f / page.pixel_size.y;
	return stir::rectf (uv.p0.x * rx, uv.p0.y * ry, uv.width () * rx, uv.height () * ry);
}

inline std::string generate_name (char const* name, int page)
{
	char buf [256];
	std::snprintf (buf, sizeof(buf)/sizeof(buf[0]), "%s_%03d", name, page);
	return buf;
}

}}}

#endif
