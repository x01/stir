#ifndef STIR_AUDIO_HPP_
#define STIR_AUDIO_HPP_

#include <stdint.h>

#include "resfwd.hpp"
#include "audio/openal_audio.hpp"	

namespace stir {
	
	extern index_manager<uintptr_t, loader<uintptr_t>> r_audio;
	typedef index_wrap<uintptr_t> i_audio;
}

#endif // STIR_AUDIO_HPP_
