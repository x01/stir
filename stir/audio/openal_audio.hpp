#ifndef STIR_OPENAL_AUDIO_HPP_
#define STIR_OPENAL_AUDIO_HPP_

#include <OpenAL/al.h>
#include <OpenAL/alc.h>

namespace stir {

void check_aol_error(char const* reason);
#define CHECK_OAL_ERROR(x) check_aol_error(x)
#define CALL_OAL(f,...) do { f(__VA_ARGS__); CHECK_OAL_ERROR(#f ", " #__VA_ARGS__); } while(0)

	
class openal_context
{
	ALCdevice *dev_;
    ALCcontext *ctx_;
	
public:
	openal_context();
	~openal_context();
	
	bool is_good () { return dev_ && ctx_; }
};

struct openal_audio {
	ALuint	source_;
};

	
}
#endif
