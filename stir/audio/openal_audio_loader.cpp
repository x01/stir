#include <OpenAL/al.h>
#include <OpenAL/alc.h>

#include <stir/stir_logs.hpp>
#include <stir/asset/audio_asset.hpp>
#include <stir/filesystem.hpp>

//#include <pulmotor/blit_holder.hpp>

#include "openal_audio_loader.hpp"
#include "openal_audio.hpp"

namespace stir {
	
	
openal_audio_loader::openal_audio_loader(path const& prefix)
	:	prefix_ (prefix)
{
}

openal_audio_loader::~openal_audio_loader()
{
}

uintptr_t openal_audio_loader::load( resource_id const& id, int profile )
{
	path p = prefix_ / id.get_id() + ".saud";
	
	ALuint buffer;
	alGenBuffers (1, &buffer);
	CHECK_OAL_ERROR("create buffer");
	
	// format: AL_FORMAT_STEREO16, AL_FORMAT_MONO16
//	path p = loader_aux::find_available_file (prefix_ / id.get_id(), &*suffixes_.begin (), suffixes_.size ());
	
	// load from file (using apple functions)
	STIR_LOG (resource) << "Loading " << p.string () << ", ";
	
	// TODO: loading a pulmotor texture is hacked in here. a proper way would be to
	// move this code to another loader and either specify another loader when loading a texture
	// or make a 'facade' loader which dynamically switches between the two depending
	// on the file extension.
	if (p.extension () == ".saud")
	{
#define USE_ARCHIVE 1
#if USE_ARCHIVE
		std::error_code ec;
		asset::audio_asset audio;
		filesystem::input_buffer in (p, ec);
		if (ec) {
			STIR_LOG (resource).printf ("failed to open file. Error (%d): %s\n", ec.value(), ec.message().c_str());
			goto return_invalid;
		}
		pulmotor::input_archive inA (in);
		pulmotor::archive(inA, audio);
		if (inA.ec) {
			STIR_LOG (resource).printf ("failed to read file. Error (%d): %s\n", ec.value(), ec.message().c_str());
			goto return_invalid;
		}
#else
		pulmotor::blit_holder<asset::audio_asset> ass;
		ass.load (p);
		if (!ass.good ())
			goto return_ivalid;

		asset::audio_asset const& audio = ass.ref ();
#endif
		
		STIR_LOG (resource).printf ("@%d, %s bit %s\n", audio.sampling_rate, asset::format_name ((asset::audio_format)audio.format), audio.channel_count == 1 ? "mono" : "stereo");
		
		if (! (audio.flags & asset::audio_flag_channels_interleaved)) {
			STIR_LOG (error) << "Non-interleaved audio is not supported: " << p.string () << log::endl;
			goto return_invalid;
		}

		if (audio.channel_count > 2) {
			STIR_LOG (error).printf ("Only 1 or 2 channel audio is supported (file has: %d): %s\n", audio.channel_count, p.c_str());
			goto return_invalid;
		}
		
		if (audio.format != stir::asset::wave_s16) {
			STIR_LOG (error).printf ("Only wave_s16 audio format is supported (file is: %d): %s\n", audio.format, p.c_str());
			goto return_invalid;
		}
		
		if (audio.channel_data.empty ()) {
			STIR_LOG (error).printf ("Should be at least one stream in the file (file has: %d): %s\n", audio.channel_data.size (), p.c_str());
			goto return_invalid;
		}
		
		ALint format = audio.channel_count == 1 ? AL_FORMAT_MONO16 : AL_FORMAT_STEREO16;
		size_t sampleSize = asset::format_size_bits ((asset::audio_format)audio.format);
		assert(audio.channel_data[0].duration <= UINT_MAX);
		size_t dataSize = (size_t)audio.channel_data[0].duration * (sampleSize >> 3);
		void const* data = &*audio.channel_data[0].data.begin ();
		
		alBufferData (buffer, format, data, (ALsizei)dataSize, audio.sampling_rate);
		CHECK_OAL_ERROR("set buffer data");
	}
			
	return buffer;
				
return_invalid:
	STIR_LOG (resource).printf ("\n");
	return 0;
}
	
void openal_audio_loader::unload( uintptr_t buffer, int profile )
{
	ALuint buf = (ALuint)buffer;
	alDeleteBuffers(1, &buf);
}	
	
}
