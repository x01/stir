#ifndef STIR_IOS_AUDIO_LOADER_HPP_
#define STIR_IOS_AUDIO_LOADER_HPP_

#include <stir/resource_loader.hpp>
#include <stir/resource_id.hpp>
#include <stir/path.hpp>
//#include "openal_audio.hpp"

namespace stir {

class openal_audio_loader : public loader<uintptr_t>
{
	path	prefix_;
	
public:
	openal_audio_loader (path const& prefix);
	~openal_audio_loader ();
	
	virtual uintptr_t load (resource_id const& id, int profile);
	virtual void unload (uintptr_t, int profile);	
};	
	
}

#endif // STIR_IOS_AUDIO_LOADER_HPP_
