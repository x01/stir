#ifndef STIR_WAVE_READER_HPP_
#define STIR_WAVE_READER_HPP_

namespace stir { namespace asset { namespace wave {

#define STIR_WAVE_BE(x) ((((x)&0xff000000) >> 24) | (((x)&0x00ff0000) >> 8) | (((x)&0x0000ff00) << 8) | (((x)&0x000000ff) << 24))

typedef stir::u8 u8;

#define WAVE_HEADER_SIZE 8

struct riff_chunk
{
	u32	id;
	u32	size;
	u32	format;
};

struct fmt_chunk
{
	u32	id;
	u32	size;
	u16 audio_format;
	u16	channel_count;
	u32	sample_rate;
	u32 byte_rate;
	u16 block_align;
	u16 bits_per_sample;
};

struct data_chunk
{
	u32 id;
	u32 size;
	// data follows

	void const* data_ptr () const { return &this->size + sizeof(u32); }
};


enum wave_result
{
	status_ok = 0,
	status_not_wave = 1,
	status_no_fmt_chunk = 2,
	status_no_data_chunk = 3
};

wave_result parse_header (u8 const* data, size_t header_size, fmt_chunk const** pfmt, data_chunk const** pdata)
{
	riff_chunk const* riff = (riff_chunk const*)data;

	if (riff->id != STIR_WAVE_BE('RIFF'))
		return status_not_wave;

	if (riff->format != STIR_WAVE_BE('WAVE'))
		return status_not_wave;
	
	fmt_chunk const* ch_fmt = (fmt_chunk const*)((u8*)data + WAVE_HEADER_SIZE + 4);
	if (ch_fmt->id != STIR_WAVE_BE('fmt '))
		return status_no_fmt_chunk;

	data_chunk const* ch_data = (data_chunk const*)((u8*)ch_fmt + ch_fmt->size + WAVE_HEADER_SIZE);
	if (ch_data->id != STIR_WAVE_BE('data'))
		return status_no_data_chunk;

	if (pfmt)
		*pfmt = ch_fmt;
	if (pdata)
		*pdata = ch_data;

	return status_ok;
}

}}}

#endif
