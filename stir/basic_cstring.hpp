#ifndef STIR_BASIC_CSTRING_HPP_
#define STIR_BASIC_CSTRING_HPP_

#include <string>
#include <memory>
//#include <pulmotor/ser.hpp>
#include <pulmotor/archive.hpp>

namespace stir {

// use this when string size is accessed rarely and strings are not being copied

template<class T, class Traits = std::char_traits<T> >
class basic_cstring
{
public:

	typedef T value_type;
	typedef Traits char_traits;
	typedef T* iterator;
	typedef T const* const_iterator;
	
private:
	T*	data_;
	
	static value_type null_elem_;
	static value_type* null_str () { return &null_elem_; }

public:
	basic_cstring () : data_(nullptr) {}

	basic_cstring (T const* a) {
		size_t size = a ? char_traits::length(a) : 0;
		if (size)
			uassign (a, size);
		else
			data_ = nullptr;
	}

	basic_cstring (T const* a, size_t size) {
		assert (char_traits::length(a) >= size);
		if (size)
			uassign (a, size);
		else
			data_ = nullptr;
	}
	basic_cstring (basic_cstring const& a) {
		if (a.data_ == nullptr)
			data_ = nullptr;
		else
			uassign (a.c_str(), a.size ());
	}
	basic_cstring& operator= (basic_cstring const& a) {
		basic_cstring temp (a);
		temp.swap (*this);
		return *this;
	}
	
	~basic_cstring() {
		delete_store();
	}
	
	iterator begin () { return data_ ? data_ : null_str(); }
	const_iterator begin () const { return data_ ? data_ : null_str(); }
	
	iterator end () { return begin() + size (); }
	const_iterator end () const { return begin() + size (); }
	
	void swap (basic_cstring& a) {
		std::swap (data_, a.data_);
	}

	T const* c_str () const {
		return data_ == nullptr ? null_str() : data_;
	}
	
	size_t size () const {
		return data_ == nullptr ? 0 : char_traits::length (data_);
	}
	
	bool empty () const {
		return data_ == nullptr || data_[0] == 0;
	}
	
	void assign (T const* a, size_t size) {
		delete []data_;
		uassign (a, size);
	}
	
	void resize_uninitialized (size_t size) {
		delete []data_;
		data_ = new T [size + 1];
	}		
	
	template<class ArchiveT>
	void serialize (ArchiveT& ar, unsigned version) {
		using namespace pulmotor;
		ar | ptr (data_, data_ ? Traits::length (data_) + 1 : 0);
	}
	
	PULMOTOR_ARCHIVE_SPLIT()
	PULMOTOR_ARCHIVE_READ()
	{
		pulmotor::u32 sz = 0;
		ar | nv_("size", sz);
		if (data_) {
			delete_store();
			data_ = nullptr;
		}
		
		if (sz > 0) {
			data_ = alloc_store(sz + 1);
			ar | pulmotor::memblock (data_, sz);
			data_[sz] = 0;
		}
	}
	PULMOTOR_ARCHIVE_WRITE()
	{
		pulmotor::u32 sz = (pulmotor::u32)size();
		ar | nv_("size", sz);
		ar | pulmotor::memblock (data_, sz);
	}
	
private:
	void uassign (T const* a, size_t size) {
		data_ = alloc_store (size + 1);
		Traits::copy (data_, a, size);
		data_ [size] = 0;
	}
	
	T* alloc_store(size_t size) {
		return new T[size];
	}
	
	void delete_store() {
		delete []data_;
	}
};

template<class T, class Traits>
typename basic_cstring<T, Traits>::value_type
basic_cstring<T, Traits>::null_elem_ = 0;

template<class T, class Traits>
inline bool operator== (basic_cstring<T, Traits> const& a, basic_cstring<T, Traits> const& b) {
	return Traits::compare (a.c_str(), b.c_str(), std::min (a.size(), b.size())) == 0;
}

template<class T, class Traits>
inline bool operator< (basic_cstring<T, Traits> const& a, basic_cstring<T, Traits> const& b) {
	return Traits::compare (a.c_str(), b.c_str(), std::min (a.size(), b.size())) < 0;
}
	
	
// string manipulation
template<class T, class Traits>
basic_cstring<T, Traits> operator+(basic_cstring<T, Traits> const& a, basic_cstring<T, Traits> const& b)
{
	size_t newSize = a.size () + b.size ();
	basic_cstring<T, Traits> result;
	result.resize_uninitialized (newSize);
	std::uninitialized_copy (a.begin (), a.end (), result.begin ());
	std::uninitialized_copy (b.begin (), b.end (), result.begin () + a.size ());
	return result;
}

template<class T, class Traits>
basic_cstring<T, Traits> operator+(basic_cstring<T, Traits> const& a, T const* b)
{
	size_t bSize = Traits::length (b);
	size_t newSize = a.size () + bSize;
	basic_cstring<T, Traits> result;
	result.resize_uninitialized (newSize);
	std::uninitialized_copy (a.begin (), a.end (), result.begin ());
	std::uninitialized_copy (b, b + bSize, result.begin () + a.size ());
	return result;
}
	
// string-interconnection
template<class T, class Traits>
bool operator==(basic_cstring<T, Traits> const& a, std::basic_string<T, Traits> const& b) {
	return Traits::compare (a.c_str(), b.c_str(), std::min (a.size(), b.size())) == 0;
}
template<class T, class Traits>
bool operator<(std::basic_string<T, Traits> const& a, basic_cstring<T, Traits> const& b) {
	return Traits::compare (a.c_str(), b.c_str(), std::min (a.size(), b.size())) < 0;
}
template<class T, class Traits>
bool operator<(basic_cstring<T, Traits> const& a, std::basic_string<T, Traits> const& b) {
	return Traits::compare (a.c_str(), b.c_str(), std::min (a.size(), b.size())) < 0;
}
	


typedef	basic_cstring<char, std::char_traits<char> >		castring;
typedef	basic_cstring<wchar_t, std::char_traits<wchar_t> >	cwstring;

typedef	cwstring	cstring;

}

#endif
