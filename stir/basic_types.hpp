#ifndef STIR_BASIC_TYPES_HPP_
#define STIR_BASIC_TYPES_HPP_

// int32_t (if will be still needed) and the like

#include "platform.hpp"
#include <cstdint>

namespace stir
{
#if STIR_WIDE_STRING
	typedef wchar_t				native_char_t;
#define STIR_SPFF "%ls"
#define STIR_STRING_PREFIX L
#else
#define STIR_SPFF "%s"
#define STIR_STRING_PREFIX
	typedef char				native_char_t;
#endif
	
	typedef char				int8_t;
	typedef unsigned char		uint8_t;

	typedef short int			int16_t;
	typedef unsigned short int	uint16_t;

	typedef int					int32_t;
	typedef unsigned int		uint32_t;

#ifdef _MSCVER
	typedef __int64				int64_t;
	typedef unsigned __int64	uint64_t;
#else
	typedef long long			int64_t;
	typedef unsigned long long	uint64_t;
#endif
	
	typedef	std::int8_t		s8;
	typedef	std::uint8_t	u8;

	typedef	std::int16_t	s16;
	typedef	std::uint16_t	u16;

	typedef	std::int32_t	s32;
	typedef	std::uint32_t	u32;

	typedef	std::int64_t	s64;
	typedef	std::uint64_t	u64;
}

#endif
