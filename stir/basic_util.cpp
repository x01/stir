#include "platform.hpp"
#include "basic_util.hpp"
#include <utf8/utf8.h>
#include <stdexcept>

#if !STIR_EXCEPTIONS
namespace boost {

void throw_exception (std::exception const& e)
{
	stir::native_char_t buf [256];
	std::snprintf (buf, 256, STIR_STRING_PREFIX "Exception was thrown, but exceptions are disabled: %s\n", e.what ());
	stir::utility::stir_fatal (buf);
}
	
}
#endif

namespace stir { namespace utility {

STIR_ATTR_DLL string to_stir (char const* str, size_t size) {	
#if STIR_WIDE_STRING
	string res;
	res.resize (size, ' ');
	for (size_t i=0; i<size; ++i)
		res[i] (str [i]);		
	return res;
#else
	return string (str, size);	
#endif
}
	
STIR_ATTR_DLL string to_stir(wchar_t const* str, size_t size) {
#if STIR_WIDE_STRING
	return string (str, size);	
#else
	string res;
	res.reserve (size);
	// todo: correct and optimize
	for (size_t i=0; i<size; ++i)
		utf8::unchecked::append (str[i], std::back_inserter (res));

	return res;
#endif
}
	

STIR_ATTR_DLL void stir_fatal (stir::string const& msg)
{
#if STIR_EXCEPTIONS
	throw std::runtime_error ("stir_fatal");
#else
	stir_fatal ("STIR-FATAL: %s\n", msg.c_str());
#endif
}
	
STIR_ATTR_DLL void stir_fatal (char const* fmt, ...)
{
	va_list va;
	va_start(va, fmt);
	char buf[1024];
	vsnprintf (buf, sizeof(buf), fmt, va);
	printf("%s", buf);
	va_end(va);
}


}}
