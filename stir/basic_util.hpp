#ifndef STIR_BASIC_UTIL_HPP_
#define STIR_BASIC_UTIL_HPP_

#include "platform.hpp"
#include "const_string.hpp"
#include "basic_types.hpp"
#include "complex_types.hpp"

#include <cstdio>
#include <cassert>
#include <iterator>
#include <boost/noncopyable.hpp>

#if STIR_WINDOWS
#define native_path(x) L ## x
typedef wchar_t native_pchar;
#else
#define native_path(x) x
typedef char native_pchar;
#endif

#define STIR_DECL_ENUM_FLAGS(T) \
	inline T operator|(T a, T b) { return (T)((int)a | (int)b); } \
	inline T operator&(T a, T b) { return (T)((int)a & (int)b); } \
	inline T operator~(T a) { return (T)(~(int)a); }


namespace stir {
	
template<class T>
inline T* stride (T* p, size_t bytes)
{
	return (T*)((char*)p + bytes);
}
	
template<class T>
inline T align (T value, size_t alignment)
{
	assert (alignment > 0 && (alignment & (alignment-1)) == 0);
	return (value-1 | alignment-1) + 1;
}

template<class T>
inline T is_aligned (T value, size_t alignment)
{
	assert (alignment > 0 && (alignment & (alignment-1)) == 0);
	return (value & (alignment-1)) == 0;
}

template<class T>
inline T round_up(T a, T roundingValue)
{
	assert (roundingValue > 0 && (roundingValue & (roundingValue-1)) == 0);
	return ((a - 1) | (roundingValue-1)) + 1;
}

template<class T>
inline T round_down(T a, T roundingValue)
{
	assert (roundingValue > 0 && (roundingValue & (roundingValue-1)) == 0);
	return a & (roundingValue-1);
}
	

template<class T>
class del_ptr : boost::noncopyable
{
public:
	del_ptr( T* p = 0 ) : m_ptr( p ) {}
	~del_ptr()				{	delete m_ptr; }

	T* operator=( T* p )	{	if( m_ptr ) delete m_ptr; return m_ptr = p; }
	T* operator->() const	{	return m_ptr; }
	T& operator*() const	{	return *m_ptr; }
	T** operator&()			{	return &m_ptr; }
	operator bool()			{	return m_ptr != 0; }

	T* release()			{ T* p = m_ptr; m_ptr = 0; return p; }
	T* get() const			{ return m_ptr; }
	void reset( T* p = 0)	{ delete m_ptr; m_ptr = p; }

private:
	T*	m_ptr;

public:
	operator std::auto_ptr<T>()
	{	T* p = m_ptr; m_ptr = 0; return std::auto_ptr<T>( p ); }
};

namespace utility {

STIR_ATTR_DLL void stir_fatal (stir::string const& msg);
STIR_ATTR_DLL void stir_fatal (char const* fmt, ...);

namespace detail {

struct ascii_to_wchar {
	wchar_t operator () (char a ) const { return a; }
};
struct wchar_to_ascii {
	char operator () (wchar_t a ) const { return a > 0xff ? '.' : (a&0x7f); }
};
	
inline size_t string_length(char const* p) {
	return strlen(p);
}
inline size_t string_length(wchar_t const* p) {
	return wcslen(p);
}

}

/*std::string to_ascii (stir::string const& a) {
	std::string result;
	result.resize (a.size());
	for (int i=0, size = a.size(); i<size; ++i)
		result [i] = detail::wchar_to_ascii() (a[i]);
	return result;
}*/

STIR_ATTR_DLL string to_stir(char const* str, size_t size);
STIR_ATTR_DLL string to_stir(wchar_t const* str, size_t size);

#if STIR_WIDE_STRING
inline string to_stir (wchar_t const* str) {
	return to_stir (str, wcslen(str) );
}
inline string to_stir (std::wstring const& str) {
	return to_stir (str, str.size());
}
#else
inline string to_stir (char const* str) {
	return to_stir (str, strlen (str));
}
inline string to_stir (std::string const& str) {
	return to_stir (str.c_str(), str.size());
}
#endif
	
struct deleter
{
	typedef void result_type;

	template<class T>
	void operator()( T const*& p ) const
	{	delete p; p = 0; }

	template<class T>
	void operator()( T const* p ) const
	{	delete p; }
};

struct second_deleter
{
	typedef void result_type;

	template<class T>
	void operator()( T& p ) const
	{	delete p.second; p.second = 0; }
};

struct second_deleter_rvalue
{
	typedef void result_type;

	template<class T>
	void operator()( T p ) const
	{	delete p.second; }
};

struct select_second
{
	typedef void result_type;

	template<class T>
	typename T::second_type& operator()( T& p ) const
	{	return p.second; }

	template<class T>
	typename T::second_type const& operator()( T const& p ) const
	{	return p.second; }
};

template<class T>
struct dereferer
{
	typedef T& result_type;

	template<class A>
	T& operator()( A& p ) const
	{	return *p; }

	template<class A>
	T const& operator()( A const& p ) const
	{	return *p; }
};

template<typename ItT>
bool is_sorted (ItT begin, ItT end)
{
	if (begin == end)
		return true;
	
	for (ItT prev = begin++; begin != end; ++begin)
		if (*begin < *prev)
			return false;

	return true;
}

template<typename ItT, typename CompareT>
bool is_sorted (ItT begin, ItT end, CompareT comp)
{
	if (begin == end)
		return true;
	
	for (ItT prev = begin++; begin != end; ++begin)
		if (comp (*begin, *prev))
			return false;
	
	return true;
}

template<class FwdIteratorT, class KeyT>
inline FwdIteratorT binary_find (FwdIteratorT begin, FwdIteratorT end, KeyT const& key)
{
	FwdIteratorT it = std::lower_bound (begin, end, key);
	return it != end && !(key < *it) ? it : end;
}

template<class FwdIteratorT, class KeyT, class LessOpT>
inline FwdIteratorT binary_find (FwdIteratorT begin, FwdIteratorT end, KeyT const& key, LessOpT op)
{
	FwdIteratorT it = std::lower_bound (begin, end, key, op);
	return it != end && !op(key, *it) ? it : end;
}

}

using utility::stir_fatal;

} // stir :: utility

#endif
