#ifndef STIR_BITS_HPP_
#define STIR_BITS_HPP_

#include <cassert>

namespace stir { namespace bits {

template<class T>
inline T next_power_of_two (T a)
{
	// calc next power of two		
	unsigned x = a - 1;
	
//		if (sizeof(T)>=8)
//			x |= x >> 32;
	
	if (sizeof(T)>=4)
		x |= x >> 16;

	if (sizeof(T)>=2)
		x |= x >> 8;
	
	x |= x >> 4;
	x |= x >> 2;
	x |= x >> 1;

	assert (a <= x+1);

	return x+1;
}
	
template<class T>
inline bool is_pot (T a)
{
	return (a & a-1) == 0;
}

}} // stir::bits

#endif
