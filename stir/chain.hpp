#ifndef STIR_CHAIN_HPP
#define STIR_CHAIN_HPP

#include <boost/noncopyable.hpp>
#include <cassert>

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4355)
#endif

namespace stir {


	//      -->  -->  -->(h)
	//      H    X    T
	// (t)<--  <--  <--
template<class DerivedT>
struct double_chain : boost::noncopyable
{
	double_chain(DerivedT* p, DerivedT* n) : m_prev(p), m_next(n) {}
	
public:
	double_chain() : m_next (static_cast<DerivedT*> (this)), m_prev (static_cast<DerivedT*> (this))
	{}
	
	double_chain& operator=(double_chain&& a)
	{
		unlink_this();
		m_next = a.m_next;
		m_prev = a.m_prev;
	}
	
	void head_insert (double_chain& a)
	{
		DerivedT *tail = m_prev;
		DerivedT *head = static_cast<DerivedT*> (this);
		
		head->m_next = static_cast<DerivedT*>(&a);
		tail->m_prev = static_cast<DerivedT*>(&a);
		
		a.m_prev = head;
		a.m_next = tail;
	}

	void tail_insert (double_chain& a)
	{
		DerivedT *tail = m_prev;
		DerivedT *head = static_cast<DerivedT*> (this);
		
		tail->m_next = static_cast<DerivedT*>(&a);
		head->m_prev = static_cast<DerivedT*>(&a);
		
		a.m_next = head;
		a.m_prev = tail;
	}
	
	size_t count_size () const {
		
		size_t s = 0;
		double_chain<DerivedT> const* cur = m_next;
		while (cur != this) {
			++s;
			cur = cur->m_next;
		}
		
		return s;
	}

	DerivedT* get_next ()
	{ return static_cast<DerivedT*> (m_next); }
	DerivedT* get_prev ()
	{ return static_cast<DerivedT*> (m_prev); }
	
	// A (B) C D
	// A C D     B
	//
	void unlink_this()
	{
		DerivedT* p = static_cast<DerivedT*> (m_prev);
		DerivedT* n = static_cast<DerivedT*> (m_next);
		
		p->m_next = n;
		n->m_prev = p;
		
		m_next = m_prev = static_cast<DerivedT*> (this);
	}

	// returns the cut tail starting after this node
	DerivedT* cut_tail ()
	{
		DerivedT* tail = m_next;
		if (tail)
			tail->m_prev = nullptr;
		
		m_next = nullptr;
		return tail;
	}

	bool is_solitary () const
	{
		return m_prev == m_next;
	}

	DerivedT	*m_next, *m_prev;
};

struct double_link
{
	double_link() : m_prev(this), m_next(this) {}
	double_link *m_prev, *m_next;
};
	
	// H h h h   C c c
	// C c c H h h h
inline double_link* prepend (double_link* head, double_link* chain)
{
	assert (chain);

	if (head) {
		double_link* t = head->m_prev;
		double_link* ht = chain->m_prev;
		
		ht->m_next = head;
		chain->m_prev = head->m_prev;
		
		t->m_next = chain;
		head->m_prev = ht;
	}
	
	return chain;
}

	
// H h h h   C c c
// H h h h C c c
inline double_link* append (double_link* head, double_link* chain)
{
	assert (chain);

	if (head) {
		double_link* t = chain->m_prev;
		double_link* ht = head->m_prev;

		ht->m_next = chain;
		chain->m_prev = ht;
		
		t->m_next = head;
		head->m_prev = t;
		
		return head;
	} else
		return chain;
}

	// H h h h
	// H   h h h
inline double_link* unlink_first(double_link* head)
{
	double_link* t = head->m_prev;
	double_link* nh = head->m_next;

	if (t == nh)
		return nullptr;
	
	head->m_next = head;
	head->m_prev = head;
	
	t->m_next = nh;
	nh->m_prev = t;

	return nh;
}
	
inline size_t count_size (double_link* head)
{
	if (head == nullptr)
		return 0;
	
	size_t cnt = 1;
	for (double_link const* p = head->m_next; p != head; p = p->m_next)
		++cnt;
	
	return cnt;
}

//
template<class ChainedT>
struct double_chain_iterator
{
	double_chain_iterator (double_chain<ChainedT>* c = 0)
		:	m_current (c)
	{}

	double_chain_iterator& operator++(){ m_current = m_current->get_next(); return *this; }
	ChainedT& operator*() const		{ return *static_cast<ChainedT*> (m_current); }
	ChainedT* operator->() const		{ return static_cast<ChainedT*> (m_current); }

	bool operator== (double_chain_iterator const& a) const
	{	return m_current == a.m_current; }
	bool operator!= (double_chain_iterator const& a) const
	{	return m_current != a.m_current; }

private:
	double_chain<ChainedT>*	m_current;
};

}

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#endif // STIR_CHAIN_HPP
