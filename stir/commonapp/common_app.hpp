#ifndef STIR_COMMON_APP_HPP_
#define STIR_COMMON_APP_HPP_

#include <stir/windows/win_include.hpp>
#include <stir/windows/mdump.h>
#include <stir/path.hpp>
#include <stir/basic_util.hpp>
#include <stir/io/virtual_file.hpp>

#include <stir/windows/dx9/device.hpp>
//#include "../dx9/font_renderer.hpp"
//#include <crtdbg.h>

#include <stir/windows/wnd_plain_windows.hpp>
//!#include "../camera.hpp"

#if STIR_INTERNAL
#error "This header should be included only in the application's translation unit!"
#endif

namespace stir
{

class common_module
	:	public stir::render_interface
	,	public stir::mouse_listener_interface
	,	public stir::keyboard_listener_interface
	,	public stir::window_message_listener
{

protected:
	directx9::device&			device_;
	color						main_clear_color_;
//!	stir::font_renderer*		info_text_renderer;
//!	stir::basic_camera_factory	main_camera_factory;
//!	basic_camera_controller*	main_camera_controller;

public:
	common_module( directx9::device& dev )
		:	device_( dev ), main_clear_color_( 0xff102030 )
//!		,	main_camera_controller( 0 )
	{
//!		info_text_renderer = new stir::font_renderer( device_, _T("Arial"), 12 );
//!		main_camera_controller = new stir::basic_camera_controller_maya( vector3(0,0,0) );
	}

	~common_module()
	{
//!		delete main_camera_controller;
//!		delete info_text_renderer;
	}

	// render_interface
	virtual void on_render()
	{
	}

	virtual void on_resize( int width, int height )
	{
		device_.resize_buffers( width, height );
	}

	virtual bool is_idle() const
	{
		return false;
	}

	// mouse_listener_interface
	virtual void move( int x, int y )
	{
//!		if( main_camera_controller )
//!			main_camera_controller->change( x / 25.0f, y / 25.0f );
	}

	virtual void button_change( int buttonindex, bool pressed )
	{
/*		if( main_camera_controller )
		{
			if( pressed )
			{
				basic_camera_controller::edit_mode editmode;
				switch( buttonindex )
				{
					default:
					case 1:	editmode = basic_camera_controller::edit_rotation; break;
					case 2:	editmode = basic_camera_controller::edit_zoom; break;
					case 3:	editmode = basic_camera_controller::edit_pan; break;
				}
				main_camera_controller->begin( main_camera_factory, editmode );
			} else {
				main_camera_controller->accept();
			}
		}
*/	}

	// keyboard_listener_interface
	virtual void key_change( unsigned long key, unsigned long lparam, bool pressed )
	{}

	// window_message_listener
	virtual LRESULT window_message( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam, bool& msgProcessed )
	{	return 0; }

	//void render_fps()
	//{
	//	stir::stringstream ssfps;
	//	ssfps << fpscounter.get_avg_fps();
	//	stir::string sfps = ssfps.str();

	//	RECT r = { 0, 0, 200, 200 };
	//	info_text_renderer->calc_text_rect( r, sfps, DT_SINGLELINE );

	//	r.right -= r.left; r.bottom -= r.top;

	//	r.left = device_ctx.get_viewport().Width - r.right - 4;
	//	r.right += r.left;
	//	r.top = 2;
	//	r.bottom += r.top;
	//	info_text_renderer->draw_text( r, sfps, stir::color( 1,1,1,1 ), DT_EXPANDTABS | DT_SINGLELINE );
	//}
};

class common_app
{
public:
	common_app( string const& title )
		:	title_( title )
		,	main_window (stir::app_wnd::default_style)
	{
		using namespace stir;
		using namespace std;
		
		_CrtSetDbgFlag( _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG) | _CRTDBG_LEAK_CHECK_DF );
//!		MiniDumper mdumper( title_.c_str() );

		TCHAR currentdir[MAX_PATH];
		GetCurrentDirectory( MAX_PATH, currentdir );

		TCHAR fnamebuf[MAX_PATH];
		GetModuleFileName( GetModuleHandle(0), fnamebuf, MAX_PATH );

//			if( stir::string( GetCommandLine() ).find( _T("--log") ) != stir::string::npos )
//			{
			stir::platform_path logfname = fnamebuf;
			logfname += ".log";

			log::add_writer (L"*", write_to_file (logfname));
			log::add_writer (L"*", write_to_debug_output ());

//!			init_boost_log_to_file( "*", stir::to_ascii( logfname ).c_str() );
//			}

		STIR_LOG( debug ) << "-------- Start ----------" << log::endl;
		STIR_LOG( debug ) << "CommandLine: " << GetCommandLine() << log::endl;
		STIR_LOG( debug ) << "Module path: " << fnamebuf << log::endl;
		STIR_LOG( debug ) << "Current path: " << currentdir << log::endl;
	}

	~common_app()
	{
		assert( !devctx );
		assert( !dx9factory );
	}

	void create_window( HINSTANCE hInstance, int w, int h, bool fullscreen, unsigned zbits, unsigned stencilbits, D3DDEVTYPE devType = D3DDEVTYPE_HAL )
	{
		d3d9 = Direct3DCreate9( D3D_SDK_VERSION );

		if( !main_window.create( hInstance, title_, 0, 0, w, h, fullscreen ) )
		{	throw std::runtime_error( "Cannot create main window, the application will now exit" ); }

		main_window.adjust_to_fit( w, h );
		main_window.show_window();

		if( !d3d9 )
		{	throw std::runtime_error( "Cannot create Direct3D9 object, the application will now exit.\nProgram requires at least DirectX 9.0c" ); }

		dx9factory = new directx9::device_factory( *d3d9, devType, D3DADAPTER_DEFAULT );
//		dx9factory->enable_shader_debugging( stir::dx9_device_factory::vtx_shader_debug );
		if( fullscreen )
			dx9factory->request_fullscreen( w, h, -1 );
		if( !dx9factory->request_zstencil_creation( zbits, stencilbits ) )
		{	throw std::runtime_error( "Cannot find sufficient depth/stencil surface" ); }
		
//		dx9factory->enable_shader_debugging( stir::dx9_device_factory::vtx_shader_debug );

		d3device = dx9factory->create( main_window.get_hwnd() );
		devctx = new stir::directx9::device( *d3device, *dx9factory );
	}

	void clean()
	{
		main_window.destroy();

		delete devctx;
		devctx = 0;

		if( d3device )
			d3device->Release();
		d3device = 0;

		delete dx9factory;
		dx9factory = 0;
		if( d3d9 )
			d3d9->Release();
		d3d9 = 0;
	}

	void run( common_module& mod )
	{
		try
		{
			main_window.set_mouse_listener( &mod );
			main_window.set_keyboard_listener( &mod );
			main_window.set_message_listener( &mod );
			main_window.run( mod );

		} catch( std::runtime_error& error )
		{
			main_window.set_message_listener( 0 );
			main_window.set_mouse_listener( 0 );
			main_window.set_keyboard_listener( 0 );

			STIR_LOG( debug ) << "std::runtime_error: " << error.what() << log::endl;
			MessageBox(
					0, utility::to_stir( error.what() ).c_str(), L"stir application: Fatal error",
					MB_OK|MB_ICONERROR|MB_SETFOREGROUND
				);
		}
	}

	app_wnd& get_wnd()
	{	return main_window; }
	directx9::device_factory& get_device_factory()
	{	return *dx9factory; }
	directx9::device& get_device()
	{	return *devctx; }
	
private:
	string						title_;
	stir::app_wnd				main_window;
	IDirect3D9*					d3d9;
	IDirect3DDevice9*			d3device;
	directx9::device_factory*	dx9factory;
	directx9::device*			devctx;
};



}

#endif // STIR_COMMON_APP_HPP_
