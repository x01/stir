#include <stir/platform.hpp>
#include "common_resource.hpp"

namespace stir
{

//
common_resources::fix_params::fix_params()
:	palette_size( 32 )
{
}

common_resources::fix_params::~fix_params()
{
}

//
common_resources::common_resources( directx9::device& dev, stir::string const& prefix_, fix_params const& fp )
:	device_( dev ), prefix( prefix_ )
//!,	ssound_loader(0), strmsound_loader(0)
//!,	ssound_mgr(0), strmsound_mgr(0)
//!,	dsound_ctx(0), animskinmesh_loader(0)
{
	create_common( fp );
}

/*common_resources::common_resources( directx9::device& dev, stir::directsound::dsound_context& dsctx, stir::string const& prefix_, fix_params const& fp )
:	device_( dev ), prefix( prefix_ )
,	ssound_loader(0), strmsound_loader(0)
,	ssound_mgr(0), strmsound_mgr(0)
,	dsound_ctx(&dsctx), animskinmesh_loader(0)
{
	create_common( fp );

//!	ssound_loader		= new stir::static_sound_loader_file( *dsound_ctx, prefix + fp.staticsound_prefix, L"" );
//!	strmsound_loader	= new stir::streaming_sound_loader_file( *dsound_ctx, prefix + fp.streamsound_prefix, L".ogg" );
//!	ssound_mgr			= new stir::static_sound_manager( *boost::get_pointer( ssound_loader ) );
//!	strmsound_mgr		= new stir::streaming_sound_manager( *boost::get_pointer( strmsound_loader ) );
}*/

void common_resources::create_common( fix_params const& fp )
{
//!	effect_loader		= new stir::effect_loader_file( device_ctx, prefix + fp.effect_prefix, _T(".fx"), true );
//!	staticmesh_loader	= new stir::static_mesh_loader_file( device_ctx, prefix + fp.staticmesh_prefix + _T("") );
	texture_loader		= new directx9::texture_loader_file( device_, prefix + fp.texture_prefix + L"", L".tga:.png:.dds" );
//!	cube_texture_loader	= new stir::cube_texture_loader_file( device_ctx, prefix + fp.cube_texture_prefix + _T(""), _T(".dds") );
//!	animskinmesh_loader	= new stir::anim_skin_mesh_loader_file( device_ctx, prefix + fp.anim_skinmesh_prefix + _T(""), fp.palette_size );

//!	efx_mgr				= new stir::effect_manager( device_ctx, *boost::get_pointer( effect_loader ) );
	tex_mgr				= new directx9::texture_manager( device_, *texture_loader );
//!	cubetex_mgr			= new stir::cube_texture_manager( device_ctx, *boost::get_pointer( cube_texture_loader ) );
//!	smesh_mgr			= new stir::static_mesh_manager( *boost::get_pointer( staticmesh_loader ) );
//!	askinmesh_mgr		= new stir::anim_skin_mesh_manager( *boost::get_pointer( animskinmesh_loader ) );
}

common_resources::~common_resources()
{
//!	delete strmsound_mgr;
//!	delete ssound_mgr;
//!	delete efx_mgr;
	delete tex_mgr;
//!	delete cubetex_mgr;
//!	delete smesh_mgr;
//!	delete askinmesh_mgr;

//!	delete strmsound_loader;
//!	delete ssound_loader;
//!	delete effect_loader;
//!	delete staticmesh_loader;
//!	delete cube_texture_loader;
	delete texture_loader;
//!	delete animskinmesh_loader;
} 

}
