#ifndef STIR_COMMON_RESOURCE_HPP_
#define STIR_COMMON_RESOURCE_HPP_

#include <stir/platform.hpp>
//!#include <stir/dx9/effect_resource.hpp>
//!#include <stir/dx9/mesh_resource.hpp>
#include <stir/windows/dx9/texture.hpp>
#include <stir/windows/dx9/texture_loader.hpp>
//!#include <stir/dx9/cube_texture_resource.hpp>
//!#include <stir/dx9/anim_skin_mesh.hpp>
//!#include <stir/sound/sound_resource.hpp>

#include <stir/windows/dx9/device.hpp>

namespace stir
{

class STIR_ATTR_DLL common_resources
{
public:
	struct STIR_ATTR_DLL fix_params
	{
		fix_params();
		~fix_params();

		string
			staticmesh_prefix, texture_prefix, cube_texture_prefix,
			effect_prefix, staticsound_prefix,
			streamsound_prefix, anim_skinmesh_prefix;
		int palette_size;
	};

	common_resources( directx9::device& dev, stir::string const& prefix_, fix_params const& fp = fix_params() );
//!	common_resources( stir::device_context& devctx, stir::directsound::dsound_context& dsound_ctx, stir::string const& prefix_, fix_params const& fp = fix_params() );
	~common_resources();

//!	stir::effect_manager& effect_mgr() const
//!	{	assert( efx_mgr ); return *efx_mgr; }
	directx9::texture_manager& texture_mgr() const
	{	assert( tex_mgr ); return *tex_mgr; }
//!	stir::static_mesh_manager& static_mesh_mgr() const
//!	{	assert( smesh_mgr ); return *smesh_mgr; }
//!	stir::anim_skin_mesh_manager& anim_skin_mesh_mgr() const
//!	{	assert( askinmesh_mgr ); return *askinmesh_mgr; }
//!	stir::static_sound_manager& static_sound_mgr() const
//!	{	assert( ssound_mgr ); return *ssound_mgr; }
//!	stir::streaming_sound_manager& streaming_sound_mgr() const
//!	{	assert( strmsound_mgr ); return *strmsound_mgr; }
//!	stir::cube_texture_manager& cube_texture_mgr() const
//!	{	assert( cubetex_mgr ); return *cubetex_mgr; }

//!	stir::effect_loader_file& get_effect_loader() const
//!	{	assert( effect_loader ); return *effect_loader; }

private:
	void create_common( fix_params const& fp );

	directx9::device&				device_;
//!	stir::directsound::dsound_context* dsound_ctx;
	stir::string					prefix;

//!    stir::effect_loader_file*		effect_loader;
//!	stir::static_mesh_loader_file*	staticmesh_loader;
	directx9::texture_loader_file*	texture_loader;
//!	stir::cube_texture_loader_file*	cube_texture_loader;
//!	stir::static_sound_loader_file*	ssound_loader;
//!	stir::streaming_sound_loader_file* strmsound_loader;
//!	stir::anim_skin_mesh_loader_file* animskinmesh_loader;

//!	stir::effect_manager*			efx_mgr;
	directx9::texture_manager*		tex_mgr;
//!	stir::cube_texture_manager*		cubetex_mgr;
//!	stir::static_mesh_manager*		smesh_mgr;
//!	stir::static_sound_manager*		ssound_mgr;
//!	stir::streaming_sound_manager*	strmsound_mgr;
//!	stir::anim_skin_mesh_manager*	askinmesh_mgr;
};

namespace common
{

extern common_resources* g_common_resources;

template<class T>
inline T get (resource_id const& rid, int profile = 0);

template<>
inline directx9::texture get<directx9::texture> (resource_id const& id, int profile) {
	return g_common_resources->texture_mgr ().get (id, profile);
}

}



}

#endif
