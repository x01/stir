#ifndef STIR_COMPLEX_TYPES_HPP_
#define STIR_COMPLEX_TYPES_HPP_

#include "platform.hpp"
#include "basic_types.hpp"
#include <string>
#include <iosfwd>

namespace stir
{
#if STIR_WIDE_STRING
	typedef std::wstring			string;
	typedef std::wstringstream		stringstream;
#else
	typedef std::string				string;
	typedef std::stringstream		stringstream;
#endif
}

#endif
