#ifndef STIR_CONST_STRING_HPP_
#define STIR_CONST_STRING_HPP_

#include <functional>
#include <cstddef>
#include <algorithm>

namespace stir {

template<class CharT>
struct const_string
{
	size_t string_length (char const* str) {
		return ::strlen (str);
	}
	size_t string_length (wchar_t const* str) {
		return ::wcslen (str);
	}

	typedef CharT value_type;
	
	static const std::size_t compact_flag = 0x01UL << (sizeof (std::size_t) * 8 - 1);
	static const std::size_t compact_char_flag = 0x01UL << (sizeof (CharT) * 8 - 1);
	// size of the string that can be put inline (including 0 terminating character)
	static const size_t compact_max = sizeof (size_t) + sizeof (value_type*) - sizeof(CharT);
	
private:
	value_type*	data_;
	size_t		size_;
	
public:

	typedef CharT* iterator;
	typedef CharT const* const_iterator;

	const_string ()
	:	size_(0), data_(0)
	{
		set_compact (true, 0);
	}
	
	const_string (CharT const* s) {
		assign_ (s, string_length(s));
	}

	const_string (CharT const* s, size_t len) {
		assign_ (s, len);
	}
	
	const_string (const_string const& a) {
		assign_ (a.ptr (), a.size ());
	}

	const_string& operator=(const_string const& a) {
		const_string temp (a);
		temp.swap (*this);
		return *this;
	}
	
	~const_string () {
		if (!is_compact ()) {
			delete[] data_;
		}
	}
	
	size_t size () const {
		return is_compact() ? *_csz () : size_;
	}
	
	iterator begin () { return ptr (); }
	const_iterator begin () const { return ptr (); }

	iterator end () { return is_compact () ? _cstr () + *_csz () : data_ + size_; }
	const_iterator end () const { return is_compact () ? _cstr () + *_csz () : data_ + size_; }
	
	CharT const* c_str () const { return ptr (); }
	
	CharT operator [] (size_t index) const { return ptr () [index]; }
	CharT& operator [] (size_t index) { return ptr () [index]; }
	
	bool operator== (const_string const& a) const {
		return memcmp (a.ptr (), ptr (), std::min (a.size (), size()));
	}
	
	void swap (const_string const& a) {
		std::swap (size_, a.size_);
		std::swap (data_, a.size_);
	}

private:
	bool is_compact () const { return (*_csz () & compact_char_flag) != 0; }
	void set_compact (bool compact, size_t size) {
		// for little-endian
		*_csz () = compact_char_flag | size;
	}

	CharT* _csz () { return ((CharT*)&size_) + sizeof(size_) - 1; }
	CharT const* _csz () const { return ((CharT const*)&size_) + sizeof(size_) - 1; }
	CharT* _cstr () { return (CharT*)&data_; }
	CharT const* _cstr () const { return (CharT const*)&data_; }
	
	CharT const* ptr () const{ return is_compact () ? _cstr () : data_; }
	size_t compact_size () const { return *(CharT*)&size_ & ~compact_char_flag; }
	
	void assign_ (CharT const* s, size_t len) {
		if (len < compact_max) {
			set_compact (true, len);
			memcpy (_cstr(), s, len * sizeof (CharT));
			_cstr() [len] = 0;
		} else {
			data_ = new CharT [len + 1];
			size_ = len;
			memcpy (data_, s, len * sizeof (CharT));
			data_[len] = 0;
		}
	}	
};

template<class T>
inline const_string<T> to_conststring (int a) {
	T buf [16];
	
	int n = 0;
	do {
		buf [n++] = '0' + a % 10;
		a /= 10;
	} while (a);

	for (T* p=buf, *q = buf + n - 1; p < q; ++p, --q)
		std::swap (*p, *q);

	return buf;
}

inline const_string<char> to_string (int a) {
	char buf [16];
#ifdef _MSC_VER
	_snprintf (buf, 16, "%d", a);
#else
	snprintf (buf, 16, "%d", a);
#endif
	return buf;
}

}

#endif
