#ifndef STIR_CONSTANTS_HPP_
#define STIR_CONSTANTS_HPP_

namespace stir {

static const float PI			= 3.141592653589793f;
static const float PId2		= 1.570796326794896f;
static const float PIx2		= 6.283185307179586f;
static const float PIinv		= 0.318309886183790f;
static const float PIsqrt		= 1.772453850905516f;

static const int index16_max	= 65535;

}

#endif