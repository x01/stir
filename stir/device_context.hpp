#ifndef STIR_DEVICE_CONTET_HPP_
#define STIR_DEVICE_CONTET_HPP_

#include <stir/platform.hpp>

#if STIR_IPHONE && (STIR_GLES1 || STIR_GLES2)

#include <stir/gles/gles.h>
namespace stir
{
	typedef stir::gles::device_context device_context;
}

#else
#error "Unsupported platform"
#endif

#endif