#ifndef STIR_DYNARRAY_HPP_
#define STIR_DYNARRAY_HPP_

#include <memory>
#include <stdexcept>

#include <pulmotor/pulmotor_fwd.hpp>
#include <pulmotor/archive.hpp>

namespace stir {

template<typename T, typename AllocatorT = std::allocator<T> >
struct dynamic_array : AllocatorT
{
	enum { version = pulmotor::version_dont_track };
	
    // types:
    typedef       T                               value_type;
    typedef       T&                              reference;
    typedef const T&                              const_reference;
    typedef       T*                              iterator;
    typedef const T*                              const_iterator;
    typedef std::reverse_iterator<iterator>       reverse_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator;
    typedef size_t                                size_type;
    typedef ptrdiff_t                             difference_type;

    // fields:
private:
    T*        store;
    size_type count;
    
    AllocatorT& getalloc () { return (AllocatorT&)*this; }

    // helper functions:
    void check_size(size_type n)
        {
		 if ( n >= count )
		  throw std::out_of_range("dynamic_array");
		   }
    T* alloc(size_type n)
        { return n > 0 ? reinterpret_cast<T*>( getalloc ().allocate (n * sizeof(T)) ) : 0; }
    void dealloc(T* p)
        { if (p != 0) getalloc ().deallocate (p, sizeof(T)); }

public:
	template<class ArchiveT>
	void serialize (ArchiveT& ar, unsigned version)
	{
		ar | count | pulmotor::ptr (store, count);
	}
	
	PULMOTOR_ARCHIVE_SPLIT()
	PULMOTOR_ARCHIVE_READ()
	{
		if (store) {
			destroy_ (store, count);
			dealloc(store);
			store = nullptr;
			count = 0;
		}
		
		ar | nv_("count", pulmotor::as<u32>(count));
		
		if (count > 0) {
			store = alloc (count);
			size_t i;
#if STIR_EXCEPTIONS
			try {
#endif
				for ( i = 0; i < count; ++i )
					new (store+i) T ();
#if STIR_EXCEPTIONS
			} catch ( ... ) {
				for ( ; i > 0; --i )
					(store+(i-1))->~T();
				dealloc (store);
				store = nullptr;
				throw;
			}
#endif
						
			ar | nv_("store", pulmotor::memblock(store, count));
		}
	}
	PULMOTOR_ARCHIVE_WRITE()
	{
		ar | nv_("count", pulmotor::as<u32>(count));
		if (count > 0)
			ar | nv_("store", pulmotor::memblock(store, count));
	}
    
public:
    // construct and destruct:
//    dynarray() CPP0X( = delete ) ;
//    const dynarray operator=(const dynarray&) CPP0X( = delete ) ;

    explicit dynamic_array(size_type c = 0)
        : store( c == 0 ? 0 : alloc( c ) ), count( c )
        { size_type i;
#if STIR_EXCEPTIONS
          try {
#endif
              for ( i = 0; i < count; ++i )
                  new (store+i) T ();
 #if STIR_EXCEPTIONS
          } catch ( ... ) {
              for ( ; i > 0; --i )
                 (store+(i-1))->~T();
              dealloc (store);
              throw;
          }
#endif
	}
	
    dynamic_array(T const* values, size_t n)
        : store( alloc( n ) ), count( n )
    {
	   std::uninitialized_copy( values, values + n, store );
	}

    dynamic_array(const dynamic_array& d)
	: store( alloc( d.count ) ), count( d.count )
	{
#if STIR_EXCEPTIONS
		try {
#endif
			std::uninitialized_copy( d.begin(), d.end(), begin() );
#if STIR_EXCEPTIONS
		} catch ( ... ) { dealloc(store); throw; }
#endif
	}

    dynamic_array const& operator=(const dynamic_array& arg)
	{
		if (count == arg.count)
			std::copy (arg.store, arg.store + arg.count, store);
		else
		{
			// destroy old data
			destroy_ (store, count);
			dealloc (store);

			// allocate new buffer and copy data
			store = alloc (count = arg.count);
			
#if STIR_EXCEPTIONS
			try {
#endif
				std::uninitialized_copy (arg.store, arg.store + arg.count, store);
#if STIR_EXCEPTIONS
			} catch (...) {
				dealloc (store); store = NULL; throw;
			}
#endif
		}

		return *this;
	}

    ~dynamic_array()
	{
		destroy_ (store, count);
		dealloc(store);
	}
	
	void push_back (value_type const& value)
	{
		resize (count + 1, value);
	}
		  
	void resize (size_t n, value_type const& value = T())
	{
		if (count == n)
			return;
			
		value_type* newStore = alloc (n);
		
		if (store)
		{
			std::uninitialized_copy (store, store + count, newStore);
	   
			for (size_t i=0; i<count; ++i)
				(store+i)->~T();
				
			dealloc (store);
		}
		
		for (size_type i=count; i<n; ++i)
		{
			new (newStore+i) T (value);
		}
		
		count = n;
		store = newStore;
	}

	template<class IteratorT>
	void assign (IteratorT begin, IteratorT end)
	{
		size_t n = std::distance (begin, end);
		assign (&*begin, n);
	}

	template<class Y>
	void assign (Y const* begin, size_t n)
	{
		// this is not exception safe!
		if (n != count)
		{
			destroy_ (store, count);
			dealloc (store);
			store = n == 0 ? NULL : alloc (n);
			count = n;
		} else
			destroy_ (store, count);

		std::uninitialized_copy (begin, begin + n, store);
		count = n;
	}

    // iterators:
    iterator       begin()        { return store; }
    const_iterator begin()  const { return store; }
    const_iterator cbegin() const { return store; }
    iterator       end()          { return store + count; }
    const_iterator end()    const { return store + count; }
    const_iterator cend()   const { return store + count; }

    reverse_iterator       rbegin()       
        { return reverse_iterator(end()); }
    const_reverse_iterator rbegin()  const
        { return reverse_iterator(end()); }
    reverse_iterator       rend()         
        { return reverse_iterator(begin()); }
    const_reverse_iterator rend()    const
        { return reverse_iterator(begin()); }

    // capacity:
    size_type size()     const { return count; }
    size_type max_size() const { return count; }
    bool      empty()    const { return false; }

    // element access:
    reference       operator[](size_type n)       { return store[n]; }
    const_reference operator[](size_type n) const { return store[n]; }

    reference       front()       { return store[0]; }
    const_reference front() const { return store[0]; }
    reference       back()        { return store[count-1]; }
    const_reference back()  const { return store[count-1]; }

    const_reference at(size_type n) const { check_size(n); return store[n]; }
    reference       at(size_type n)       { check_size(n); return store[n]; }

    // data access:
    T*       data()       { return store; }
    const T* data() const { return store; }
    
    void swap (dynamic_array& ar)
    {
		std::swap (store, ar.store);
		std::swap (count, ar.count);
    }
	
private:
	void destroy_ (T* p, size_t n) {
		while (n --> 0)
			(p++)->~T();
	}
};
	
}

#endif
