#include "fade_helper.hpp"
#include <cassert>

namespace stir
{

fade_helper::fade_helper( float indur, float outdur )
:	m_in_duration(indur), m_out_duration(outdur), m_fading_out(false), m_brightness(0)
,	m_current(0), m_fade_start(0)
{
	assert( indur > 0.0f );
	assert( outdur > 0.0f );
}

fade_helper::~fade_helper()
{
}

void fade_helper::start( stir::time_type start, bool fading_out )
{
	if( !is_clear() && !is_finished() )
	{
		// 
		if( is_fading_out() )
			m_fade_start = (m_brightness - 1.0f) * m_out_duration + start;
		else
			m_fade_start = start - m_brightness * m_in_duration;
	}
	else
		m_fade_start = start;

	m_fading_out = fading_out;
	update( start );
}

bool fade_helper::is_finished() const
{	return m_fading_out && m_current - m_fade_start >= m_out_duration; }

bool fade_helper::is_clear() const
{	return !m_fading_out && m_current - m_fade_start >= m_in_duration; }

void fade_helper::update( stir::time_type current )
{
	m_current = current;

	m_brightness = 1.0f;

	if( m_fading_out )
	{
		stir::time_type s = m_current - m_fade_start;

		if( s < m_out_duration )
			m_brightness = 1.0f - float(s / m_out_duration);
		else
			m_brightness = 0.0f;
	}
	else
	{
		stir::time_type s = m_current - m_fade_start;
		if( s >= m_in_duration )
			m_brightness = 1.0f;
		else
			m_brightness = float( s / m_in_duration );
	}
}

}
