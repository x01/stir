#ifndef STIR_FADE_HELPER_HPP_
#define STIR_FADE_HELPER_HPP_

#include "frame_time.hpp"

namespace stir
{

//
class fade_helper
{
public:
	fade_helper( float indur, float outdur );
	~fade_helper();

	void start( stir::time_type start, bool fading_out );
	void update( stir::time_type current );
	stir::time_type const& get_start() const { return m_fade_start; }
	stir::time_type const& get_current() const { return m_current; }
	stir::time_type get_active_duration() const { return m_current - m_fade_start; }

	bool is_fading_out() const		{ return m_fading_out; }
	bool is_finished() const;
	bool is_clear() const;
	float get_brightness() const	{	return m_brightness; }

private:
	bool				m_fading_out;
	float				m_in_duration, m_out_duration, m_brightness;
	stir::time_type	m_current, m_fade_start;
};

}

#endif // STIR_FADE_HELPER_HPP_
