#include <stir/platform.hpp>
#include <stir/path.hpp>
#include <stir/log.hpp>
#include <stir/stir_logs.hpp>
#include <errno.h>

#if STIR_SUPPORT_PHYSFS
#include <physfs.h>
#endif

#include "filesystem.hpp"

#if STIR_SUPPORT_PHYSFS
namespace stir { namespace filesystem {

file_handle::file_handle (path const& path, permission perm, std::error_code& ec)
{
	if (perm == permission::read)
		handle_ = PHYSFS_openRead(path.c_str());
	else if (perm == permission::write)
		handle_ = PHYSFS_openWrite(path.c_str());
	else
		handle_ = nullptr;
	
	if (handle_)
		ec.clear();
	else
		ec = std::make_error_code (std::errc::no_such_file_or_directory);
}

file_handle::~file_handle ()
{
	if (handle_)
		PHYSFS_close (handle_);
}

bool STIR_ATTR_DLL is_file(path const& p)
{
	return PHYSFS_exists (p.c_str()) && !PHYSFS_isDirectory(p.c_str());
}

bool STIR_ATTR_DLL is_directory (path const& p)
{
	return PHYSFS_isDirectory (p.c_str());
}

bool STIR_ATTR_DLL path_exists (path const& p)
{
	return PHYSFS_exists (p.c_str());
}

size_t STIR_ATTR_DLL file_size (char const* file_name)
{
	std::error_code ec;
	file_handle fh (file_name, permission::read, ec);
	if (!ec)
		return (size_t)PHYSFS_fileLength(fh);
	return 0;
}

size_t STIR_ATTR_DLL file_size (file_handle& fh)
{
	if (fh.is_good())
		return (size_t)PHYSFS_fileLength(fh);
	return 0;
}
	
size_t STIR_ATTR_DLL file_size (file_handle& fh, std::error_code& ec)
{
	ec.clear();
	if (fh.is_good()) {
		PHYSFS_sint64 sz = PHYSFS_fileLength(fh);
		if (sz >= 0) {
			return (size_t)sz;
		}
	}
	ec = std::make_error_code (std::errc::no_such_file_or_directory);
	return 0;
}
	
size_t STIR_ATTR_DLL read_file (file_handle handle, void* ptr, size_t count, std::error_code& ec)
{
	ec.clear();
	if (!handle.is_good()) {
		ec = std::make_error_code (std::errc::no_such_file_or_directory);
		return 0;
	}
	
	PHYSFS_sint64 wasRead = (size_t)PHYSFS_read(handle, ptr, (PHYSFS_uint32)count, 1);
	if (wasRead < 0) {
		ec = std::make_error_code (std::errc::no_such_file_or_directory);
		return 0;
	}
	
	return (size_t)wasRead;
}

size_t STIR_ATTR_DLL read_file (file_handle handle, void* ptr, size_t count)
{
	std::error_code ec;
	int s = (int)read_file(handle, ptr, count, ec);
	if (ec) return 0;

	return (size_t)s;
}
		

bool STIR_ATTR_DLL is_eof (file_handle handle)
{
	return !handle.is_good() || PHYSFS_eof(handle);
}

input_buffer::input_buffer (path const& file_name, std::error_code& ec)
#ifdef _DEBUG
:	name_(file_name)
#endif
{
	if ((file_ = PHYSFS_openRead(file_name.c_str())))
		ec.clear();
	else
		ec = std::make_error_code (std::errc::no_such_file_or_directory);
}
	
input_buffer::~input_buffer()
{
	if (file_)
		PHYSFS_close (file_);
}
	
size_t input_buffer::read (void* dest, size_t count, std::error_code& ec)
{
	if (!file_)
	{
		ec = std::make_error_code (std::errc::bad_file_descriptor);
		return 0;
	}
	
	size_t wasRead = (size_t)PHYSFS_read(file_, dest, (unsigned)count, 1);
	if (wasRead != 1)
	{
		ec = std::make_error_code (std::errc::io_error);
		return 0;
	}
	
	return wasRead * count;
}


output_buffer::output_buffer (path const& file_name, std::error_code& ec)
#ifdef _DEBUG
:	name_(file_name)
#endif
{
	if ((file_ = PHYSFS_openWrite(file_name.c_str())))
		ec.clear();
	else
		ec = std::make_error_code (std::errc::no_such_file_or_directory);
}

output_buffer::~output_buffer()
{
	if (file_)
		PHYSFS_close (file_);
}

size_t output_buffer::write (void const* src, size_t count, std::error_code& ec)
{
	if (!file_)
	{
		ec = std::make_error_code (std::errc::bad_file_descriptor);
		return 0;
	}
	
	size_t written = (size_t)PHYSFS_write(file_, src, (unsigned)count, 1);
	if (written != 1)
	{
		ec = std::make_error_code (std::errc::io_error);
		return 0;
	}
	
	return written * count;
}


}}
#endif

#if STIR_WINDOWS && !STIR_SUPPORT_PHYSFS
#include <stir/windows/win_include.hpp>

namespace stir { namespace filesystem { namespace physical {

path STIR_ATTR_DLL get_current_path()
{
	wchar_t curp[ MAX_PATH + 1 ];
	GetCurrentDirectory( MAX_PATH, curp );
	return curp;
}
	
bool STIR_ATTR_DLL is_file( path const& path )
{
	return GetFileAttributes( path.c_str() ) != FILE_ATTRIBUTE_DIRECTORY;
}

bool STIR_ATTR_DLL is_directory( path const& path )
{
	return (GetFileAttributes( path.c_str() ) & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY;
}

bool STIR_ATTR_DLL path_exists( path const& path )
{
	//  from: directory_posix_windows.cpp  ---------------------------------------------//

	//  Copyright © 2002 Beman Dawes
	//  Copyright © 2001 Dietmar K¸hl
	//  Use, modification, and distribution is subject to the Boost Software
	//  License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy
	//  at http://www.boost.org/LICENSE_1_0.txt)
	if(::GetFileAttributes( path.c_str() ) == INVALID_FILE_ATTRIBUTES)
	{
		UINT err = ::GetLastError();
		if((err == ERROR_FILE_NOT_FOUND)
			|| (err == ERROR_INVALID_PARAMETER)
			|| (err == ERROR_NOT_READY)
			|| (err == ERROR_PATH_NOT_FOUND)
			|| (err == ERROR_INVALID_NAME)
			|| (err == ERROR_BAD_NETPATH ))
			return false; // GetFileAttributes failed because the path does not exist
		// for any other error we assume the file does exist and fall through,
		// this may not be the best policy though...  (JM 20040330)
		return true;
	}
	return true;
}

bool STIR_ATTR_DLL create_directory( path const& path )
{
	return CreateDirectory( path.c_str(), 0 ) != 0;
}
	
size_t STIR_ATTR_DLL file_size (wchar_t const* filename)
{
	return GetFileSize (filename);
}

#if STIR_WINDOWS && 0
stir::string STIR_ATTR_DLL get_user_application_data_folder( HWND owner )
{
	TCHAR path[MAX_PATH+1];
#if 1
	BOOL result = SHGetSpecialFolderPath( owner, path, CSIDL_APPDATA, 1 );
	if( result )
		return string( path );
#else
	HRESULT hr = SHGetFolderPath( owner, CSIDL_APPDATA|CSIDL_FLAG_CREATE, 0, SHGFP_TYPE_CURRENT, path );
	if( SUCCEEDED(hr) )
		return string( path );
#endif
	return string();
}
#endif

}}}

#endif // STIR_WINDOWS

#if STIR_MACOSX || STIR_IPHONE

#include <sys/stat.h>

namespace stir { namespace filesystem {
	
#if !STIR_SUPPORT_PHYSFS
	inline
#endif
	namespace physical {

path STIR_ATTR_DLL get_current_path()
{
	char* p = getcwd (0, 0);
	path pp (p);
	free (p);
	return pp;
}

bool STIR_ATTR_DLL is_file( path const& p )
{
	struct stat s;
	return stat(p.c_str(), &s) == 0 && S_ISREG(s.st_mode);
}

bool STIR_ATTR_DLL is_directory( path const& p )
{
	struct stat s;
	return stat(p.c_str(), &s) == 0 && S_ISDIR(s.st_mode);
}

bool STIR_ATTR_DLL path_exists( path const& p )
{
    struct stat s;
	int result = stat(p.c_str(), &s);
	STIR_LOG (fs).printf ("path_exists failed: errorno=%d", errno);
	return result == 0 &&
   		(S_ISREG(s.st_mode) || S_ISDIR(s.st_mode) || S_ISCHR(s.st_mode) || S_ISBLK(s.st_mode) || S_ISFIFO(s.st_mode) || S_ISLNK(s.st_mode));
}

bool STIR_ATTR_DLL create_directory( path const& p )
{
	return mkdir( p.c_str(), 0777 ) == 0;
}
	
bool STIR_ATTR_DLL rename (path const& from, path const& to)
{
	if (::rename (from.c_str(), to.c_str()) == 0)
		return true;
	
	return false;
}
	
size_t STIR_ATTR_DLL file_size (char const* filename)
{
    struct stat s;
	int result = stat(filename, &s);
	return result == 0 ? (size_t)s.st_size : 0;
}
	
}}}

#endif // STIR_IPHONE

namespace stir { namespace filesystem {

// common

#if STIR_SUPPORT_PHYSFS
// common version
bool STIR_ATTR_DLL read_file (path const& p, std::vector<u8>& out)
{
	std::error_code ec;
	file_handle fh (p, permission::read, ec);
	if (ec)
		return 0;
	
	size_t fs = file_size (fh);
	out.resize (fs);
	size_t wasRead = read_file (fh, out.data(), out.size());
	
	return wasRead == fs;
}

template<class ContainerT>
size_t STIR_ATTR_DLL read_file_impl (path const& p, ContainerT& out, std::error_code& ec)
{
	file_handle fh (p, permission::read, ec);
	if (ec) return 0;
	
	size_t fs = file_size (fh, ec);
	if (ec) return 0;
	
	out.resize (fs);
	size_t wasRead = read_file (fh, (typename ContainerT::value_type*)out.data(), out.size(), ec);
	return wasRead;
}
	
size_t STIR_ATTR_DLL read_file (path const& p, std::vector<u8>& out, std::error_code& ec)
{
	return read_file_impl (p, out, ec);
}
	
size_t STIR_ATTR_DLL read_file (path const& p, std::string& out, std::error_code& ec)
{
	return read_file_impl (p, out, ec);
}


// common version
bool STIR_ATTR_DLL write_file (path const& p, void* ptr, size_t size, std::error_code& ec)
{
	file_handle fh (p, permission::read, ec);
	if (ec)
		return false;
	
	size_t fs = file_size (fh);
	size_t wasWritten = (size_t)PHYSFS_write(fh, ptr, (unsigned)size, 1);
	if (wasWritten != size)
		ec = std::make_error_code(std::errc::io_error);
	
	return wasWritten == fs;
}

#endif

bool STIR_ATTR_DLL create_directories( path const& p )
{
	STIR_LOG (debug) << "create-dir: " << p.c_str() << log::endl;
		
	if( !path_exists( p ) )
	{
		path parent = p.parent ();

		if (parent.empty ())
			return physical::create_directory (p);
		
		if (!create_directories (parent))
			return false;

		return physical::create_directory (p);
	}
	
	return true;
}

}}
