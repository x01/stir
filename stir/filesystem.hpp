#ifndef STIR_FILESYSTEM_HPP_
#define STIR_FILESYSTEM_HPP_

#include <stir/platform.hpp>
#include <stir/path.hpp>
#include <vector>
#include <system_error>

#include <pulmotor/stream.hpp>

//#if STIR_MACOSX
#include <dirent.h>
//#endif

struct PHYSFS_File;

namespace stir { namespace filesystem {

enum permission { read = 0x01, write = 0x02 };

#if STIR_SUPPORT_PHYSFS
struct file_handle
{
	file_handle (path const& path, permission perm, std::error_code& ec);
	~file_handle ();

	bool is_good () const { return handle_ != nullptr; }
	
	operator PHYSFS_File*() { return handle_; }

private:
	PHYSFS_File* handle_;
	//	uintptr_t	handle_;
};
#endif

#if !STIR_SUPPORT_PHYSFS
inline
#endif
namespace physical {
	// TODO: write a file_handle for native FS
/*	struct file_handle
	{
		file_handle (path const& path, permission perm = permission::read);
		~file_handle ();
		bool is_good () const { return handle_ != nullptr; }
		operator PHYSFS_File*() { return handle_; }
	};*/

	class input_buffer : public pulmotor::cfile_input_buffer
	{
	public:
		input_buffer (stir::path const& file_name, std::error_code& ec)
		:	pulmotor::cfile_input_buffer(file_name.c_str(), ec)
		{}
	};
	
	class output_buffer : public pulmotor::cfile_output_buffer
	{
	public:
		output_buffer (stir::path const& file_name, std::error_code& ec)
		:	pulmotor::cfile_output_buffer(file_name.c_str(), ec)
		{}
	};
	
	path STIR_ATTR_DLL get_current_path ();
	
	bool STIR_ATTR_DLL is_directory( path const& p);
	bool STIR_ATTR_DLL is_file( path const& p);
	bool STIR_ATTR_DLL path_exists( path const& p);
//	size_t STIR_ATTR_DLL file_size (file_handle fh);
	size_t STIR_ATTR_DLL file_size (char const* file_name);
	bool STIR_ATTR_DLL rename (path const& from, path const& to);
	bool STIR_ATTR_DLL create_directory( path const& p );
	
	class directory_iterator
	{
		DIR* m_dir;
		struct dirent* m_entry;
	public:
		directory_iterator() : m_dir(nullptr), m_entry(nullptr)
		{}
		
		explicit directory_iterator(path const& p) : m_entry(nullptr) {
			 m_dir = opendir(p.c_str());
		}
		
		~directory_iterator () {
			if (m_dir) closedir(m_dir);
		}
		
		directory_iterator& operator++() { return *this; }
		bool operator==(directory_iterator const& arg) const { return m_entry == arg.m_entry; }
		bool operator!=(directory_iterator const& arg) const { return m_entry != arg.m_entry; }
		
		path operator*() { return m_entry ? m_entry->d_name : ""; }
	};
}

	
#if STIR_SUPPORT_PHYSFS
bool STIR_ATTR_DLL is_directory(path const& p);
bool STIR_ATTR_DLL is_file( path const& p);
bool STIR_ATTR_DLL path_exists( path const& p);
size_t STIR_ATTR_DLL file_size (char const* file_name);

size_t STIR_ATTR_DLL file_size (file_handle& fh);
size_t STIR_ATTR_DLL read_file (file_handle handle, void* ptr, size_t count);
bool STIR_ATTR_DLL is_eof (file_handle handle);
	
class STIR_ATTR_DLL input_buffer : public pulmotor::basic_input_buffer
{
public:
	input_buffer (stir::path const& file_name, std::error_code& ec);
	virtual ~input_buffer();
	virtual size_t read (void* dest, size_t count, std::error_code& ec);
	bool good () const { return file_ != 0; }
	
private:
#ifdef _DEBUG
	path	name_;
#endif
	PHYSFS_File* file_;
};

class STIR_ATTR_DLL output_buffer : public pulmotor::basic_output_buffer
{
public:
	output_buffer (stir::path const& file_name, std::error_code& ec);
	~output_buffer();
	
	virtual size_t write (void const* src, size_t count, std::error_code& ec);
	bool good () const { return file_ != 0; }
	
private:
#ifdef _DEBUG
	path	name_;
#endif
	PHYSFS_File* file_;
};
	
#else
#endif

bool STIR_ATTR_DLL create_directories( path const& p );
bool STIR_ATTR_DLL read_file (path const& p, std::vector<u8>& out);
size_t STIR_ATTR_DLL read_file (path const& p, std::vector<u8>& out, std::error_code& ec);
size_t STIR_ATTR_DLL read_file (path const& p, std::string& out, std::error_code& ec);
//void STIR_ATTR_DLL write_file (path const& p, void* ptr, size_t size, std::error_code& ec);

#if STIR_WINDOWS && 0
stir::string STIR_ATTR_DLL get_user_application_data_folder( HWND owner );
#endif
	
#if STIR_IPHONE || STIR_MACOSX
path STIR_ATTR_DLL get_main_data_path ();
path STIR_ATTR_DLL get_document_directory ();
path STIR_ATTR_DLL get_home_directory ();
#endif

}}

#endif // STIR_FILESYSTEM_HPP_
