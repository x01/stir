#include "platform.hpp"
#include "filesystem.hpp"

#import <Foundation/NSBundle.h>
#import <Foundation/NSString.h>
#import <Foundation/NSPathUtilities.h>

namespace stir { namespace filesystem {
	
path STIR_ATTR_DLL get_main_data_path ()
{
	NSString* resources = [[NSBundle mainBundle] resourcePath];
	return path( [resources UTF8String], [resources length]);
}
	
path STIR_ATTR_DLL get_document_directory ()
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	return path ([documentsDirectory UTF8String], [documentsDirectory length]);
}
	
path STIR_ATTR_DLL get_home_directory ()
{
	NSString* homeDirectory = NSHomeDirectory ();
	return path ([homeDirectory UTF8String], [homeDirectory length]);
}

}}
