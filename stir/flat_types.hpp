#ifndef STIR_FLAT_TYPE_HPP_
#define STIR_FLAT_TYPE_HPP_

#include "math.hpp"
#include "basic_types.hpp"
#include "basic_util.hpp"
#include <pulmotor/pulmotor_fwd.hpp>

#include <cassert>
#include <algorithm>

namespace stir {
	
template<class T>
struct area_tag;
	
	
namespace math
{
	inline float sqrt(float a) { return ::sqrtf(a); }
	inline float abs(float a) { return ::fabsf(a); }

	inline double sqrt(double a) { return ::sqrt(a); }
	inline double abs(double a) { return ::fabs(a); }
}

template<class T>
struct vector2_tag
{
	enum { version = pulmotor::version_dont_track };
	
	vector2_tag() {}
	vector2_tag( T x_, T y_ ) : x( x_ ), y( y_ ) {}
	
	template<class OtherT>
	explicit vector2_tag( OtherT const& a ) : x( T(a.x) ), y( T(a.y) ) {}
	
	bool operator==(vector2_tag const& a) const {
		return x == a.x && y == a.y;
	}
	bool operator!=(vector2_tag const& a) const {
		return x != a.x || y != a.y;
	}

	vector2_tag operator+( vector2_tag const& r ) const	{	return vector2_tag( x + r.x, y + r.y ); } 
	vector2_tag operator-( vector2_tag const& r ) const	{	return vector2_tag( x - r.x, y - r.y ); } 
	vector2_tag& operator+=( vector2_tag const& r )     {	x += r.x; y += r.y; return *this; } 
	vector2_tag& operator-=( vector2_tag const& r )     {	x -= r.x; y -= r.y; return *this; } 
	vector2_tag& operator*=( float s )					{	x *= s; y *= s; return *this; }

	vector2_tag operator/( T d ) const		{	return vector2_tag( x / d, y / d ); }
	vector2_tag& operator/=( T d )			{	x /= d; y /= d; return *this; }
	vector2_tag operator*( float s ) const	{	return vector2_tag( T(x * s), T(y * s) ); }
	
	vector2_tag scale (T a, T b) const { return vector2_tag (x*a, y*b); }
	vector2_tag absolute () const { using namespace std; return vector2_tag (abs(x), abs(y)); }
	vector2_tag inverse () const { return vector2_tag (T(1)/x, T(1)/y); }
	T sq_length() const { return x*x + y*y; }
	T length() const { return math::sqrt(x*x + y*y); }
	void normalize(T l) { x /= l; y /= l; }

	void swap (vector2_tag& a) {
		std::swap (a.x, x);
		std::swap (a.y, y);
	}
	
	bool is_null () const { return x == 0 && y == 0; }
	static vector2_tag<T> get_null() { return vector2_tag<T> (0, 0); }

	T x, y;
};

template<class T>
struct circle_tag
{
	enum { version = pulmotor::version_dont_track };

	circle_tag() {}
	circle_tag( vector2_tag<T> const& center, T r )
	:	center_( center ), radius_( r ) 
	{}

	circle_tag( T x, T y, T r )
		:	center_( x, y ), radius_( r )
	{}

	T get_minx() const	{ return center_.x - radius_; }
	T get_miny() const	{ return center_.y - radius_; }

	T get_maxx() const	{ return center_.x + radius_; }
	T get_maxy() const	{ return center_.y + radius_; }

	bool contains( vector2_tag<T> const& p ) const
	{	return sq_length( p - center_ ) <= radius_*radius_; }

	void swap (circle_tag<T>& a) {
		std::swap (a.center_, center_);
		std::swap (a.radius_, radius_);
	}

	vector2_tag<T>	center_;
	T				radius_;
};

template<class T>
struct rect_tag
{
#define check_rect_values(arg) \
	{ \
		assert ((arg).s.x >= 0); \
		assert ((arg).s.y >= 0); \
	}

	enum { version = pulmotor::version_dont_track };
	
	rect_tag()	{}
	rect_tag( vector2_tag<T> const& p_, vector2_tag<T> const& s_ )
	:	p( p_ ), s( s_ ) { check_rect_values(*this); }

	rect_tag( T x, T y, T w, T h )
	:	p( x, y ), s( w, h ) { check_rect_values(*this); }

	rect_tag( T x, T y, vector2_tag<T> const& s_ )
	:	p( x, y ), s( s_ ) { check_rect_values(*this); }

	rect_tag( vector2_tag<T> const& p_, T w, T h )
	:	p( p_ ), s( w, h ) { check_rect_values(*this); }

	template<class OtherT>
	explicit rect_tag( rect_tag<OtherT> const& a )
	:	p( vector2_tag<OtherT>(a.p) ), s( vector2_tag<OtherT>(a.s) ) { check_rect_values(*this); }
	
	bool operator==(rect_tag const& a) const {
		return p == a.p && s == a.s;
	}
	bool operator!=(rect_tag const& a) const {
		return p != a.p || s != a.s;
	}
	
	template<class U>
	bool contains (U px, U py) const {
		return
			px >= p.x && px < p.x + s.x &&
			py >= p.y && py < p.y + s.y;
	}
		
	bool contains( vector2_tag<T> const& pt ) const {
		return contains <T> (pt.x, pt.y);
	}

	vector2_tag<T> top_left() const		{	return p; }
	vector2_tag<T> top_right() const	{	return vector2_tag<T> (p.x + s.x, p.y); }
	vector2_tag<T> bottom_left() const	{	return vector2_tag<T> (p.x, p.y + s.y); }
	vector2_tag<T> bottom_right() const	{	return p + s; }
	
	vector2_tag<T> size () const { return s; }
	
	T left() const		{ return p.x; }
	T right() const		{ return p.x + s.x; }
	T top() const		{ return p.y; }
	T bottom() const	{ return p.y + s.y; }
	
	void set_left(T a) const	{ T w = p.x + s.x - a; p.x = a; s.x = w; }
	void set_right(T a) const	{ s.x = a - p.x; }
	void set_top(T a) const		{ T h = p.y + s.y - a; p.y = a; s.y = h; }
	void set_bottom(T a) const	{ s.y = a - p.y; }
	
	void set_width(T a) { s.x = a; }
	void set_height(T a) { s.y = a; }
	void set_posx(T a) { p.x = a; }
	void set_posy(T a) { p.y = a; }

	T width() const		{ return s.x; }
	T height() const	{ return s.y; }
	
	T area() const		{ return s.x * s.y; }
	
	rect_tag<T> operator*(float k) const { return rect_tag<T> (p * k, s * k); }
	rect_tag<T> const& operator*=(float k) { s *= k; p *= k; return *this; }
	
	bool empty () const	{ return s.is_null (); }

	void swap (rect_tag<T>& a) {
		check_rect_values(a);
		std::swap (a.p, p);
		std::swap (a.s, s);
	}
	
	static rect_tag<T> get_null() { return rect_tag<T> (0, 0, 0, 0); }

	static rect_tag<T> normalized_rect (T x, T y, T w, T h)
	{
		if (w < 0)
		{
			w = -w;
			x -= w;
		}
		
		if (h < 0)
		{
			h = -h;
			y -= h;
		}
		
		return rect_tag<T> (x, y, w, h);
	}
	
	static rect_tag<T> normalized_rect (stir::vector2_tag<T> const& p_, stir::vector2_tag<T> const& s_)
	{
		return normalized_rect(p_.x, p_.y, s_.x, s_.y);
	}
	
	template<class U>
	rect_tag<U> remap (U sx, U sy) const {
		return rect_tag<U> (U(p.x*sx), U(p.y*sy), U(s.x*sx), U(s.y*sy));
	}
	
	area_tag<T> to_area () const { return area_tag<T> (p.x, p.y, right(), bottom()); }

	vector2_tag<T>	p, s;
#undef check_rect_values
};

template<class T>
struct area_tag
{
	enum { version = pulmotor::version_dont_track };
	
	struct init_zero {};
	
	area_tag() {}
	explicit area_tag( init_zero ) : p0(0,0), p1(0,0) {}
	explicit area_tag( float x0, float y0, float x1, float y1 ) : p0( x0, y0 ), p1( x1, y1 ) {}
	explicit area_tag( vector2_tag<T> const& pa, vector2_tag<T> const& pb ) : p0( pa ), p1( pb ) {}
	
	template<bool GlStyle>
	T* walk_round (T* dest, size_t vectorStride = sizeof(T)) const
	{
		if (GlStyle) {
			dest[0] = p0.x; dest[1] = p1.y; dest = stride(dest, vectorStride);
			dest[0] = p1.x; dest[1] = p1.y; dest = stride(dest, vectorStride);
			dest[0] = p0.x; dest[1] = p0.y; dest = stride(dest, vectorStride);
			dest[0] = p1.x; dest[1] = p0.y; dest = stride(dest, vectorStride);
		} else {
			dest[0] = p0.x; dest[1] = p0.y; dest = stride(dest, vectorStride);
			dest[0] = p1.x; dest[1] = p0.y; dest = stride(dest, vectorStride);
			dest[0] = p1.x; dest[1] = p1.y; dest = stride(dest, vectorStride);
			dest[0] = p0.x; dest[1] = p1.y; dest = stride(dest, vectorStride);
		}
		return dest;
	}
	
	T width () const { return p1.x - p0.x; }
	T height () const { return p1.y - p0.y; }
	
	T minx () const { return std::min (p0.x, p1.x); }
	T miny () const { return std::min (p0.y, p1.y); }
	T maxx () const { return std::max (p0.x, p1.x); }
	T maxy () const { return std::max (p0.y, p1.y); }
	
	rect_tag<T> to_rect() const { return rect_tag<T>(p0, p1-p0); }
	rect_tag<T> to_rect(float sx, float sy) const
	{ return rect_tag<T>(p0.x*sx, p0.y*sy, width()*sx, height()*sy); }
	rect_tag<T> to_rect(vector2_tag<float> const& s) const
	{ return rect_tag<T>(p0.x*s.x, p0.y*s.y, width()*s.x, height()*s.y); }
	
	static area_tag zero_to_one();
	
	vector2_tag<T>	p0, p1;
};

template<> inline area_tag<float> area_tag<float>::zero_to_one () { return area_tag<float> (0,0,1.0f,1.0f); }
template<> inline area_tag<double> area_tag<double>::zero_to_one () { return area_tag<double> (0,0,1,1); }

typedef vector2_tag<int>		vector2i;
typedef vector2_tag<int16_t>	vector2i16;
typedef vector2_tag<int8_t>		vector2i8;
typedef vector2_tag<float>		vector2f;

typedef circle_tag<int>		circlei;
typedef circle_tag<int16_t>	circlei16;
typedef circle_tag<float>	circlef;

typedef rect_tag<int16_t>	recti16;
typedef rect_tag<int>		recti;
typedef rect_tag<float>		rectf;
		
typedef area_tag<float>	areaf;	
typedef area_tag<int>	areai;
typedef area_tag<short> areai16;

template<class T>
inline rect_tag<T> unite (rect_tag<T> const& a, rect_tag<T> const& b)
{
	using namespace std;
	T x = min (a.left (), b.left ());
	T y = min (a.top (), b.top ());
	T x1 = min (a.right (), b.right ());
	T y1 = min (a.bottom (), b.bottom ());
	
	return rect_tag<T> (x, y, x1 - x, y1 - y);	
}

template<class T>
inline T sq_length( vector2_tag<T> const& a )
{	return a.x * a.x + a.y * a.y; }

} // stir

#endif // STIR_FLAT_TYPE_HPP_
