#include "platform.hpp"

#include "frame_time.hpp"
#include <cassert>
#include "mul_div_64.hpp"

namespace stir {
	
time_type k_long_ago = -10000;
	
#if STIR_WINDOWS
#include "windows/win_include.hpp"

precise_time_type get_absolute_time_hp ()
{												
	LARGE_INTEGER s, f;
	QueryPerformanceCounter( &s );
	QueryPerformanceFrequency( &f );
	return stir::utility::muldiv64( s.QuadPart, time_sec_k, f.QuadPart );
}
	
#elif STIR_MACOSX || STIR_IPHONE

#include <mach/mach_time.h>

//precise_time_type get_absolute_time_hp ()
//{
//	mach_timebase_info_data_t tdat;
//	mach_timebase_info (&tdat);
//	stir::uint64_t t = mach_absolute_time();
//	
//	double secs = 1e-9 * tdat.numer / tdat.denom * t;
//	
//	return (precise_time_type)(secs * 0x100000000LL);
//		
////	assert ("NOT IMPLEMENTED CORRECTLY");	
////	return stir::utility::muldiv64( t, time_sec_k, *(stir::uint64_t*)&tdat );
//}
	
time_type get_absolute_time ()
{
	// the frequency of that clock is: (sToNanosDenominator / sToNanosNumerator) * 10^9
	mach_timebase_info_data_t tdat;
	mach_timebase_info (&tdat);
	
//	time_type freq = static_cast<time_type>(tdat.denom) / static_cast<time_type>(tdat.numer) * 1000000000.0;
	
	uint64_t t = mach_absolute_time();
	return (time_type)t * (time_type)tdat.numer / (time_type)tdat.denom / 1e9;
}


#endif

//
	
//
//frame_time::frame_time()
//{	start(); }
//
//void frame_time::update()
//{	QueryPerformanceCounter( &current ); }
//
////interval_start frame_time::begin_interval() const
////{	return interval_start( current.QuadPart ); }
//
//time_position frame_time::get_difference( time_position const& is ) const
//{	return time_position( current.QuadPart - is.value() ); }
//
//time_type frame_time::get_seconds( time_position const& td ) const
//{	return time_type( td.value() ) / time_type( freq.QuadPart ); }
//
//void frame_time::start()
//{
//	QueryPerformanceFrequency( &freq );
//	QueryPerformanceCounter( &start_time );
//}

} // stir
