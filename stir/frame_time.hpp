#ifndef STIR_FRAME_TIME_HPP_
#define STIR_FRAME_TIME_HPP_

#include "platform.hpp"
#include "basic_types.hpp"

#include <iosfwd>

namespace stir
{
	typedef double time_type;
	extern time_type k_long_ago;

	//	typedef int64_t precise_time_type;
	//	precise_time_type const time_sec_k = 0x0000000100000000ULL;
	//	precise_time_type get_absolute_time_hp ();
	
	time_type get_absolute_time ();
	
	//
	class STIR_ATTR_DLL time_position_with_pause
	{
	public:
		time_position_with_pause()
		:	m_pause_start(0), m_pause_amount(0)
		,	m_paused( false )
		{
		}

		bool is_paused() const
		{
			return m_paused;
		}

		void advance( time_type delta )
		{
			m_pause_amount += delta;
		}

		void pause( time_type cur, bool enablePause )
		{
			if( m_paused )
			{
				if( !enablePause )
				{
					m_pause_amount += cur - m_pause_start;
					m_pause_start = cur;
					m_paused = false;
				}
			}
			else
			{
				if( enablePause )
				{
					m_pause_start = cur;
					m_paused = true;
				}
			}
		}

		void reset()
		{
			m_pause_start = m_pause_amount = 0;
			m_paused = false;
		}

		time_type process_current( time_type const& tp ) const
		{
			time_type additional = m_paused ? (tp - m_pause_start) : 0;
			return tp - m_pause_amount - additional;
		}

	private:
		time_type	m_pause_start, m_pause_amount;
		bool		m_paused;
	};
	
	class STIR_ATTR_DLL fps_counter
	{
	public:
		fps_counter( unsigned ms = 1000 )
		:	last_calc( 0 ), last_frame( 0 ), current_fps( 0 ), total_delta( 0 ), avg_delta( 0 ), avg_period( ms ), frames( 0 )
		{}

		void start( time_type const& ft )
		{	last_frame = last_calc = ft; avg_delta = total_delta = 0; }

		time_type get_avg_fps()		{	return current_fps; }
		time_type get_avg_delta()	{	return avg_delta; }

		void frame_rendered( time_type const& ft )
		{
			time_type dt = ft - last_calc;
			frames++;

			total_delta += ft - last_frame;

			if( dt >= 1 )
			{
				current_fps = time_type( frames ) / dt;
				avg_delta = total_delta / time_type( frames );
				last_calc = ft;
				frames = 0;
				total_delta = 0;
			}

			last_frame = ft;
		}

	private:
		time_type	last_calc, last_frame;
		time_type	current_fps, total_delta, avg_delta;
		unsigned	avg_period, frames;
	};
}

#endif // STIR_FRAME_TIME_HPP_
