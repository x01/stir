#include <stir/gl.hpp>

#include <stir/image.hpp>
#include <stir/texture.hpp>

namespace stir { namespace gl {
	
size_t component_count(PixelFormat format)
{
	if (format == PixelFormat::ALPHA || format == PixelFormat::LUMINANCE)
		return 1;
	else if (format == PixelFormat::LUMINANCE_ALPHA)
		return 2;
	else if (format == PixelFormat::RGB)
		return 3;
	else if (format == PixelFormat::RGBA)
		return 4;
	else
		return 1;
}
	
void tex_image_2d(int level, gl::PixelFormat textureFormat, int w, int h, void const* data, gl::PixelType dataType)
{
	gl::TexImage2D(gl::TextureTarget::TEXTURE_2D, level, (GLint)textureFormat, w, h, 0, textureFormat, dataType, data);
}
	
void read_framebuffer (image& img)
{
//	gl::BindTexture(gl::TextureTarget::TEXTURE_2D, tex->tex_name);
//	
//	texture_info ti (tex);
//	size_t comp = component_count(tex->format);
//	GLint packAlign = 4;
//	gl::GetIntegerv(gl::GetPName::PACK_ALIGNMENT, &packAlign);
//	if (img.width != ti.width() || img.height != ti.height() || img.comp != comp || !img.is_aligned(packAlign))
//		image (ti.width(), ti.height(), comp, image::align_pitch{packAlign}).swap(img);
//	
//	gl::ReadPixels(0, 0, img.width, img.height, tex->format, gl::PixelType::UNSIGNED_BYTE, img.pixels);
}


struct GlErrorContainer { int err; char const* str; char const* desc; }
static gl_errs[] =
{
	{ GL_NO_ERROR, "NO_ERROR", "No error has been recorded. The value of this symbolic constant is guaranteed to be 0." },
	{ GL_INVALID_ENUM, "INVALID_ENUM", "An unacceptable value is specified for an enumerated argument. The offending command is ignored and has no other side effect than to set the error flag." },
	{ GL_INVALID_VALUE, "INVALID_VALUE", "A numeric argument is out of range. The offending command is ignored and has no other side effect than to set the error flag." },
	{ GL_INVALID_OPERATION, "INVALID_OPERATION", "The specified operation is not allowed in the current state. The offending command is ignored and has no other side effect than to set the error flag." },
	//	{ GL_INVALID_FRAMEBUFFER_OPERATION, "INVALID_FRAMEBUFFER_OPERATION", "The framebuffer object is not complete. The offending command is ignored and has no other side effect than to set the error flag." },
	{ GL_OUT_OF_MEMORY, "OUT_OF_MEMORY", "There is not enough memory left to execute the command. The state of the GL is undefined, except for the state of the error flags, after this error is recorded." },
#if STIR_GLES1
	{ GL_STACK_UNDERFLOW, "STACK_UNDERFLOW", "An attempt has been made to perform an operation that would cause an internal stack to underflow." },
	{ GL_STACK_OVERFLOW, "STACK_OVERFLOW", "An attempt has been made to perform an operation that would cause an internal stack to overflow." },
#endif
};

char const* error_str(int err)
{
	for (auto const& s : gl_errs) {
		if (s.err == err)
			return s.str;
	}
	return "unknown";
}

char const* error_desc(int err)
{
	for (auto const& s : gl_errs) {
		if (s.err == err)
			return s.desc;
	}
	return "unknown";
}
	
//inline void TexImage2D (int level, PixelFormat textureFormat, int w, int h, void* pixels, PixelType pixelType = PixelType::ubyte) {
//	glTexImage2D(GL_TEXTURE_2D, level, (GLint)textureFormat, w, h, 0, (GLint)textureFormat, (GLenum)pixelType, pixels);
//	STIR_GLRESULT();
//}
//
//inline void TexSubImage2D (int level, int xoffset, int yoffset, int w, int h, void* pixels, PixelFormat textureFormat, PixelType pixelType = PixelType::unsigned_byte) {
//	glTexSubImage2D (GL_TEXTURE_2D, level, xoffset, yoffset, w, h, (GLenum)textureFormat, (GLenum)pixelType, pixels);
//	STIR_GLRESULT();
//}
//
//inline void BindTexture (TextureTarget target, GLuint texture) {
//	glBindTexture ((GLenum)target, texture);
//	STIR_GLRESULT();
//}
	


}}
