#ifndef STIR_GL_HPP
#define STIR_GL_HPP

#include <stir/platform.hpp>
#include <stir/basic_util.hpp>

namespace stir { namespace gl {
//
// helpers
//
char const* error_str(int err);
char const* error_desc(int err);
	
}}

#define STIR_GLRESULT()

/*#define STIR_GLRESULT() do { \
	int err = glGetError(); \
	if (err != GL_NO_ERROR) \
	stir_fatal(">>> OPEN GL reported error %s(%d) in function %s: %s\n", \
	::stir::gl::error_str(err), err, __FUNCTION__, error_desc(err)); \
	} while(0)
 */

#define STIR_GLRESULT_F(f) do { \
	int err = glGetError(); \
	if (err != GL_NO_ERROR) \
	stir_fatal(">>> GL %s reported error %s(%d) in function %s: %s\n", \
	#f, ::stir::gl::error_str(err), err, __FUNCTION__, ::stir::gl::error_desc(err)); \
	} while(0)

#define STIR_GLCALL(x, ...) x (__VA_ARGS__); STIR_GLRESULT_F(x);


//namespace stir {
//
//struct glext
//{
//	GL_API GLvoid GL_APIENTRY (*glGetBufferPointervOES) (GLenum target, GLenum pname, GLvoid **params);
//	GL_API GLvoid * GL_APIENTRY (*glMapBufferOES) (GLenum target, GLenum access);
//	GL_API GLboolean GL_APIENTRY (*glUnmapBufferOES) (GLenum target);
//};
//	
//}

#if STIR_GLES2

#include <OpenGLES/ES2/gl.h>
#include <OpenGLES/ES2/glext.h>

#include <stir/gles/gles2_bindings.hpp>

#elif STIR_GLES1

#include <OpenGLES/ES1/gl.h>
#include <OpenGLES/ES1/glext.h>

#elif STIR_OPENGL

#include <gl.h>
#include <glext.h>

#else

#error "which open gl?"

#endif

namespace stir {
	
	struct image;
 
namespace gl {
	
size_t component_count(PixelFormat format);
void tex_image_2d(int level, gl::PixelFormat textureFormat, int w, int h, void const* data, gl::PixelType dataType = gl::PixelType::UNSIGNED_BYTE);
	
void read_framebuffer (image& img);

} }

#endif
