#include <stir/gles/buffer.hpp>

namespace stir { namespace gl {
	
char const* g_bind_names[] = {
	"byte", "ubyte", "short", "ushort", "fixed", "float"
};
	
}}
