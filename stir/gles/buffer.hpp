#ifndef stir_buffer__
#define stir_buffer__

#include <stir/gl.hpp>
#include <stir/gles/gles_shader.hpp>
#include <stir/stir_logs.hpp>

namespace stir {

template<class T>
struct swap_chain {
	T m_p[2];
	size_t m_current = 0;
	
	T& operator[](int i) { return m_p[i]; }
	
	T& current () { return m_p[m_current]; }
	size_t current_index () { return m_current; }
	T& get_and_flip () { size_t cur = m_current; m_current = 1 - m_current; return m_p[cur]; }
	T& flip_and_get () { m_current = 1 - m_current; return m_p[m_current]; }
	
	T* begin() { return m_p; }
	T* end() { return m_p + 2; }
};
	
namespace gl {
	
#if STIR_GLES2
enum class BufferAccess : GLuint {
	WriteOnly = GL_WRITE_ONLY_OES
};
#endif
	
struct buffer
{
	GLuint m_buffer = 0;

	buffer(buffer const&) = delete;
	buffer& operator=(buffer const&) = delete;

	buffer() {
		gl::GenBuffers(1, &m_buffer);
	}

	buffer (GLuint buf)
	: m_buffer(buf) {
	}

	buffer(buffer&& b)
	: m_buffer(b.m_buffer) {
		b.m_buffer = 0;
	}

	buffer& operator=(buffer&& b) {
		gl::DeleteBuffers(1, &m_buffer);
		m_buffer = b.m_buffer;
		b.m_buffer = 0;
		return *this;
	}

	~buffer() {
		gl::DeleteBuffers(1, &m_buffer);
	}

	void bind(gl::BufferTarget target = gl::BufferTarget::ARRAY_BUFFER) {
		gl::BindBuffer(target, m_buffer);
	}

	static void unbind(gl::BufferTarget target = gl::BufferTarget::ARRAY_BUFFER) {
		gl::BindBuffer(target, 0);
	}
	
	static void data(BufferTarget target, GLsizeiptr size, GLvoid* data, gl::BufferUsage usage) {
		gl::BufferData(target, size, data, usage);
	}

	#if STIR_GLES2
	static void* map(BufferTarget target = gl::BufferTarget::ARRAY_BUFFER, BufferAccess access = BufferAccess::WriteOnly) {
		void* p = STIR_GLCALL(glMapBufferOES, (GLuint)target, (GLuint)access);
		return p;
	}

	template<class T>
	static T* map (BufferTarget target = gl::BufferTarget::ARRAY_BUFFER, BufferAccess access = BufferAccess::WriteOnly) {
		return static_cast<T*>(map(target, access));
	}

	static void unmap(BufferTarget target = gl::BufferTarget::ARRAY_BUFFER) {
		STIR_GLCALL(glUnmapBufferOES, (GLenum)target);
	}
	#endif
};

enum class bind_type : u8
{
	byte, ubyte, short_, ushort, fixed, float_
};
extern char const* g_bind_names[];

struct bind_stream {
	char const* name;
	u8 count;
	bind_type type;
	bool do_norm;
	u8 offset, stride;
};
	
template<int StreamN>
class binding {

	static gl::VertexAttribPointerType conv_type(bind_type t)
	{
		return
			t == bind_type::byte ? gl::VertexAttribPointerType::BYTE :
			t == bind_type::ubyte ? gl::VertexAttribPointerType::UNSIGNED_BYTE :
			t == bind_type::short_ ? gl::VertexAttribPointerType::SHORT :
			t == bind_type::ushort ? gl::VertexAttribPointerType::UNSIGNED_SHORT :
			t == bind_type::fixed ? gl::VertexAttribPointerType::FIXED : gl::VertexAttribPointerType::FLOAT;
	}

	bind_stream m_streams[StreamN];
	GLint m_attrib[StreamN];
	gpu_program* m_gpuprog = nullptr;

	void config_stream (int i) {
		
		if (m_attrib[i] < 0) {
			STIR_LOG (stir::gfx).printf("While binding shader %s, attribute for stream %d %s, type %s%d not bound\n",
										m_gpuprog->m_name.c_str(), i,
										m_streams[i].name, g_bind_names[(int)m_streams[i].type], m_streams[i].count);
		}
		
		gl::EnableVertexAttribArray(i);
		gl::VertexAttribPointer(m_attrib[i], m_streams[i].count, conv_type(m_streams[i].type),
			m_streams[i].do_norm, m_streams[i].stride, (void*)(uintptr_t)m_streams[i].offset);
	}
	
public:
	binding (std::initializer_list<bind_stream> streams) {
		
		std::fill_n(m_attrib, StreamN, -2);
//		memset (m_attrib, 0xff, sizeof(m_attrib));
		
		//		static_assert(streams.size() == StreamN, "bad");
		assert (streams.size() == StreamN);
		
		int i = 0;
		for (bind_stream const& s : streams)
			m_streams[i++] = s;
	}
	
	void bind_attrs(gpu_program& gp) {
		m_gpuprog = &gp;
		for (int i=0; i<StreamN; ++i)
			m_attrib[i] = gp.attrib(m_streams[i].name);
	}

	void use(GLuint vertexBuffer, GLuint indexBuffer) {
		
		assert (m_gpuprog != nullptr && "");

		gl::BindBuffer(gl::BufferTarget::ARRAY_BUFFER, vertexBuffer);
		gl::BindBuffer(gl::BufferTarget::ELEMENT_ARRAY_BUFFER, indexBuffer);
		
		for (int i=0; i<StreamN; ++i)
			config_stream (i);
	}

	void use(std::initializer_list<GLuint> vertexBuffers, GLuint indexBuffer) {
		
		assert(vertexBuffers.size() == StreamN);

		gl::BindBuffer(gl::BufferTarget::ELEMENT_ARRAY_BUFFER, indexBuffer);
		
		for (int i=0; i<StreamN; ++i) {
			gl::BindBuffer(gl::BufferTarget::ARRAY_BUFFER, *(vertexBuffers.begin () + i));
			config_stream (i);
		}
	}

	void unbind() {
		for (int i=0; i<StreamN; ++i) {
			gl::BindBuffer(gl::BufferTarget::ARRAY_BUFFER, 0);
			gl::DisableVertexAttribArray(i);
		}
	}
};
	
	
#define USE_VAO 1
	
#if USE_VAO
class vao
{
	GLuint m_vao = 0;
public:
	
	vao() {
		glGenVertexArraysOES(1, &m_vao);
	}
	
	~vao() {
		if (m_vao)
			glDeleteVertexArraysOES (1, &m_vao);
	}
	
	template<int N>
	void create(binding<N>& binding, GLuint vertexBuffer, GLuint indexBuffer) {
		use();
		binding.use (vertexBuffer, indexBuffer);
		dont_use();
	}
	
	// Pass same buffer say twice if two streams use that buffer
	template<int N>
	void create(binding<N>& binding, std::initializer_list<GLuint> buffers, GLuint indexBuffer) {
		assert (buffers.size() == N);
		use();
		binding.use (buffers, indexBuffer);
		dont_use();
	}
	
	void use() {
		glBindVertexArrayOES(m_vao);
	}
	
	void dont_use () {
		glBindVertexArrayOES(0);
	}
};
#endif
	
template<class T>
struct use;

template<>
struct use<gl::buffer>
{
	gl::BufferTarget target;
	
	use (gl::buffer& b, gl::BufferTarget targ) : target(targ) { b.bind(target); }
	
	void reuse (gl::buffer& b) { b.bind(target); }
	
	void data (size_t sz, void* p, gl::BufferUsage usage) { gl::buffer::data(target, sz, p, usage); }
	void* map (gl::BufferAccess acc) { return gl::buffer::map(target, acc); }
	void unmap () { gl::buffer::unmap(target); }
	void unbind () { gl::buffer::unbind(target); }
};
	
}}

#endif
