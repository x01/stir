uniform sampler2D tex;
uniform lowp vec4 color;

varying highp vec2 tex_coord;

void main()
{
	lowp vec4 texC = texture2D(tex, tex_coord);	
	gl_FragColor = vec4(color.rgb, color.a * texC.a);
}
