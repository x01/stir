#ifndef OGL_ES_DEVICE_HPP_
#define OGL_ES_DEVICE_HPP_

#include <objc/objc.h>

#if __OBJC__
#import <UIKit/UIKit.h>
#import <UIKit/UIApplication.h>
#import <UIKit/UIViewController.h>
#import <QuartzCore/QuartzCore.h>

#import <OpenGLES/EAGL.h>

@interface gles_view_controller : UIViewController {
};
	
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation	;
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration ;
- (void)willAnimateFirstHalfOfRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation ;
- (void)willAnimateSecondHalfOfRotationFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation duration:(NSTimeInterval)duration ;
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation ;

- (void)viewDidLoad;
- (void)viewWillAppear:(BOOL)animated;
- (void)viewDidAppear:(BOOL)animated;
- (void)viewWillDisappear:(BOOL)animated;
- (void)viewDidDisappear:(BOOL)animated;

@end

@interface gles_view : UIView
{
@public
	EAGLContext*			context_;
//	gles_view_controller*	view_controller_;
@private
};
@end
#endif

#include <stir/gl.hpp>


namespace stir { namespace gles {

struct device_context;

struct factory
{
	factory ();

//	device_context* create_context (void* eaglContext, CAEAGLLayer* eaglLayer);
	device_context* create_context (id eaglContext, id eaglLayer, float pscale);
	
private:	
};

struct device_context
{
	// device_context (EAGLContext* eaglContext, CAEAGLLayer* eaglLayer);
	device_context ();
	device_context (id eaglContext, id eaglLayer, float psz);
	~device_context ();
		
	void recreate (id eaglContext, id eaglLayer);
	void update_state (id eaglLayer);
	bool is_valid () const;
	
	GLint width () const { return backingWidth; }
	GLint height () const { return backingHeight; }
	float point_size() const { return point_size_; }
	
	GLint logical_width () const { return backingWidth / point_size_; }
	GLint logical_height () const { return backingHeight / point_size_; }
	
	GLuint render_buffer () const { return viewRenderbuffer; }
	GLuint frame_buffer () const { return viewFramebuffer; }
	GLuint depth_buffer () const { return depthRenderbuffer; }

private:
	void create (id eaglContext, id eaglLayer);
	void destroy ();
	
	id		eagl_context_; // EAGLContext
	
	GLint	backingWidth, backingHeight;
	GLuint	viewRenderbuffer, viewFramebuffer;
	GLuint	depthRenderbuffer;

	float point_size_;
	bool	valid_;
};

#if STIR_GLES1
void set_combiner (GLenum combineModeRGB, GLenum operandColor, GLenum src0RGB, GLenum src1RGB,
				   GLenum combineModeALPHA, GLenum operandAlpha, GLenum src0ALPHA, GLenum src1ALPHA);
	
void disable_client_states ();
#endif
	
}

}

#endif // OGL_ES_DEVICE_HPP_
