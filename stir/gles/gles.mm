#import <QuartzCore/QuartzCore.h>
#import <OpenGLES/EAGLDrawable.h>

#include "gles.h"
#include <stir/stir_logs.hpp>

#import <UIKit/UIKit.h>

#define USE_DEPTH_BUFFER 0

@implementation gles_view

+ (Class)layerClass {
	return [CAEAGLLayer class];
}

- (id)initWithFrame:(CGRect)frame {
	if (self = [super initWithFrame:frame]) {
		// Initialization code
	}
	return self;
}

- (id)initWithCoder:(NSCoder*)coder {
	
	if ((self = [super initWithCoder:coder])) {
		// Get the layer
		CAEAGLLayer *eaglLayer = (CAEAGLLayer *)self.layer;
		
		eaglLayer.opaque = YES;
		eaglLayer.drawableProperties =
			[NSDictionary dictionaryWithObjectsAndKeys:
				[NSNumber numberWithBool:NO],
				kEAGLDrawablePropertyRetainedBacking,
				kEAGLColorFormatRGBA8,
				kEAGLDrawablePropertyColorFormat,
				nil
			];
		
		context_ = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
		
		if (!context_ || ![EAGLContext setCurrentContext:context_]) {
			[self release];
			return nil;
		}		
	}
	
	return self;
}

@end

namespace stir { namespace gles {
	
device_context* factory::create_context (id eaglContext, id eaglLayer, float pscale)
{
	return new device_context (eaglContext, eaglLayer, pscale);
}

device_context::device_context ()
:	valid_ (false)
,	eagl_context_ (0)
,	depthRenderbuffer(0)
,	backingWidth(0), backingHeight(0)
,	viewRenderbuffer(0), viewFramebuffer(0)
{
}

device_context::device_context (id eaglContext, id eaglLayer, float pscale)
:	valid_ (false)
,	eagl_context_ (eaglContext)
,	depthRenderbuffer(0)
,	backingWidth(0), backingHeight(0)
,	viewRenderbuffer(0), viewFramebuffer(0)
,	point_size_(pscale)
{
	create (eaglContext, eaglLayer);
}

device_context::~device_context()
{
	destroy ();
}
	
#if STIR_GLES1
void device_context::destroy ()
{
	glBindFramebufferOES(GL_FRAMEBUFFER_OES, 0);
	glBindRenderbufferOES(GL_RENDERBUFFER_OES, 0);
	if (viewFramebuffer) {
		glDeleteFramebuffersOES(1, &viewFramebuffer);
		viewFramebuffer = 0;
	}
	
	if (viewRenderbuffer) {
		glDeleteRenderbuffersOES(1, &viewRenderbuffer);
		viewRenderbuffer = 0;
	}
	
	if(depthRenderbuffer) {
		glDeleteRenderbuffersOES(1, &depthRenderbuffer);
		depthRenderbuffer = 0;
	}
}

void device_context::create (id eaglContext, id eaglLayer)
{
	assert (viewFramebuffer == 0);
	assert (viewRenderbuffer == 0);
	
	// create ogl stuff
	glGenFramebuffersOES(1, &viewFramebuffer);
	glGenRenderbuffersOES(1, &viewRenderbuffer);
	
	glBindFramebufferOES(GL_FRAMEBUFFER_OES, viewFramebuffer);
	glBindRenderbufferOES(GL_RENDERBUFFER_OES, viewRenderbuffer);
	//	[eagl_context_ renderbufferStorage:GL_RENDERBUFFER_OES fromDrawable:(CAEAGLLayer*)eaglLayer];
	eagl_context_ = eaglContext;
	[eagl_context_ renderbufferStorage:GL_RENDERBUFFER_OES fromDrawable:(id<EAGLDrawable>)eaglLayer];
	glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES, GL_COLOR_ATTACHMENT0_OES, GL_RENDERBUFFER_OES, viewRenderbuffer);
	
	glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES, GL_RENDERBUFFER_WIDTH_OES, &backingWidth);
	glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES, GL_RENDERBUFFER_HEIGHT_OES, &backingHeight);
	
	STIR_LOG(debug).printf("Backbuffer: %d x %d, point size: %f (effective %d x %d)\n", backingWidth, backingHeight, point_size_, int(backingWidth / point_size_), int(backingHeight / point_size_));
	
	if (USE_DEPTH_BUFFER) {
		
		glGenRenderbuffersOES(1, &depthRenderbuffer);
		glBindRenderbufferOES(GL_RENDERBUFFER_OES, depthRenderbuffer);
		glRenderbufferStorageOES(GL_RENDERBUFFER_OES, GL_DEPTH_COMPONENT16_OES, backingWidth, backingHeight);
		glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES, GL_DEPTH_ATTACHMENT_OES, GL_RENDERBUFFER_OES, depthRenderbuffer);
	}
	
	if(glCheckFramebufferStatusOES(GL_FRAMEBUFFER_OES) != GL_FRAMEBUFFER_COMPLETE_OES) {
		NSLog(@"failed to make complete framebuffer object %x", glCheckFramebufferStatusOES(GL_FRAMEBUFFER_OES));
	}
	
	valid_ = true;	
}
	
#elif STIR_GLES2

void device_context::destroy ()
{
	gl::BindFramebuffer(gl::BufferBinding::FRAMEBUFFER, 0);
	gl::BindRenderbuffer(gl::RenderbufferTarget::RENDERBUFFER, 0);
	if (viewFramebuffer) {
		gl::DeleteFramebuffers(1, &viewFramebuffer);
		viewFramebuffer = 0;
	}
	
	if (viewRenderbuffer) {
		gl::DeleteFramebuffers(1, &viewRenderbuffer);
		viewRenderbuffer = 0;
	}
	
	if(depthRenderbuffer) {
		gl::DeleteFramebuffers(1, &depthRenderbuffer);
		depthRenderbuffer = 0;
	}
}

void device_context::create (id eaglContext, id eaglLayer)
{
	assert (viewFramebuffer == 0);
	assert (viewRenderbuffer == 0);
	
	// create ogl stuff
	gl::GenFramebuffers(1, &viewFramebuffer);
	gl::GenRenderbuffers(1, &viewRenderbuffer);

	gl::BindFramebuffer(gl::BufferBinding::FRAMEBUFFER, viewFramebuffer);
	gl::BindRenderbuffer(gl::RenderbufferTarget::RENDERBUFFER, viewRenderbuffer);
	//	[eagl_context_ renderbufferStorage:GL_RENDERBUFFER_OES fromDrawable:(CAEAGLLayer*)eaglLayer];
	eagl_context_ = eaglContext;
	[eagl_context_ renderbufferStorage:GL_RENDERBUFFER fromDrawable:(id<EAGLDrawable>)eaglLayer];
	gl::FramebufferRenderbuffer(gl::FramebufferTarget::FRAMEBUFFER, gl::FramebufferAttachment::COLOR_ATTACHMENT0, gl::RenderbufferTarget::RENDERBUFFER, viewRenderbuffer);
	
	gl::GetRenderbufferParameteriv(gl::RenderbufferTarget::RENDERBUFFER, gl::RenderbufferPName::RENDERBUFFER_WIDTH, &backingWidth);
	gl::GetRenderbufferParameteriv(gl::RenderbufferTarget::RENDERBUFFER, gl::RenderbufferPName::RENDERBUFFER_HEIGHT, &backingHeight);
	
	STIR_LOG(debug).printf("Backbuffer: %d x %d, point size: %f (effective %d x %d)\n", backingWidth, backingHeight, point_size_, int(backingWidth / point_size_), int(backingHeight / point_size_));
	
	if (USE_DEPTH_BUFFER) {
		gl::GenRenderbuffers(1, &depthRenderbuffer);
		gl::BindRenderbuffer(gl::RenderbufferTarget::RENDERBUFFER, depthRenderbuffer);
		gl::RenderbufferStorage(gl::RenderbufferTarget::RENDERBUFFER, gl::InternalFormat::DEPTH_COMPONENT16, backingWidth, backingHeight);
		gl::FramebufferRenderbuffer(gl::FramebufferTarget::FRAMEBUFFER, gl::FramebufferAttachment::DEPTH_ATTACHMENT,
									gl::RenderbufferTarget::RENDERBUFFER, depthRenderbuffer);
	}
	
	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		NSLog(@"failed to make complete framebuffer object %x", gl::CheckFramebufferStatus(gl::FramebufferTarget::FRAMEBUFFER));
	}
	
	valid_ = true;	
}
#endif

void device_context::recreate (id eaglContext, id eaglLayer)
{
	if (valid_)
		destroy ();
	
	create (eaglContext, eaglLayer);
}
	

	
void device_context::update_state (id eaglLayer)
{
	recreate (eagl_context_, eaglLayer);
	
//	glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES, GL_RENDERBUFFER_WIDTH_OES, &backingWidth);
//	glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES, GL_RENDERBUFFER_HEIGHT_OES, &backingHeight);
//	
//	if (USE_DEPTH_BUFFER) {
//		if(depthRenderbuffer)
//			glDeleteRenderbuffersOES(1, &depthRenderbuffer), depthRenderbuffer = 0;
//		
//		glGenRenderbuffersOES(1, &depthRenderbuffer);
//		glBindRenderbufferOES(GL_RENDERBUFFER_OES, depthRenderbuffer);
//		glRenderbufferStorageOES(GL_RENDERBUFFER_OES, GL_DEPTH_COMPONENT16_OES, backingWidth, backingHeight);
//		glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES, GL_DEPTH_ATTACHMENT_OES, GL_RENDERBUFFER_OES, depthRenderbuffer);
//	}
//	
//	if(glCheckFramebufferStatusOES(GL_FRAMEBUFFER_OES) != GL_FRAMEBUFFER_COMPLETE_OES) {
//		NSLog(@"framebuffer is not complete after 'update_state', object %x", glCheckFramebufferStatusOES(GL_FRAMEBUFFER_OES));
//	}	
}

#if STIR_GLES1
// docs:
	// texture expansion
//    TEXFMT	Color	Alpha
	//	A		0,0,0	A
	//	L		L,L,L	1
	//	LA		L,L,L	A
	//	I		I,I,I	I
	//	RGB		R,G,B	1
	//	RGBA	R,G,B	A
	
//	GL_TEXTURE_ENV_MODE:	REPLACE, MODULATE, DECAL, BLEND, ADD, or COMBINE
//	f - primary, s - texture, c - color, 
//	- REPLACE:
//		Out = [Cs, As] (Af, if Alpha is not available)
//	- MODULATE
//		Out = [Cf * Cs, Af * As]
//	- DECAL
//		Out = [Cf * (1-As) + Cs*As), Af]
//	- BLEND
//		Out = [Cf * (1-Cs) + Cc * Cs, Af * As]
//	- ADD
//		Out = [Cf + Cs, Af * As]
//	- COMBINE:
//		* COMBINE_RGB
//			+ REPLACE: Arg0
//			+ MODULATE: Arg0 * Arg1
//			+ ADD: Arg0 + Arg1
//			+ ADD_SIGNED: Arg0 + Arg1 - 0.5
//			+ INTERPOLATE: Arg0 * Arg2 + Arg1 * (1-Arg2)
//			+ SUBTRACT: Arg0 - Arg1
//			+ ADD_SIGNED: Arg0 + Arg1 - 0.5
//			+ DOT3_RGB:	4 x ((Arg0x - 0.5) * (Arg1x - 0.5)[x=rgb] ?
//			+ DOT3_RGBA: 4 x ((Arg0x - 0.5) * (Arg1x - 0.5)[x=rgba] ?
//		* COMBINE_ALPHA
//			+ REPLACE: Arg0
//			+ MODULATE:	Arg0 * Arg1
//			+ ADD: Arg0 + Arg1
//			+ ADD_SIGNED: Arg0 + Arg1 - 0.5
//			+ INTERPOLATE: Arg0 * Arg2 + Arg1 * (1-Arg2)
//			+ SUBTRACT:	Arg0 - Arg1
//		* SRCx_RGB
//			+ TEXTURE, TEXTUREn, CONSTANT, PRIMARY_COLOR, PREVIOUS
//		* OPERANDx_RGB
//			+ SRC_COLOR, ONE_MINUS_SRC_COLOR, SRC_ALPHA, ONE_MINUS_SRC_ALPHA
//		* SRCx_ALPHA
//			+ TEXTURE, TEXTUREn, CONSTANT, PRIMARY_COLOR, PREVIOUS
//		* OPERANDx_ALPHA
//			+ SRC_ALPHA, ONE_MINUS_SRC_ALPHA

void set_combiner (GLenum combineModeRGB, GLenum operandColor, GLenum src0RGB, GLenum src1RGB,
				   GLenum combineModeALPHA, GLenum operandAlpha, GLenum src0ALPHA, GLenum src1ALPHA)
{
	glTexEnvi (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE);
	glTexEnvi (GL_TEXTURE_ENV, GL_COMBINE_RGB, combineModeRGB);
	glTexEnvi (GL_TEXTURE_ENV, GL_COMBINE_ALPHA, combineModeALPHA);
	glTexEnvi (GL_TEXTURE_ENV, GL_SRC0_RGB, src0RGB);
	glTexEnvi (GL_TEXTURE_ENV, GL_SRC1_RGB, src1RGB);
	glTexEnvi (GL_TEXTURE_ENV, GL_SRC0_ALPHA, src0ALPHA);
	glTexEnvi (GL_TEXTURE_ENV, GL_SRC1_ALPHA, src1ALPHA);
	glTexEnvi (GL_TEXTURE_ENV, GL_OPERAND0_RGB, operandColor);
	glTexEnvi (GL_TEXTURE_ENV, GL_OPERAND0_ALPHA, operandAlpha);	
}

void set_replace ()
{	
	glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
}
	
	
void disable_client_states ()
{
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}
#endif
	
}} // stir :: gles
