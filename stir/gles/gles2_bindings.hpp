#ifndef STIR_GLES2_2_0_INCLUDED
#define STIR_GLES2_2_0_INCLUDED

namespace stir { namespace gl {

enum class AttachmentPName : GLenum
{
	FRAMEBUFFER_ATTACHMENT_OBJECT_NAME = GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME,
	FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE = GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE,
	FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE = GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE,
	FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL = GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL,
};

enum class AttributeType : GLenum
{
	BOOL = GL_BOOL,
	BOOL_VEC2 = GL_BOOL_VEC2,
	BOOL_VEC3 = GL_BOOL_VEC3,
	BOOL_VEC4 = GL_BOOL_VEC4,
	FLOAT = GL_FLOAT,
	FLOAT_MAT2 = GL_FLOAT_MAT2,
	FLOAT_MAT3 = GL_FLOAT_MAT3,
	FLOAT_MAT4 = GL_FLOAT_MAT4,
	FLOAT_VEC2 = GL_FLOAT_VEC2,
	FLOAT_VEC3 = GL_FLOAT_VEC3,
	FLOAT_VEC4 = GL_FLOAT_VEC4,
	INT_VEC2 = GL_INT_VEC2,
	INT_VEC3 = GL_INT_VEC3,
	INT_VEC4 = GL_INT_VEC4,
	SAMPLER_2D = GL_SAMPLER_2D,
	SAMPLER_CUBE = GL_SAMPLER_CUBE,
};

enum class BlendEquationMode : GLenum
{
	FUNC_ADD = GL_FUNC_ADD,
	FUNC_REVERSE_SUBTRACT = GL_FUNC_REVERSE_SUBTRACT,
	FUNC_SUBTRACT = GL_FUNC_SUBTRACT,
};

enum class BlendFuncSeparateParameter : GLenum
{
	CONSTANT_ALPHA = GL_CONSTANT_ALPHA,
	CONSTANT_COLOR = GL_CONSTANT_COLOR,
	DST_ALPHA = GL_DST_ALPHA,
	DST_COLOR = GL_DST_COLOR,
	ONE = GL_ONE,
	ONE_MINUS_CONSTANT_ALPHA = GL_ONE_MINUS_CONSTANT_ALPHA,
	ONE_MINUS_CONSTANT_COLOR = GL_ONE_MINUS_CONSTANT_COLOR,
	ONE_MINUS_DST_ALPHA = GL_ONE_MINUS_DST_ALPHA,
	ONE_MINUS_DST_COLOR = GL_ONE_MINUS_DST_COLOR,
	ONE_MINUS_SRC_ALPHA = GL_ONE_MINUS_SRC_ALPHA,
	ONE_MINUS_SRC_COLOR = GL_ONE_MINUS_SRC_COLOR,
	SRC_ALPHA = GL_SRC_ALPHA,
	SRC_COLOR = GL_SRC_COLOR,
	ZERO = GL_ZERO,
};

enum class BlendingFactorDest : GLenum
{
	DST_ALPHA = GL_DST_ALPHA,
	ONE = GL_ONE,
	ONE_MINUS_DST_ALPHA = GL_ONE_MINUS_DST_ALPHA,
	ONE_MINUS_SRC_ALPHA = GL_ONE_MINUS_SRC_ALPHA,
	ONE_MINUS_SRC_COLOR = GL_ONE_MINUS_SRC_COLOR,
	SRC_ALPHA = GL_SRC_ALPHA,
	SRC_COLOR = GL_SRC_COLOR,
	ZERO = GL_ZERO,
};

enum class BlendingFactorSrc : GLenum
{
	DST_ALPHA = GL_DST_ALPHA,
	DST_COLOR = GL_DST_COLOR,
	ONE = GL_ONE,
	ONE_MINUS_DST_ALPHA = GL_ONE_MINUS_DST_ALPHA,
	ONE_MINUS_DST_COLOR = GL_ONE_MINUS_DST_COLOR,
	ONE_MINUS_SRC_ALPHA = GL_ONE_MINUS_SRC_ALPHA,
	SRC_ALPHA = GL_SRC_ALPHA,
	SRC_ALPHA_SATURATE = GL_SRC_ALPHA_SATURATE,
	ZERO = GL_ZERO,
};

typedef bool Boolean;
	
	//	inline operator GLboolean(bool b) { return b ? GL_TRUE : GL_FALSE; }

enum class BufferBinding : GLenum
{
	FRAMEBUFFER = GL_FRAMEBUFFER,
	RENDERBUFFER = GL_RENDERBUFFER,
};

enum class BufferTarget : GLenum
{
	ARRAY_BUFFER = GL_ARRAY_BUFFER,
	ELEMENT_ARRAY_BUFFER = GL_ELEMENT_ARRAY_BUFFER,
};

enum class BufferUsage : GLenum
{
	DYNAMIC_DRAW = GL_DYNAMIC_DRAW,
	STATIC_DRAW = GL_STATIC_DRAW,
	STREAM_DRAW = GL_STREAM_DRAW,
};

enum class ClearBufferMask : GLbitfield
{
	COLOR_BUFFER_BIT = GL_COLOR_BUFFER_BIT,
	DEPTH_BUFFER_BIT = GL_DEPTH_BUFFER_BIT,
	STENCIL_BUFFER_BIT = GL_STENCIL_BUFFER_BIT,
};
inline ClearBufferMask operator| (ClearBufferMask a, ClearBufferMask b) {
	return (ClearBufferMask) ((GLbitfield)a|(GLbitfield)b);
}

enum class CullFaceMode : GLenum
{
	BACK = GL_BACK,
	FRONT = GL_FRONT,
	FRONT_AND_BACK = GL_FRONT_AND_BACK,
};

enum class DepthFunction : GLenum
{
	ALWAYS = GL_ALWAYS,
	EQUAL = GL_EQUAL,
	GEQUAL = GL_GEQUAL,
	GREATER = GL_GREATER,
	LEQUAL = GL_LEQUAL,
	LESS = GL_LESS,
	NEVER = GL_NEVER,
	NOTEQUAL = GL_NOTEQUAL,
};

enum class DrawElementsType : GLenum
{
	UNSIGNED_BYTE = GL_UNSIGNED_BYTE,
	UNSIGNED_SHORT = GL_UNSIGNED_SHORT,
};

enum class EnableCap : GLenum
{
	BLEND = GL_BLEND,
	CULL_FACE = GL_CULL_FACE,
	DEPTH_TEST = GL_DEPTH_TEST,
	DITHER = GL_DITHER,
	POLYGON_OFFSET_FILL = GL_POLYGON_OFFSET_FILL,
	SCISSOR_TEST = GL_SCISSOR_TEST,
	STENCIL_TEST = GL_STENCIL_TEST,
	TEXTURE_2D = GL_TEXTURE_2D,
};

enum class FramebufferAttachment : GLenum
{
	COLOR_ATTACHMENT0 = GL_COLOR_ATTACHMENT0,
	DEPTH_ATTACHMENT = GL_DEPTH_ATTACHMENT,
	STENCIL_ATTACHMENT = GL_STENCIL_ATTACHMENT,
};

enum class FramebufferTarget : GLenum
{
	FRAMEBUFFER = GL_FRAMEBUFFER,
};

enum class FrontFaceDirection : GLenum
{
	CCW = GL_CCW,
	CW = GL_CW,
};

enum class GetPName : GLenum
{
	ALIASED_LINE_WIDTH_RANGE = GL_ALIASED_LINE_WIDTH_RANGE,
	ALIASED_POINT_SIZE_RANGE = GL_ALIASED_POINT_SIZE_RANGE,
	ALPHA_BITS = GL_ALPHA_BITS,
	BLEND = GL_BLEND,
	BLUE_BITS = GL_BLUE_BITS,
	COLOR_CLEAR_VALUE = GL_COLOR_CLEAR_VALUE,
	COLOR_WRITEMASK = GL_COLOR_WRITEMASK,
	CULL_FACE = GL_CULL_FACE,
	CULL_FACE_MODE = GL_CULL_FACE_MODE,
	DEPTH_BITS = GL_DEPTH_BITS,
	DEPTH_CLEAR_VALUE = GL_DEPTH_CLEAR_VALUE,
	DEPTH_FUNC = GL_DEPTH_FUNC,
	DEPTH_RANGE = GL_DEPTH_RANGE,
	DEPTH_TEST = GL_DEPTH_TEST,
	DEPTH_WRITEMASK = GL_DEPTH_WRITEMASK,
	DITHER = GL_DITHER,
	FRONT_FACE = GL_FRONT_FACE,
	GREEN_BITS = GL_GREEN_BITS,
	LINE_WIDTH = GL_LINE_WIDTH,
	MAX_TEXTURE_SIZE = GL_MAX_TEXTURE_SIZE,
	MAX_VIEWPORT_DIMS = GL_MAX_VIEWPORT_DIMS,
	PACK_ALIGNMENT = GL_PACK_ALIGNMENT,
	POLYGON_OFFSET_FACTOR = GL_POLYGON_OFFSET_FACTOR,
	POLYGON_OFFSET_FILL = GL_POLYGON_OFFSET_FILL,
	POLYGON_OFFSET_UNITS = GL_POLYGON_OFFSET_UNITS,
	RED_BITS = GL_RED_BITS,
	SCISSOR_BOX = GL_SCISSOR_BOX,
	SCISSOR_TEST = GL_SCISSOR_TEST,
	STENCIL_BITS = GL_STENCIL_BITS,
	STENCIL_CLEAR_VALUE = GL_STENCIL_CLEAR_VALUE,
	STENCIL_FAIL = GL_STENCIL_FAIL,
	STENCIL_FUNC = GL_STENCIL_FUNC,
	STENCIL_PASS_DEPTH_FAIL = GL_STENCIL_PASS_DEPTH_FAIL,
	STENCIL_PASS_DEPTH_PASS = GL_STENCIL_PASS_DEPTH_PASS,
	STENCIL_REF = GL_STENCIL_REF,
	STENCIL_TEST = GL_STENCIL_TEST,
	STENCIL_VALUE_MASK = GL_STENCIL_VALUE_MASK,
	STENCIL_WRITEMASK = GL_STENCIL_WRITEMASK,
	SUBPIXEL_BITS = GL_SUBPIXEL_BITS,
	TEXTURE_2D = GL_TEXTURE_2D,
	TEXTURE_BINDING_2D = GL_TEXTURE_BINDING_2D,
	UNPACK_ALIGNMENT = GL_UNPACK_ALIGNMENT,
	VIEWPORT = GL_VIEWPORT,
};

enum class GetShaderPName : GLenum
{
	COMPILE_STATUS = GL_COMPILE_STATUS,
	DELETE_STATUS = GL_DELETE_STATUS,
	INFO_LOG_LENGTH = GL_INFO_LOG_LENGTH,
	SHADER_SOURCE_LENGTH = GL_SHADER_SOURCE_LENGTH,
	SHADER_TYPE = GL_SHADER_TYPE,
};

enum class GetTextureParameter : GLenum
{
	TEXTURE_MAG_FILTER = GL_TEXTURE_MAG_FILTER,
	TEXTURE_MIN_FILTER = GL_TEXTURE_MIN_FILTER,
	TEXTURE_WRAP_S = GL_TEXTURE_WRAP_S,
	TEXTURE_WRAP_T = GL_TEXTURE_WRAP_T,
};

enum class HintMode : GLenum
{
	DONT_CARE = GL_DONT_CARE,
	FASTEST = GL_FASTEST,
	NICEST = GL_NICEST,
};

enum class HintTarget : GLenum
{
	GENERATE_MIPMAP_HINT = GL_GENERATE_MIPMAP_HINT,
};

enum class InternalFormat : GLenum
{
	DEPTH_COMPONENT16 = GL_DEPTH_COMPONENT16,
	RGB565 = GL_RGB565,
	RGB5_A1 = GL_RGB5_A1,
	RGBA4 = GL_RGBA4,
	STENCIL_INDEX8 = GL_STENCIL_INDEX8,
};

enum class PixelFormat : GLenum
{
	ALPHA = GL_ALPHA,
	DEPTH_COMPONENT = GL_DEPTH_COMPONENT,
	LUMINANCE = GL_LUMINANCE,
	LUMINANCE_ALPHA = GL_LUMINANCE_ALPHA,
	RGB = GL_RGB,
	RGBA = GL_RGBA,
	UNSIGNED_INT = GL_UNSIGNED_INT,
	UNSIGNED_SHORT = GL_UNSIGNED_SHORT,
};

enum class PixelStoreParameter : GLenum
{
	PACK_ALIGNMENT = GL_PACK_ALIGNMENT,
	UNPACK_ALIGNMENT = GL_UNPACK_ALIGNMENT,
};

enum class PixelType : GLenum
{
	BYTE = GL_BYTE,
	FLOAT = GL_FLOAT,
	INT = GL_INT,
	SHORT = GL_SHORT,
	UNSIGNED_BYTE = GL_UNSIGNED_BYTE,
	UNSIGNED_INT = GL_UNSIGNED_INT,
	UNSIGNED_SHORT = GL_UNSIGNED_SHORT,
	UNSIGNED_SHORT_4_4_4_4 = GL_UNSIGNED_SHORT_4_4_4_4,
	UNSIGNED_SHORT_5_5_5_1 = GL_UNSIGNED_SHORT_5_5_5_1,
};

enum class PrimitiveType : GLenum
{
	LINES = GL_LINES,
	LINE_LOOP = GL_LINE_LOOP,
	LINE_STRIP = GL_LINE_STRIP,
	POINTS = GL_POINTS,
	TRIANGLES = GL_TRIANGLES,
	TRIANGLE_FAN = GL_TRIANGLE_FAN,
	TRIANGLE_STRIP = GL_TRIANGLE_STRIP,
};

enum class RenderbufferPName : GLenum
{
	RENDERBUFFER_ALPHA_SIZE = GL_RENDERBUFFER_ALPHA_SIZE,
	RENDERBUFFER_BLUE_SIZE = GL_RENDERBUFFER_BLUE_SIZE,
	RENDERBUFFER_DEPTH_SIZE = GL_RENDERBUFFER_DEPTH_SIZE,
	RENDERBUFFER_GREEN_SIZE = GL_RENDERBUFFER_GREEN_SIZE,
	RENDERBUFFER_HEIGHT = GL_RENDERBUFFER_HEIGHT,
	RENDERBUFFER_INTERNAL_FORMAT = GL_RENDERBUFFER_INTERNAL_FORMAT,
	RENDERBUFFER_RED_SIZE = GL_RENDERBUFFER_RED_SIZE,
	RENDERBUFFER_STENCIL_SIZE = GL_RENDERBUFFER_STENCIL_SIZE,
	RENDERBUFFER_WIDTH = GL_RENDERBUFFER_WIDTH,
};

enum class RenderbufferTarget : GLenum
{
	RENDERBUFFER = GL_RENDERBUFFER,
};

enum class ShaderType : GLenum
{
	FRAGMENT_SHADER = GL_FRAGMENT_SHADER,
	VERTEX_SHADER = GL_VERTEX_SHADER,
};

enum class StencilFaceDirection : GLenum
{
	BACK = GL_BACK,
	FRONT = GL_FRONT,
};

enum class StencilFunction : GLenum
{
	ALWAYS = GL_ALWAYS,
	EQUAL = GL_EQUAL,
	GEQUAL = GL_GEQUAL,
	GREATER = GL_GREATER,
	LEQUAL = GL_LEQUAL,
	LESS = GL_LESS,
	NEVER = GL_NEVER,
	NOTEQUAL = GL_NOTEQUAL,
};

enum class StencilOper : GLenum
{
	DECR = GL_DECR,
	INCR = GL_INCR,
	INVERT = GL_INVERT,
	KEEP = GL_KEEP,
	REPLACE = GL_REPLACE,
	ZERO = GL_ZERO,
};

enum class StringName : GLenum
{
	EXTENSIONS = GL_EXTENSIONS,
	RENDERER = GL_RENDERER,
	VENDOR = GL_VENDOR,
	VERSION = GL_VERSION,
};

enum class TextureParameterName : GLenum
{
	TEXTURE_MAG_FILTER = GL_TEXTURE_MAG_FILTER,
	TEXTURE_MIN_FILTER = GL_TEXTURE_MIN_FILTER,
	TEXTURE_WRAP_S = GL_TEXTURE_WRAP_S,
	TEXTURE_WRAP_T = GL_TEXTURE_WRAP_T,
};

enum class TextureTarget : GLenum
{
	TEXTURE_2D = GL_TEXTURE_2D,
	TEXTURE_CUBE_MAP = GL_TEXTURE_CUBE_MAP,
	TEXTURE_CUBE_MAP_NEGATIVE_X = GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
	TEXTURE_CUBE_MAP_NEGATIVE_Y = GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
	TEXTURE_CUBE_MAP_NEGATIVE_Z = GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
	TEXTURE_CUBE_MAP_POSITIVE_X = GL_TEXTURE_CUBE_MAP_POSITIVE_X,
	TEXTURE_CUBE_MAP_POSITIVE_Y = GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
	TEXTURE_CUBE_MAP_POSITIVE_Z = GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
};

enum class TextureUnit : GLenum
{
	TEXTURE0 = GL_TEXTURE0,
	TEXTURE1 = GL_TEXTURE1,
	TEXTURE10 = GL_TEXTURE10,
	TEXTURE11 = GL_TEXTURE11,
	TEXTURE12 = GL_TEXTURE12,
	TEXTURE13 = GL_TEXTURE13,
	TEXTURE14 = GL_TEXTURE14,
	TEXTURE15 = GL_TEXTURE15,
	TEXTURE16 = GL_TEXTURE16,
	TEXTURE17 = GL_TEXTURE17,
	TEXTURE18 = GL_TEXTURE18,
	TEXTURE19 = GL_TEXTURE19,
	TEXTURE2 = GL_TEXTURE2,
	TEXTURE20 = GL_TEXTURE20,
	TEXTURE21 = GL_TEXTURE21,
	TEXTURE22 = GL_TEXTURE22,
	TEXTURE23 = GL_TEXTURE23,
	TEXTURE24 = GL_TEXTURE24,
	TEXTURE25 = GL_TEXTURE25,
	TEXTURE26 = GL_TEXTURE26,
	TEXTURE27 = GL_TEXTURE27,
	TEXTURE28 = GL_TEXTURE28,
	TEXTURE29 = GL_TEXTURE29,
	TEXTURE3 = GL_TEXTURE3,
	TEXTURE30 = GL_TEXTURE30,
	TEXTURE31 = GL_TEXTURE31,
	TEXTURE4 = GL_TEXTURE4,
	TEXTURE5 = GL_TEXTURE5,
	TEXTURE6 = GL_TEXTURE6,
	TEXTURE7 = GL_TEXTURE7,
	TEXTURE8 = GL_TEXTURE8,
	TEXTURE9 = GL_TEXTURE9,
};

enum class VertexAttribPointerType : GLenum
{
	BYTE = GL_BYTE,
	FIXED = GL_FIXED,
	FLOAT = GL_FLOAT,
	SHORT = GL_SHORT,
	UNSIGNED_BYTE = GL_UNSIGNED_BYTE,
	UNSIGNED_SHORT = GL_UNSIGNED_SHORT,
};

inline void ActiveTexture(TextureUnit texture)
{
	glActiveTexture((GLenum)texture);
	STIR_GLRESULT();
}
inline void AttachShader(GLuint program, GLuint shader)
{
	glAttachShader(program, shader);
	STIR_GLRESULT();
}
inline void BindAttribLocation(GLuint program, GLuint index, const GLchar * name)
{
	glBindAttribLocation(program, index, name);
	STIR_GLRESULT();
}
inline void BindBuffer(BufferTarget target, GLuint buffer)
{
	glBindBuffer((GLenum)target, buffer);
	STIR_GLRESULT();
}
inline void BindFramebuffer(BufferBinding target, GLuint framebuffer)
{
	glBindFramebuffer((GLenum)target, framebuffer);
	STIR_GLRESULT();
}
inline void BindRenderbuffer(RenderbufferTarget target, GLuint renderbuffer)
{
	glBindRenderbuffer((GLenum)target, renderbuffer);
	STIR_GLRESULT();
}
inline void BindTexture(TextureTarget target, GLuint texture)
{
	glBindTexture((GLenum)target, texture);
	STIR_GLRESULT();
}
inline void BlendColor(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha)
{
	glBlendColor(red, green, blue, alpha);
	STIR_GLRESULT();
}
inline void BlendEquation(BlendEquationMode mode)
{
	glBlendEquation((GLenum)mode);
	STIR_GLRESULT();
}
inline void BlendEquationSeparate(BlendEquationMode modeRGB, BlendEquationMode modeAlpha)
{
	glBlendEquationSeparate((GLenum)modeRGB, (GLenum)modeAlpha);
	STIR_GLRESULT();
}
inline void BlendFunc(BlendingFactorSrc sfactor, BlendingFactorDest dfactor)
{
	glBlendFunc((GLenum)sfactor, (GLenum)dfactor);
	STIR_GLRESULT();
}
inline void BlendFuncSeparate(BlendFuncSeparateParameter sfactorRGB, BlendFuncSeparateParameter dfactorRGB, BlendFuncSeparateParameter sfactorAlpha, BlendFuncSeparateParameter dfactorAlpha)
{
	glBlendFuncSeparate((GLenum)sfactorRGB, (GLenum)dfactorRGB, (GLenum)sfactorAlpha, (GLenum)dfactorAlpha);
	STIR_GLRESULT();
}
inline void BufferData(BufferTarget target, GLsizeiptr size, const void * data, BufferUsage usage)
{
	glBufferData((GLenum)target, size, data, (GLenum)usage);
	STIR_GLRESULT();
}
inline void BufferSubData(BufferTarget target, GLintptr offset, GLsizeiptr size, const void * data)
{
	glBufferSubData((GLenum)target, offset, size, data);
	STIR_GLRESULT();
}
inline GLenum CheckFramebufferStatus(FramebufferTarget target)
{
	GLenum result = glCheckFramebufferStatus((GLenum)target);
	STIR_GLRESULT();
	return result;
}
inline void Clear(ClearBufferMask mask)
{
	glClear((GLbitfield)mask);
	STIR_GLRESULT();
}
inline void ClearColor(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha)
{
	glClearColor(red, green, blue, alpha);
	STIR_GLRESULT();
}
inline void ClearDepthf(GLfloat d)
{
	glClearDepthf(d);
	STIR_GLRESULT();
}
inline void ClearStencil(GLint s)
{
	glClearStencil(s);
	STIR_GLRESULT();
}
inline void ColorMask(GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha)
{
	glColorMask(red, green, blue, alpha);
	STIR_GLRESULT();
}
inline void CompileShader(GLuint shader)
{
	glCompileShader(shader);
	STIR_GLRESULT();
}
inline void CompressedTexImage2D(TextureTarget target, GLint level, InternalFormat internalformat, GLsizei width, GLsizei height, GLint border, GLsizei imageSize, const void * data)
{
	glCompressedTexImage2D((GLenum)target, level, (GLenum)internalformat, width, height, border, imageSize, data);
	STIR_GLRESULT();
}
inline void CompressedTexSubImage2D(TextureTarget target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, PixelFormat format, GLsizei imageSize, const void * data)
{
	glCompressedTexSubImage2D((GLenum)target, level, xoffset, yoffset, width, height, (GLenum)format, imageSize, data);
	STIR_GLRESULT();
}
inline void CopyTexImage2D(TextureTarget target, GLint level, InternalFormat internalformat, GLint x, GLint y, GLsizei width, GLsizei height, GLint border)
{
	glCopyTexImage2D((GLenum)target, level, (GLenum)internalformat, x, y, width, height, border);
	STIR_GLRESULT();
}
inline void CopyTexSubImage2D(TextureTarget target, GLint level, GLint xoffset, GLint yoffset, GLint x, GLint y, GLsizei width, GLsizei height)
{
	glCopyTexSubImage2D((GLenum)target, level, xoffset, yoffset, x, y, width, height);
	STIR_GLRESULT();
}
inline GLuint CreateProgram()
{
	GLuint result = glCreateProgram();
	STIR_GLRESULT();
	return result;
}
inline GLuint CreateShader(ShaderType type)
{
	GLuint result = glCreateShader((GLenum)type);
	STIR_GLRESULT();
	return result;
}
inline void CullFace(CullFaceMode mode)
{
	glCullFace((GLenum)mode);
	STIR_GLRESULT();
}
inline void DeleteBuffers(GLsizei n, const GLuint * buffers)
{
	glDeleteBuffers(n, buffers);
	STIR_GLRESULT();
}
inline void DeleteFramebuffers(GLsizei n, const GLuint * framebuffers)
{
	glDeleteFramebuffers(n, framebuffers);
	STIR_GLRESULT();
}
inline void DeleteProgram(GLuint program)
{
	glDeleteProgram(program);
	STIR_GLRESULT();
}
inline void DeleteRenderbuffers(GLsizei n, const GLuint * renderbuffers)
{
	glDeleteRenderbuffers(n, renderbuffers);
	STIR_GLRESULT();
}
inline void DeleteShader(GLuint shader)
{
	glDeleteShader(shader);
	STIR_GLRESULT();
}
inline void DeleteTextures(GLsizei n, const GLuint * textures)
{
	glDeleteTextures(n, textures);
	STIR_GLRESULT();
}
inline void DepthFunc(DepthFunction func)
{
	glDepthFunc((GLenum)func);
	STIR_GLRESULT();
}
inline void DepthMask(GLboolean flag)
{
	glDepthMask(flag);
	STIR_GLRESULT();
}
inline void DepthRangef(GLfloat n, GLfloat f)
{
	glDepthRangef(n, f);
	STIR_GLRESULT();
}
inline void DetachShader(GLuint program, GLuint shader)
{
	glDetachShader(program, shader);
	STIR_GLRESULT();
}
inline void Disable(EnableCap cap)
{
	glDisable((GLenum)cap);
	STIR_GLRESULT();
}
inline void DisableVertexAttribArray(GLuint index)
{
	glDisableVertexAttribArray(index);
	STIR_GLRESULT();
}
inline void DrawArrays(PrimitiveType mode, GLint first, GLsizei count)
{
	glDrawArrays((GLenum)mode, first, count);
	STIR_GLRESULT();
}
inline void DrawElements(PrimitiveType mode, GLsizei count, DrawElementsType type, const void * indices)
{
	glDrawElements((GLenum)mode, count, (GLenum)type, indices);
	STIR_GLRESULT();
}
inline void Enable(EnableCap cap)
{
	glEnable((GLenum)cap);
	STIR_GLRESULT();
}
inline void EnableVertexAttribArray(GLuint index)
{
	glEnableVertexAttribArray(index);
	STIR_GLRESULT();
}
inline void Finish()
{
	glFinish();
	STIR_GLRESULT();
}
inline void Flush()
{
	glFlush();
	STIR_GLRESULT();
}
inline void FramebufferRenderbuffer(FramebufferTarget target, FramebufferAttachment attachment, RenderbufferTarget renderbuffertarget, GLuint renderbuffer)
{
	glFramebufferRenderbuffer((GLenum)target, (GLenum)attachment, (GLenum)renderbuffertarget, renderbuffer);
	STIR_GLRESULT();
}
inline void FramebufferTexture2D(FramebufferTarget target, FramebufferAttachment attachment, GLenum textarget, GLuint texture, GLint level)
{
	glFramebufferTexture2D((GLenum)target, (GLenum)attachment, textarget, texture, level);
	STIR_GLRESULT();
}
inline void FrontFace(FrontFaceDirection mode)
{
	glFrontFace((GLenum)mode);
	STIR_GLRESULT();
}
inline void GenBuffers(GLsizei n, GLuint * buffers)
{
	glGenBuffers(n, buffers);
	STIR_GLRESULT();
}
inline void GenFramebuffers(GLsizei n, GLuint * framebuffers)
{
	glGenFramebuffers(n, framebuffers);
	STIR_GLRESULT();
}
inline void GenRenderbuffers(GLsizei n, GLuint * renderbuffers)
{
	glGenRenderbuffers(n, renderbuffers);
	STIR_GLRESULT();
}
inline void GenTextures(GLsizei n, GLuint * textures)
{
	glGenTextures(n, textures);
	STIR_GLRESULT();
}
inline void GenerateMipmap(GLenum target)
{
	glGenerateMipmap(target);
	STIR_GLRESULT();
}
inline void GetActiveAttrib(GLuint program, GLuint index, GLsizei bufSize, GLsizei * length, GLint * size, AttributeType type, GLchar * name)
{
	glGetActiveAttrib(program, index, bufSize, length, size, (GLenum *)type, name);
	STIR_GLRESULT();
}
inline void GetActiveUniform(GLuint program, GLuint index, GLsizei bufSize, GLsizei * length, GLint * size, GLenum * type, GLchar * name)
{
	glGetActiveUniform(program, index, bufSize, length, size, type, name);
	STIR_GLRESULT();
}
inline void GetAttachedShaders(GLuint program, GLsizei maxCount, GLsizei * count, GLuint * shaders)
{
	glGetAttachedShaders(program, maxCount, count, shaders);
	STIR_GLRESULT();
}
inline GLint GetAttribLocation(GLuint program, const GLchar * name)
{
	GLint result = glGetAttribLocation(program, name);
	STIR_GLRESULT();
	return result;
}
inline void GetBooleanv(GetPName pname, GLboolean * data)
{
	glGetBooleanv((GLenum)pname, data);
	STIR_GLRESULT();
}
inline void GetBufferParameteriv(BufferTarget target, GLenum pname, GLint * params)
{
	glGetBufferParameteriv((GLenum)target, pname, params);
	STIR_GLRESULT();
}
inline GLenum GetError()
{
	GLenum result = glGetError();
	STIR_GLRESULT();
	return result;
}
inline void GetFloatv(GetPName pname, GLfloat * data)
{
	glGetFloatv((GLenum)pname, data);
	STIR_GLRESULT();
}
inline void GetFramebufferAttachmentParameteriv(FramebufferTarget target, FramebufferAttachment attachment, AttachmentPName pname, GLint * params)
{
	glGetFramebufferAttachmentParameteriv((GLenum)target, (GLenum)attachment, (GLenum)pname, params);
	STIR_GLRESULT();
}
inline void GetIntegerv(GetPName pname, GLint * data)
{
	glGetIntegerv((GLenum)pname, data);
	STIR_GLRESULT();
}
inline void GetProgramInfoLog(GLuint program, GLsizei bufSize, GLsizei * length, GLchar * infoLog)
{
	glGetProgramInfoLog(program, bufSize, length, infoLog);
	STIR_GLRESULT();
}
inline void GetProgramiv(GLuint program, GLenum pname, GLint * params)
{
	glGetProgramiv(program, pname, params);
	STIR_GLRESULT();
}
inline void GetRenderbufferParameteriv(RenderbufferTarget target, RenderbufferPName pname, GLint * params)
{
	glGetRenderbufferParameteriv((GLenum)target, (GLenum)pname, params);
	STIR_GLRESULT();
}
inline void GetShaderInfoLog(GLuint shader, GLsizei bufSize, GLsizei * length, GLchar * infoLog)
{
	glGetShaderInfoLog(shader, bufSize, length, infoLog);
	STIR_GLRESULT();
}
inline void GetShaderPrecisionFormat(GLenum shadertype, GLenum precisiontype, GLint * range, GLint * precision)
{
	glGetShaderPrecisionFormat(shadertype, precisiontype, range, precision);
	STIR_GLRESULT();
}
inline void GetShaderSource(GLuint shader, GLsizei bufSize, GLsizei * length, GLchar * source)
{
	glGetShaderSource(shader, bufSize, length, source);
	STIR_GLRESULT();
}
inline void GetShaderiv(GLuint shader, GetShaderPName pname, GLint * params)
{
	glGetShaderiv(shader, (GLenum)pname, params);
	STIR_GLRESULT();
}
inline const GLubyte * GetString(StringName name)
{
	const GLubyte * result = glGetString((GLenum)name);
	STIR_GLRESULT();
	return result;
}
inline void GetTexParameterfv(TextureTarget target, GetTextureParameter pname, GLfloat * params)
{
	glGetTexParameterfv((GLenum)target, (GLenum)pname, params);
	STIR_GLRESULT();
}
inline void GetTexParameteriv(TextureTarget target, GetTextureParameter pname, GLint * params)
{
	glGetTexParameteriv((GLenum)target, (GLenum)pname, params);
	STIR_GLRESULT();
}
inline GLint GetUniformLocation(GLuint program, const GLchar * name)
{
	GLint result = glGetUniformLocation(program, name);
	STIR_GLRESULT();
	return result;
}
inline void GetUniformfv(GLuint program, GLint location, GLfloat * params)
{
	glGetUniformfv(program, location, params);
	STIR_GLRESULT();
}
inline void GetUniformiv(GLuint program, GLint location, GLint * params)
{
	glGetUniformiv(program, location, params);
	STIR_GLRESULT();
}
inline void GetVertexAttribPointerv(GLuint index, GLenum pname, void ** pointer)
{
	glGetVertexAttribPointerv(index, pname, pointer);
	STIR_GLRESULT();
}
inline void GetVertexAttribfv(GLuint index, GLenum pname, GLfloat * params)
{
	glGetVertexAttribfv(index, pname, params);
	STIR_GLRESULT();
}
inline void GetVertexAttribiv(GLuint index, GLenum pname, GLint * params)
{
	glGetVertexAttribiv(index, pname, params);
	STIR_GLRESULT();
}
inline void Hint(HintTarget target, HintMode mode)
{
	glHint((GLenum)target, (GLenum)mode);
	STIR_GLRESULT();
}
inline GLboolean IsBuffer(GLuint buffer)
{
	GLboolean result = glIsBuffer(buffer);
	STIR_GLRESULT();
	return result;
}
inline GLboolean IsEnabled(EnableCap cap)
{
	GLboolean result = glIsEnabled((GLenum)cap);
	STIR_GLRESULT();
	return result;
}
inline GLboolean IsFramebuffer(GLuint framebuffer)
{
	GLboolean result = glIsFramebuffer(framebuffer);
	STIR_GLRESULT();
	return result;
}
inline GLboolean IsProgram(GLuint program)
{
	GLboolean result = glIsProgram(program);
	STIR_GLRESULT();
	return result;
}
inline GLboolean IsRenderbuffer(GLuint renderbuffer)
{
	GLboolean result = glIsRenderbuffer(renderbuffer);
	STIR_GLRESULT();
	return result;
}
inline GLboolean IsShader(GLuint shader)
{
	GLboolean result = glIsShader(shader);
	STIR_GLRESULT();
	return result;
}
inline GLboolean IsTexture(GLuint texture)
{
	GLboolean result = glIsTexture(texture);
	STIR_GLRESULT();
	return result;
}
inline void LineWidth(GLfloat width)
{
	glLineWidth(width);
	STIR_GLRESULT();
}
inline void LinkProgram(GLuint program)
{
	glLinkProgram(program);
	STIR_GLRESULT();
}
inline void PixelStorei(PixelStoreParameter pname, GLint param)
{
	glPixelStorei((GLenum)pname, param);
	STIR_GLRESULT();
}
inline void PolygonOffset(GLfloat factor, GLfloat units)
{
	glPolygonOffset(factor, units);
	STIR_GLRESULT();
}
inline void ReadPixels(GLint x, GLint y, GLsizei width, GLsizei height, PixelFormat format, PixelType type, void * pixels)
{
	glReadPixels(x, y, width, height, (GLenum)format, (GLenum)type, pixels);
	STIR_GLRESULT();
}
inline void ReleaseShaderCompiler()
{
	glReleaseShaderCompiler();
	STIR_GLRESULT();
}
inline void RenderbufferStorage(RenderbufferTarget target, InternalFormat internalformat, GLsizei width, GLsizei height)
{
	glRenderbufferStorage((GLenum)target, (GLenum)internalformat, width, height);
	STIR_GLRESULT();
}
inline void SampleCoverage(GLfloat value, GLboolean invert)
{
	glSampleCoverage(value, invert);
	STIR_GLRESULT();
}
inline void Scissor(GLint x, GLint y, GLsizei width, GLsizei height)
{
	glScissor(x, y, width, height);
	STIR_GLRESULT();
}
inline void ShaderBinary(GLsizei count, const GLuint * shaders, GLenum binaryformat, const void * binary, GLsizei length)
{
	glShaderBinary(count, shaders, binaryformat, binary, length);
	STIR_GLRESULT();
}
inline void ShaderSource(GLuint shader, GLsizei count, const GLchar *const* string, const GLint * length)
{
	glShaderSource(shader, count, string, length);
	STIR_GLRESULT();
}
inline void StencilFunc(StencilFunction func, GLint ref, GLuint mask)
{
	glStencilFunc((GLenum)func, ref, mask);
	STIR_GLRESULT();
}
inline void StencilFuncSeparate(StencilFaceDirection face, StencilFunction func, GLint ref, GLuint mask)
{
	glStencilFuncSeparate((GLenum)face, (GLenum)func, ref, mask);
	STIR_GLRESULT();
}
inline void StencilMask(GLuint mask)
{
	glStencilMask(mask);
	STIR_GLRESULT();
}
inline void StencilMaskSeparate(StencilFaceDirection face, GLuint mask)
{
	glStencilMaskSeparate((GLenum)face, mask);
	STIR_GLRESULT();
}
inline void StencilOp(StencilOper fail, StencilOper zfail, StencilOper zpass)
{
	glStencilOp((GLenum)fail, (GLenum)zfail, (GLenum)zpass);
	STIR_GLRESULT();
}
inline void StencilOpSeparate(StencilFaceDirection face, StencilOper sfail, StencilOper dpfail, StencilOper dppass)
{
	glStencilOpSeparate((GLenum)face, (GLenum)sfail, (GLenum)dpfail, (GLenum)dppass);
	STIR_GLRESULT();
}
inline void TexImage2D(TextureTarget target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, PixelFormat format, PixelType type, const void * pixels)
{
	glTexImage2D((GLenum)target, level, internalformat, width, height, border, (GLenum)format, (GLenum)type, pixels);
	STIR_GLRESULT();
}
inline void TexParameterf(TextureTarget target, TextureParameterName pname, GLfloat param)
{
	glTexParameterf((GLenum)target, (GLenum)pname, param);
	STIR_GLRESULT();
}
inline void TexParameterfv(TextureTarget target, TextureParameterName pname, const GLfloat * params)
{
	glTexParameterfv((GLenum)target, (GLenum)pname, params);
	STIR_GLRESULT();
}
inline void TexParameteri(TextureTarget target, TextureParameterName pname, GLint param)
{
	glTexParameteri((GLenum)target, (GLenum)pname, param);
	STIR_GLRESULT();
}
inline void TexParameteriv(TextureTarget target, TextureParameterName pname, const GLint * params)
{
	glTexParameteriv((GLenum)target, (GLenum)pname, params);
	STIR_GLRESULT();
}
inline void TexSubImage2D(TextureTarget target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, PixelFormat format, PixelType type, const void * pixels)
{
	glTexSubImage2D((GLenum)target, level, xoffset, yoffset, width, height, (GLenum)format, (GLenum)type, pixels);
	STIR_GLRESULT();
}
inline void Uniform1f(GLint location, GLfloat v0)
{
	glUniform1f(location, v0);
	STIR_GLRESULT();
}
inline void Uniform1fv(GLint location, GLsizei count, const GLfloat * value)
{
	glUniform1fv(location, count, value);
	STIR_GLRESULT();
}
inline void Uniform1i(GLint location, GLint v0)
{
	glUniform1i(location, v0);
	STIR_GLRESULT();
}
inline void Uniform1iv(GLint location, GLsizei count, const GLint * value)
{
	glUniform1iv(location, count, value);
	STIR_GLRESULT();
}
inline void Uniform2f(GLint location, GLfloat v0, GLfloat v1)
{
	glUniform2f(location, v0, v1);
	STIR_GLRESULT();
}
inline void Uniform2fv(GLint location, GLsizei count, const GLfloat * value)
{
	glUniform2fv(location, count, value);
	STIR_GLRESULT();
}
inline void Uniform2i(GLint location, GLint v0, GLint v1)
{
	glUniform2i(location, v0, v1);
	STIR_GLRESULT();
}
inline void Uniform2iv(GLint location, GLsizei count, const GLint * value)
{
	glUniform2iv(location, count, value);
	STIR_GLRESULT();
}
inline void Uniform3f(GLint location, GLfloat v0, GLfloat v1, GLfloat v2)
{
	glUniform3f(location, v0, v1, v2);
	STIR_GLRESULT();
}
inline void Uniform3fv(GLint location, GLsizei count, const GLfloat * value)
{
	glUniform3fv(location, count, value);
	STIR_GLRESULT();
}
inline void Uniform3i(GLint location, GLint v0, GLint v1, GLint v2)
{
	glUniform3i(location, v0, v1, v2);
	STIR_GLRESULT();
}
inline void Uniform3iv(GLint location, GLsizei count, const GLint * value)
{
	glUniform3iv(location, count, value);
	STIR_GLRESULT();
}
inline void Uniform4f(GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3)
{
	glUniform4f(location, v0, v1, v2, v3);
	STIR_GLRESULT();
}
inline void Uniform4fv(GLint location, GLsizei count, const GLfloat * value)
{
	glUniform4fv(location, count, value);
	STIR_GLRESULT();
}
inline void Uniform4i(GLint location, GLint v0, GLint v1, GLint v2, GLint v3)
{
	glUniform4i(location, v0, v1, v2, v3);
	STIR_GLRESULT();
}
inline void Uniform4iv(GLint location, GLsizei count, const GLint * value)
{
	glUniform4iv(location, count, value);
	STIR_GLRESULT();
}
inline void UniformMatrix2fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat * value)
{
	glUniformMatrix2fv(location, count, transpose, value);
	STIR_GLRESULT();
}
inline void UniformMatrix3fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat * value)
{
	glUniformMatrix3fv(location, count, transpose, value);
	STIR_GLRESULT();
}
inline void UniformMatrix4fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat * value)
{
	glUniformMatrix4fv(location, count, transpose, value);
	STIR_GLRESULT();
}
inline void UseProgram(GLuint program)
{
	glUseProgram(program);
	STIR_GLRESULT();
}
inline void ValidateProgram(GLuint program)
{
	glValidateProgram(program);
	STIR_GLRESULT();
}
inline void VertexAttrib1f(GLuint index, GLfloat x)
{
	glVertexAttrib1f(index, x);
	STIR_GLRESULT();
}
inline void VertexAttrib1fv(GLuint index, const GLfloat * v)
{
	glVertexAttrib1fv(index, v);
	STIR_GLRESULT();
}
inline void VertexAttrib2f(GLuint index, GLfloat x, GLfloat y)
{
	glVertexAttrib2f(index, x, y);
	STIR_GLRESULT();
}
inline void VertexAttrib2fv(GLuint index, const GLfloat * v)
{
	glVertexAttrib2fv(index, v);
	STIR_GLRESULT();
}
inline void VertexAttrib3f(GLuint index, GLfloat x, GLfloat y, GLfloat z)
{
	glVertexAttrib3f(index, x, y, z);
	STIR_GLRESULT();
}
inline void VertexAttrib3fv(GLuint index, const GLfloat * v)
{
	glVertexAttrib3fv(index, v);
	STIR_GLRESULT();
}
inline void VertexAttrib4f(GLuint index, GLfloat x, GLfloat y, GLfloat z, GLfloat w)
{
	glVertexAttrib4f(index, x, y, z, w);
	STIR_GLRESULT();
}
inline void VertexAttrib4fv(GLuint index, const GLfloat * v)
{
	glVertexAttrib4fv(index, v);
	STIR_GLRESULT();
}
inline void VertexAttribPointer(GLuint index, GLint size, VertexAttribPointerType type, GLboolean normalized, GLsizei stride, const void * pointer)
{
	glVertexAttribPointer(index, size, (GLenum)type, normalized, stride, pointer);
	STIR_GLRESULT();
}
inline void Viewport(GLint x, GLint y, GLsizei width, GLsizei height)
{
	glViewport(x, y, width, height);
	STIR_GLRESULT();
}
// Constants that were not emitted into groups: (93)
static const int ACTIVE_ATTRIBUTES = GL_ACTIVE_ATTRIBUTES;
static const int ACTIVE_ATTRIBUTE_MAX_LENGTH = GL_ACTIVE_ATTRIBUTE_MAX_LENGTH;
static const int ACTIVE_TEXTURE = GL_ACTIVE_TEXTURE;
static const int ACTIVE_UNIFORMS = GL_ACTIVE_UNIFORMS;
static const int ACTIVE_UNIFORM_MAX_LENGTH = GL_ACTIVE_UNIFORM_MAX_LENGTH;
static const int ARRAY_BUFFER_BINDING = GL_ARRAY_BUFFER_BINDING;
static const int ATTACHED_SHADERS = GL_ATTACHED_SHADERS;
static const int BLEND_COLOR = GL_BLEND_COLOR;
static const int BLEND_DST_ALPHA = GL_BLEND_DST_ALPHA;
static const int BLEND_DST_RGB = GL_BLEND_DST_RGB;
static const int BLEND_EQUATION = GL_BLEND_EQUATION;
static const int BLEND_EQUATION_ALPHA = GL_BLEND_EQUATION_ALPHA;
static const int BLEND_EQUATION_RGB = GL_BLEND_EQUATION_RGB;
static const int BLEND_SRC_ALPHA = GL_BLEND_SRC_ALPHA;
static const int BLEND_SRC_RGB = GL_BLEND_SRC_RGB;
static const int BUFFER_SIZE = GL_BUFFER_SIZE;
static const int BUFFER_USAGE = GL_BUFFER_USAGE;
static const int CLAMP_TO_EDGE = GL_CLAMP_TO_EDGE;
static const int COMPRESSED_TEXTURE_FORMATS = GL_COMPRESSED_TEXTURE_FORMATS;
static const int CURRENT_PROGRAM = GL_CURRENT_PROGRAM;
static const int CURRENT_VERTEX_ATTRIB = GL_CURRENT_VERTEX_ATTRIB;
static const int DECR_WRAP = GL_DECR_WRAP;
static const int ELEMENT_ARRAY_BUFFER_BINDING = GL_ELEMENT_ARRAY_BUFFER_BINDING;
static const int FRAMEBUFFER_BINDING = GL_FRAMEBUFFER_BINDING;
static const int FRAMEBUFFER_COMPLETE = GL_FRAMEBUFFER_COMPLETE;
static const int FRAMEBUFFER_INCOMPLETE_ATTACHMENT = GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT;
static const int FRAMEBUFFER_INCOMPLETE_DIMENSIONS = GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS;
static const int FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT = GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT;
static const int FRAMEBUFFER_UNSUPPORTED = GL_FRAMEBUFFER_UNSUPPORTED;
static const int HIGH_FLOAT = GL_HIGH_FLOAT;
static const int HIGH_INT = GL_HIGH_INT;
static const int IMPLEMENTATION_COLOR_READ_FORMAT = GL_IMPLEMENTATION_COLOR_READ_FORMAT;
static const int IMPLEMENTATION_COLOR_READ_TYPE = GL_IMPLEMENTATION_COLOR_READ_TYPE;
static const int INCR_WRAP = GL_INCR_WRAP;
static const int INVALID_ENUM = GL_INVALID_ENUM;
static const int INVALID_FRAMEBUFFER_OPERATION = GL_INVALID_FRAMEBUFFER_OPERATION;
static const int INVALID_OPERATION = GL_INVALID_OPERATION;
static const int INVALID_VALUE = GL_INVALID_VALUE;
static const int LINEAR = GL_LINEAR;
static const int LINEAR_MIPMAP_LINEAR = GL_LINEAR_MIPMAP_LINEAR;
static const int LINEAR_MIPMAP_NEAREST = GL_LINEAR_MIPMAP_NEAREST;
static const int LINK_STATUS = GL_LINK_STATUS;
static const int LOW_FLOAT = GL_LOW_FLOAT;
static const int LOW_INT = GL_LOW_INT;
static const int MAX_COMBINED_TEXTURE_IMAGE_UNITS = GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS;
static const int MAX_CUBE_MAP_TEXTURE_SIZE = GL_MAX_CUBE_MAP_TEXTURE_SIZE;
static const int MAX_FRAGMENT_UNIFORM_VECTORS = GL_MAX_FRAGMENT_UNIFORM_VECTORS;
static const int MAX_RENDERBUFFER_SIZE = GL_MAX_RENDERBUFFER_SIZE;
static const int MAX_TEXTURE_IMAGE_UNITS = GL_MAX_TEXTURE_IMAGE_UNITS;
static const int MAX_VARYING_VECTORS = GL_MAX_VARYING_VECTORS;
static const int MAX_VERTEX_ATTRIBS = GL_MAX_VERTEX_ATTRIBS;
static const int MAX_VERTEX_TEXTURE_IMAGE_UNITS = GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS;
static const int MAX_VERTEX_UNIFORM_VECTORS = GL_MAX_VERTEX_UNIFORM_VECTORS;
static const int MEDIUM_FLOAT = GL_MEDIUM_FLOAT;
static const int MEDIUM_INT = GL_MEDIUM_INT;
static const int MIRRORED_REPEAT = GL_MIRRORED_REPEAT;
static const int NEAREST = GL_NEAREST;
static const int NEAREST_MIPMAP_LINEAR = GL_NEAREST_MIPMAP_LINEAR;
static const int NEAREST_MIPMAP_NEAREST = GL_NEAREST_MIPMAP_NEAREST;
static const int NONE = GL_NONE;
static const int NO_ERROR = GL_NO_ERROR;
static const int NUM_COMPRESSED_TEXTURE_FORMATS = GL_NUM_COMPRESSED_TEXTURE_FORMATS;
static const int NUM_SHADER_BINARY_FORMATS = GL_NUM_SHADER_BINARY_FORMATS;
static const int OUT_OF_MEMORY = GL_OUT_OF_MEMORY;
static const int RENDERBUFFER_BINDING = GL_RENDERBUFFER_BINDING;
static const int REPEAT = GL_REPEAT;
static const int SAMPLES = GL_SAMPLES;
static const int SAMPLE_ALPHA_TO_COVERAGE = GL_SAMPLE_ALPHA_TO_COVERAGE;
static const int SAMPLE_BUFFERS = GL_SAMPLE_BUFFERS;
static const int SAMPLE_COVERAGE = GL_SAMPLE_COVERAGE;
static const int SAMPLE_COVERAGE_INVERT = GL_SAMPLE_COVERAGE_INVERT;
static const int SAMPLE_COVERAGE_VALUE = GL_SAMPLE_COVERAGE_VALUE;
static const int SHADER_BINARY_FORMATS = GL_SHADER_BINARY_FORMATS;
static const int SHADER_COMPILER = GL_SHADER_COMPILER;
static const int SHADING_LANGUAGE_VERSION = GL_SHADING_LANGUAGE_VERSION;
static const int STENCIL_BACK_FAIL = GL_STENCIL_BACK_FAIL;
static const int STENCIL_BACK_FUNC = GL_STENCIL_BACK_FUNC;
static const int STENCIL_BACK_PASS_DEPTH_FAIL = GL_STENCIL_BACK_PASS_DEPTH_FAIL;
static const int STENCIL_BACK_PASS_DEPTH_PASS = GL_STENCIL_BACK_PASS_DEPTH_PASS;
static const int STENCIL_BACK_REF = GL_STENCIL_BACK_REF;
static const int STENCIL_BACK_VALUE_MASK = GL_STENCIL_BACK_VALUE_MASK;
static const int STENCIL_BACK_WRITEMASK = GL_STENCIL_BACK_WRITEMASK;
static const int TEXTURE = GL_TEXTURE;
static const int TEXTURE_BINDING_CUBE_MAP = GL_TEXTURE_BINDING_CUBE_MAP;
static const int UNSIGNED_SHORT_5_6_5 = GL_UNSIGNED_SHORT_5_6_5;
static const int VALIDATE_STATUS = GL_VALIDATE_STATUS;
static const int VERTEX_ATTRIB_ARRAY_BUFFER_BINDING = GL_VERTEX_ATTRIB_ARRAY_BUFFER_BINDING;
static const int VERTEX_ATTRIB_ARRAY_ENABLED = GL_VERTEX_ATTRIB_ARRAY_ENABLED;
static const int VERTEX_ATTRIB_ARRAY_NORMALIZED = GL_VERTEX_ATTRIB_ARRAY_NORMALIZED;
static const int VERTEX_ATTRIB_ARRAY_POINTER = GL_VERTEX_ATTRIB_ARRAY_POINTER;
static const int VERTEX_ATTRIB_ARRAY_SIZE = GL_VERTEX_ATTRIB_ARRAY_SIZE;
static const int VERTEX_ATTRIB_ARRAY_STRIDE = GL_VERTEX_ATTRIB_ARRAY_STRIDE;
static const int VERTEX_ATTRIB_ARRAY_TYPE = GL_VERTEX_ATTRIB_ARRAY_TYPE;

} } // stir :: gl

#endif
