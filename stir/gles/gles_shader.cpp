#include <stir/gles/gles_shader.hpp>

#include <cassert>
#include <stir/filesystem.hpp>
#include <stir/path.hpp>
#include <stir/stir_logs.hpp>

namespace stir { namespace gl {
	
enum class gl_error
{
	ok = 0,
	shader_compile_error,
};
	
class gl_error_category_impl : public std::error_category
{
public:
	virtual const char* name() const noexcept {
		return "gl_error";
	}
	
	virtual std::string message(int ev) const {
		std::string rv;
		switch ((gl_error)ev)
		{
			default:
			case gl_error::ok: rv = "success"; break;
			case gl_error::shader_compile_error: rv = "shader compile error"; break;
		}
		return rv;
	}
};

std::error_category& gl_error_category () { static gl_error_category_impl g_errcat; return g_errcat; }

inline std::error_condition make_error_condition(gl_error e) {
	return std::error_condition (static_cast<int>(e), gl_error_category());
}

inline std::error_code make_error_code(gl_error e) {
	return std::error_code (static_cast<int>(e), gl_error_category());
}

}} // stir :: gl

namespace std {
	template<> struct is_error_condition_enum<stir::gl::gl_error> : std::true_type { };
	template<> struct is_error_code_enum<stir::gl::gl_error> : std::true_type { };
}
	
namespace stir { namespace gl {

namespace fs = stir::filesystem;
	
gpu_program::gpu_program (std::string const& name)
:	m_program(0), m_name(name)
{
}

gpu_program::~gpu_program()
{
	glDeleteProgram (m_program);
}
	
gpu_program::loader::loader (gpu_program& prog)
:	m_result(prog)
{
	assert (m_result.m_program == 0);

	memset(m_state, 0, sizeof(m_state));
}


bool gpu_program::loader::load_file(char const* vsP, char const* psP, std::error_code& ec)
{
	ec.clear();
	std::string vsC, psC;
	fs::read_file(vsP, vsC, ec);
	if (ec) return false;

	fs::read_file(psP, psC, ec);
	if (ec) return false;

	return load_data(vsC.c_str(), psC.c_str(), ec);
}

bool gpu_program::loader::load_data(char const* vsS, char const* psS, std::error_code& ec)
{	
	int logLen;

	// Vertex shader
	GLuint vs = gl::CreateShader(gl::ShaderType::VERTEX_SHADER);
	gl::ShaderSource(vs, 1, &vsS, NULL);
	gl::CompileShader(vs);
	gl::GetShaderiv(vs, gl::GetShaderPName::COMPILE_STATUS, &m_state[vert]);
	gl::GetShaderiv(vs, gl::GetShaderPName::INFO_LOG_LENGTH, &logLen);

	if (logLen > 0) {
		m_message[Shader::vert].resize (logLen);
		gl::GetShaderInfoLog(vs, logLen, NULL, const_cast<char*>(m_message[Shader::vert].data()));
		
		if (m_state[Shader::vert] != GL_TRUE) {
			ec = make_error_code (gl_error::shader_compile_error);
			return false;
		}
	}
	
	// Fragment shader
	GLuint ps = gl::CreateShader(gl::ShaderType::FRAGMENT_SHADER);
	gl::ShaderSource(ps, 1, &psS, NULL);
	gl::CompileShader(ps);
	gl::GetShaderiv(ps, gl::GetShaderPName::COMPILE_STATUS, &m_state[Shader::frag]);
	gl::GetShaderiv(ps, gl::GetShaderPName::INFO_LOG_LENGTH, &logLen);

	if (logLen > 0) {
		m_message[Shader::frag].resize (logLen);
		gl::GetShaderInfoLog(ps, logLen, NULL, const_cast<char*>(m_message[Shader::frag].data()));

		if (m_state[Shader::frag] != GL_TRUE) {
			ec = make_error_code (gl_error::shader_compile_error);
			return false;
		}
	}

	// Link shaders into program		
	GLuint prog = gl::CreateProgram();
	gl::AttachShader(prog, vs);
	gl::AttachShader(prog, ps);
	gl::LinkProgram(prog);
	gl::GetProgramiv(prog, gl::LINK_STATUS, &m_state[Shader::program]);
	gl::GetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLen);

	if (logLen > 0) {
		m_message[Shader::program].resize (logLen);
		gl::GetProgramInfoLog(prog, logLen, NULL, const_cast<char*>(m_message[Shader::program].data()));
		if (m_state[Shader::program] != GL_TRUE) {
			ec = make_error_code (gl_error::shader_compile_error);
			gl::DeleteProgram(prog);
			return false;
		}
	}

	m_result.m_program = prog;
	
	gl::DeleteShader(vs);
	gl::DeleteShader(ps);
	
	return true;
}
	
static char const* type2str(GLenum type)
{
	switch (type)
	{
		case GL_FLOAT:   return "float";
		case GL_FLOAT_VEC2: return "float2";
		case GL_FLOAT_VEC3: return "float3";
		case GL_FLOAT_VEC4: return "float4";
		case GL_INT: return "int";
		case GL_INT_VEC2: return "int2";
		case GL_INT_VEC3: return "int3";
		case GL_INT_VEC4: return "int4";
		case GL_BOOL: return "bool";
		case GL_BOOL_VEC2: return "bool2";
		case GL_BOOL_VEC3: return "bool3";
		case GL_BOOL_VEC4: return "bool4";
		case GL_FLOAT_MAT2: return "float2";
		case GL_FLOAT_MAT3: return "float3";
		case GL_FLOAT_MAT4: return "float4";
		case GL_SAMPLER_2D: return "sampler_2d";
		case GL_SAMPLER_CUBE: return "sampler_cube";
		default: return "unknown-type";
	}
}
	
void gpu_program::query_uniforms()
{
	int maxL = 0, unis = 0;
	glGetProgramiv(m_program, GL_ACTIVE_UNIFORM_MAX_LENGTH, &maxL);
	glGetProgramiv(m_program, GL_ACTIVE_UNIFORMS, &unis);
	
	if (maxL > 256)
		maxL = 256;
	
	char* buffer = (char*)alloca(maxL);
	for (int i=0; i<unis; ++i)
	{
		uniform uni;
		GLsizei len = 0;
		uni.index = i;
		glGetActiveUniform(m_program, i, maxL, &len, &uni.size, &uni.type, buffer);
		if (glGetError() != GL_NO_ERROR)
			continue;
		uni.name.assign (buffer, len);
		
		STIR_LOG(debug).printf("  %s %s (%d)\n", type2str(uni.type), uni.name.c_str(), uni.size);
		m_uniforms.push_back (uni);
	}
}

void gpu_program::use()
{
	gl::UseProgram(m_program);
}
	
void gpu_program::dont_use()
{
	gl::UseProgram(0);
}


}}
