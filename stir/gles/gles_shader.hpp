#ifndef STIR_GLES_SHADER_
#define STIR_GLES_SHADER_

#include <stir/platform.hpp>
#include <system_error>
#include <memory>
#include <vector>
#include <string>
#include <list>

#include "gles_shader.hpp"
#include <stir/gl.hpp>

namespace stir { namespace gl {
	
struct scalar_tag {};
struct vector_tag {};
struct matrix_tag {};
struct sampler_tag {};

template<typename TagT, typename T, int N>
struct var
{
	int m_location;
};
	
typedef var<scalar_tag, float, 1> float1;
typedef var<scalar_tag, float, 2> float2;
typedef var<scalar_tag, float, 3> float3;
typedef var<scalar_tag, float, 4> float4;
	
typedef var<scalar_tag, int, 1> int1;
typedef var<scalar_tag, int, 2> int2;
typedef var<scalar_tag, int, 3> int3;
typedef var<scalar_tag, int, 4> int4;

typedef var<matrix_tag, float, 2> mat22;
typedef var<matrix_tag, float, 3> mat33;
typedef var<matrix_tag, float, 4> mat44;
	
typedef var<sampler_tag, int, 1> sampler;
	
class gpu_program
{
public:
	GLuint m_program;
	std::string m_name; // basically for debug/info

	gpu_program (std::string const& name);
	~gpu_program();

	gpu_program& operator= (gpu_program const&) = delete;
	gpu_program (gpu_program const&) = delete;
	
	struct uniform
	{
		unsigned index;
		GLint size;
		std::string name;
		GLenum type;
	};

private:
	std::list<uniform> m_uniforms;
	
	void query_uniforms();
	
public:
	struct loader
	{
		enum Shader { vert, frag, program, count };
		std::string m_message[count];
		int m_state[count];
		
		gpu_program& m_result;

		loader (gpu_program& prog);

		bool load_file(char const* vsP, char const* psP, std::error_code& ec);
		bool load_data(char const* vsS, char const* psS, std::error_code& ec);
		
		bool has_info() const { for (auto const& s : m_message) if (!s.empty ()) return true; return false; }
		std::string const& msg(Shader index) const { return m_message[index]; }
	};

	std::string const& name() const { return m_name; }
	void set_name(std::string const& name) { m_name = name; }
	
	void use();
	void dont_use();

	template<typename Tag, typename T, int N>
	var<Tag, T, N> location(char const* name)
	{
		return var<Tag, T, N> { glGetUniformLocation(m_program, name) };
	}
	
	template<typename Var>
	Var location(char const* name)
	{
		return Var { glGetUniformLocation(m_program, name) };
	}
	
	GLint attrib(char const* name) {
		return stir::gl::GetAttribLocation (m_program, name);
	}
	
	// float
	void set (var<scalar_tag, float, 1> const& loc, float x)
	{ gl::Uniform1f (loc.m_location, x); }
	void set (var<scalar_tag, float, 2> const& loc, float x, float y)
	{ gl::Uniform2f (loc.m_location, x, y); }
	void set (var<scalar_tag, float, 3> const& loc, float x, float y, float z)
	{ gl::Uniform3f (loc.m_location, x, y, z); }
	void set (var<scalar_tag, float, 4> const& loc, float x, float y, float z, float w)
	{ gl::Uniform4f (loc.m_location, x, y, z, w); }
	
	void set (var<scalar_tag, float, 1> const& loc, int count, float const* s)
	{ gl::Uniform1fv (loc.m_location, count, s); }
	void set (var<scalar_tag, float, 2> const& loc, int count, float const* s)
	{ gl::Uniform2fv (loc.m_location, count, s); }
	//	void set (var<scalar_tag, float, 2> const& loc, int count, vector2 const* s)
	//{ glUniform2fv (loc.m_location, count, (float const*)s); }
	//	void set (var<scalar_tag, float, 2> const& loc, int count, vector2f const* s)
	// { glUniform2fv (loc.m_location, count, (float const*)s); }
	void set (var<scalar_tag, float, 3> const& loc, int count, float const* s)
	{ gl::Uniform3fv (loc.m_location, count, s); }
	//	void set (var<scalar_tag, float, 3> const& loc, int count, vector3 const* s)
	//{ glUniform3fv (loc.m_location, count, (float const*)s); }
	void set (var<scalar_tag, float, 4> const& loc, int count, float const* s)
	{ gl::Uniform4fv (loc.m_location, count, s); }
	//	void set (var<scalar_tag, float, 4> const& loc, int count, vector4 const* s)
	//{ glUniform4fv (loc.m_location, count, (float const*)s); }

	// int
	void set (var<scalar_tag, int, 1> const& loc, int x)
	{ gl::Uniform1i (loc.m_location, x); }
	void set (var<scalar_tag, int, 2> const& loc, int x, int y)
	{ gl::Uniform2i (loc.m_location, x, y); }
	void set (var<scalar_tag, int, 3> const& loc, int x, int y, int z)
	{ gl::Uniform3i (loc.m_location, x, y, z); }
	void set (var<scalar_tag, int, 4> const& loc, int x, int y, int z, int w)
	{ gl::Uniform4i (loc.m_location, x, y, z, w); }

	// matrix
	void set (var<matrix_tag, float, 2> const& loc, float const* m)
	{ gl::UniformMatrix2fv (loc.m_location, 1, false, m); }
	void set (var<matrix_tag, float, 2> const& loc, int count, float const* m, bool transpose = false)
	{ gl::UniformMatrix2fv (loc.m_location, count, transpose, m); }
	void set (var<matrix_tag, float, 3> const& loc, float const* m)
	{ gl::UniformMatrix3fv (loc.m_location, 1, false, m); }
	void set (var<matrix_tag, float, 3> const& loc, int count, float const* m, bool transpose = false)
	{ gl::UniformMatrix3fv (loc.m_location, count, transpose, m); }
	void set (var<matrix_tag, float, 4> const& loc, float const* m)
	{ gl::UniformMatrix4fv (loc.m_location, 1, false, m); }
	void set (var<matrix_tag, float, 4> const& loc, int count, float const* m, bool transpose)
	{ gl::UniformMatrix4fv (loc.m_location, count, transpose, m); }

	// sampler
	void set (var<sampler_tag, int, 1> const& loc, int samp)
	{ gl::Uniform1i (loc.m_location, samp); }
	void set (var<sampler_tag, int, 1> const& loc, int count, int const* samp)
	{ gl::Uniform1iv (loc.m_location, count, samp); }
	
	void set_texture (var<sampler_tag, int, 1> const& loc, GLint texName, int index = 0, gl::TextureTarget target = gl::TextureTarget::TEXTURE_2D)
	{
		gl::ActiveTexture((gl::TextureUnit)((int)gl::TextureUnit::TEXTURE0 + index));
		gl::BindTexture(target, texName);
		set (loc, index);
	}
};
	
}}

#endif
