#include "platform.hpp"
#include "basic_util.hpp"
#include "image.hpp"

#include "gles_texture.hpp"
#include <stir/asset/texture_asset.hpp>

namespace stir { namespace gles {
	
gles_texture* create_texture(size_t w, size_t h, size_t levels, gl::PixelFormat textureFormat, gl::PixelType dataType, void* data)
{
	gles_texture* tex = new gles_texture();
	
	tex->width = w;
	tex->height = h;
	tex->levels = levels;
	tex->format = textureFormat;
	
	gl::GenTextures(1, &tex->tex_name);
	gl::ActiveTexture(gl::TextureUnit::TEXTURE0);
	gl::BindTexture(gl::TextureTarget::TEXTURE_2D, tex->tex_name);
	//	glBindTexture (GL_TEXTURE_2D, tex->tex_name);
	//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if (!data) {
		image img (w, h, component_count(textureFormat));
		img.fill(color8::argb(0xff0000ff));

		gl::tex_image_2d(0, textureFormat, (int)w, (int)h, img.pixels, gl::PixelType::UNSIGNED_BYTE);
	} else
		gl::tex_image_2d(0, textureFormat, (int)w, (int)h, data, gl::PixelType::UNSIGNED_BYTE);

	return tex;
}

void release_texture(gles_texture* tex)
{
	gl::DeleteTextures (1, &tex->tex_name);
	delete tex;
}
	
gles_texture* create_texture(asset::texture_asset const& ass)
{
	gles_texture* tex = new gles_texture ();
	gl::GenTextures(1, &tex->tex_name);
	gl::BindTexture(gl::TextureTarget::TEXTURE_2D, tex->tex_name);
	gl::TexParameteri(gl::TextureTarget::TEXTURE_2D, gl::TextureParameterName::TEXTURE_MIN_FILTER, gl::LINEAR);
	gl::TexParameteri(gl::TextureTarget::TEXTURE_2D, gl::TextureParameterName::TEXTURE_MAG_FILTER, gl::LINEAR);
	if (ass.format == stir::asset::i8)
		gl::tex_image_2d (0, gl::PixelFormat::ALPHA, ass.width, ass.height, ass.levels[0].pixels, gl::PixelType::UNSIGNED_BYTE);
	else
		gl::tex_image_2d (0, gl::PixelFormat::RGBA, ass.width, ass.height, ass.levels[0].pixels, gl::PixelType::UNSIGNED_BYTE);
	
	tex->levels = 1;
	tex->width = ass.width;
	tex->height = ass.height;
	
	return tex;
}
	
}}
