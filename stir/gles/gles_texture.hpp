#ifndef STIR_GLES_TEXTURE_HPP_
#define STIR_GLES_TEXTURE_HPP_

#include <stir/platform.hpp>
#include <stir/basic_types.hpp>
#include <stir/basic_util.hpp>
#include <stddef.h>

#include <stir/gl.hpp>

//enum class PixelInternalFormat : int {
//#define GL_PALETTE4_RGB8_OES              0x8B90
//#define GL_PALETTE4_RGBA8_OES             0x8B91
//#define GL_PALETTE4_R5_G6_B5_OES          0x8B92
//#define GL_PALETTE4_RGBA4_OES             0x8B93
//#define GL_PALETTE4_RGB5_A1_OES           0x8B94
//#define GL_PALETTE8_RGB8_OES              0x8B95
//#define GL_PALETTE8_RGBA8_OES             0x8B96
//#define GL_PALETTE8_R5_G6_B5_OES          0x8B97
//#define GL_PALETTE8_RGBA4_OES             0x8B98
//#define GL_PALETTE8_RGB5_A1_OES           0x8B99
//	
//};
//
namespace stir {
	
namespace asset { struct texture_asset; }	

namespace gles {
	
struct gles_texture
{
	GLenum tex_name;
	u16	width, height;
	u8	levels;
	gl::PixelFormat format;
};

class texture_info
{
	gles_texture* t;
	
public:
	explicit texture_info (gles_texture* tex, int level = 0) : t(tex) { }
	
	size_t width () const { return t->width; }
	size_t height () const { return t->height; }
	
	void get_inv (float& ix, float& iy) const { ix = 1.0f / (float)t->width; iy = 1.0f / (float)t->height; }
};
	
gles_texture* create_texture(size_t w, size_t h, size_t levels, gl::PixelFormat textureFormat, gl::PixelType dataType, void* data);
void release_texture(gles_texture* tex);
gles_texture* create_texture(asset::texture_asset const& ass);

} } // stir::gles

#endif // STIR_GLES_TEXTURE_HPP_
