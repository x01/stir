#include "gles_texture_loader.hpp"
#include <CoreGraphics/CGImage.h>
#include <CoreGraphics/CGBitmapContext.h>
#include <stir/stir_logs.hpp>
#include <stir/log.hpp>
#include <stir/bits.hpp>
#include <stir/filesystem.hpp>

#include <stir/asset/texture_asset.hpp>
#include <pulmotor/blit_holder.hpp>
#include <pulmotor/archive.hpp>

namespace stir { namespace gles {

texture_loader::texture_loader	(path const& prefix, path const& suffix)
:	prefix_ (prefix)
,	err_texture_ (NULL)
{
	loader_aux::tokenize_path (suffix, native_path(":|"), suffixes_);
}

texture_loader::~texture_loader ()
{
	if (err_texture_)
		unload (err_texture_, 0);
}
	
gles_texture* texture_loader::load_stex(path const& p, std::error_code& ec)
{
	filesystem::input_buffer inS (p, ec);
	if (ec)
		return nullptr;
	
	stir::asset::texture_asset t;
	pulmotor::input_archive inA (inS);
//	pulmotor::debug_archive<pulmotor::input_archive> inAD (inA, std::cout);
	pulmotor::archive (inA, t);
	if (inA.ec) {
		ec = inA.ec;
		return nullptr;
	}
	
	gles_texture* tex = create_texture(t.width, t.height, t.levels.size(), t.format == asset::i8 ? gl::PixelFormat::ALPHA : gl::PixelFormat::RGBA, gl::PixelType::UNSIGNED_BYTE, t.levels[0].pixels);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	
	ec.clear ();
	return tex;
}
	
gles_texture* texture_loader::load (resource_id const& id, int profile)
{
	// find the available file
	path p = loader_aux::find_available_file (prefix_ / id.get_id(), &*suffixes_.begin (), suffixes_.size ());
	
	// load from file (using apple functions)
	STIR_LOG (resource) << "Loading " << p.string () << log::endl;
	
	// TODO: loading a pulmotor texture is hacked in here. a proper way would be to
	// move this code to another loader and either specify another loader when loading a texture
	// or make a 'facade' loader which dynamically switches between the two depending
	// on the file extension.
	if (p.extension () == ".stex")
	{
		std::error_code ec;
		if (gles_texture* tex = load_stex (p, ec))
			return tex;
		else {
			STIR_LOG (resource).printf ("Failed to load a file '%s', error (%d): %s\n",
										p.c_str(), ec.value(), ec.message().c_str());
			return nullptr;
		}
	}
	
	p = filesystem::get_main_data_path() / p;
	
	CGDataProviderRef dataProvider = CGDataProviderCreateWithFilename (p.c_str ());
	if (!dataProvider)
	{
		STIR_LOG (resource) << "Failed to open a file '" << p.string () << "'" << log::endl;
		return 0;
	}
	
	CGImageRef img = CGImageCreateWithPNGDataProvider (dataProvider, NULL, false, kCGRenderingIntentDefault);
	if (!img)
	{
		CGDataProviderRelease (dataProvider);
		STIR_LOG (resource) << "Failed decode image '" << p.string () << "'" << log::endl;
		return 0;
	}
	
    // Get the width and height of the image
    size_t width = CGImageGetWidth(img);
    size_t height = CGImageGetHeight(img);
	CGColorSpaceRef colorSpace = CGImageGetColorSpace(img);
//	CGImageAlphaInfo alphaInfo = CGImageGetAlphaInfo(img);
	
	if (!bits::is_pot (width) || !bits::is_pot(height))
	{
		CGImageRelease(img);
		CGDataProviderRelease (dataProvider);
		STIR_LOG (resource) << "Image must be power-of-two dimensions '" << p.string () << "'" << log::endl;
		return 0;
	}

	// create texture, copy data and fill info
	gles_texture* tex = new gles_texture ();
	
    // Texture dimensions must be a power of 2. If you write an application that allows users to supply an image,
    // you'll want to add code that checks the dimensions and takes appropriate action if they are not a power of 2.
	
	// render the image into the bitmap context
//	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB ();
	GLubyte* spriteData = (GLubyte *) new char[width * height * 4];
	CGContextRef spriteContext = CGBitmapContextCreate (spriteData, width, height, 8, width * 4, colorSpace, kCGImageAlphaPremultipliedLast);
//	CGContextRef spriteContext = CGBitmapContextCreate (spriteData, width, height, 8, width * 4, colorSpace, kCGImageAlphaLast);
//	CGContextTranslateCTM (spriteContext, 0, height);
//	CGContextScaleCTM (spriteContext, 1.0f, -1.0f);
	CGContextDrawImage (spriteContext, CGRectMake(0.0, 0.0, (CGFloat)width, (CGFloat)height), img);
	CGContextRelease (spriteContext);
//	CGColorSpaceRelease (colorSpace);
	
//	memset (spriteData, 0x80, width * height / 16 * 4);
	
	// get the image bytes and create an ogl texture
	gl::GenTextures(1, &tex->tex_name);
	gl::BindTexture (gl::TextureTarget::TEXTURE_2D, tex->tex_name);
	gl::TexParameteri(gl::TextureTarget::TEXTURE_2D, gl::TextureParameterName::TEXTURE_MIN_FILTER, gl::LINEAR);
	gl::TexParameteri(gl::TextureTarget::TEXTURE_2D, gl::TextureParameterName::TEXTURE_MAG_FILTER, gl::LINEAR);
	
	gl::tex_image_2d(0, gl::PixelFormat::RGBA, (int)width, (int)height, spriteData, gl::PixelType::UNSIGNED_BYTE);
	delete []spriteData;
	
	tex->levels = 1;
	tex->width = width;
	tex->height = height;

	CGDataProviderRelease (dataProvider);
	CGImageRelease(img);
	
//	glCompressedTexImage2D (tex->tex_name, level, -666, w, h, 0, GLsizei imageSize, pixels);
	return tex;
}

void texture_loader::unload( gles_texture* resource, int profile )
{
	gl::DeleteTextures(1, &resource->tex_name);
	delete resource;
}

}}
