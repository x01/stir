#ifndef STIR_GLES_TEXTURE_LOADER_HPP_
#define STIR_GLES_TEXTURE_LOADER_HPP_

#include <system_error>
#include <stir/platform.hpp>

#include <stir/resource_id.hpp>
#include <stir/resource_loader.hpp>
#include <stir/gles/gles_texture.hpp>

namespace stir { namespace gles {

class STIR_ATTR_DLL texture_loader : public loader<gles_texture*>
{
public:
	texture_loader	(path const& prefix, path const& suffix);
	~texture_loader ();
	
	virtual gles_texture* load( resource_id const& id, int profile );
	virtual void unload( gles_texture* resource, int profile );
	
private:	
	gles_texture* create_err_texture( int size );
	gles_texture* load_stex(path const& p, std::error_code& ec);
	
	gles_texture*	err_texture_;
	path				prefix_;
	std::vector<path>	suffixes_;
};

}} // stir::gles

#endif
