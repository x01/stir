varying highp vec4 vtx_color;

void main()
{
	gl_FragColor = vtx_color;
}
