uniform sampler2D tex;

varying highp vec2 tex_coord;
varying highp vec4 vtx_color;

void main()
{
	lowp vec4 texC = texture2D(tex, tex_coord);
	gl_FragColor = vtx_color * texC;
}
