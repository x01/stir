#pragma glsl_1.1

uniform mat4 proj_mat;

attribute vec3 in_pos;
attribute vec2 in_uv;

varying highp vec2 tex_coord;

void main()
{
	tex_coord = in_uv;
	gl_Position = proj_mat * vec4(in_pos, 1);
}