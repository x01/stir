#pragma glsl_1.1

uniform mat4 proj_mat;

attribute vec3 in_pos;
attribute vec4 in_color;

varying mediump vec4 vtx_color;

void main()
{
	vtx_color = in_color;
	gl_Position = proj_mat * vec4(in_pos, 1);
}