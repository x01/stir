#include "graphics_util.hpp"
#include <stir/stir_logs.hpp>
#include <stir/optional.hpp>
#include <stir/gles/buffer.hpp>
#include <stir/partition.hpp>
#include <stir/r_gpuprog.hpp>
#include <stir/texture.hpp>

namespace stir {
	
text_style g_text_default;
text_style g_text_condensed { 1.0f, 0.8f, TextAlign::left };
text_style g_smaller_condensed { 0.75f, 0.8f, TextAlign::left };
text_style g_text_half { 0.5f, 0.9f, TextAlign::left };
text_style g_text_half_condensed { 0.5f, 0.7f, TextAlign::left };

u16 g_quad_indices[6] = { 0, 1, 2, 2, 3, 0 };
	
char const* get_gl_error_string (GLenum err)
{
	switch(err) {
		case GL_INVALID_ENUM: return "INVALID_ENUM, ￼enum argument out of range"; 
		case GL_INVALID_VALUE: return "INVALID_VALUE, ￼Numeric argument out of range";
		case GL_INVALID_OPERATION: return "￼INVALID_OPERATION, Operation illegal in current state";
#if STIR_GLES1
		case GL_STACK_OVERFLOW: return "￼STACK_OVERFLOW, Command would cause a stack overflow";
		case GL_STACK_UNDERFLOW: return "￼STACK_UNDERFLOW, Command would cause a stack underflow";
#endif
		case GL_OUT_OF_MEMORY: return "￼￼OUT_OF_MEMORY, Not enough memory left to exe- cute command";
//		case GL_TABLE_TOO_LARGE: return "￼￼TABLE_TOO_LARGE, The specified table is too large";
		case GL_NO_ERROR: return "NO_ERROR!";
		default: return "<GL error code not defined>";
	}
}

void assert_valid_gl_state (char const* cmd)
{
	GLenum err = glGetError();
	if (err != GL_NO_ERROR)
	{
		char const* es = get_gl_error_string (err);
		STIR_LOG(debug).printf ("GL Error: %d (%s)\nAfter calling '%s'\n", err, es, cmd);
	}
}
	
class imm_render : private stir::gpuprog_listener
{
public:
	struct part1 {
		size_t start, count;
		blendmode srcBlendMode, dstBlendMode;
		
		void init (size_t start_, blendmode srcBM, blendmode dstBM) {
			start = start_; count = 0; srcBlendMode = srcBM; dstBlendMode = dstBM;
		}
		
		bool compatible (blendmode srcBM, blendmode dstBM) const {
			return srcBlendMode == srcBM && dstBlendMode == dstBM;
		}
	};
	
	struct part2 {
		size_t start, count;
		stir::texture tex;
		blendmode srcBlendMode, dstBlendMode;
		
		void init (size_t start_, stir::texture t, blendmode srcBM, blendmode dstBM) {
			start = start_; count = 0; tex = t; srcBlendMode = srcBM; dstBlendMode = dstBM;
		}
		
		bool compatible (stir::texture t, blendmode srcBM, blendmode dstBM) const {
			return tex == t && srcBlendMode == srcBM && dstBlendMode == dstBM;
		}
	};
	
	gl::buffer m_pos_color_uv;
	gl::buffer m_pos_color;
	gl::buffer m_indices;
	
	stir::partition<part1, 16> m_partition1;
	stir::partition<part2, 16> m_partition2;
	
	gl::vao m_vao1, m_vao2;
	rindex_t m_gpuprog1 = stir::rinvalid, m_gpuprog2 = stir::rinvalid;
	size_t m_quads;
	stir::device_context& m_dev;
	
	struct vertex1 {
		float x, y;
		color8 color;
	};
	vertex1* m_p1 = nullptr;
	
	struct vertex2 {
		float x, y;
		color8 color;
		float u,v;
	};
	vertex2* m_p2 = nullptr;
	
	struct {
		gl::mat44 proj_mat;
		gl::sampler tex;
	} m_locs1, m_locs2;
	
	void on_gpuprog_was_reloaded() { get_gpuprog_info(); }

	void get_gpuprog_info() {
		gpu_program& p1 = r_gpuprog % m_gpuprog1;
		m_locs1.proj_mat = p1.location<gl::mat44>("proj_mat");

		gl::binding<2> b1
		{
			{ "in_pos", 2, gl::bind_type::float_, false, offsetof(vertex1, x), sizeof(vertex1) },
			{ "in_color", 4, gl::bind_type::ubyte, true, offsetof(vertex1, color), sizeof(vertex1) },
		};
		b1.bind_attrs(p1);
		m_vao1.create(b1, m_pos_color.m_buffer, m_indices.m_buffer);

		gpu_program& p2 = r_gpuprog % m_gpuprog2;
		m_locs2.proj_mat = p2.location<gl::mat44>("proj_mat");
		m_locs2.tex = p2.location<gl::sampler>("tex");
		
		gl::binding<3> b2
		{
			{ "in_pos", 2, gl::bind_type::float_, false, offsetof(vertex2, x), sizeof(vertex2) },
			{ "in_color", 4, gl::bind_type::ubyte, false, offsetof(vertex2, color), sizeof(vertex2) },
			{ "in_uv", 2, gl::bind_type::float_, true, offsetof(vertex2, u), sizeof(vertex2) }
		};
		
		b2.bind_attrs(p2);
		m_vao2.create(b2, m_pos_color_uv.m_buffer, m_indices.m_buffer);
	}
	
	imm_render(device_context& dev, size_t quads = 256)
 	:	m_dev(dev), m_quads (quads), m_partition1(quads*4), m_partition2(quads*4)
	{
		m_gpuprog1 = r_gpuprog.load_cached("imm_vtx_color");
		m_gpuprog2 = r_gpuprog.load_cached("imm_vtx_color_tex");
		get_gpuprog_info();
		
		m_pos_color.bind();
		gl::buffer::data(gl::BufferTarget::ARRAY_BUFFER, quads*4*sizeof(vertex1), nullptr, gl::BufferUsage::DYNAMIC_DRAW);
		
		m_pos_color_uv.bind();
		gl::buffer::data(gl::BufferTarget::ARRAY_BUFFER, quads*4*sizeof(vertex2), nullptr, gl::BufferUsage::DYNAMIC_DRAW);
		
		m_indices.bind(gl::BufferTarget::ELEMENT_ARRAY_BUFFER);
		gl::buffer::data(gl::BufferTarget::ELEMENT_ARRAY_BUFFER, m_quads * 6, nullptr, gl::BufferUsage::STATIC_DRAW);
		u16* ib = (u16*)m_indices.map(gl::BufferTarget::ELEMENT_ARRAY_BUFFER);
		int base = 0;
		for (u16* e=ib + quads*6; ib != e; ib += 6, base += 4) {
			ib[0] = base + 0;
			ib[1] = base + 1;
			ib[2] = base + 2;
			ib[3] = base + 0;
			ib[4] = base + 2;
			ib[5] = base + 3;
		}
		m_indices.unmap(gl::BufferTarget::ELEMENT_ARRAY_BUFFER);
	}
	
	void set_blendmode(blendmode srcBlendMode, blendmode dstBlendMode) {
		if (srcBlendMode != blendmode::blend_off) {
			gl::BlendFunc((gl::BlendingFactorSrc)srcBlendMode, (gl::BlendingFactorDest)dstBlendMode);
			gl::Enable (gl::EnableCap::BLEND);
		} else
			gl::Disable (gl::EnableCap::BLEND);
	}
	
	void flush1(bool doMap) {
		gpu_program& prog = r_gpuprog % m_gpuprog1;
		
		m_pos_color.bind();
		gl::buffer::unmap();

		prog.use();
		matrix4 m = matrix4::orthographic(0, m_dev.logical_width(), m_dev.logical_height(), 0, 0, 1);
		prog.set(m_locs1.proj_mat, &m[0][0]);
		
		m_vao1.use();
		for (part1 const& p : m_partition1) {
			set_blendmode(p.srcBlendMode, p.dstBlendMode);
			size_t s = p.start / 4 * 6, c = p.count / 4 * 6;
			gl::DrawElements(gl::PrimitiveType::TRIANGLES, (GLuint)c, gl::DrawElementsType::UNSIGNED_SHORT, (void*)(uintptr_t)s);
		}
		m_partition1.restart(m_quads*4);
		m_vao1.dont_use();
		
		if (doMap) {
			m_pos_color.bind();
			m_p1 = (vertex1*)gl::buffer::map();
		} else
			m_p1 = nullptr;
	}
	
	void flush2(bool doMap) {
		gpu_program& prog = r_gpuprog % m_gpuprog2;
		
		m_pos_color_uv.bind();
		gl::buffer::unmap();
		
		prog.use();
		matrix4 m = matrix4::orthographic(0, m_dev.logical_width(), m_dev.logical_height(), 0, 0, 1);
		prog.set(m_locs2.proj_mat, &m[0][0]);
		
		m_vao2.use();
		for (part2 const& p : m_partition2) {
			set_blendmode(p.srcBlendMode, p.dstBlendMode);
			prog.set_texture (m_locs2.tex, p.tex->tex_name);
			size_t s = p.start / 4 * 6, c = p.count / 4 * 6;
			gl::DrawElements(gl::PrimitiveType::TRIANGLES, (GLuint)c, gl::DrawElementsType::UNSIGNED_SHORT, (void*)(uintptr_t)s);
		}
		m_vao2.dont_use();
		
		m_partition2.restart(m_quads*4);
		if (doMap) {
			m_pos_color_uv.bind();
			m_p2 = (vertex2*)gl::buffer::map();
		} else
			m_p2 = nullptr;
	}
	
	void sequence(int keepMask)
	{
		if (!(keepMask & 0x01) && !m_partition1.empty())
			flush1(false);
		if (!(keepMask & 0x02) && !m_partition2.empty())
			flush2(false);
	}
	
	void end() {
		if (!m_partition1.empty())
			flush1(false);
//		m_p1 = nullptr;
		
		if (!m_partition2.empty())
			flush2(false);
//		m_p2 = nullptr;
	}

	void draw_rect(rectf const& rc, color8 color, blendmode srcBlendMode, blendmode dstBlendMode) {
		sequence (0x01);
		
		if (!m_p1) {
			m_pos_color.bind();
			m_p1 = (vertex1*)gl::buffer::map();
		}
		
		while(m_p1) {
			if (!m_partition1.part(4, srcBlendMode, dstBlendMode)) {
				flush1 (true);
			} else {
				m_p1[0] = {	rc.p.x,				rc.p.y, 			color };
				m_p1[1] = {	rc.p.x + rc.s.x,	rc.p.y,				color };
				m_p1[2] = {	rc.p.x + rc.s.x,	rc.p.y + rc.s.y,	color };
				m_p1[3] = {	rc.p.x,				rc.p.y + rc.s.y,	color };
				
				m_p1 += 4;
				
				return;
			}
		};
	}
	
	void draw_rect_uv (rectf const& rc, stir::texture tex, stir::areaf const& area, color8 color, blendmode srcBlendMode, blendmode dstBlendMode)
	{
		sequence (0x02);
		
		if (!m_p2) {
			m_pos_color_uv.bind();
			m_p2 = (vertex2*)gl::buffer::map();
		}
		
		while (m_p2) {
			if (!m_partition2.part(4, tex, srcBlendMode, dstBlendMode)) {
				flush2 (true);
			} else {
				m_p2[0] = {	rc.p.x,				rc.p.y, 			color, area.p0.x, area.p0.y };
				m_p2[1] = {	rc.p.x + rc.s.x,	rc.p.y,				color, area.p1.x, area.p0.y };
				m_p2[2] = {	rc.p.x + rc.s.x,	rc.p.y + rc.s.y,	color, area.p1.x, area.p1.y };
				m_p2[3] = {	rc.p.x,				rc.p.y + rc.s.y,	color, area.p0.x, area.p1.y };
				
				m_p2 += 4;
				
				return;
			}
		};
	}
};

bool instant_imm = true;
stir::optional<imm_render> g_imm;
void init_imm(device_context& dev)
{
	g_imm.emplace (dev);
}

void draw_rect(rectf const& rc, color8 color, blendmode srcBlendMode, blendmode dstBlendMode)
{
#if STIR_GLES1
	imm_render::vertex v[ 4 ] =
	{
		{	(float)rc.p.x,					(float)rc.p.y,					0 },
		{	(float)rc.p.x + (float)rc.s.x,	(float)rc.p.y,					0 },
		{	(float)rc.p.x + (float)rc.s.x,	(float)rc.p.y + (float)rc.s.y,	0 },
		{	(float)rc.p.x,					(float)rc.p.y + (float)rc.s.y,	0 }
	};

	stir::gles::disable_client_states ();
	
	GL(glVertexPointer, (3, GL_FLOAT, 0, v));
	GL(glEnableClientState, (GL_VERTEX_ARRAY));

//	GL(glTexEnvfv, (GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, color.ptr ()));
	GL(glColor4f, (color.r, color.g, color.b, color.a));
	stir::gles::set_combiner (GL_REPLACE, GL_SRC_COLOR, GL_CONSTANT, GL_CONSTANT,
							  GL_REPLACE, GL_SRC_ALPHA, GL_CONSTANT, GL_CONSTANT);
	
	if (srcBlendMode != blend_off)
	{
		GL(glEnable, (GL_BLEND));
		GL(glBlendFunc, (srcBlendMode, dstBlendMode));
	}
	else
		GL(glDisable, (GL_BLEND));
	
	GL(glDrawElements, (GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, g_quad_indices));
	
	stir::gles::disable_client_states ();
	
	
#elif STIR_GLES2

	assert (g_imm);
	g_imm->draw_rect(rc, color, srcBlendMode, dstBlendMode);
	
	if (instant_imm) g_imm->sequence(0);
#endif
}
	
void draw_rect_uv (rectf const& rc, stir::texture tex, stir::areaf const& area, color8 color, blendmode srcBlendMode, blendmode dstBlendMode)
{
#if STIR_GLES1
	struct imm_render::uv_vertex v [ 4 ] =
	{
		{	(float)rc.p.x,					(float)rc.p.y,					0, 0, 0 },
		{	(float)rc.p.x + (float)rc.s.x,	(float)rc.p.y,					0, 0, 0 },
		{	(float)rc.p.x + (float)rc.s.x,	(float)rc.p.y + (float)rc.s.y,	0, 0, 0 },
		{	(float)rc.p.x,					(float)rc.p.y + (float)rc.s.y,	0, 0, 0 }
	};
	
	float uvs [8];
	area.walk_round<true> (uvs);
	
	glVertexPointer(3, GL_FLOAT, 0, v);
	glEnableClientState(GL_VERTEX_ARRAY);
	
	glVertexPointer(2, GL_FLOAT, 0, uvs);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	
//	glTexEnvfv (GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, color.ptr ());
	GL(glColor4f, (color.r, color.g, color.b, color.a));
	stir::gles::set_combiner (GL_MODULATE, GL_SRC_COLOR, GL_TEXTURE, GL_CONSTANT,
							  GL_MODULATE, GL_SRC_ALPHA, GL_TEXTURE, GL_CONSTANT);
	
	if (srcBlendMode != blend_off)
	{
		GL(glBlendFunc, (srcBlendMode, dstBlendMode));
		GL(glEnable, (GL_BLEND));
	}
	else
		GL(glDisable, (GL_BLEND));
	
	GL(glDrawElements, (GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, g_quad_indices));
	//	glDrawArrays(GL_TRIANGLE_STRIP, 0, 2);
	
	stir::gles::disable_client_states ();
	
#elif STIR_GLES2
	assert (g_imm);
	
	g_imm->draw_rect_uv(rc, tex, area, color, srcBlendMode, dstBlendMode);
	if (instant_imm) g_imm->sequence(0);

#endif
}
	
void STIR_ATTR_DLL draw_text (stir::font_renderer& fr, rectf const& r, color8 color, char const* text, text_style const& ts)
{
	if (!g_imm) init_imm(fr.device());
	
	g_imm->sequence(0);
	
	float sx = 0, sy = fr.ascender() * ts.scale;
	
	if (ts.alignFlags & (TextAlign::hcenter|TextAlign::right))
	{
		float w = fr.calc_width (ts.scale, text);
		if (ts.alignFlags & TextAlign::hcenter)
			sx = r.width () / 2.0f - w / 2.0f;
		else if (ts.alignFlags & TextAlign::right)
			sx = r.width () - w;
	}
	
	if (ts.alignFlags & (TextAlign::vcenter|TextAlign::bottom))
	{
		float h = fr.next_line() * ts.scale;
		if (ts.alignFlags & TextAlign::vcenter)
			sy = r.height () / 2 - h / 2;
		else if (ts.alignFlags & TextAlign::bottom)
			sy = r.height () - h;
		
		// now convert sy from top point to baseline
		sy += fr.ascender() * ts.scale;
	}
	
	stir::font_renderer::text_context ctx (vector2f(sx, sy), color, ts.scale, ts.condense);
	fr.render_text(ctx, r, text);
}

void begin_imm(device_context& dev)
{
	if (!g_imm) init_imm(dev);
}

void end_imm()
{
	g_imm->end();
}

}
