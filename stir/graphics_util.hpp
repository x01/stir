#ifndef STIR_GRAPHICS_UTIL_HPP_
#define STIR_GRAPHICS_UTIL_HPP_

#include "platform.hpp"
#include "device_context.hpp"
#include "texture.hpp"
#include "gui/font_ft.hpp"

#include "flat_types.hpp"

namespace stir {
	
enum blendmode
{
#if STIR_OPENGL
	blend_off = -1,
	blend_zero = GL_ZERO,
	blend_one = GL_ONE,
	blend_src_alpha = GL_SRC_ALPHA,
	blend_inv_src_alpha = GL_ONE_MINUS_SRC_ALPHA,
#elif STIR_DIRECTX
	blend_off = -1,
	blend_zero = D3D__,
	blend_one = D3D__,
	blend_src_alpha = D3D__,
	blend_inv_src_alpha = D3D__,
#endif
};

void begin_imm(device_context& dev);
void end_imm();
	
struct text_style
{
	float scale = 1.0f;
	float condense = 1.0f;
	TextAlign alignFlags = TextAlign::left;
};
	
extern text_style g_text_default, g_text_condensed, g_text_half, g_text_half_condensed, g_smaller_condensed;

void STIR_ATTR_DLL draw_rect (rectf const& rc, color8 color, blendmode srcBlendMode, blendmode dstBlendMode = blend_one);
void STIR_ATTR_DLL draw_rect_uv (rectf const& rc, stir::texture tex, stir::areaf const& area, color8 color, blendmode srcBlendMode, blendmode dstBlendMode = blend_one);
void STIR_ATTR_DLL draw_text (stir::font_renderer& fr, rectf const& r, color8 color, char const* text, text_style const& ts);
inline void draw_text (stir::font_renderer& fr, rectf const& r, color8 color, std::string const& text, text_style const& ts)
{
	draw_text (fr, r, color, text.c_str (), ts);
}

}

#endif
