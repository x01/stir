#ifndef STIR_FONT_DATA_HPP_
#define STIR_FONT_DATA_HPP_

#include "../platform.hpp"
#include "../flat_types.hpp"
#include "../math_types.hpp"
#include "../dynamic_array.hpp"
#include "../basic_cstring.hpp"
#include "../basic_util.hpp"

#include "gui_types.hpp"

namespace stir { namespace storage {
	
struct basic_font_data
{
	typedef uint16_t key_type;
	
	struct glyph
	{
		glyph() {}
		//		glyph (key_type code, int texid, recti16 const& uv, vector2i const& s, vector2i const& bear, int adv)
		glyph (key_type code, int texid, areaf const& uv, vector2f const& s, vector2f const& bear, int adv)
		:	size_(s), bearing_(bear), advance_(adv), uv_(uv), code_(code), texture_(texid)
		{}
		
		bool operator<(glyph const& a) const { return code_ < a.code_; }
		
		vector2f	size_, bearing_;
		float		advance_;
		areaf		uv_;
		uint16_t	code_;
		uint16_t	texture_;
	};
	
	struct kern
	{
		uint16_t	code_a_, code_b_;
		float		kerning_;
		
		bool operator<(kern const& a) const {
			if (code_a_ < a.code_a_)
				return true;
			else
				return code_b_ < a.code_b_;
		}
	};
	
	struct code_page
	{
		code_page () : glyph_offsets_ (0), first_index_ (0) {}
		code_page (uint16_t first, bool has_elements) :
		glyph_offsets_ (has_elements ? new uint16_t[256] : 0), first_index_(first) {}
		code_page (code_page const& a) : first_index_ (a.first_index_) {
			if (a.glyph_offsets_) {
				glyph_offsets_ = new uint16_t [256];
				std::copy (a.glyph_offsets_, a.glyph_offsets_ + 256, glyph_offsets_);
			} else
				glyph_offsets_ = 0;
		}
		code_page& operator= (code_page a) { a.swap (*this); return *this; }
		~code_page () {	delete []glyph_offsets_; }
		
		void swap (code_page& a) {
			std::swap (a.glyph_offsets_, glyph_offsets_);
			std::swap (a.first_index_, first_index_);
		}
		
		bool has_glyphs () const { return glyph_offsets_ != 0; }
		
		uint16_t*		glyph_offsets_;
		uint16_t		first_index_;
	};
	
	struct kern_first_less
	{
		bool operator ()(kern const& a, key_type b) const { return a.code_a_ < b; }
		bool operator ()(key_type a, kern const& b) const { return a < b.code_a_; }
	};
	
	struct glyph_code_less
	{
		bool operator ()(glyph const& a, glyph const& b) const { return a < b; }
		bool operator ()(key_type a, glyph const& b) const { return a < b.code_; }
		bool operator ()(glyph const& a, key_type b) const { return a.code_ < b; }
	};
	
	
	basic_font_data ()
	{}
	
	basic_font_data (float height, float next_line, float ascender, float descender, int spacing = 0)
	:	height_ (height), next_line_ (next_line), ascender_ (ascender), descender_ (descender)
	{}
	
	enum { CP_COUNT = 256 };
	
	typedef stir::dynamic_array<glyph>		container_t;
	typedef stir::dynamic_array<kern>		kern_container_t;
	
	container_t 				glyphs_;
	kern_container_t			kerning_;
	float 						height_, next_line_, ascender_, descender_;
};
	
class font_data : public basic_font_data
{
public:
	typedef stir::dynamic_array<castring>	texture_name_container_t;
	
	texture_name_container_t	texture_names_;
	code_page					code_pages_ [CP_COUNT];
	
	font_data (float height, float next_line, float ascender, float descender, int spacing = 0)
	:	basic_font_data (height, next_line, ascender, descender)
	{
	}
	
	void set_glyphs (glyph const* g, size_t count)
	{
		assert (stir::utility::is_sorted (g, g+count, glyph_code_less ()));
		glyphs_.resize (count);
		glyphs_.assign (g, g+count);

//		std::sort (glyphs_.begin(), glyphs_.end(), glyph_code_less ());
		
		// construct code-pages
		std::fill_n (code_pages_, 256, code_page (0, false));
		for (glyph const& g : glyphs_)
		{
			size_t index = &g - &*glyphs_.begin ();
			
			unsigned cp = (g.code_ & 0xff00) >> 8;
			unsigned code = g.code_ & 0x00ff;
			
			if (!code_pages_ [cp].has_glyphs ()) {
				code_pages_ [cp] = code_page (index, true);
				std::fill_n (code_pages_ [cp].glyph_offsets_, 256, (uint16_t)-1);
			}
			
			code_pages_ [cp].glyph_offsets_ [code] = index - code_pages_ [cp].first_index_;
		}
	}
	
	void set_kern_pairs (kern const* k, size_t count)
	{
		assert (count == kerning_.size ());
		std::copy (k, k + count, kerning_.begin());
	}
	
	void set_tex_names (castring const* t, size_t count)
	{
		assert (count == texture_names_.size ());
		std::copy (t, t + count, texture_names_.begin());
	}

	glyph const& operator[] (key_type k) const
	{
		container_t::const_iterator it = stir::utility::binary_find (glyphs_.begin (), glyphs_.end (), k, glyph_code_less ());
		assert (it != glyphs_.end ());
		return *it;
	}

	glyph const* get_glyph (key_type k) const
	{
		unsigned cp = (k & 0xff00) >> 8;
		
		if (code_pages_ [cp].has_glyphs ())
		{
			unsigned code = k & 0x00ff;
			unsigned o = code_pages_ [cp].glyph_offsets_ [code];
			
			return (o == (unsigned)-1)
				? 0
				: &glyphs_ [code_pages_ [cp].first_index_ + o];
		}
		
		return 0;
	}
	
	float get_kerning (key_type first, key_type second) const
	{
		kern_container_t::const_iterator it =
		stir::utility::binary_find (kerning_.begin (), kerning_.end (), first, kern_first_less ());
		return it == kerning_.end () ? 0 : it->kerning_;
	}
	
/*	int calc_width( wchar_t const* str, size_t len, int scale ) const
	{
		using namespace font_math;
		int totalLen = 0;
		
		key_type prevChar = 0;
		for( size_t i=0; i<len; ++i )
		{
			glyph const& g = (*this)[ str[i] ];
			
			int kern = get_kerning( prevChar, str[i] );

			totalLen += mulshift(g.advance + kern, scale);
			prevChar = str[i];
		}
		
		return totalLen;
	}*/

private:
};

}}

namespace stir { namespace storage {

template<class ArchiveT>
inline void serialize (ArchiveT& ar, font_data& o, unsigned version)
{
	ar	| o.glyphs_
		| o.kerning_
		| o.texture_names_
		| o.height_
		| o.next_line_
		| o.ascender_
		| o.descender_
		| o.code_pages_
		;
}

template<class ArchiveT>
inline void serialize (ArchiveT& ar, font_data::kern& o, unsigned version)
{
	ar	| o.code_a_
		| o.code_b_
		| o.kerning_
		;
}

template<class ArchiveT>
inline void serialize (ArchiveT& ar, font_data::glyph& o, unsigned version)
{
	ar	| o.code_
		| o.texture_
		| o.uv_
		| o.size_
		| o.bearing_
		| o.advance_
		;
}

template<class ArchiveT>
inline void serialize (ArchiveT& ar, font_data::code_page& o, unsigned version)
{
	using namespace pulmotor;
	ar	| o.first_index_
		| ptr (o.glyph_offsets_, 256);
		;
}

} } // stir :: storage

#endif
