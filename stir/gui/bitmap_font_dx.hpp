#ifndef STIR_GUI2_FONT_HPP_
#define STIR_GUI2_FONT_HPP_

#include <stir/platform.hpp>
#include <stir/dynamic_array.hpp>
#include <stir/flat_types.hpp>

#include "font_data.hpp"

#if STIR_IPHONE

#include "bitmap_font_gl.hpp"

#else

#include <stir/windows/dx9/texture.hpp>
#include <d3d9.h>

namespace stir { namespace gui {

namespace font_math
{
	inline int mulshift (int a, int b) {
		return a * b >> 10;
	}
	inline int mul (int a, int b) {
		return a * b;
	}
	inline int shift (int a) {
		return a >> 6;
	}
}

#pragma pack (push, 1)
struct font_vertex
{
	float			x, y;
	unsigned int	color;
	float			u, v;
};
#pragma pack (pop)
	
inline float scf (int x, int scale) {
	// 26:6 for position
	// 4:10 for scale
	//	return float (double (x * scale) / double(1 << (15 + 6)));
	return float (x * scale << (6+10));
}

inline float scf (int x, float scale) {
	// 26:6 for position
	// 4:10 for scale
	return float (double (x * scale) / double(1 << 6));
}
	
void build_glyph (font_data::glyph const& g, vector2f& screen_pos,
				  unsigned scale, unsigned color, float tkx, float tky, font_vertex* vtx);
	
// scale: 15 bits
void build_mesh (
	font_data const& font, vector2f& screen_pos, unsigned scale,
	font_vertex* vtx, wchar_t const* str, size_t str_length,
	int char_spacing, unsigned color, vector2f const& texScale)
{
	font_data::key_type prev = 0;
	for (font_data::key_type const* end = str + str_length; str != end; str++, vtx += 4)
	{
		font_data::glyph const* g = font.get_glyph (*str);
		
		if (!g)
			continue;

		float kern = scf (font.get_kerning (prev, *str), scale);
		float adv = scf (g->advance_, scale);

		prev = *str;

		build_glyph (*g, screen_pos, scale, color, texScale.x, texScale.y, vtx);
		
		screen_pos.x += adv + kern;
	}
}

}

class font_runtime
{
	typedef dynamic_array<directx9::texture> texture_cont;
	texture_cont	textures_;
	
public:
	font_runtime (gui::font_data const& fd, directx9::texture_provider& texget)
	:	textures_ (fd.texture_names_.size())
	{
		texture_cont::iterator o = textures_.begin ();
		for (cstring const& name : fd.texture_names_)
			*o++ = texget.get (name.c_str());
	}
	
	directx9::texture get_texture (int index) const {
		return textures_ [index];
	}
};

class dx9_font_renderer : public directx9::lost_reset_interface
{
	directx9::device&						m_device;
	
	com_ptr<IDirect3DVertexBuffer9>			m_vb;
	com_ptr<IDirect3DIndexBuffer9>			m_ib;
	com_ptr<IDirect3DVertexDeclaration9>	m_vdecl;
	
	size_t									m_vb_size, m_vb_offset;

public:
	static const int VB_GLYPHS = 256;

	dx9_font_renderer (directx9::device& dev)
	:	m_device (dev)
	,	m_vb_size (VB_GLYPHS * 4), m_vb_offset (0)
	{
		m_device.register_listener (*this);
	}
	
	~dx9_font_renderer ()
	{
		m_device.remove_listener (*this);
	}
	
	size_t lock_vertices (size_t num, size_t& locked_offset, void*& p)
	{
		HRESULT hr;
		size_t can_lock = (std::min) (num, m_vb_size);
		if (m_vb_offset + can_lock > m_vb_size) {
			locked_offset = m_vb_offset = 0;
			hr = m_vb->Lock (m_vb_offset * sizeof(gui::font_vertex), can_lock * sizeof(gui::font_vertex), &p, D3DLOCK_DISACRD);
		} else {
			locked_offset = m_vb_offset;
			hr = m_vb->Lock (m_vb_offset * sizeof(gui::font_vertex), can_lock * sizeof(gui::font_vertex), &p, D3DLOCK_NOOVERWRITE);
		}

		m_vb_offset += can_lock;
		return can_lock;
	}
	
	void unlock_vertices (size_t offset, size_t count)
	{
		HRESULT hr = m_vb->Unlock ();
	}

	int build_geometry (gui::font_data const& fd, gui::font_vertex* output, wchar_t const* str, size_t strsize, vector2f& screen_pos, unsigned scale, unsigned spacing, unsigned color)
	{
		build_mesh (
			fd, screen_pos, scale,
			output, str, strsize, spacing, color
		);
	}

	void submit_geometry (gui::font_data const& fd, wchar_t const* str, size_t strsize, vector2f const& screen_pos, short scale, short spacing, unsigned color)
	{		
		HRESULT hr = S_OK;
		font_vertex* vtx = 0;
		size_t locked_offset;
		for (int consumed=0, allocated=0;
			allocated = lock_vertices ((strsize-consumed)*4, locked_offset, vtx), consumed < strsize;
			consumed += allocated)
		{
			build_mesh (fd, screen_pos, scale, vtx, str + consumed, allocated, spacing, color);
			unlock_vertices ();
			
			for (effect::pass p(m_effect); p.valid(); ++p)
			{
				hr = m_device.device().SetVertexDeclaration (m_vdecl);
				hr = m_device.device().SetIndices (m_ib);
				hr = m_device.device().SetStreamSource(0, m_vb, locked_offset * sizeof font_vertex, sizeof font_vertex);
				hr = m_device.device().DrawIndexedPrimitive (D3DPT_TRIANGLELIST, 0, 0, allocated * 4, 0, allocated / 3);
			}
		}
	}

	// device_context
	virtual void device_reset()
	{
		D3DVERTEXELEMENT9 decl[] =
		{
			{	0, 0, D3DDECLTYPE_FLOAT2,	D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION,	0 },
			{	0, 8, D3DDECLTYPE_FLOAT2,	D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD,	0 },
			{	0, 16,D3DDECLTYPE_D3DCOLOR,	D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR,		0 },
			D3DDECL_END()
		};
		
		m_vdecl.attach() = create_vertex_declaration (m_devctx.device(), decl);
		
		m_ib.attach() = create_index_buffer (m_devctx.device(), VB_GLYPHS * 6, D3DFMT_INDEX16, 0,
			m_devctx.is_mixed_vp() ? D3DPOOL_SYSTEMMEM : D3DPOOL_DEFAULT);
		make_triangle_list_indices (*m_ib, VB_GLYPHS);
	
		m_vb.attach() = create_dynamic_vertex_buffer (m_device.device (), VB_GLYPHS * sizeof(font_vertex), 0);
		m_vb_offset = 0;
	}

	virtual void device_lost()
	{
		m_vb.reset ();
		m_ib.reset ();
	}
};

/*
class font_renderer : public stir::dx9::lost_reset_interface
{
	font_data&				m_font_data;
	device_context&			m_device;
	effect					m_effect;
	dynamic_array<texture>	m_textures;
	
	com_ptr<IDirect3DVertexBuffer9>			m_vb;
	com_ptr<IDirect3DIndexBuffer9>			m_ib;
	com_ptr<IDirect3DVertexDeclaration9>	m_vdecl;
	
#pragma pack( push, 1 )
	struct font_vertex
	{
		float		x, y;
		unsigned	color;
		float		u, v;
	};
#pragma pack( pop )

public:
	static const int VB_GLYPHS = 256;

	font_renderer (stir::device_context& dev, font_data& font, texture_getter& texget, effect efx)
	:	m_device (dev), m_font_data (font), m_effect (efx), m_texget (texget)
	,	m_vb_size (VB_GLYPHS * 4), m_vb_offset (0), m_textures (font.texture_names_.size())
	{
		m_device.register_listener (*this);
		
		// fetch textures
		texture_cont::iterator o = m_textures.begin ();
		for (cstring const& name : m_font_data.texture_names_)
			*o++ = texget.get (name.c_str());
	}
	
	~font_renderer ()
	{
		m_device.remove_listener (*this);
	}
	
	size_t lock_vertices (size_t num, size_t& locked_offset, void*& p)
	{
		HRESULT hr;
		size_t can_lock = std::min (num, m_vb_size);
		if (m_vb_offset + can_lock > m_vb_size) {
			locked_offset = m_vb_offset = 0;
			hr = m_vb->Lock (m_vb_offset * sizeof(font_vertex), can_lock * sizeof(font_vertex), &p, D3DLOCK_DISACRD);
		} else {
			locked_offset = m_vb_offset;
			hr = m_vb->Lock (m_vb_offset * sizeof(font_vertex), can_lock * sizeof(font_vertex), &p, D3DLOCK_NOOVERWRITE);
		}

		m_vb_offset += can_lock;
		return can_lock;
	}
	
	void unlock_vertices (size_t offset, size_t count)
	{
		HRESULT hr = m_vb->Unlock ();
	}

	int build_geometry (font_vertex* output, wchar_t const* str, size_t strsize, vector2& screen_pos, unsigned scale, unsigned spacing, unsigned color)
	{
		build_mesh (
			m_font, screen_pos, scale,
			output, str, strsize, spacing, color
		);
	}

	void render (wchar_t const* str, size_t strsize, vector2 const& screen_pos, short scale, short spacing, unsigned color)
	{		
		HRESULT hr = S_OK;
		font_vertex* vtx = 0;
		size_t locked_offset;
		for (int consumed=0, allocated = 0;
				allocated = lock_vertices ((strsize-consumed)*4, locked_offset, vtx), consumed < strsize;
				consumed += allocated)
		{
			build_mesh (m_font, screen_pos, scale, vtx, str + consumed, allocated, spacing, color);
			unlock_vertices ();
			
			for (effect::pass p(m_effect); p.valid(); ++p)
			{
				hr = m_device.device().SetVertexDeclaration (m_vdecl);
				hr = m_device.device().SetIndices (m_ib);
				hr = m_device.device().SetStreamSource(0, m_vb, locked_offset * sizeof font_vertex, sizeof font_vertex);
				hr = m_device.device().DrawIndexedPrimitive (D3DPT_TRIANGLELIST, 0, 0, allocated * 4, 0, allocated / 3);
			}
		}
	}

	// device_context
	virtual void device_reset()
	{
		D3DVERTEXELEMENT9 decl[] =
		{
			{	0, 0, D3DDECLTYPE_FLOAT2,	D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION,	0 },
			{	0, 8, D3DDECLTYPE_FLOAT2,	D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD,	0 },
			{	0, 16,D3DDECLTYPE_D3DCOLOR,	D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR,		0 },
			D3DDECL_END()
		};
		
		m_vdecl.attach() = create_vertex_declaration (m_devctx.device(), decl);
		
		m_ib.attach() = create_index_buffer (m_devctx.device(), VB_GLYPHS * 6, D3DFMT_INDEX16, 0,
			m_devctx.is_mixed_vp() ? D3DPOOL_SYSTEMMEM : D3DPOOL_DEFAULT);
		make_triangle_list_indices (*m_ib, VB_GLYPHS);
	
		m_vb.attach() = create_dynamic_vertex_buffer (m_device.device (), VB_GLYPHS * sizeof(font_vertex), 0);
		m_vb_offset = 0;
	}

	virtual void device_lost()
	{
		m_vb.reset ();
		m_ib.reset ();
	}
};
*/

/*
void render (font_renderer& fr, vector2 const& position, wchar_t const* str, size_t length, unsigned scale)
{
	HRESULT hr = S_OK;
	font_vertex* vtx = 0;
	size_t locked_offset;
	for (int consumed=0, allocated = 0;
			allocated = lock_vertices ((strsize-consumed)*4, locked_offset, vtx), consumed < strsize;
			consumed += allocated)
	{
		build_mesh (m_font, screen_pos, scale, vtx, str + consumed, allocated, spacing, color);
		unlock_vertices ();
		
		for (effect::pass p(m_effect); p.valid(); ++p)
		{
			hr = m_device.device().SetVertexDeclaration (m_vdecl);
			hr = m_device.device().SetIndices (m_ib);
			hr = m_device.device().SetStreamSource(0, m_vb, locked_offset * sizeof font_vertex, sizeof font_vertex);
			hr = m_device.device().DrawIndexedPrimitive (D3DPT_TRIANGLELIST, 0, 0, allocated * 4, 0, allocated / 3);
		}
	}
}


void render (vector2 const& p, wchar_t const* str, size_t len, float scale)
{
	vector2 cur_pos = p;
	
	for (int i=0; i<len; ++i)
	{
		glyph const& g = (*this)[ str[i] ];
		
		float kern = 0;
		float kern = get_kerning( prevChar, str[i] );
		
		add_rect (g.tex, g.uv, cur_pos + g.size * scale, cur_pos + g.offset * scale);

		totalLen += (g.advance + kern) * scale;
		prevChar = str[i];
	}
}*/

}

#endif

#endif
