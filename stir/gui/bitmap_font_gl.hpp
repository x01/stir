#ifndef STIR_FONT_RENDERER_GL_HPP_
#define STIR_FONT_RENDERER_GL_HPP_

#include "stir/platform.hpp"

#include "bitmap_font_data.hpp"
#include "bitmap_font_util.hpp"
#include <stir/static_vector.hpp>
#include <stir/resfwd.hpp>
#include <stir/gles/gles.h>
#include <stir/texture.hpp>
#include <stir/resmgr.hpp>
#include <stir/r_fontdata.hpp>
#include <stir/r_gpuprog.hpp>

namespace stir {
	
	typedef storage::font_data font_data;
	
	inline float scf (int x, int scale) {
		// 26:6 for position
		// 4:10 for scale
		//	return float (double (x * scale) / double(1 << (15 + 6)));
		return float (x * scale) / float(1 << (6+10));
	}
	
	inline float scf (int x, float scale) {
		// 26:6 for position
		// 4:10 for scale
		return float (double (x * scale) / double(1 << 6));
	}
	
	inline int mulshift (int a, int b) {
		return a * b >> (10 + 6);
	}
	inline float mulshiftf (int a, int b) {
		return a * b / float (1 << (10 + 6));
	}
	
	inline int mul (int a, int b) {
		return a * b;
	}
	inline int shift (int a) {
		return a >> 6;
	}
	
	struct gvertex
	{
		float			x,y;
		stir::color8	color;
		float			u, v;
	};
	
	inline void build_glyph (font_data::glyph const& g, stir::vector2f const& screen_pos, float scale, stir::color8 color, gvertex*& vtx)
	{
		float w = g.size_.x * scale;
		float h = g.size_.y * scale;
		float x = screen_pos.x + g.bearing_.x * scale;
		float y = screen_pos.y - g.bearing_.y * scale;
				
		vtx->x = x;
		vtx->y = y;
		vtx->u = g.uv_.p0.x;
		vtx->v = g.uv_.p0.y;
		vtx->color = color;
		++vtx;
		
		vtx->x = x + w;
		vtx->y = y;
		vtx->u = g.uv_.p1.x;
		vtx->v = g.uv_.p0.y;
		vtx->color = color;
		++vtx;
		
		vtx->x = x + w;
		vtx->y = y + h;
		vtx->u = g.uv_.p1.x;
		vtx->v = g.uv_.p1.y;
		vtx->color = color;
		++vtx;
		
		vtx->x = x;
		vtx->y = y + h;
		vtx->u = g.uv_.p0.x;
		vtx->v = g.uv_.p1.y;
		vtx->color = color;
		++vtx;
	}
	
class gl_font_renderer
{
	static const int BATCH_CHARS = 256;
	static const int MAX_TEX = 8;
	
	typedef static_vector<rindex_t, MAX_TEX> tex_container_t;
	
	gles::device_context&	device_;
	rindex_t				font_data_;
	
	gvertex*				buffer_;
	u16*					indices_;
	int						active_texture_;
	tex_container_t			textures_;
	rindex_t				gpu_program_;
	
public:	
	gl_font_renderer (gles::device_context& dev, rindex_t fontId, rindex_t gpuProg = rinvalid)
	:	device_(dev)
	,	font_data_(fontId)
	,	active_texture_ (-1)
	,	gpu_program_(gpuProg)
	{
		// load default shader
		if (gpu_program_ == rinvalid)
		{
			gpu_program_ = stir::r_gpuprog.load_cached ("vs:font-shader.vert|fs:font-shader.frag");
		}
		
		font_data const& fontData = *stir::r_fontdata.resolve (font_data_);
		
		for (castring const& name : fontData.texture_names_)
		{
			rindex_t texId = stir::r_texture.load_cached (name.c_str());
			textures_.push_back (texId);
		}
		
		size_t vtxcount = BATCH_CHARS * 4;
		size_t idxcount = BATCH_CHARS * 6;
		buffer_ = new /*(ALID(alid::geometry))*/ gvertex [vtxcount];
		indices_ = new /*(ALID(alid::geometry))*/ u16 [idxcount];
		
		for (int i=0; i<BATCH_CHARS; ++i)
		{
			size_t o = i * 6;
			size_t v = i * 4;
			
			indices_ [o + 0] = v + 0;
			indices_ [o + 1] = v + 1;
			indices_ [o + 2] = v + 2;
			
			indices_ [o + 3] = v + 2;
			indices_ [o + 4] = v + 3;
			indices_ [o + 5] = v + 0;
		}
	}
	
	~gl_font_renderer ()
	{
		for (rindex_t idx : textures_)
			stir::r_texture.free (idx);
		
		delete[] /*(AL_ID(alid::geometry))*/ buffer_;
		delete[] indices_;
	}
	
	enum FormatFlags
	{
		wrap		= 0x01,
		wrap_words	= 0x02
	};
	
	static inline bool isblank (int ch)
	{
		return ch == ' ' || ch == '\t';
	}
	
	// Only for temporary use, do not store a ref to it.
	stir::storage::font_data const& get_font_data () const
	{
		return *stir::r_fontdata.resolve (font_data_);
	}
	
	// font height, whatever that means
	float height () const
	{
		font_data const& fontData = *stir::r_fontdata.resolve (font_data_);
		return fontData.height_;
	}
	
	// distance between two baselines
	float next_line () const
	{
		font_data const& fontData = *stir::r_fontdata.resolve (font_data_);
		return fontData.next_line_;
	}
	
	// from baseline to most top
	float ascender () const
	{
		font_data const& fontData = *stir::r_fontdata.resolve (font_data_);
		return fontData.ascender_;
	}
	
	// from baseline to most bottom
	float descender () const
	{
		font_data const& fontData = *stir::r_fontdata.resolve (font_data_);
		return fontData.descender_;
	}
	
	void flush (gvertex* buffer, size_t glyphsFilled, rindex_t texIdx)
	{
		assert (glyphsFilled <= BATCH_CHARS);
		
#if STIR_GLES1
		// render what's in the buffer
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
				
		glVertexPointer(2, GL_FLOAT, sizeof(gvertex), (char*)buffer + offsetof(gvertex, x));
		glColorPointer(4, GL_UNSIGNED_BYTE, sizeof(gvertex), (char*)buffer + offsetof(gvertex, color));
		glTexCoordPointer(2, GL_FLOAT, sizeof(gvertex), (char*)buffer + offsetof(gvertex, u));
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		
		glEnable(GL_TEXTURE_2D);
		stir::texture tex = r_texture.resolve (texIdx);
		glBindTexture(GL_TEXTURE_2D, tex->tex_name);
		
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_BLEND);
		
		stir::gles::set_combiner (GL_REPLACE, GL_SRC_COLOR, GL_PRIMARY_COLOR, GL_TEXTURE,
								  GL_MODULATE, GL_SRC_ALPHA, GL_TEXTURE, GL_PRIMARY_COLOR);
#elseif STIR_GLES2
#error "impl!"
#endif
		
		glDrawElements(GL_TRIANGLES, (GLsizei)glyphsFilled * 6, GL_UNSIGNED_SHORT, indices_);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	
	struct text_context
	{
		explicit text_context (vector2f const& lpos = vector2f(0,0))
		:	localPos (lpos), color(color8::abgr(0xffffffff)), scale(1.0f)
		{}
		text_context (vector2f const& lpos, color8 color_, float textScale)
		:	localPos (lpos), color(color_), scale(textScale)
		{}
		vector2f	localPos;
		color8		color;
		float		scale;
	};

	void render (text_context& ctx, rectf const& r, wchar_t const* text, int format = wrap|wrap_words)
	{
		ctx.localPos = render (ctx.localPos, r, ctx.color, ctx.scale, text, format);
	}
	
	// localPos - baseline (bottom) position where to render relative to 'r'
	// r - destination rectangle where to render text
	vector2f render (vector2f const& localPos, rectf const& r, color8 color, float scale, wchar_t const* text, int format = wrap|wrap_words)
	{
		gvertex const* vertexEnd = buffer_ + BATCH_CHARS * 4;
		
		size_t const CACHE_SIZE = 16;
		struct { storage::font_data::glyph const* ptr; float kern; } cache [CACHE_SIZE];
		
		color8 smallColor = color;
		int batchStart = 0;
		
		vector2f screenPos = localPos;
		
//		glMatrixMode(GL_PROJECTION);
//		glLoadIdentity();		
//		glOrthof (0, device_.width(), device_.height(), 0, -1.0f, 0);
//		glMatrixMode(GL_MODELVIEW);
//		glLoadIdentity();

		gvertex* pv = buffer_;
		
		font_data const& fontData = *stir::r_fontdata.resolve (font_data_);
		
		while (1)
		{
			size_t cached = 0;			
			float lw = 0;
			bool done = false;
			int ch, idx, safe = batchStart, prevChar = -1;

			if ((format & wrap_words) || (format & wrap))
			{
				for (idx=batchStart; ;)
				{
//					wchar_t const* curChar = text + idx;
					if ((ch = text [idx]) == 0)
					{
						safe = idx;
						done = true;
						break;
					}
					
					if (ch == '\n') {
						safe = idx;
						break;
					}
					
					font_data::glyph const& gd = fontData [ch];
					float kern = fontData.get_kerning (prevChar, text[idx]);
					if (cached < CACHE_SIZE) {
						cache [cached].ptr = &gd;
						cache [cached++].kern = kern;
					}
					
					float thisAdvance = (gd.advance_ + kern) * scale;
					
					if ((format & wrap_words)) {
						if (isblank (ch))
							safe = idx;

						if (lw + gd.size_.x * scale > r.width ())
						{
							// we may have long text without spaces
							if (batchStart == safe)
								safe = idx;
							
							break;
						} 
						
					} else if (format & wrap) {
						if (lw + gd.size_.x * scale > r.width ())
						{
							safe = idx;
							break;
						}
					}
					
					lw += thisAdvance;
					++idx;
				}
				
				// render cache, then others
				int renderCached = std::min ((int)cached, safe-batchStart);
				for (int i=0; i<renderCached; ++i)
				{
					font_data::glyph const& gd = *cache[i].ptr;					
					if (active_texture_ != gd.texture_ || pv == vertexEnd)
					{
						active_texture_ = gd.texture_;
						if (pv != buffer_)
						{
							flush (buffer_, (pv - buffer_) >> 2, textures_ [active_texture_]);
							pv = buffer_;
						}
					}
					
					build_glyph (gd, r.p + screenPos, scale, smallColor, pv);
					screenPos.x += (gd.advance_ + cache[i].kern) * scale;
				}
				
				for (int i=batchStart + renderCached; i<safe; ++i)
				{
					ch = text[i];

					font_data::glyph const& gd = fontData [ch];
					float kern = fontData.get_kerning (prevChar, ch);					

					if (active_texture_ != gd.texture_ || pv == vertexEnd)
					{
						active_texture_ = gd.texture_;
						if (pv != buffer_)
						{
							flush (buffer_, (pv - buffer_) >> 2, textures_ [active_texture_]);
							pv = buffer_;
						}
					}
					
					build_glyph (gd, r.p + screenPos, scale, smallColor, pv);
					screenPos.x += (gd.advance_ + kern) * scale;
					
					prevChar = ch;					
				}

				// we have finished generating geometry, render the buffer
				if (done)
				{
					flush (buffer_, (pv - buffer_) >> 2, textures_ [active_texture_]);
					break;
				}
				
				// move to the next line
				float lineAdvance = fontData.next_line_ * scale;
				screenPos.x = localPos.x;
				screenPos.y += lineAdvance;
				
				if (screenPos.y > r.height() - lineAdvance)
					break;
				
				// move next batch to start not on space until first non-whitespace
				while (text [safe] != 0 && isblank (text [safe]))
					++safe;
				
				// in case we have stopped at a new-line, skip it
				if (text [safe] == '\n')
					++safe;
				
				batchStart = safe;
			}
		}
		
#if STIR_GLES1	
		stir::gles::disable_client_states ();
#endif
		
		return screenPos;
	}
	
	float calc_width (float scale, wchar_t const* text, size_t count)
	{
		return stir::calc_width (*(r_fontdata % font_data_), scale, text, count);
	}
	
	float calc_width (float scale, wchar_t const* text)
	{
		return stir::calc_width (*(r_fontdata % font_data_), scale, text);
	}
	
};
	
}

#endif // STIR_FONT_RENDERER_GL_HPP_
