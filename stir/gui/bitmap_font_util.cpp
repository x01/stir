#include <stir/platform.hpp>
#include <stir/resmgr.hpp>

#include "bitmap_font_gl.hpp"
#include "bitmap_font_util.hpp"
#include "bitmap_font_data.hpp"


namespace stir {

namespace detail
{
	inline float calc_width (storage::font_data const& fd, int ch, int& prev)
	{
		storage::font_data::glyph const& gd = fd [ch];
		float w = (gd.advance_ + fd.get_kerning (prev, ch));
		prev = ch;
		return w;
	}
}

float calc_width (storage::font_data const& fontdata, float scale, wchar_t const* text, size_t count)
{
	float w = 0;
	int prevChar = -1;
	
	// TODO: check result for correctness of rendering if
	// multiplication by the scale is moved out of the loop
	for (size_t i=0; i<count; ++i)
		w += detail::calc_width (fontdata, text[i], prevChar) * scale;
	
	return w;
}
	
float calc_width (storage::font_data const& fontdata, float scale, wchar_t const* text)
{
	float w = 0;
	int prevChar = -1;
	
	while (*text)
		w += detail::calc_width (fontdata, *text++, prevChar) * scale;
	
	return w;
}

}
