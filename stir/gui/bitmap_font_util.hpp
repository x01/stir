#ifndef STIR_GUI_FONT_UTIL_HPP_HPP_
#define STIR_GUI_FONT_UTIL_HPP_HPP_

#include <stir/flat_types.hpp>
#include <stir/basic_cstring.hpp>
#include <stir/r_fontdata.hpp>

namespace stir {
	
class gl_font_renderer;
	
float calc_width (storage::font_data const& fontdata, float scale, wchar_t const* text, size_t codePointCount);
float calc_width (storage::font_data const& fontdata, float scale, wchar_t const* text);
	
}

#endif // STIR_GUI_FONT_UTIL_HPP_HPP_



