uniform sampler2D font_tex;

varying highp vec2 tex_coord;
varying mediump vec4 glyph_color;

void main()
{
	mediump vec4 texC = texture2D(font_tex, tex_coord);
	
	//	gl_FragColor = vec4(glyph_color.rgb * texC.a, glyph_color.a * texC.a);
	gl_FragColor = vec4(glyph_color.rgb, texC.a);
}
