#pragma glsl_1.1

attribute vec2 in_pos;
attribute vec4 in_color;
attribute vec2 in_uv;

uniform mat4 proj_mat;
uniform vec4 font_color;

varying highp vec2 tex_coord;
varying lowp vec4 glyph_color;

void main()
{
	tex_coord = in_uv;
	glyph_color = font_color * in_color;
	gl_Position = proj_mat * vec4(in_pos, 0, 1);
}