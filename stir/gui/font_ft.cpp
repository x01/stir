#include "font_ft.hpp"

#include <stir/stir_utf.hpp>
#include <stir/filesystem.hpp>
#include <stir/stir_logs.hpp>
#include <stir/partition.hpp>
#include <stir/r_gpuprog.hpp>
#include <stir/gles/gles_shader.hpp>
#include <stir/resmgr.hpp>
#include <system_error>

namespace stir {
	
struct fterr_desc { int errcode; char const* msg; } ft_err_strs[] =
{
#undef __FTERRORS_H__
#define FT_ERRORDEF( e, v, s )  { e, s },
#include FT_ERRORS_H
};

class freetype_error_category_impl : public std::error_category {
public:
	virtual const char* name() const noexcept {
		return "freetype_error";
	}
	
	virtual std::string message(int ev) const {
		for (auto const& desc : ft_err_strs)
			if (desc.errcode == ev)
				return desc.msg;
		return "freetype error";
	}
};

std::error_category& ft_error_category ()
{
	static freetype_error_category_impl errcat;
	return errcat;
}
	
FT_Library ft_init (std::error_code& ec) {
	FT_Library lib = nullptr;
	ec = (fterr)FT_Init_FreeType (&lib);
	return lib;
}
	
void ft_free(FT_Library lib) {
	FT_Done_FreeType(lib);
}
	
	
ft_library::ft_library ()
{
	std::error_code err = (fterr)FT_Init_FreeType (&m_library);
	if (err) {
		printf("Failed to initialize FreeType\n");
	}
}

ft_library::~ft_library ()
{
	FT_Done_FreeType(m_library);
}
	
	
ft_font_generator::ft_font_generator(FT_Library ftLib, path const& fontPathName, std::error_code& err)
:	m_library(ftLib)
#ifdef DEBUG
,	m_font_pathname(fontName.string())
#endif
,	m_face(nullptr)
{
	FT_Error error;
	
//	error = FT_New_Face (m_library, m_font_pathname.c_str(), 0, &m_face );
//	if (error != FT_Err_Ok) {
//		err = (fterr)error;
//		return;
//	}
	
	std::error_code ec;
	filesystem::file_handle fh (fontPathName, filesystem::permission::read, ec);
	
	size_t fileSize = stir::filesystem::file_size(fh);
	if (fileSize <= 0) {
		err = (fterr)FT_Err_Cannot_Open_Stream;
		return;
	}
	
	m_font_data.resize(fileSize);
	if (filesystem::read_file(fh, m_font_data.data(), fileSize) != fileSize)
		err = (fterr)FT_Err_Invalid_Stream_Read;
	
	error = FT_New_Memory_Face(m_library, m_font_data.data(), m_font_data.size(), 0, &m_face);
	if (error != FT_Err_Ok) {
		err = (fterr)error;
		return;
	}
	
	err.clear ();
}

ft_font_generator::~ft_font_generator() {
	if (m_face)
		FT_Done_Face(m_face);
}
	
static char const* ftstr(char const* a, char const* def) { return a ? a : def; }
	
void ft_font_generator::set_pixel_sizes(int heightInPixels, std::error_code& err)
{
	m_height_pix = heightInPixels;
	err = (fterr)FT_Set_Pixel_Sizes (m_face, 0, heightInPixels);
	if (err)
		return;
	
	FT_Size_Metrics& met = m_face->size->metrics;
	
	m_height = ftf(met.height);
	m_ascender = ftf(met.ascender);
	m_descender = ftf(met.descender);
	
	STIR_LOG(font).printf("font %s/%s loaded: h:%3.2f pxh:%d asc:%3.2f dsc:%3.2f\n",
						  ftstr(m_face->family_name, "<unnamed>"), ftstr(m_face->style_name, "<unnamed>"),
						  m_height, m_height_pix, m_ascender, m_descender);
}
	
void ft_font_generator::rasterize (int charcode, std::error_code& ec)
{
	int glyph_index = FT_Get_Char_Index(m_face, charcode);
	
	FT_Error error;
	error = FT_Load_Glyph (m_face, glyph_index, FT_LOAD_DEFAULT);
	if (error) {
		ec = (fterr)error;
		return;
	}
	
	error = FT_Render_Glyph (m_face->glyph, FT_RENDER_MODE_NORMAL);
	if (error) {
		ec = (fterr)error;
		return;
	}
	
	ec.clear ();
}

bool ft_font_generator::get_result (image_ref& img)
{
	if (img.width < m_face->glyph->bitmap.width || img.height < m_face->glyph->bitmap.rows)
		return false;
	
	img.put1 (0, 0, m_face->glyph->bitmap.buffer, m_face->glyph->bitmap.width, m_face->glyph->bitmap.rows, m_face->glyph->bitmap.pitch);
	//		memcpy (img.pixels, m_face->glyph->bitmap.buffer
	
	return true;
}


	// ft_texture_builder
	
ft_texture_builder::page_info::page_info(int w, int h, int border, int glyphHeight)
:	m_atlaser(w, h, border, glyphHeight)
//		, img(w,h,1)
{
#if STIR_OPENGL
	
	stir::image test (w, h, 1, image::align_pitch{4});
	for (int i=0; i<test.height; ++i) {
		std::generate_n(test.ptr_to(i, 0), test.width/2, []() { return rand()&0xff;} );
		std::generate_n(test.ptr_to(i, test.width/2), test.width/2, []() { return 0x80;} );
	}
	
	m_texture = gles::create_texture (w, h, 1, gl::PixelFormat::ALPHA, gl::PixelType::UNSIGNED_BYTE, test.ptr_to(0, 0));
	gl::BindTexture(gl::TextureTarget::TEXTURE_2D, m_texture->tex_name);
	gl::TexParameteri(gl::TextureTarget::TEXTURE_2D, gl::TextureParameterName::TEXTURE_MIN_FILTER, GL_LINEAR);
	gl::TexParameteri(gl::TextureTarget::TEXTURE_2D, gl::TextureParameterName::TEXTURE_MAG_FILTER, GL_LINEAR);
//	gl::TexParameteri(gl::TextureTarget::TEXTURE_2D, gl::TextureParameterName::TEXTURE_MIN_FILTER, GL_NEAREST);
//	gl::TexParameteri(gl::TextureTarget::TEXTURE_2D, gl::TextureParameterName::TEXTURE_MAG_FILTER, GL_NEAREST);
	//	m_texture = gles::create_texture (w, h, 1, gl::PixelFormat::ALPHA, gl::PixelType::UNSIGNED_BYTE, nullptr);
	//			tex = gles::create_texture (w, h, 1, gles::PixelFormat::rgba, gles::PixelType::ubyte, nullptr);
#else
	--> unsupported
#endif
}

ft_texture_builder::page_info::~page_info ()
{
#if STIR_OPENGL
	gles::release_texture(m_texture);
#else
	--> unsupported
#endif
}
	
	
ft_texture_builder::atlaser::rect ft_texture_builder::atlaser::allocate (int w, int h) {
	if (h > m_glyph_height)
		return rect (0, 0, 0, 0, 0);
	
	int needW = w + 2*m_border;
	int leftW = m_size.x - m_used.right();
	if (leftW < needW)
	{
		int needH = m_glyph_height + 2*m_border;
		int leftH = m_size.y - m_used.bottom();
		if (leftH < needH)
			return rect (0, 0, 0, 0, 0);
		
		m_used.p.x = m_used.s.x = 0;
		m_used.p.y += m_used.s.y;
		m_used.s.y = needH;
	}
	
	int startX = m_used.s.x + m_border;
	int startY = m_used.p.y + m_border;
	m_used.s.x += needW;
	return rect (startX, startY, w, h, m_border);
}

	// ft_texture_builder
	
	u16 ind[] = { 0, 1, 2, 1, 3, 2 };

	struct ft_builder_debug : private gpuprog_listener
	{
		struct vtx { float x, y; float u, v; };
		
		gles::device_context& m_dev;
		gl::buffer m_buffer;
		gl::vao m_vao;
		gl::binding<2> m_binding;
		struct locs {
			gl::mat44 proj_mat;
			gl::sampler tex;
			gl::float4 color;
		} m_locs;
		
		rindex_t m_shader;
		
		void on_gpuprog_was_reloaded() override {
			fetch_gpuprog_info();
		}
		
		void fetch_gpuprog_info() {
			gpu_program& gpuprog = r_gpuprog % m_shader;
			m_locs.proj_mat = gpuprog.location<gl::mat44>("proj_mat");
			m_locs.tex = gpuprog.location<gl::sampler>("tex");
			m_locs.color = gpuprog.location<gl::float4>("color");
			m_binding.bind_attrs(gpuprog);
			m_vao.create(m_binding, m_buffer.m_buffer, 0);
		}
		
		ft_builder_debug(gles::device_context& dev)
		: m_binding( {
			{ "in_pos", 2, gl::bind_type::float_, false, 0, sizeof(vtx) },
			{ "in_uv", 2, gl::bind_type::float_, false, 8, sizeof(vtx) }
		}),
		m_dev(dev)
		{
			stir::r_gpuprog.data().add_listener(*this);
			m_shader = r_gpuprog.load_cached("vs:textured_vtx.vert|ps:color_tex.frag");
			fetch_gpuprog_info();
		}
		
		void render(std::list<ft_texture_builder::page_info> const& pages) {
			std::vector<vtx> verts (pages.size() * 4);
			for (size_t b=0; b<pages.size(); ++b) {
				stir::rectf r (0, 0, 300, 300);
				r.to_area().walk_round<true>((float*)&verts[0].x, sizeof(vtx));
				verts[b + 0].u = 0; verts[b + 0].v = 1;
				verts[b + 1].u = 1; verts[b + 1].v = 1;
				verts[b + 2].u = 0; verts[b + 2].v = 0;
				verts[b + 3].u = 1; verts[b + 3].v = 0;
			}
			
			m_buffer.bind();
			gl::buffer::data(gl::BufferTarget::ARRAY_BUFFER, verts.size()*sizeof(vtx), verts.data(), gl::BufferUsage::DYNAMIC_DRAW);
			
			stir::matrix4 m = matrix4::orthographic(0, m_dev.logical_width(), m_dev.logical_height(), 0, 0, 1);

			gpu_program& gpuprog = r_gpuprog % m_shader;
			gpuprog.use();
			
			gpuprog.set(m_locs.proj_mat, &m[0][0]);
			gpuprog.set(m_locs.color, 1,1,1,1);

			m_vao.use();
//			m_binding.use(m_buffer.m_buffer);
			
			for (ft_texture_builder::page_info const& p : pages) {
				
				// texture tex = r_texture % r_texture.load_cached("CliffRock");
				
				gpuprog.set_texture(m_locs.tex, p.get_tex()->tex_name, 0);
				//gpuprog.set_texture(m_locs.tex, tex->tex_name, 0);
				
				gl::DrawElements(gl::PrimitiveType::TRIANGLES, 6, gl::DrawElementsType::UNSIGNED_SHORT, ind);
//				gl::DrawArrays(gl::PrimitiveType::TRIANGLES, 0, 3);
			}
			m_vao.dont_use();
		}
	};
	
	
ft_texture_builder::ft_texture_builder(ft_font_generator& fg, int texW, int texH)
:	m_fg(fg), m_size(texW, texH)
,	m_gi(-1)
{
	m_gi.reserve(128);
	m_textures.emplace_back(m_size.x, m_size.y, 1, m_fg.height_pix());
}

ft_texture_builder::~ft_texture_builder ()
{
	for (auto kv : m_gi)
		delete kv.v();
	
	if (m_debug_impl)
		delete static_cast<ft_builder_debug*>(m_debug_impl);
}
	
void ft_texture_builder::render_pages(gles::device_context& dev)
{
	if (!m_debug_impl)
		m_debug_impl = new ft_builder_debug(dev);
	
	static_cast<ft_builder_debug*> (m_debug_impl)->render(m_textures);
}

void ft_texture_builder::preload(int begin, int end)
{
	if (begin > end)
		std::swap(begin, end);
	
	while (begin < end)
		get_glyph(begin++);
}

vector2f ft_texture_builder::get_kerning (int a, int b)
{
	FT_Vector k;
	FT_Get_Kerning(m_fg.face(), a, b, FT_KERNING_DEFAULT, &k);
	return vector2f( ftf (k.x), ftf(k.y));
}

ft_texture_builder::glyph_info* ft_texture_builder::get_glyph (int g)
{
	auto it = m_gi.find (g);
	if (it == m_gi.lean_end ())
		return new_glyph (g);
	
	return it->v();
}
	
ft_texture_builder::page_info* ft_texture_builder::find_spot(int w, int h, atlaser::rect& r)
{
	if (w < 1)
		w = 1;
	
	if (h < 1)
		h = 1;
	
	for (auto& page : m_textures) {
		r = page.alloc_rect(w, h);
		
		if (!r.empty())
			return &page;
		
//		if (page.m_atlaser.empty())
//			return nullptr;
		
		//		m_textures.emplace_back(m_size.x, m_size.y, 1, m_glyph_height);
	}
	
	return nullptr;
}

ft_texture_builder::glyph_info* ft_texture_builder::new_glyph(int g) {
	
	std::error_code ec;
	m_fg.rasterize(g, ec);
	if (ec)
		return nullptr;
	
	FT_GlyphSlot gs = m_fg.glyph_slot();
	
	float w = ftf (gs->metrics.width);
	float h = ftf (gs->metrics.height);
	
	int width = ftfix(ftceil (gs->metrics.width));
	int height = ftfix(ftceil (gs->metrics.height));
	
	atlaser::rect r;
	page_info* pi = find_spot (width, height, r);
	if (!pi)
		return nullptr;
	
	auto const& met = gs->metrics;
	
	areaf uv = pi->get_uv(r.no_border()).to_area();
	//	uv.p0.y = 1.0f - uv.p0.y;
	//	uv.p1.y = 1.0f - uv.p1.y;
	glyph_info* gi = new glyph_info (vector2f(ftf(met.horiBearingX), ftf(met.horiBearingY)),
									 vector2f(w, h), ftf(met.horiAdvance), uv, pi);
	auto it = m_gi.insert(g, gi);
	if (it == m_gi.lean_end ()) {
		delete gi;
		return nullptr;
	}
	
	image withBorder;
	image_ref raster (gs->bitmap.width, gs->bitmap.rows, gs->bitmap.buffer, 1, gs->bitmap.pitch);
	
	// Need to create borders!
	recti wr = r.with_border();

	GLint packAlign = 4;
	gl::GetIntegerv(gl::GetPName::PACK_ALIGNMENT, &packAlign);
	
	// for open gl, row data must be aligned on glStorei/GL_UNPACK_ALIGNMENT
	if (!raster.pitch_is_width() || r.m_border != 0 || raster.is_row_aligned(packAlign)) {
		
		image tmp (wr.width(), wr.height(), raster.comp, image::align_pitch{packAlign});
		tmp.put(r.m_border, r.m_border, raster);
		swap (tmp, withBorder);
		
		raster = withBorder;
	}
	
//	path dst = path("/tmp") / (fr().family_name() + "_" + std::to_string(fr().height()) + std::to_string(g) + ".tga");
//	stir::save_image (dst, raster);
	
#if STIR_OPENGL
	// upload image rect
	//	pi->img.put (wr.p.x, wr.p.y, raster);
	gl::IsTexture(pi->get_tex()->tex_name);
	gl::BindTexture(gl::TextureTarget::TEXTURE_2D, pi->get_tex()->tex_name);
	gl::TexSubImage2D(gl::TextureTarget::TEXTURE_2D, 0, wr.p.x, wr.p.y, raster.width, raster.height, gl::PixelFormat::ALPHA, gl::PixelType::UNSIGNED_BYTE, raster.pixels);
	
/*	static int n=0;
	if (n++ == 32) {
		path imgdst = path("/tmp") / (fr().family_name() + "_" + std::to_string(fr().height()) + "_WHOLE.tga");
		stir::save_image (imgdst, pi->img);
		
		path gldst = path("/tmp") / (fr().family_name() + "_" + std::to_string(fr().height()) + "_GL.tga");
		image glimg4 (pi->tex->width, pi->tex->height, 4);
		glimg4.fill(0xff4080c0);
		gles::ReadPixels (0, 0, glimg4.width, glimg4.height, gles::PixelFormat::rgba, gles::PixelType::ubyte, glimg4.pixels);
		glFlush();
		stir::save_image (gldst.parent() / (gldst.stem() + "_rgba.tga"), glimg4);
	}*/

#else
	--> unsupported
#endif
	
	gl::BindTexture(gl::TextureTarget::TEXTURE_2D, 0);
	
	return gi;
}

	//
	// font_renderer
	//
	
font_renderer::wrapper::const_iterator font_renderer::wrapper::process (text_context& tc, ft_texture_builder& tb, rectf const& r)
{
	m_width = 0;
	m_cached = 0;
	const_iterator safe = m_text;
	const_iterator p = m_text;
	
	float h = 0;
	float w = 0;
	
	while (1)
	{
		int ch = *p;

		if (ch == 0)
			return p;
		else if (is_cmd(ch)) {
			command_helper::skip_cmd (p);
			continue;
		} else if (is_nextline(ch)) {
			++p;
			
			w = 0;
			h += tb.fg().height() * tc.scale;
			if (h >= r.height())
				return p;
			
			continue;
		}
		
		glyph_info const* g = tb.get_glyph (ch);
		
		float scaledW = g->size.x * tc.scale;
		
		if (m_cached < CACHE_COUNT)
			m_cache[m_cached++] = { g, 0
#ifdef _DEBUG
				, (char)ch
#endif
			};
		
		if (m_format & wrap_words) {
			if (isblank (ch)) {
				safe = p;
				m_width = w;
			}
			
			if (w + scaledW > r.width ()) {
				// we may have long text without spaces, so break word
				if (m_text == safe) {
					// but first check if a single character did fit and if not, advance p
					if (p == m_text)
						++p;
					return p;
				}
				
				return safe;
			}
		} else if (m_format & wrap) {
			if (w + scaledW > r.width ()) {
				if (m_text == safe)
					++safe;
				
				return safe;
			}
		}
		
		++p;
		w += g->hori_advance * tc.scale * tc.condense;
	}
	
	return p;
}
	
inline int a2h (int ch) { return ch <= '9' ? (ch - '0') : ch >= 'a' ? (ch - 'a' + 0xa) : (ch - 'A' + 0xa); }

	void font_renderer::command_helper::skip_cmd (stir::utf8_const_iterator& it)
{
	assert (*it == '{');
	while (true) {
		int ch = *it;
		if (!ch) return;
		
		++it;
		
		if (ch == '}') return;
	}
}

void font_renderer::command_helper::process (text_context& ctx, stir::utf8_const_iterator& it, stir::utf8_const_iterator end)
{
	assert (is_cmd (*it));
	
	enum Cmds { None = 0, CmdStart, CmdColor, CmdScale };
	
	u32 argb = 0;
	float scale = 1.0f;

	Cmds state = None;
	while (it != end)
	{
		int ch = *it;
		
		if (ch == '}') {
			switch (state) {
				default:
				case None:
					break;
				case CmdColor:
					ctx.cur_color = color8::argb(argb);
					break;
				case CmdScale:
					ctx.scale = scale;
					break;
			}
			return;
		}
		
		++it;
		
		switch (state) {
			default:
			case CmdScale:
			case None:
				if (ch == '{')
					state = CmdStart;
				else
					return;
				break;
			case CmdStart:
				if (ch == 'c')
					state = CmdColor;
				else if (ch == 's')
					state = CmdScale;
				break;
			case CmdColor:
				if (ch != '#')
				{
					argb <<= 4;
					argb |= a2h(ch);
				}
				break;
		}
	}
	
}
	
font_renderer::font_renderer (gles::device_context& dev, ft_font_generator& fg, int texWH)
:	m_device(dev)
,	m_binding
({
		{ "in_pos", 2, gl::bind_type::float_, false, offsetof(gvertex, x), sizeof(gvertex) },
		{ "in_color", 4, gl::bind_type::ubyte, true, offsetof(gvertex, color), sizeof(gvertex) },
		{ "in_uv", 2, gl::bind_type::float_, false, offsetof(gvertex, u), sizeof(gvertex) }
})
{
	r_gpuprog.data().add_listener(*this);
	
	m_shader = stir::r_gpuprog.load_cached("font");
	fetch_gpuprog_info();
	
	m_tb = new ft_texture_builder (fg, texWH, texWH);
	
	size_t vtxcount = BATCH_CHARS * 4;
	size_t idxcount = BATCH_CHARS * 6;
	
	for (gl::buffer& buf : m_vb) {
		buf.bind();
		gl::buffer::data(gl::BufferTarget::ARRAY_BUFFER, vtxcount * sizeof(gvertex), nullptr, gl::BufferUsage::STREAM_DRAW);
	}
	
	m_indices.bind(gl::BufferTarget::ELEMENT_ARRAY_BUFFER);
	gl::buffer::data(gl::BufferTarget::ELEMENT_ARRAY_BUFFER, idxcount * sizeof(u16), nullptr, gl::BufferUsage::STATIC_DRAW);
	u16* pi = m_indices.map<u16>(gl::BufferTarget::ELEMENT_ARRAY_BUFFER, gl::BufferAccess::WriteOnly);
	
	assert (BATCH_CHARS*6 < ct_pow2<sizeof(u16)*8>::value);
	
	for (int i=0; i<BATCH_CHARS; ++i)
	{
		size_t o = i * 6;
		size_t v = i * 4;
		
		pi [o + 0] = v + 0;
		pi [o + 1] = v + 1;
		pi [o + 2] = v + 2;
		
		pi [o + 3] = v + 2;
		pi [o + 4] = v + 3;
		pi [o + 5] = v + 0;
	}
	
	gl::buffer::unmap(gl::BufferTarget::ELEMENT_ARRAY_BUFFER);
}
	
font_renderer::~font_renderer ()
{
	unlink_this();
	delete m_tb;
}
	
void font_renderer::fetch_gpuprog_info()
{
	gpu_program& prog = r_gpuprog % m_shader;
	
	m_locs.proj_matrix = prog.location<gl::mat44> ("proj_mat");
	m_locs.font_color = prog.location<gl::float4> ("font_color");
	m_locs.font_tex = prog.location<gl::sampler> ("font_tex");
	
	m_binding.bind_attrs (prog);
	
	m_vao[0].create (m_binding, m_vb[0].m_buffer, m_indices.m_buffer);
	m_vao[1].create (m_binding, m_vb[1].m_buffer, m_indices.m_buffer);	
}
	
void font_renderer::on_gpuprog_was_reloaded()
{
	fetch_gpuprog_info();
}

void font_renderer::build_glyph (glyph_info const& g, vector2f const& screen_pos, float scale, color8 color, gvertex*& vtx)
{
	float w = g.size.x * scale;
	float h = g.size.y * scale;
	float x = screen_pos.x + g.bearing.x * scale;
	float y = screen_pos.y - g.bearing.y * scale;
	
	//		x=y=0;
	//		w=h=256;
	
	float u0 = g.uv.p0.x, v0 = g.uv.p0.y, u1 = g.uv.p1.x, v1 = g.uv.p1.y;
	//float u0 = 0.1, v0 = 0.1, u1 = 1, v1 = 1;
	
	vtx->x = x;
	vtx->y = y;
	vtx->u = u0;
	vtx->v = v0;
	vtx->color = color;
	++vtx;
	
	vtx->x = x + w;
	vtx->y = y;
	vtx->u = u1;
	vtx->v = v0;
	vtx->color = color;
	++vtx;
	
	vtx->x = x + w;
	vtx->y = y + h;
	vtx->u = u1;
	vtx->v = v1;
	vtx->color = color;
	++vtx;
	
	vtx->x = x;
	vtx->y = y + h;
	vtx->u = u0;
	vtx->v = v1;
	vtx->color = color;
	++vtx;
}
	
void font_renderer::render_buffer (gl::buffer& buf, Part const* part, size_t parts)
{
	gl::BlendFunc(gl::BlendingFactorSrc::SRC_ALPHA, gl::BlendingFactorDest::ONE_MINUS_SRC_ALPHA);
	gl::Enable(gl::EnableCap::BLEND);

#if STIR_GLES2
	stir::gpu_program& prog = r_gpuprog % m_shader;
	
	matrix ortho = matrix::orthographic(0, m_device.logical_width(), m_device.logical_height(), 0, -1, 1);

	prog.use();
	m_vao[m_vb.current_index()].use();
//	m_binding.use(buf.m_buffer);
	prog.set (m_locs.proj_matrix, &ortho[0][0]);
	prog.set (m_locs.font_color, 1, 1, 1, 1);

	for (size_t i=0; i<parts; ++i)
	{
		prog.set_texture (m_locs.font_tex, part[i].tex->tex_name, 0, gl::TextureTarget::TEXTURE_2D);
		
		size_t o = part[i].start / 4 * 6;
		size_t c = part[i].count / 4 * 6;
		
		gl::DrawElements(gl::PrimitiveType::TRIANGLES, (GLsizei)c, gl::DrawElementsType::UNSIGNED_SHORT, (void*)(uintptr_t)(o*2));
	}

	m_vao[m_vb.current_index()].dont_use();
	
	prog.dont_use();
#endif
}

void font_renderer::render_text (text_context& ctx, rectf const& r, char const* utf8Text, int format)
{
	gl::buffer* vb = &m_vb.current();
	
	gl::BindBuffer(gl::BufferTarget::ARRAY_BUFFER, vb->m_buffer);
	gvertex* pv = (gvertex*)vb->map(gl::BufferTarget::ARRAY_BUFFER);
	
	size_t bufferSize = BATCH_CHARS * 4;
	partition<Part, 16> part (bufferSize);
	
	command_helper cmd;

	wrapper w (wrapper::const_iterator(utf8Text), format);
	
	do
	{
		auto batchE = w.process (ctx, *m_tb, r);

		size_t i = 0;
		for (wrapper::const_iterator p = w.m_text; p != batchE; ++p)
		{
			int ch = *p;
			
			if (is_control(ch)) {
				if (is_nextline(ch)) {
					ctx.localPos.x = 0;
					if ((ctx.localPos.y += m_tb->fg().height() * ctx.scale) >= r.height())
						break;
				}
				continue;
			} else if (is_cmd (ch)) {
				cmd.process (ctx, p, batchE);
				continue;
			}
			
			glyph_info const* g = i < w.m_cached ? w.m_cache[i].g : m_tb->get_glyph(ch);
			++i; // commands and controls don't get recorded in the cache
			
			while (!part.part (4, g->page->get_tex())) {
				// flush buffer
				vb->unmap(gl::BufferTarget::ARRAY_BUFFER);
				
				render_buffer (*vb, part.m_part.data(), part.size());
				vb = &m_vb.flip_and_get();
				gl::BindBuffer(gl::BufferTarget::ARRAY_BUFFER, vb->m_buffer);
				pv = (gvertex*)vb->map (gl::BufferTarget::ARRAY_BUFFER);
				
				part.restart (bufferSize);
			}
			
			build_glyph (*g, r.p + ctx.localPos, ctx.scale, ctx.cur_color, pv);
			
			ctx.localPos.x += g->hori_advance * ctx.scale * ctx.condense;
		}
		
		w.done (batchE);
	} while (!w.eos());

	vb->unmap(gl::BufferTarget::ARRAY_BUFFER);
	
	if (!part.empty()) {
		render_buffer (*vb, part.m_part.data(), part.size());
		m_vb.flip_and_get(); // make sure the next time we start with another buffer
	}

#if STIR_GLES1
	stir::gles::disable_client_states ();
#endif
}
	
float font_renderer::calc_width (float scale, char const* utf8Text, int count, float condense)
{
	float w = 0;
	for (stir::utf8_const_iterator it(utf8Text); count --> 0; ++it)
	{
		glyph_info const* g = m_tb->get_glyph (*it);
		
		if (count == 0) {
			w += g->size.x * scale;
			break;
		}

		w += g->hori_advance * scale * condense;
	}
	
	return w;
}
	
float font_renderer::calc_width (float scale, char const* utf8Text, float condense)
{
	float w = 0;
	float charW = 0, adv = 0;
	for (stir::utf8_const_iterator it(utf8Text); ; ++it)
	{
		int ch = *it;
		if (!ch) {
			w += charW * scale;
			break;
		}
		w += adv * scale * condense;
		
		glyph_info const* g = m_tb->get_glyph (ch);
		charW = g->size.x;
		adv = g->hori_advance;
	}
	
	return w;
}
	
}
