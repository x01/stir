#ifndef __stir__font_ft__
#define __stir__font_ft__

#include <system_error>
#include <string>
#include <vector>
#include <list>

#include <stir/basic_types.hpp>
#include <stir/image.hpp>
#include <stir/hash_map.hpp>
#include <stir/texture.hpp>
#include <stir/flat_types.hpp>
#include <stir/atlas/atlas_maker.hpp>
#include <stir/stir_utf.hpp>
#include <stir/dynamic_array.hpp>
#include <stir/path.hpp>
#if STIR_OPENGL
#include <stir/gles/gles.h>
#include <stir/gles/gles_shader.hpp>
#include <stir/gles/buffer.hpp>
#include <stir/r_gpuprog.hpp>
#endif

#include <ft2build.h>
#include <freetype/freetype.h>

namespace stir {

enum class TextAlign : unsigned
{
	top		= 0x00000000,
	vcenter	= 0x00000010,
	bottom	= 0x00000020,
	left	= 0x00000000,
	hcenter	= 0x00000100,
	right	= 0x00000200,
};
inline TextAlign operator|(TextAlign a, TextAlign b) { return (TextAlign)(static_cast<unsigned>(a) | static_cast<unsigned>(b)); }
inline bool operator&(TextAlign a, TextAlign b) { return (static_cast<unsigned>(a) & static_cast<unsigned>(b)) != 0; }

enum fterr {
#undef __FTERRORS_H__
#define FT_ERRORDEF( e, v, s )  e = v,
#include FT_ERRORS_H
};
	
}

template<> struct std::is_error_condition_enum<stir::fterr> : std::true_type { };
template<> struct std::is_error_code_enum<stir::fterr> : std::true_type { };

namespace stir {

std::error_category& ft_error_category ();
	
inline std::error_condition make_error_condition(fterr e) {
	return std::error_condition (static_cast<int>(e), ft_error_category());
}

inline std::error_code make_error_code(fterr e) {
	return std::error_code (static_cast<int>(e), ft_error_category());
}

// convert 26.6 into float
inline float ftf(FT_Pos i) {
	return (float)i / (1 << 6);
}

inline int ftceil(FT_Pos i) {
	return (int)(((i-1) | 0x1f) + 1);
}

inline int ftfix(FT_Pos i) {
	return (int)(i >> 6);
}

FT_Library ft_init (std::error_code& ec);
void ft_free(FT_Library lib);
	
class ft_library
{
	FT_Library m_library;
	
public:
	ft_library ();
	ft_library(ft_library const&) = delete;
	~ft_library ();
	
	operator FT_Library() const { return m_library; }
};
	
	//
	// ft_font_generator
	//

class ft_font_generator
{
	FT_Face m_face;
	FT_Library m_library;
#ifdef DEBUG
	std::string m_font_pathname;
#endif
	stir::dynamic_array<unsigned char> m_font_data;
	
	int m_height_pix = 0;
	float m_height = 0, m_ascender = 0, m_descender = 0;
	
public:
	ft_font_generator(FT_Library ftLib, path const& fontPathName, std::error_code& err);
	~ft_font_generator();
	
	std::string family_name () const { return m_face->family_name; }
	std::string style_name () const { return m_face->style_name; }
	
	FT_Library library() const { return m_library; }
	FT_Face face() const { return m_face; }
	
	float height () const { return m_height; }
	float ascender () const { return m_ascender; }
	float descender () const { return m_descender; }
	
	void set_pixel_sizes(int heightInPixels, std::error_code& err);
	int height_pix() const { return m_height_pix; }
	
	void rasterize (int charcode, std::error_code& ec);
	
	FT_Bitmap const& bitmap() { return m_face->glyph->bitmap; }
	FT_GlyphSlot const& glyph_slot() { return m_face->glyph; }
	
	bool get_result (image_ref& img);
};
	
	//
	// ft_texture_builder
	//
	
class ft_texture_builder
{
	struct atlaser
	{
		struct rect : private stir::recti {
			int m_border;
			
			rect () {}
			rect (int x, int y, int w, int h, int border) : recti(x, y, w, h), m_border(border) {}
			
			recti with_border() const {
				return recti (p.x - m_border, p.y - m_border, s.x + 2*m_border, s.y + 2*m_border);
			}
			recti& no_border() { return *this; }
			
			using recti::empty;
		};
		vector2i m_size;
		int m_border;
		
		int m_glyph_height;
		recti m_used;
		
		atlaser (int w, int h, int border, int glyphHeight)
		:	m_size(w, h), m_border(border), m_glyph_height(glyphHeight)
		,	m_used(0,0,0,glyphHeight)
		{
		}
		
		bool empty() const { return m_used.empty(); }
		
		atlaser::rect allocate (int w, int h);
	};
	
public:
	struct page_info
	{
		page_info(page_info const&) = delete;
		page_info operator=(page_info const&) = delete;
		
		page_info(int w, int h, int border, int glyphHeight);
		~page_info ();
		
		rectf get_uv (recti r) const
		{
			texture_info i (m_texture);
			float ix, iy;
			i.get_inv(ix, iy);
			return r.remap<float> (ix, iy);
		}
		
		atlaser::rect alloc_rect(int w, int h) { return m_atlaser.allocate(w,h); }
		texture get_tex() const { return m_texture; }
		
	private:
		atlaser m_atlaser;
		texture m_texture;
	};
	
	struct glyph_info {
		glyph_info (vector2f horiBearing, vector2f sz, float hadv, areaf uv_, page_info* pi)
		:	bearing(horiBearing), size(sz), hori_advance(hadv), uv(uv_), page(pi)
		{
		}
		
		vector2f bearing, size;
		float hori_advance;
		areaf uv;
		page_info* page;
	};
	
	ft_font_generator& m_fg;
	
	std::list<page_info> m_textures;
	// Allocate glyph_info* on heap, because returned pointers to glyph_info
	// must stay valid. If we store this struct by value, the hashmap storage moves upon resize
	// pointers get invalidated.
	stir::hash_map<int, glyph_info*> m_gi;
	vector2i m_size = {256, 256};
//	int m_glyph_height;
	
	void* m_debug_impl = nullptr;
	
public:
	ft_texture_builder(ft_font_generator& fg, int texW, int texH);
	~ft_texture_builder ();
	
	// for debugging
	void render_pages(gles::device_context& dev);
	
	void preload(int begin, int end);
	
	vector2f get_kerning (int a, int b);
	glyph_info* get_glyph (int g);
	
	ft_font_generator& fg() { return m_fg; }
	
private:
	page_info* find_spot(int w, int h, atlaser::rect& r);
	glyph_info* new_glyph(int g);
};

class font_renderer : private gpuprog_listener
{
	static const int BATCH_CHARS = 256;
	
	struct shader_locations
	{
		gl::mat44 proj_matrix;
		gl::float4 font_color;
		gl::sampler font_tex;
	};
	
	gles::device_context& m_device;
	i_gpuprog m_shader;
	ft_texture_builder* m_tb;

	swap_chain<gl::buffer> m_vb;
	gl::vao m_vao[2];
	gl::buffer m_indices;
	gl::binding<3> m_binding;
	shader_locations m_locs;
	
	struct gvertex
	{
		float			x,y;
		stir::color8	color;
		float			u, v;
	};
	
	struct Part {
		size_t start, count;
		stir::texture tex = nullptr;

		void init (size_t start_, stir::texture t) { start = start_; count = 0; tex = t; }
		
		bool compatible(stir::texture a) const { return tex == a; }
	};
	
	typedef ft_texture_builder::glyph_info glyph_info;
	typedef ft_texture_builder::page_info page_info;
	
	void build_glyph (glyph_info const& g, vector2f const& screen_pos, float scale, color8 color, gvertex*& vtx);
	
	static inline bool isblank (int ch) { return ch == ' ' || ch == '\t'; }
	
	void on_gpuprog_was_reloaded() override;
	void fetch_gpuprog_info();
	
public:
	font_renderer (gles::device_context& dev, ft_font_generator& fg, int texWH);
	~font_renderer ();

	gles::device_context& device() const { return m_device; }
	ft_texture_builder& tb() const { return *m_tb; }
	
	enum FormatFlags
	{
		wrap		= 0x01,
		wrap_words	= 0x02
	};
	
	float height () const { return m_tb->fg().height(); }
	float next_line () const { return m_tb->fg().height(); }
	float ascender () const { return m_tb->fg().ascender(); }
	float descender () const { return m_tb->fg().descender(); }
	
	void render_buffer (gl::buffer& buf, Part const* part, size_t p);
	
	struct text_context
	{
		text_context (vector2f const& lpos, color8 const& color_, float textScale, float condens = 1.0f)
		:	localPos (lpos), cur_color(color_), scale(textScale), condense(condens)
		{}
		explicit text_context (color8 const& col, vector2f const& lpos = vector2f(0,0))
		:	text_context (lpos, col, 1.0f)
		{}
		explicit text_context (vector2f const& lpos = vector2f(0,0))
		:	text_context (lpos, color8::argb(0xffffffff), 1.0f)
		{}
		
		vector2f	localPos;
		color8		cur_color;
		float		scale, condense;
	};
	
	//		template<class T>
	struct wrapper
	{
		typedef stir::utf8_const_iterator const_iterator;
		
		typedef char T;
		float m_width;
		const_iterator m_text;
		int m_format;
		
		static const size_t CACHE_COUNT = 32;
		struct { glyph_info const* g; float kern;
#ifdef _DEBUG
			char ch;
#endif
		} m_cache[CACHE_COUNT];
		int m_cached = 0;
		
		wrapper (const_iterator p, int fmt) : m_text(p), m_width(0), m_format(fmt) {}
		
		// return end iterator of a block that should rendered, i.e [m_text, <result>)
		const_iterator process (text_context& tc, ft_texture_builder& tb, rectf const& r);
		
		void done (const_iterator p)
		{
			m_text = p;
		}
		
		bool eos () const { return *m_text == 0; }
	};
	
	struct command_helper
	{
		void process (text_context& ctx, stir::utf8_const_iterator& it, stir::utf8_const_iterator end);
		static void skip_cmd (stir::utf8_const_iterator& it);
	};
	
	static bool is_nextline(int ch) { return ch == '\n'; }
	static bool is_control(int ch) { return ch < 0x20; }
	static bool is_cmd(int ch) { return ch == '{'; } // {c:#fafafadf}, {c:p}, {s:p}
	
	void draw_text (rectf const& r, char const* utf8Text, color8 color)
	{
		text_context ctx (color, vector2f ());
		render_text (ctx, r, utf8Text);
	}

	// localPos - baseline (bottom) position where to render relative to 'r'
	// r - destination rectangle where to render text
	void render_text (text_context& ctx, rectf const& r, char const* utf8Text, int format = wrap|wrap_words);
	
	float calc_width (float scale, char const* utf8Text, int count, float condense = 1.0f);
	float calc_width (float scale, char const* utf8Text, float condense = 1.0f);
	
/*	float calc_width (float scale, wchar_t const* text, size_t count)
	{
		return stir::calc_width (*(r_fontdata % font_data_), scale, text, count);
	}
	
	float calc_width (float scale, wchar_t const* text)
	{
		return stir::calc_width (*(r_fontdata % font_data_), scale, text);
	}*/
	
	
};
	
}

#endif
