#ifndef STIR_GUI_TYPES_HPP_
#define STIR_GUI_TYPES_HPP_

#include "../flat_types.hpp"

namespace stir { namespace gui {

typedef vector2f	vector2f;
typedef vector2i	vector2i;

typedef rectf		rectf;
typedef recti		recti;
typedef recti16	recti16;

typedef areaf		areaf;

}}

#endif
