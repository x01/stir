#ifndef stir_hash_map_h
#define stir_hash_map_h

#include <type_traits>

namespace stir
{
	
template<class K, class V>
struct hash_entry
{
	hash_entry() {}
	hash_entry(K const& k) : key(k) {}
	
	bool empty (K const& emptyKey) const { return key == emptyKey; }
	V const& v() const& { return (V&)value[0]; }
	V& v() & { return (V&)value[0]; }
	V&& v() && { return (V&&)value[0]; }
	
	K key; //
	typename std::aligned_storage<sizeof(V)>::type value[1];
};
	
//	
//template<class K, class V>
//class hash_iterator
//{
//	typedef hash_entry<K, V> hentry;
//	
//	hash_iterator (hentry* e) : m_entry(e) {}
//
//	void operator++() { e +=
//	
//};
	
//template<class T>
//struct hash;
//	
//template<> struct hash<char> : public std::unary_function<char, size_t> {

template<class T>
struct hasher;
 
template<>
struct hasher<int> { typedef int value_type; int operator() (int id) const { return id; } };
	
template<class K, class T, class Hasher = hasher<K> >
class hash_map;
	
	
template<class K, class T, class Hasher>
struct hash_map_iterator : public std::iterator<std::forward_iterator_tag, hash_entry<K, T>>
{
	typedef hash_entry<K, T> hentry;
	typedef hash_map<K, T, Hasher> htable;
	
	hentry* m_cur;
	htable& m_table;

	hash_map_iterator (hentry* p, htable& t) : m_cur(p), m_table(t) { advance_over_empty(); }
	
	hash_map_iterator& operator++() { ++m_cur; advance_over_empty(); return *this; }
	hentry& operator*() { return *m_cur; }
	hentry* operator->() { return m_cur; }
	
	bool operator==(hash_map_iterator const& a) { return m_cur == a.m_cur; }
	bool operator!=(hash_map_iterator const& a) { return m_cur != a.m_cur; }
	
	void advance_over_empty();
};

template<class K, class T, class Hasher>
struct hash_map_const_iterator : public std::iterator<std::forward_iterator_tag, hash_entry<K, T> const>
{
	typedef hash_entry<K, T> hentry;
	typedef hash_map<K, T, Hasher> htable;
	
	hentry const* m_cur;
	htable const& m_table;
	
	hash_map_const_iterator (hentry const* p, htable const& t) : m_cur(p), m_table(t) { advance_over_empty(); }
	
	hash_map_const_iterator& operator++() { ++m_cur; advance_over_empty(); return *this; }
	hentry const& operator*() { return *m_cur; }
	hentry const* operator->() { return m_cur; }
	
	bool operator==(hash_map_const_iterator const& a) { return m_cur == a.m_cur; }
	bool operator!=(hash_map_const_iterator const& a) { return m_cur != a.m_cur; }
	
	void advance_over_empty();
};
	
	
template<class K, class T, class Hasher>
class hash_map : private Hasher
{
#define IS_OCC(index) (m_table[index].key != m_empty_key)
	//	typedef std::pair<K, T> value_type;
	typedef Hasher hasher_type;
	typedef typename hasher_type::value_type hash_type;
	typedef hash_entry<K, T> hentry;
	
	size_t m_capacity, m_size = 0;
	hentry* m_table;
	
	K m_empty_key;
	
	hasher_type const& hasher_() const { return *static_cast<hasher_type const*>(this); }
//	alloc_type& al_() { return *static_cast<Hasher>(this); }
	
	void alloc (size_t cap) {
		m_table = (hentry*)new char [sizeof(hentry) * cap];
		m_capacity = cap;
		for (int i=0; i<cap; ++i)
			new (&m_table[i].key) K (m_empty_key);
	}
	
	void destroy (hentry* entr, size_t sz) {
		for (int i=0; i<sz; ++i) {
			if (!entr[i].empty (m_empty_key))
				entr[i].v().~T();
			entr[i].key.~K();
		}
	}
	
	void dealloc () {
		delete[] (char*)m_table;
	}
	
	size_t find_slot (K const& k) const {
		hash_type hash = hasher_()(k);
		int index = hash % m_capacity;
		
		size_t start = index;
		while (IS_OCC(index) && m_table[index].key != k) {
			index = (index + 1) % m_capacity;
			
			if (index == start)
				break; // we searched the whole table and no empty was found!
		}
		
		return index;
	}

	hash_type calc_hash (hash_entry<K, T> const& e) const
	{
		return hasher_ () (e.key);
	}
	
	void rehash (hentry* s, size_t ssz) {
		for (int i=0; i<ssz; ++i) {
			if (s[i].empty (m_empty_key))
				continue;
			insert (s->key, s->v());
		}
	}

public:
	typedef hash_entry<K, T>* lean_iterator;
	typedef hash_entry<K, T> const* lean_const_iterator;
	typedef hash_map_iterator<K, T, Hasher> iterator;
	typedef hash_map_const_iterator<K, T, Hasher> const_iterator;
	
	enum { INITIAL_GROW = 16 };
	
	explicit hash_map (K const& emptyKey, size_t initialCapacity = 0)
	:	m_empty_key(emptyKey)
	{
		if (initialCapacity != 0)
			alloc (initialCapacity);
		else {
			m_capacity = m_size = 0;
			m_table = nullptr;
		}
	}
	
	hash_map (hash_map const& a)
	:	m_empty_key (a.m_empty_key), m_size(a.m_size), m_capacity(a.m_capacity)
	{
		if (m_capacity)
			m_table = (hentry*)new char [sizeof(hentry) * a.m_capacity];
		else
			m_table = nullptr;
		for (int i=0; i<m_capacity; ++i) {
			new (&m_table[i].key) K (a.m_table[i].key);
			if (IS_OCC(i))
				new (&m_table[i].value) T (m_table[i].v());
		}
	}
	
	hash_map (hash_map&& a)
	:	m_empty_key (a.m_empty_key), m_size(a.m_size), m_capacity(a.m_capacity)
	{
		m_table = a.m_table;
		a.m_table = nullptr;
		a.m_size = a.m_capacity = 0;
	}
	
	hash_map& operator=(hash_map const& a) {
		destroy(m_table, m_capacity);
		dealloc();
		m_capacity = a.m_capacity;
		m_size = a.m_size;
		m_table = (hentry*)new char [sizeof(hentry) * a.m_capacity];
		for (int i=0; i<m_capacity; ++i) {
			new (&m_table[i].key) K (a.m_table[i].key);
			if (IS_OCC(i))
				new (&m_table[i].value) T (m_table[i].v());
		}
	}
	
	hash_map& operator=(hash_map&& a) {
		destroy(m_table, m_capacity);
		dealloc();
		m_capacity = a.m_capacity;
		m_size = a.m_size;
		m_table = a.m_table;
		
		a.m_table = nullptr;
		a.m_size = a.m_capacity = 0;
		return *this;
	}
	
	~hash_map()
	{
		destroy(m_table, m_capacity);
		dealloc();
	}
	
	void swap (hash_map& a) {
		std::swap (m_capacity, a.m_capacity);
		std::swap (m_size, a.m_size);
		std::swap (m_table, a.m_table);
		std::swap (m_empty_key, a.m_empty_key);
		std::swap (m_capacity, a.m_capacity);
		
	}
	
	K const& empty_key () const { return m_empty_key; }
	
	lean_iterator lean_begin () { return m_table; }
	lean_const_iterator lean_begin () const { return m_table; }
	lean_iterator lean_end () { return m_table + m_capacity; }
	lean_const_iterator lean_end () const { return m_table + m_capacity; }
	
	iterator begin () { return iterator(m_table, *this); }
	const_iterator begin () const { return const_iterator(m_table, *this);; }
	iterator end () { return iterator(m_table + m_capacity, *this); }
	const_iterator end () const { return const_iterator(m_table + m_capacity, *this); }
	
	bool empty () const { return m_size == 0; }
	size_t size() const { return m_size; }
	size_t capacity() const { return m_capacity; }
	
	int load_100 () const { return (int)(m_capacity ? (m_size * 100 / m_capacity) : 0); }
	
	void reserve (size_t newCap) {
		if (m_capacity < newCap) {
			hentry* oldTable = m_table;
			size_t oldSize = m_size;
			alloc (newCap);
			if (oldTable) {
				rehash (oldTable, oldSize);
				destroy(oldTable, oldSize);
			}
		}
	}
	
	T& operator[] (K const& k) {
		assert (k != m_empty_key);

		if (!m_capacity)
			reserve (INITIAL_GROW);
		
		size_t index = find_slot (k);
		if (m_table[index].key != k) {
			if (IS_OCC(index)) {
				reserve (m_capacity * 2);
				index = find_slot (k);
			}
			m_table[index].key = k;
			new (m_table[index].value) T ();
			++m_size;
		}
		
		return m_table[index].v();
	}
	
	lean_iterator insert (K const& k, T const& v) {
		if (k == m_empty_key) // fatal error
			return lean_end();
		
		if (!m_capacity)
			reserve (INITIAL_GROW);
		
		size_t index = find_slot (k);
		if (IS_OCC(index)) {
			reserve (m_capacity * 2);
			index = find_slot (k);
		}
		
		m_table[index].key = k;
		new (m_table[index].value) T (v);
		
		++m_size;
		return m_table + index;
	}
	
	template<class... Args>
	lean_iterator emplace (K const& k, Args&&... args) {
		if (k == m_empty_key) // fatal error
			return lean_end();
		
		if (!m_capacity)
			reserve (INITIAL_GROW);
		
		size_t index = find_slot (k);
		if (IS_OCC(index)) {
			reserve (m_capacity * 2);
			index = find_slot (k);
		}
		
		m_table[index].key = k;
		new (m_table[index].value) T (std::forward<Args&&> (args)...);
		
		++m_size;
		return m_table + index;
	}
	
	void remove (K const& key)
	{
		if (key == m_empty_key)
			return;
		
		size_t i = find_slot (key);
		if (!IS_OCC(i)) // not found
			return;
		
		// removal taken from wikipedia: http://en.wikipedia.org/wiki/Open_addressing
		
		size_t j = i;
		
		while (true)
		{
			m_table[i].key = m_empty_key;
			m_table[i].v().~T();
			
		next:
			j = (j+1) % m_capacity;
			if (!IS_OCC(j))
				break;
			
			size_t k = hasher_()(m_table[j].key) % m_capacity;
			if (i<=j ? ((i<k)&&(k<=j)) : ((i<k)||(k<=j)))
				goto next;
			
			m_table[i] = m_table[j];
			i = j;
		}
		
		--m_size;
	}
	
	lean_iterator find (K const& key) {
		
		if (m_capacity == 0)
			return lean_end();

		hash_type hash = hasher_()(key);
		size_t index = hash % m_capacity;
		size_t start = index;
		while (IS_OCC(index)) {
			if (m_table[index].key == key)
				return m_table + index;
			
			++index;
			
			if (index == start)
				return lean_end(); // we searched the whole table and no key was found!
		}
		
		return lean_end();
	}
	
	lean_const_iterator find (K const& key) const {
		
		if (m_capacity == 0)
			return lean_end();
		
		hash_type hash = hasher_()(key);
		int index = hash % m_capacity;
		size_t start = index;
		
		while (IS_OCC(index)) {
			if (m_table[index].key == key)
				return m_table + index;
			
			++index;
			
			if (index == start)
				return lean_end(); // we searched the whole table and no key was found!
		}
		
		return lean_end();
	}
	
	
};

template<class K, class T, class Hasher>
inline void hash_map_iterator<K, T, Hasher>::advance_over_empty()
{
	hentry* e = m_table.lean_end();
	while (m_cur != e && m_cur->empty (m_table.empty_key()))
		++m_cur;
}

template<class K, class T, class Hasher>
inline void hash_map_const_iterator<K, T, Hasher>::advance_over_empty()
{
	hentry const* e = m_table.lean_end();
	while (m_cur != e && m_cur->empty (m_table.empty_key()))
		++m_cur;
}

	
}


#endif
