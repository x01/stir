#ifndef STIR_HEXDUMP_ITERATOR_
#define STIR_HEXDUMP_ITERATOR_

#include <string>
#include <cstdlib>

namespace stir {

class hexdump_iterator
{
	unsigned char const	*m_begin, *m_ptr, *m_end;
	size_t				m_per_line;
	int					m_flags;

public:

	enum flags {
		show_offset = 0x01,
		show_ascii = 0x02
	};

	hexdump_iterator ()
		:	m_ptr(NULL)
	{}
	hexdump_iterator (void const* ptr, size_t sizeInByte, size_t perLine, int flags = show_offset)
		:	m_ptr((unsigned char const*)ptr), m_end((unsigned char const*)ptr + sizeInByte), m_per_line(perLine), m_flags(flags)
	{
		m_begin = m_ptr;
	}

	bool operator==(hexdump_iterator const& a) const { return m_ptr == a.m_ptr; }
	bool operator!=(hexdump_iterator const& a) const { return m_ptr != a.m_ptr; }

	hexdump_iterator& operator++() {
		unsigned char const* next = m_ptr + m_per_line;
		m_ptr = next >= m_end ? NULL : next;
	}

	std::string operator*() {
		unsigned char const* next = m_ptr + m_per_line;
		size_t count = std::min (next, m_end) - m_ptr;

		size_t bufSize = 10 + count * 5;
		char* buffer = (char*)alloca (bufSize);
		char* cur = buffer, *end = buffer + bufSize;

		if (m_flags & show_offset)
			cur += snprintf (cur, end - cur, "%08x: ", m_ptr - m_begin);

		for (int i=0; i<m_per_line; ++i)
		{
			if (i < count)
				cur += snprintf (cur, end - cur, "%02x ", m_ptr[i]);
			else
				cur += snprintf (cur, end - cur, "   ");
		}

		return std::string (buffer, cur - buffer);
	}
};

}

#endif
