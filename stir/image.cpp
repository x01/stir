#include "image.hpp"
#include <stb_image.h>

#include <algorithm>

namespace stir {

namespace {
	
struct expand_u8_to_u32alpha {
	u32 operator()( u8 a ) const {
		return ((u32)a << 24) | 0xffffff;
	}
};

struct convert_u32alpha_to_u8 {
	u8 operator()( u32 a ) const {
		return (u8)((a >> 24)&0xff);
	}
};
	
} //


void image_ref::put1 (int x, int y, void* src, int w, int h, int srcPitch)
{
	for (int i=0; i<h; ++i) {
		void* d = (u8*)pixels + pitch * (y+i) + x * comp;
		void* s = (u8*)src + srcPitch * i;
		
		if (comp==1)
			std::copy ((u8*)s, (u8*)s + w, (u8*)d);
		else if (comp==4)
			std::transform ((u8*)s, (u8*)s + w, (u32*)d, expand_u8_to_u32alpha() );
		else
			utility::stir_fatal ("atlas_page::put1: unsupported number of comp");
	}
}

void image_ref::put4 (int x, int y, void* src, int w, int h, int srcPitch)
{
	for (int i=0; i<h; ++i) {
		void* d = (u8*)pixels + pitch * (y+i) + x * comp;
		void* s = (u8*)src + srcPitch * i;
		
		if (comp==1)
			std::transform ((u32*)s, (u32*)s + w, (u8*)d, convert_u32alpha_to_u8() );
		else if (comp==4)
			std::copy ((u32*)s, (u32*)s + w, (u32*)d);
		else
			utility::stir_fatal("image_ref::put4: unsupported number of comp");
	}
}

void image_ref::duplicate (int tox, int toy, int w, int h, int fromx, int fromy)
{
	assert (tox >= 0);
	assert (tox < width);
	assert (toy >= 0);
	assert (toy < height);
	
	assert (fromx >= 0);
	assert (fromx < width);
	assert (fromy >= 0);
	assert (fromy < height);
	
	assert (tox + w <= width);
	assert (toy + h <= height);
	
	assert (fromx + w <= width);
	assert (fromy + h <= height);
	
	for (int i=0; i<h; ++i) {
		
		void* d = (u8*)pixels + pitch * (toy+i) + tox * comp;
		void* s = (u8*)pixels + pitch * (fromy+i) + fromx * comp;
		
		if (comp==1)
			for (u8* sp = (u8*)s, *end = (u8*)s + w, *dp = (u8*)d; sp != end; ++sp, ++dp)
				*dp = *sp;
		
		//			for (int j=0; j<w; ++j) {
		//				*((u8*)d+j) = *((u8*)s+j);
		//			}
		else if (comp==4)
			for (u32* sp = (u32*)s, *end = (u32*)s + w, *dp = (u32*)d; sp != end; ++sp, ++dp)
				*dp = *sp;
		
		//			for (int j=0; j<w; ++j) {
		//				*((u32*)d+j) = *((u32*)s+j);
		//			}
		else
			utility::stir_fatal("image_ref::duplicate: unsupported number of comp");
	}
}

void image_ref::fill_rect (int tox, int toy, int w, int h, int fromx, int fromy)
{
	for (int i=0; i<h; ++i) {
		void* d = (u8*)pixels + pitch * (toy+i) + tox * comp;
		void* s = (u8*)pixels + pitch * (fromy+i) + fromx * comp;
		
		if (comp==1) {
			u8 pix = *(u8*)s;
			for (int j=0; j<w; ++j) {
				*((u8*)d+j) = pix;
			}
		} else if (comp==4) {
			u32 pix = *(u32*)s;
			for (int j=0; j<w; ++j) {
				*((u32*)d+j) = pix;
			}
		} else
			utility::stir_fatal ("image_ref::fill_rect: unsupported number of comp");
	}
}

void image_ref::fill_rect (int tox, int toy, int w, int h, color8 color)
{
	for (int i=0; i<h; ++i) {
		void* d = (u8*)pixels + pitch * (toy+i) + tox * comp;
		
		if (comp==1) {
			for (int j=0; j<w; ++j) {
				*((u8*)d+j) = color.a;
			}
		} else if (comp==4) {
			for (int j=0; j<w; ++j) {
				*((u32*)d+j) = color.value;
			}
		} else
			utility::stir_fatal ("image_ref::fill_rect: unsupported number of comp");
	}
}

#ifndef STIR_NO_SAVE_IMAGE
bool save_image (path const& p, image_ref const& img)
{
	if (img.width*img.comp != img.pitch) {
		image tmp (img.width, img.height, img.comp);
		tmp.put (0, 0, img);
		return stbi_write_tga(p.c_str(), tmp.width, tmp.height, tmp.comp, tmp.pixels);
	} else {
		return stbi_write_tga(p.c_str(), img.width, img.height, img.comp, img.pixels);
	}
}
#endif

}
