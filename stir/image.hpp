#ifndef __stir__image__
#define __stir__image__

#include <stir/basic_util.hpp>
#include <stir/math_types.hpp>
#include <stir/path.hpp>

#include <pulmotor/archive.hpp>

namespace stir {

struct image_ref
{
	image_ref (size_t w, size_t h, unsigned char* pix, size_t components, size_t pitch_)
	:	width (w), height(h), comp(components), pixels(pix), pitch(pitch_)
	{}
	
	image_ref (size_t w, size_t h, unsigned char* pix, size_t components)
	:	image_ref(w, h, pix, components, w*components)
	{}
	
	image_ref ()
	:	image_ref(0,0,0,0,0)
	{}
	
	size_t size_bytes() const { return width * height * comp; }
	size_t size_full() const { return pitch * height; }
	bool pitch_is_width() const { return width*comp == pitch; }
	bool is_row_aligned(size_t align) const { return (pitch & (align-1)) == 0; } // test if row is aligned on `align' value
	
	u16 width, height;
	u16 comp, pitch; // pitch is in bytes, not pixels!
	unsigned char* pixels;
	
	image_ref subrect (int x, int y, int w, int h) const
	{
		return image_ref (w, h, ptr_to (y, x), comp, pitch);
	}
	
	unsigned char* ptr_to (size_t i, size_t j) const {
		return pixels + i * pitch + j * comp;
	}
	
	PULMOTOR_ARCHIVE_USE_CONSTRUCT()
	PULMOTOR_ARCHIVE_CONSTRUCT(image_ref* img)
	{
		u16 w = 0, h = 0, c = 0, p = 0;
		u32 offset = 0;
		ar | nv_("width", w) | nv_("height", h) | nv_("comp", c) | nv_("pitch", p) | nv_("offset", offset);
		
		unsigned char* base = std::get<0>(ar.ctx());
		
		new (img) image_ref (w, h, base + offset, c, p);
	}
	PULMOTOR_ARCHIVE_SAVE_CONSTRUCT()
	{
		ar | nv(width) | nv(height) | nv(comp) | nv(pitch);
		// image_ref must not save data, but offset
		u32 offset = u32(pixels - std::get<0>(ar.ctx()));
		ar | nv(offset);
		//ar | pulmotor::memblock (pixels, size_bytes());
	}
	
	
	/*	void set_from_greyscale (void* src, int stride) {
	 for (int i=0; i<height; ++i)
	 {
	 void* s = (atlas_detail::u8*)src + (i * stride);
	 void* d = pixels + (i * width) * components;
	 
	 if (components == 4)
	 std::transform ((atlas_detail::u32*)s, (atlas_detail::u32*)s + width, (atlas_detail::u8*)d, convert_u32alpha_to_u8() );
	 else if (components == 1)
	 std::copy ((atlas_detail::u8*)s, (atlas_detail::u8*)s + width, (atlas_detail::u8*)d);
	 }
	 }*/

	void put (int x, int y, image_ref const& r) {
		put (x, y, r.pixels, r.width, r.height, r.comp, r.pitch);
	}
	void put (int x, int y, void* src, int w, int h, int srcComp) {
		put (x,y,src,w,h,srcComp,w*srcComp);
	}
	void put (int x, int y, void* src, int w, int h, int srcComp, int srcPitch) {
		if (srcComp == 1) put1(x,y,src,w,h,srcPitch);
		else if (srcComp == 4) put4(x,y,src,w,h,srcPitch);
		else utility::stir_fatal ("Unsupported component count");
	}
	void put1 (int x, int y, void* src, int w, int h, int srcPitch);
	void put4 (int x, int y, void* src, int w, int h, int srcPitch);
	
	// duplicates pixel rect of size (w,h)
	void duplicate (int tox, int toy, int w, int h, int fromx, int fromy);
	
	// fills rect of size (w,h) by pixel color from (fromx,fromy) position
	void fill_rect (int tox, int toy, int w, int h, int fromx, int fromy);
	// fill a rect with specified color. alpha is used if data is 8bit
	void fill_rect (int tox, int toy, int w, int h, color8 color);
	
	void fill(color8 c) {
		for (int i=0; i<height; ++i) {
			unsigned char* p = pixels + i*pitch;
			if (comp==1)
				memset(p, c.a, width);
			else if (comp==4)
				std::fill_n ((u32*)p, width, c.value);
		}
	}
	
	void extend_borders ();
	
	void swap (image_ref& b)
	{
		std::swap (width, b.width);
		std::swap (height, b.height);
		std::swap (comp, b.comp);
		std::swap (pixels, b.pixels);
		std::swap (pitch, b.pitch);
	}
};

inline void swap (image_ref& a, image_ref& b) {
	a.swap (b);
}

struct image : public image_ref
{
	struct align_pitch { int value = 4; };
	
	image (size_t w, size_t h, size_t comp, align_pitch&& al)
	:	image (w, h, comp, round_up(w*comp, (size_t)al.value))
	{}
	
	image (size_t w, size_t h, size_t comp, size_t pitch_in_bytes)
	:	image_ref (w, h, new u8[w*h*pitch_in_bytes], comp, pitch_in_bytes)
	{
		memset (pixels, 0, pitch*h);
	}
	
	image (size_t w, size_t h, size_t comp)
	:	image (w, h, comp, w*comp)
	{}
	
	image ()
	:	image_ref()
	{}
	
	~image () { delete []pixels; }
	
	image(image const&) = delete;
	image& operator =(image const&) = delete;
	
	size_t buffer_size() const { return width * height * pitch; }
	
	PULMOTOR_ARCHIVE_USE_CONSTRUCT()
	PULMOTOR_ARCHIVE_CONSTRUCT(image* img)
	{
		u16 w, h, c, p;
		ar | w | h | c | p;
		new (img) image (w, h, c, p);
		ar | pulmotor::memblock (img->pixels, img->size_bytes());
	}
	PULMOTOR_ARCHIVE_SAVE_CONSTRUCT()
	{
		ar | width | height | comp | pitch;
		ar | pulmotor::memblock (pixels, size_bytes());
	}
	
	/*
	PULMOTOR_ARCHIVE_SPLIT()
	PULMOTOR_ARCHIVE_READ()
	{
		if (pixels) delete[] pixels;
		ar | width | height | pitch | comp;
		u32 size = 0;
		ar | size;
		if (size > 0) {
			pixels = new u8[size];
			ar | pulmotor::memblock(pixels, size);
		}
	}

	PULMOTOR_ARCHIVE_WRITE()
	{
		ar | width | height | pitch | comp;
		u32 size = buffer_size();
		ar | size;
		if (size > 0)
			ar | pulmotor::memblock(pixels, size);
	}
	*/
};

#ifndef STIR_NO_SAVE_IMAGE
bool save_image (path const& p, image_ref const& img);
#endif

}

#endif
