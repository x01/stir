#include <stir/platform.hpp>
#include "virtual_file.hpp"

#include <physfs.h>

namespace stir { namespace io {

virtual_file_input::virtual_file_input (path const& p)
{
#if STIR_WINDOWS
	// orl: this is lame. On windows converting from ucs-2 to utf8 just to convert to ucs2 back later.
	utf8_path utf8p (p.c_str());
	m_physfs_file = PHYSFS_openRead(utf8p.c_str());
#else
	m_physfs_file = PHYSFS_openRead(p.c_str());
#endif
}

virtual_file_input::virtual_file_input ()
:	m_physfs_file (0)
{
}

virtual_file_input::~virtual_file_input ()
{
	if (m_physfs_file)
		PHYSFS_close (m_physfs_file);
}

bool virtual_file_input::is_good ()
{
	return m_physfs_file != 0;
}

uint64_t virtual_file_input::length ()
{
	return PHYSFS_fileLength (m_physfs_file);
}


size_t virtual_file_input::read (void* dest, size_t count, std::error_code& ec)
{
	PHYSFS_sint64 len = PHYSFS_read (m_physfs_file, dest, 1, (int)count);
	if (len < 0) {
		ec.assign((int)std::errc::io_error, std::generic_category());
		
		if (PHYSFS_eof (m_physfs_file))
			return 0;
		else
			return -1;
	}

	ec.clear();
	
	return (int)len;
}

int virtual_file_input::mount (path const& accessPath, path const& mountPoint, bool append)
{
#if STIR_WINDOWS
	utf8_path p (accessPath.c_str()), mnt_pt (mountPoint.c_str());
	return PHYSFS_mount (p.c_str(), mnt_pt.c_str(), append);
#else
	return PHYSFS_mount (accessPath.c_str(), mountPoint.c_str(), append);
#endif
}

bool virtual_file_input::init ()
{
	return PHYSFS_init (0) != 0;
}

void virtual_file_input::finit ()
{
	PHYSFS_deinit ();
}


}}