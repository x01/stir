#ifndef VIRTUAL_FILE_HPP_
#define VIRTUAL_FILE_HPP_

#include <pulmotor/stream.hpp>
#include <stir/path.hpp>

struct PHYSFS_File;

namespace stir { namespace io {

using pulmotor::error_id;
using pulmotor::k_ok;
using pulmotor::k_eof;
using pulmotor::k_read_fail;
using pulmotor::k_write_fail;
using pulmotor::k_out_of_space;

class STIR_ATTR_DLL virtual_file_input : pulmotor::basic_input_buffer
{
	PHYSFS_File*	m_physfs_file;

public:
	virtual_file_input ();
	virtual_file_input (path const& p);
	~virtual_file_input ();

	bool is_good ();
	uint64_t length ();

	virtual size_t read (void* dest, size_t count, std::error_code& ec);
	
	void swap (virtual_file_input const& arg);
	
	static int mount (path const& accessPath, path const& mountPoint, bool append);
	
	static bool init ();
	static void finit ();
};

}}


#endif