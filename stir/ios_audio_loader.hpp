#ifndef STIR_IOS_AUDIO_LOADER_HPP_
#define STIR_IOS_AUDIO_LOADER_HPP_

#include <stir/resource_loader.hpp>
#include "ios_audio.hpp"

namespace stir {
	
class ios_audio_loader : public loader<ios_audio*>
{
public:
	ios_audio_loader ();
	~ios_audio_loader ();
	
	virtual ios_audio* load( resource_id const& id, int profile );
	virtual void unload( ios_audio* resource, int profile );
	
};
	
}

#endif // STIR_IOS_AUDIO_LOADER_HPP_
