#ifndef STIR_LANGUAGE_UTIL_HPP_
#define STIR_LANGUAGE_UTIL_HPP_

#include <cstddef>
#include "platform.hpp"

namespace stir {
	
template<class T, std::size_t N>
inline size_t array_size( T (&x)[N] )
{	return N; }

	
inline int count_ones (unsigned int value)
{
#if STIR_GCC
	return __builtin_popcount (value);
#elif STIR_MSVC
	return __popcnt (value);
#endif
}
	
inline int count_ones (unsigned long long value)
{
#if STIR_GCC
	return __builtin_popcountll (value);
#elif STIR_MSVC
	return __popcnt64 (value);
#endif
}

// compile time log2
template<int N, class T, int C>
struct ct_log2_impl
{	static const T value = ct_log2_impl<(N>>1),T,C+1>::value; };

template<class T, int C>
struct ct_log2_impl<0,T,C>
{	static const T value = C-1; };
	
template<int N, class T = int>
struct ct_log2
{	static const T value = ct_log2_impl<N,T,0>::value; };
	
template<size_t Exponent>
struct ct_pow2
{	static const size_t value = 1 << Exponent; };
	
} // stir

#endif
