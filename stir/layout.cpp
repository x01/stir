#include "layout.hpp"
#include <ostream>

namespace stir
{
	
layouter::layouter()
{}
	
layouter::~layouter()
{
	for (auto g : m_groups)
		delete g;
}

rectf layouter::operator[](std::string const& name)
{
	auto it = m_lookup.find (name);
	if (it == m_lookup.end())
		return rectf(0,0,0,0);
	else
	{
		rectf r = it->second->m_rect;
		for (group* g=it->second->m_parent; g; g=g->m_parent)
			r.p += g->m_rect.p;
		return r;
	}
}
	
layouter& layouter::begin_group(layouter::Orientation orient, rectf const& r)
{
	group* g = new group (orient, Flags::none, r);
	g->m_parent = m_stack.empty() ? nullptr : m_stack.top();
	m_groups.push_back(g);
	if (!m_stack.empty ()) top().m_children.push_back(g);
	m_stack.push(g);
	return *this;
}

layouter& layouter::begin_group(std::string const& groupName, layouter::Orientation orient, rectf const& r)
{
	group* g = new group (orient, groupName, Flags::none, r);
	g->m_parent = m_stack.empty() ? nullptr : m_stack.top();
	m_groups.push_back(g);
	if (!m_stack.empty ()) top().m_children.push_back(g);
	m_stack.push(g);
	return *this;
}

std::tuple<int, float> layouter::calc_extent (layouter::Flags flag, layouter::GroupContainer const& groups)
{
	assert ((flag & (Flags::exp_width|Flags::exp_height)) == Flags::exp_width ||
			(flag & (Flags::exp_width|Flags::exp_height)) == Flags::exp_height);
	float ext = 0;
	int expandables = 0;
	for (group* g : groups)
	{
		if ((g->m_flags & flag) != Flags::none)
			expandables++;
		else if ((flag & Flags::exp_width) != Flags::none)
			ext += g->m_rect.width();
		else
			ext += g->m_rect.height();
	}
	
	return std::make_tuple(expandables, ext);
}


float layouter::get_some_parent_width(layouter::group const& me)
{
	for (group const* g = &me; g; g = g->m_parent)
		if (!g->is_spring() && g->m_rect.width() > 0)
			return g->m_rect.width();
	return 0;
}
	
float layouter::get_some_parent_height(layouter::group const& me)
{
	for (group const* g = &me; g; g = g->m_parent)
		if (!g->is_spring() && g->m_rect.height() > 0)
			return g->m_rect.height();
	return 0;
}
	
layouter& layouter::end_group()
{
	group& me = top();
	
	float w = me.m_rect.width(), h = 0;
	if (me.m_orient == Orientation::horiz)
	{
		std::tuple<int, float> expCountAndW = calc_extent(Flags::exp_width, me.m_children);
		
		float forExpandables = get_some_parent_width(me) - std::get<1>(expCountAndW);
		float expandableWidth = std::get<0>(expCountAndW) ? forExpandables / (float)std::get<0>(expCountAndW) : 0;
		
		float x = me.m_children.empty () ? 0 : me.m_children.front()->m_rect.left ();
		for (group* g : me.m_children)
		{
			if ((g->m_flags & Flags::exp_width) != Flags::none)
			{
				g->m_rect.set_posx(x);
				g->m_rect.set_width (expandableWidth) ;
			}
			else
			{
				g->m_rect.set_posx(x);
			}
			x = g->m_rect.right();
			if (h < g->m_rect.bottom())
				h = g->m_rect.bottom();
		}
		w = x;
	}
	
	
	if (me.m_orient == Orientation::vert)
	{
		std::tuple<int, float> expCountAndH = calc_extent(Flags::exp_height, me.m_children);
		
		float forExpandables = get_some_parent_height(me) - std::get<1>(expCountAndH);
		float expandableH = std::get<0>(expCountAndH) ? forExpandables / (float)std::get<0>(expCountAndH) : 0;
		
		float y = me.m_children.empty () ? 0 : me.m_children.front()->m_rect.top ();
		for (group* g : me.m_children)
		{
			if ((g->m_flags & Flags::exp_height) != Flags::none)
			{
				g->m_rect.set_posy(y);
				g->m_rect.set_height (expandableH) ;
				y = g->m_rect.bottom();
			}
			else
			{
				g->m_rect.set_posy(y);
				y = g->m_rect.bottom ();
			}
			if (w < g->m_rect.right())
				w = g->m_rect.right();
		}
		h = y;
	}
	
	// in case our size was specified as zero, we expand it to the size of children
	if (!me.is_spring() && me.m_rect.width() == 0)
		me.m_rect.set_width(w);
	
	if (!me.is_spring() && me.m_rect.height() == 0)
		me.m_rect.set_height(h);
	
	assert (!m_stack.empty());
	m_stack.pop();
	return *this;
}
	
layouter& layouter::rect(char const* name, layouter::Flags flags, rectf const& r)
{
	assert (!m_stack.empty() && "group must be started");
	group* g = new group(Orientation::none, name, flags, r);
	g->m_parent = m_stack.empty() ? nullptr : m_stack.top();
	m_groups.push_back(g);
	top().m_children.push_back(g);
	m_lookup[name] = g;
	
	return *this;
}
	
layouter& layouter::space(float amount)
{
	group* g = new group(top().m_orient, Flags::space, rectf(0,0,amount,amount));
	g->m_parent = m_stack.empty() ? nullptr : m_stack.top();
	m_groups.push_back(g);
	top().m_children.push_back(g);
	return *this;
}
	
layouter& layouter::spring()
{
	group* g = new group(top().m_orient, flags_from_orientation(top().m_orient) | Flags::spring, rectf(0,0,0,0));
	g->m_parent = m_stack.empty() ? nullptr : m_stack.top();
	m_groups.push_back(g);
	top().m_children.push_back(g);
	return *this;
}

void layouter::dumper::dump (layouter::group* g, std::string const& prefix)
{
	if (!g) return;
	
	char buf[256];
	char const* s_orient[] = { "none", "vert", "horiz" };
	if (g->is_spring())
		snprintf (buf, sizeof(buf), "%*s<spring> orient:%s", m_indent * 3, "", s_orient[(int)g->m_orient]);
	else if (g->is_space())
		snprintf (buf, sizeof(buf), "%*s<space> (%.1f,%.1f) orient:%s", m_indent * 3, "", g->m_rect.width (), g->m_rect.height(), s_orient[(int)g->m_orient]);
	else
		snprintf (buf, sizeof(buf), "%*s'%s' (%.1f,%.1f,%.1f,%.1f) orient:%s", m_indent * 3, "", g->m_name.c_str(),
				  g->m_rect.left(), g->m_rect.top(), g->m_rect.width(), g->m_rect.height(),
				  s_orient[(int)g->m_orient]);
	
	m_os << buf << std::endl;
	
	for (auto item : g->m_children)
	{
		m_indent ++;
		dump (item, prefix);
		m_indent--;
	}
}

std::ostream& operator<<(std::ostream& os, layouter const& l)
{
	layouter::dumper d (os);
	d.dump(l.root(), "");
	return os;
}

}
