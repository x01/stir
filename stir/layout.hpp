#ifndef STIR_LAYOUT_
#define STIR_LAYOUT_

#include <vector>
#include <stack>
#include <map>
#include <string>

#include "flat_types.hpp"
#include "basic_util.hpp"

namespace stir
{

class layouter
{
public:
	enum class Flags { none = 0, exp_width = 0x01, exp_height = 0x02, spring = 0x04, space = 0x08 };
	enum class Orientation { none, vert, horiz };
	
private:
	//	layouter* m_parent;
	
	layouter (layouter const& r) = delete;
	layouter const& operator= (layouter const& r) = delete;
	
	struct group;
	typedef std::vector<group*> GroupContainer;
	
	struct group
	{
		std::string m_name;
		rectf m_rect;
		GroupContainer m_children; // contains elements on this hier level
		group* m_parent;
		Flags m_flags;
		Orientation m_orient;
		
		bool is_spring () const;
		bool is_space () const;
		
		group (Orientation orient, std::string const& name, Flags flags, rectf const& r)
		: group(orient, flags, r) { m_name = name; }
		group (Orientation orient, Flags flags, rectf const& r)
		: m_orient(orient), m_flags(flags), m_rect(r), m_parent(nullptr) {}
	};
	
	static Flags flags_from_orientation (Orientation o) {
		return o == Orientation::horiz ? Flags::exp_width : Flags::exp_height;
	}
	
	std::stack<group*> m_stack;
	
	typedef std::map<std::string, group*> GroupMap;
	GroupMap m_lookup;
	GroupContainer m_groups;
	
public:
	explicit layouter();
	~layouter();
	
	rectf operator[](std::string const& name);
	
	layouter& begin_group(Orientation orient, rectf const& r = rectf::get_null());
	layouter& begin_group(std::string const& groupName, Orientation orient, rectf const& r = rectf::get_null());
	layouter& end_group();
	layouter& rect(char const* name, Flags flags, rectf const& r);
	layouter& rect(char const* name, Flags flags, float w, float h) { return rect (name, flags, rectf(0,0,w,h)); }
	layouter& space(float amount);
	layouter& spring();
	
	std::tuple<int, float> calc_extent (Flags flag, GroupContainer const& groups);
	
	float get_some_parent_width(group const& me);
	float get_some_parent_height(group const& me);
	
	group* root () const { return m_groups.empty() ? nullptr : m_groups.front(); }
	
	friend std::ostream& operator<<(std::ostream& os, layouter const& l);
	
	class dumper
	{
		std::ostream& m_os;
		int m_indent = 0;
		
	public:
		dumper (std::ostream& os) : m_os(os) {}
		
		void dump (layouter::group* g, std::string const& prefix);
	};
	
private:
	group& top() { assert (!m_stack.empty()); return *m_stack.top(); }
};

STIR_DECL_ENUM_FLAGS(layouter::Flags)
STIR_DECL_ENUM_FLAGS(layouter::Orientation)
	
inline bool layouter::group::is_spring () const { return (m_flags & Flags::spring) != Flags::none; }
inline bool layouter::group::is_space () const { return (m_flags & Flags::space) != Flags::none; }


	

std::ostream& operator<<(std::ostream& os, layouter const& l);

}

#endif
