#ifndef STIR_LIMIT_STRING_HPP_
#define STIR_LIMIT_STRING_HPP_

#include <functional>
#include <cstddef>
#include <algorithm>
#include <cassert>
#include <pulmotor/pulmotor_fwd.hpp>

namespace stir {

namespace {
	template<int Bytes> struct select_size_type { typedef size_t type; };
	template<> struct select_size_type<1> { typedef u8 type; };
	template<> struct select_size_type<2> { typedef u16 type; };
	template<> struct select_size_type<3> { typedef u32 type; };
	template<> struct select_size_type<4> { typedef u32 type; };
}

template<size_t MaxL, class CharT = char>
struct limit_string
{
	typedef typename select_size_type<MaxL <= 255 ? 1 : MaxL >= 65536 ? 4 : 2>::type size_type;
	
	static size_t string_length (char const* str) { return ::strlen (str); }
	static size_t string_length (wchar_t const* str) { return ::wcslen (str); }

	static size_t chars_max (size_t l) { return l > MaxL ? MaxL : l; }

	typedef CharT value_type;
	
private:
	value_type data_[MaxL+1];
	size_type size_;
	
public:
	typedef CharT* iterator;
	typedef CharT const* const_iterator;
	
	static bool can_fit (value_type const* a) { return string_length (a) <= MaxL; }

	limit_string ()
	:	size_(0) {
		memset (data_, 0, sizeof(data_));
		//data_[0] = 0;
	}
	
	limit_string (CharT const* s) {
		assign_ (s, chars_max (string_length(s)));
	}

	limit_string (CharT const* s, size_t len) {
		assign_ (s, chars_max (string_length(s)));
	}
	
	limit_string (std::string const& s) {
		assign_ (s.c_str(), chars_max (s.size()));
	}
	
	template<int MaxLa>
	limit_string (limit_string<MaxLa, CharT> const& a) {
		assign_ (a.ptr (), chars_max (a.size ()));
	}

	template<int MaxLa>
	limit_string& operator=(limit_string<MaxLa, CharT> const& a) {
		assign_ (a.ptr (), chars_max (a.size ()));
		return *this;
	}
	
	~limit_string () { }
	
	size_t size () const { return size_; }
	size_t capacity () const { return MaxL; }
	
	iterator begin () { return ptr (); }
	const_iterator begin () const { return ptr (); }

	iterator end () { return ptr () + size_; }
	const_iterator end () const { return ptr () + size_; }
	
	CharT const* c_str () const { return ptr (); }
	CharT* data () { return data_; }
	
	CharT operator [] (size_t index) const { return ptr () [index]; }
	CharT& operator [] (size_t index) { return ptr () [index]; }
	
	bool operator== (limit_string const& a) const {
		return size_ == a.size_ &&
		   	memcmp (a.ptr (), ptr (), std::min (a.size (), size()) * sizeof(value_type)) == 0;
	}

	bool operator< (limit_string const& b) const { return strcmp (c_str(), b.c_str()); }
	
	template<class ArchiveT>
	void serialize (ArchiveT& ar, unsigned version) {
		using namespace pulmotor;
		ar | size_ | data_;
	}
	
	template<class ArchiveT>
	void archive (ArchiveT& ar, unsigned version) {
		ar	| pulmotor::as<pulmotor::u32>(size_)
		| pulmotor::memblock (data_, size_);
	}
	
private:
	CharT const* ptr () const{ return  data_; }
	
	void assign_ (CharT const* s, size_t len) {
		assert (len <= MaxL);
		size_ = len;
		memcpy (data_, s, len * sizeof (value_type));
#if 1
		size_t const toClean = MaxL - size_ + 1;
		assert (size_ + toClean == MaxL + 1);
		memset (data_ + size_, 0, toClean * sizeof(value_type));
#else
		data_[len] = 0;
#endif
	}	
};
	
template<int N, class T, class Traits, class Alloc>
inline bool operator== (limit_string<N, T> const& a, std::basic_string<T, Traits, Alloc> const& b) {
	return a.size() == b.size() && memcmp (a.c_str(), b.c_str(), a.size () * sizeof(T)) == 0;
}
	
template<int N, class T, class Traits, class Alloc>
inline bool operator!= (limit_string<N, T> const& a, std::basic_string<T, Traits, Alloc> const& b) {
	return a.size() != b.size() || memcmp (a.c_str(), b.c_str(), a.size () * sizeof(T)) != 0;
}
	

template<int N, class T, class Traits, class Alloc>
inline bool operator== (std::basic_string<T, Traits, Alloc> const& b, limit_string<N, T> const& a) {
	return a.size() == b.size() && memcmp (a.c_str(), b.c_str(), a.size () * sizeof(T)) == 0;
}

template<int N, class T, class Traits, class Alloc>
inline bool operator!= (std::basic_string<T, Traits, Alloc> const& b, limit_string<N, T> const& a) {
	return a.size() != b.size() || memcmp (a.c_str(), b.c_str(), a.size () * sizeof(T)) != 0;
}
	

inline limit_string<16, char> to_limit_string (int a) {
	limit_string<16, char> result;
#ifdef _MSC_VER
	_snprintf (result.data(), 16, "%d", a);
#else
	snprintf (result.data(), 16, "%d", a);
#endif
	return result;
}

}

#endif
