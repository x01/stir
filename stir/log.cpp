//#include "stdafx.hpp"
#include "log.hpp"
#include <list>
#include <cstdarg>
#include "language_util.hpp"
#include <stir/wildcard.hpp>

namespace stir { namespace log {

static double_chain<log>& get_root()
{
	static double_chain<log> log_root;
	return log_root;
}

//static log* last_log_created = 0;
verbose_t verbose_level = verbose_max;

log_iterator STIR_ATTR_DLL log_begin()	{ return log_iterator (get_root().get_next()); }
log_iterator STIR_ATTR_DLL log_end()		{ return log_iterator (&get_root()); }

log::log (log_name_t const& name)
:	m_enabled (true)
,	m_name (name)
{
	get_root().tail_insert (*this);
}

log::~log ()
{
	unlink_this ();
}

#if STIR_WINDOWS
void log::printf (wchar_t const* fmt, ...)
{
	string fmtstr;
	wchar_t buffer [2048];
	
	va_list vl;
	va_start (vl,fmt);
	
	vswprintf (buffer, stir::array_size(buffer), fmt, vl );
	
	m_buffer += buffer;
	
	flush_line();
}
#endif

void log::printf (char const* fmt, ...)
{
	string fmtstr;
	char buffer [2048];
	
	va_list vl;
	va_start (vl,fmt);
	
	size_t size = vsnprintf (buffer, stir::array_size(buffer), fmt, vl );
	
	size_t end = m_buffer.size ();
	m_buffer.resize (end + size);
	
	for (size_t i=0; i<size; ++i)
		m_buffer [end + i] = buffer [i];
	
//	m_buffer [end + size] = 0;

	flush_line();
}

void log::flush_line ()
{
	while (1)
	{
		size_t nl = m_buffer.find_first_of ('\n');
		if ( nl != string::npos) {
			for (std::list<write_function>::iterator it = m_write_func.begin (), end = m_write_func.end (); it != end; ++it)
				(*it)(m_name, m_buffer.substr (0, nl));
			m_buffer.erase (0, nl+1);
		} else
			break;
	}	
}

void STIR_ATTR_DLL add_writer (log_name_t const& pattern, write_function const& fun)
{
	for (log_iterator it = log_begin (); it != log_end (); ++it)
		if (wildcard::match (it->get_name ().c_str(), pattern.c_str()))
			it->add_write_function (fun);
}

void STIR_ATTR_DLL remove_writer (log_name_t const& pattern, write_function const& wrfun)
{
	//for (log_iterator it = log_begin (); it != log_end (); ++it)
	//	if (wildcard::match (it->get_name ().c_str(), pattern.c_str())
	//		it->remove_write_function (fun);
}

void STIR_ATTR_DLL clear_writers ()
{
	for (log_iterator it = log_begin (); it != log_end (); ++it)
		it->clear_write_functions ();
}

} } // stir::log
