#ifndef STIR_LOG_HPP_
#define STIR_LOG_HPP_

#include <boost/function.hpp>
#include <string>
#include <list>
#include "basic_types.hpp"
#include "basic_cstring.hpp"
#include "complex_types.hpp"
#include "platform.hpp"

#include "logfwd.hpp"
#include "chain.hpp"
// define STIR_DISABLE_LOGS to 1 to disable logging at compile time

namespace stir { namespace log {

typedef boost::function<void (log_name_t const&,log_message_t const&)> write_function;

// a) no overhead if disabled
// b) printf formatting

struct STIR_ATTR_DLL log : double_chain<log>
{
	log( log_name_t const& name );
	~log();

	bool is_enabled() const { return m_enabled; }
	void set_enabled(bool enable) { m_enabled = enable; }
	
	void printf (wchar_t const* fmt, ...);
	void printf (char const* fmt, ...);
	
	void add_write_function (write_function	 const& write_func)
	{	m_write_func.push_back (write_func); }
	void clear_write_functions () {
		m_write_func.clear ();
	}

//	void remove_write_function (write_function const& write_func)
//	{	m_write_func.remove (write_func); }

	//
	log_name_t const& get_name () { return m_name; }
	
private:
	void flush_line ();

private:
	bool						m_enabled;
	std::list<write_function>	m_write_func;
	log_name_t					m_name;
	string						m_buffer;
};

#define SL(x) (x.is_enabled() || !::stir::log::disable_logging)


namespace log_detail {
inline void write_string (log& l, char const* s) { l.printf ("%s", s); }
inline void write_string (log& l, wchar_t const* s) { l.printf ("%ls", s); }
}

inline log& operator<< (log& l, char const* s) { if (SL(l)) l.printf ("%s", s); return l; }
inline log& operator<< (log& l, wchar_t const* s) { if (SL(l)) l.printf ("%ls", s); return l; }

inline log& operator<< (log& l, int a) { if (SL(l)) l.printf ("%d", a); return l; }
inline log& operator<< (log& l, long a) { if (SL(l)) l.printf ("%l", a); return l; }
inline log& operator<< (log& l, char a) { if (SL(l)) l.printf ("%c", a); return l; }

inline log& operator<< (log& l, unsigned int a) { if (SL(l)) l.printf ("%du", a); return l; }
inline log& operator<< (log& l, unsigned long a) { if (SL(l)) l.printf ("%lu", a); return l; }
inline log& operator<< (log& l, unsigned char a) { if (SL(l)) l.printf ("%cu", a); return l; }

inline log& operator<< (log& l, float a) { if (SL(l)) l.printf ("%f", a); return l; }
inline log& operator<< (log& l, double a) { if (SL(l)) l.printf ("%f", a); return l; }

inline log& endl (log& l) { if (SL(l)) l.printf ("\n"); return l; }
inline log& operator<< (log& l, log& (*op) (log&)) { return op (l); }

inline log& operator<< (log& l, void const* p) { if (SL(l)) l.printf ("%p", p); return l; }
template<class T>
inline log& operator<< (log& l, T const* p) { if (SL(l)) l.printf ("(%s*)%p", typeid(T).name(), p); return l; }

template<class CharT, class TraitsT, class AllocatorT>
inline log& operator<< (log& l, std::basic_string<CharT, TraitsT, AllocatorT> const& s) {
	log_detail::write_string (l, s.c_str());
	return l;
}

#undef SL

void STIR_ATTR_DLL add_writer (log_name_t const& pattern, write_function const& wrfun);
void STIR_ATTR_DLL remove_writer (log_name_t const& pattern, write_function const& wrfun);
void STIR_ATTR_DLL clear_writers ();


/*
template<class CharT, class TraitsT>
inline log& operator<< (log& l, basic_cstring<CharT, TraitsT> const& s) { l << s.c_str(); }
template<class CharT, class TraitsT>
inline log& operator<< (log& l, basic_string<CharT, TraitsT> const& s) { l << s.c_str(); }
template<class CharT>
inline log& operator<< (log& l, const_string<CharT> const& s) { l << s.c_str(); }
*/

#define STIR_LOG(name) \
	if (!name##_stir_log.is_enabled() || ::stir::log::disable_logging) { \
	} else \
		name##_stir_log

#define STIR_LOGV(name,verbose) \
	if (!name##_stir_log.is_enabled() || ::stir::log::disable_logging || (verbose_t)verbose > ::stir::log::verbose_level ) { \
	} else \
		name##_stir_log
		
#define STIR_DEFINE_LOG(symbol,name) \
	::stir::log::log symbol##_stir_log (name)
#define STIR_DECLARE_LOG(symbol) \
	extern ::stir::log::log symbol##_stir_log

#define STIR_DEFINE_LOG_INTERNAL(symbol,name) \
	::stir::log::log STIR_ATTR_DLL symbol##_stir_log (name)
#define STIR_DECLARE_LOG_INTERNAL(symbol) \
	extern ::stir::log::log STIR_ATTR_DLL symbol##_stir_log

#define STIR_LOG_ENABLED(symbol) \
	(symbol##_stir_log.is_enabled() && !::stir::log::disable_logging)

#define STIR_LOG_ACCESS(symbol) \
	symbol##_stir_log

//STIR_LOG(name).printf( "simple %d moo" );

} } // stir::log

#endif // STIR_LOG_HPP_
