#ifndef STIR_LOG_FWD_HPP_
#define STIR_LOG_FWD_HPP_

#include "platform.hpp"
#include "basic_types.hpp"
#include "chain.hpp"

namespace stir { namespace log {

typedef unsigned int verbose_t;
enum { verbose_max = 0x7fffffff };
typedef string log_name_t;
typedef string log_message_t;

#if STIR_DISABLE_LOGS
bool const disable_logging = true;
#else
bool const disable_logging = false;
#endif

extern verbose_t verbose_level;

struct STIR_ATTR_DLL log;

typedef double_chain_iterator<log> log_iterator;

log_iterator STIR_ATTR_DLL log_begin();
log_iterator STIR_ATTR_DLL log_end();

// a) no overhead if disabled
// b) printf formatting

#define STIR_LOG(name) \
	if (!name##_stir_log.is_enabled() || ::stir::log::disable_logging) { \
	} else \
		name##_stir_log

#define STIR_LOGV(name,verbose) \
	if (!name##_stir_log.is_enabled() || ::stir::log::disable_logging || (verbose_t)verbose > ::stir::log::verbose_level ) { \
	} else \
		name##_stir_log
		
#define STIR_DEFINE_LOG(symbol,name) \
	::stir::log::log symbol##_stir_log (name)
#define STIR_DECLARE_LOG(symbol) \
	extern ::stir::log::log symbol##_stir_log

#define STIR_DEFINE_LOG_INTERNAL(symbol,name) \
	::stir::log::log STIR_ATTR_DLL symbol##_stir_log (name)
#define STIR_DECLARE_LOG_INTERNAL(symbol) \
	extern ::stir::log::log STIR_ATTR_DLL symbol##_stir_log

#define STIR_LOG_ENABLED(symbol) \
	(symbol##_stir_log.is_enabled() && !::stir::log::disable_logging)

#define STIR_LOG_ACCESS(symbol) \
	symbol##_stir_log

//STIR_LOG(name).printf( "simple %d moo" );

} } // stir::log

#endif // STIR_LOG_FWD_HPP_
