#ifndef STIR_LUA_BINDER_HPP
#define STIR_LUA_BINDER_HPP

#include <stir/lua/lua_fwd.hpp>
//#include <stir/lua/lua_utility.hpp>

#include <boost/type_traits.hpp>
#include <type_traits>

#include <tuple>

namespace stir { namespace lua {
	
namespace lconv
{
	template<class T> typename std::enable_if<std::is_integral<T>::value>::type
	pop (lua_State* L, T& i) { i = lua_tounsigned(L, -1); lua_pop(L,1); }
	
	template<class T> typename std::enable_if<std::is_floating_point<T>::value>::type
	pop (lua_State* L, T& i) { i = lua_tonumber(L, -1); lua_pop(L,1); }
	
	inline void pop (lua_State* L, std::string& s) { size_t l; char const* p = lua_tolstring(L, -1, &l); s.assign(p, l); lua_pop(L,1); }
	
	
	template<class T>
	typename std::enable_if<std::is_integral<typename std::decay<T>::type>::value>::type
	push (lua_State* L, T i)
	{ lua_pushunsigned(L, i); }
	
	template<class T>
	typename std::enable_if<std::is_floating_point<typename std::decay<T>::type>::value>::type
	push (lua_State* L, T i)
	{ lua_pushnumber(L, i); }
	
	inline void push (lua_State* L, char const* s) { lua_pushstring(L, s); }
	inline void push (lua_State* L, std::string const& s) { lua_pushstring(L, s.c_str()); }
};

template<int N, typename R, typename... Args>
struct call_helper
{
	template<class Tuple, class... CurArgs>
	static R call(lua_State* L, R(*f)(lua_State*, Args...), Tuple&& t, CurArgs... cargs)
	{
		return call_helper<N-1, R, Args...>::call (L, f, t, std::get<N-1>(t), cargs...);
	}
};


template<typename R, typename... Args>
struct call_helper<0, R, Args...>
{
	template<class Tuple, class... CurArgs>
	static R call(lua_State* L, R(*f)(lua_State*, Args...), Tuple&& t, CurArgs... cargs)
	{
		return (*f) (L, cargs...);
	}
};

template<int N>
struct arg_extractor {
	template<class T> static void ex(lua_State* L, T& t) {
		lconv::pop (L, std::get<N-1>(t));
		arg_extractor<N-1>::ex (L, t);
	}
};

template<> struct arg_extractor<0> {
	template<class T> static void ex(lua_State* L, T& t) {}
};

template<class T>
T extract_args (lua_State* L)
{
	T t;
	arg_extractor<std::tuple_size<T>::value>::ex (L, t);
	return t;
}

template<class R>
struct call_and_push_result
{
	template<class Tuple, class... Args>
	static int call(lua_State* L, R(*f)(lua_State*, Args...), Tuple&& t)
	{
		R ret = call_helper<std::tuple_size<Tuple>::value, R, Args...>::call (L, f, t);
		// push the return onto stack
		lconv::push (L, ret);
		return 1;
	}
};

template<>
struct call_and_push_result<void>
{
	template<class Tuple, class... Args>
	static int call(lua_State* L, void (*f)(lua_State*, Args...), Tuple&& t)
	{
		static const int arg_count = std::tuple_size<typename std::remove_reference<Tuple>::type>::value;
		call_helper<arg_count, void, Args...>::call (L, f, t);
		return 0;
	}
};


template<typename R, typename... Args>
struct bind_func
{
	static int bound(lua_State* L)
	{
		typedef typename std::tuple<Args...> args_tuple;
		static const int arg_count = std::tuple_size<args_tuple>::value;
		
		if (lua_gettop(L) != arg_count)
		{
			char buf[128];
			snprintf(buf, sizeof(buf), "function expected %d arguments, got %d", arg_count, lua_gettop(L));
			lua_pushstring(L, buf);
			lua_error(L);
		}
		
		void* ud = lua_touserdata(L, lua_upvalueindex(1));
		R (*pf)(lua_State*, Args...) = reinterpret_cast<R (*)(lua_State*, Args...)>(ud);
		
		int valuesOnStack = call_and_push_result<R>::call(L, pf, std::move(extract_args<args_tuple> (L)));
		
		return valuesOnStack;
	}
};

struct bridge
{
	lua_State* m_L;
	bridge(lua_State* L) : m_L(L) {}
	
	template<typename R, typename... Args>
	void bind (char const* name, R (*f)(lua_State*, Args...))
	{
		lua_pushlightuserdata(m_L, (void*)f);
		lua_pushcclosure(m_L, &bind_func<R, Args...>::bound, 1);
		lua_setglobal(m_L, name);
	}
	
	template<typename R, class C, typename... Args>
	void bind (char const* name, R (C::*f)(lua_State*, Args...))
	{
		lua_pushlightuserdata(m_L, (void*)f);
		//		lua_pushcclosure(m_L, &bind_func<R, Args...>::bound, 1);
		lua_setglobal(m_L, name);
	}
	
	template<class T>
	struct class_meta
	{
		template<class... Args>
		void create_instance(lua_State* L, Args... args)
		{
			lua_newtable(L);
			
			T* p = new T(args...);
			
			lua_pushstring(L, "_stir_object"); // attach ourselves to a lua table
			lua_pushlightuserdata(L, p);
			lua_settable(L, -3);
			
			// return table on the lua stack
		}
	};
	
	template<class T>
	struct class_binder
	{
		bridge& m_B;
		char const* m_Name;
		
		template<typename... Args>
		static void ctor(lua_State* L, Args... args)
		{
			class_meta<T>* ud = (class_meta<T>)lua_touserdata(L, lua_upvalueindex(1));
			ud->create_instance(L, args...);
		}
		
		class_binder(bridge& b, char const* name) : m_B(b), m_Name(name)
		{
			//			lua_State* L = m_B.m_L;
			
			// 1. expose constructor
			// 2. create metatable
			// 3. assign type-info to metatable(?), ctor(?)
			
			m_B.bind (m_Name, &T::T);
		}
		
		template<typename R, typename... Args>
		void bind (char const* name, R (T::*f)(lua_State*, Args...))
		{
		}
	};
	
	template<typename T>
	class_binder<T> class_(char const* className)
	{
		return class_binder<T> (*this, className);
	}
};
	
}} // stir::lua
#endif
