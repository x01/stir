#ifndef STIR_LUA_FWD_HPP_
#define STIR_LUA_FWD_HPP_

extern "C"
{

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

}

#endif // STIR_LUA_FWD_HPP_
