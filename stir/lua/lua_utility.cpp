#include <stir/lua/lua_utility.hpp>
#include <stir/stir_logs.hpp>
//#include <stir/utility.hpp>
#include <stir/filesystem.hpp>
#include <stir/path.hpp>
#include <iostream>
#include <fstream>

#include <pulmotor/stream.hpp>


namespace stir { namespace lua {
	
enum class errc
{
	ok = LUA_OK,
	yield = LUA_YIELD,
	erun = LUA_ERRRUN,
	esyntax = LUA_ERRSYNTAX,
	ememory = LUA_ERRMEM,
	egcmm = LUA_ERRGCMM,
	eerr = LUA_ERRERR,
	
	file_read_error = 100
};
	
class lua_error_category_impl : public std::error_category
{
public:
	virtual const char* name() const noexcept
	{
		return "lua_error";
	}
	
	virtual std::string message(int ev) const {
		std::string rv;
		switch ((errc)ev)
		{
			case errc::ok: rv = "success"; break;
			case errc::yield: rv = "yield"; break;
			case errc::erun: rv = "failed to run"; break;
			case errc::esyntax: rv = "syntax error"; break;
			case errc::ememory: rv = "out of memory error"; break;
			case errc::egcmm: rv = "gc memory manager (?) error"; break;
			case errc::file_read_error: rv = "can't read file"; break;
			default:
			case errc::eerr: rv = "generic lua error"; break;
		}
		return rv;
	}
};
	
std::error_category& lua_error_category () { static lua_error_category_impl g_errcat; return g_errcat; }

inline std::error_condition make_error_condition(errc e) {
	return std::error_condition (static_cast<int>(e), lua_error_category());
}

inline std::error_code make_error_code(errc e) {
	return std::error_code (static_cast<int>(e), lua_error_category());
}

}}

namespace std {
	template<> struct is_error_condition_enum<stir::lua::errc> : std::true_type { };
	template<> struct is_error_code_enum<stir::lua::errc> : std::true_type { };
}

namespace stir { namespace lua {
	
namespace fs = stir::filesystem;

void set_user_data( lua_State* L, int table_index, char const* name, void* data )
{
	::lua_pushstring( L, name );
	::lua_pushlightuserdata( L, data );
	::lua_settable( L, table_index );
}

void* get_user_data( lua_State* L, int table_index, char const* name )
{
	::lua_pushstring( L, name );
	::lua_gettable( L, table_index );
	void* p = ::lua_touserdata( L, -1 );
	::lua_pop( L, 1 );
	return p;
}

chunk load_chunk( lua_State* L, path const& fname, std::error_code& ec)
{
	using namespace pulmotor;
	
	size_t fileSize = fs::file_size (fname.c_str());
	std::fstream f (fname.c_str(), std::ios_base::in);
	if(!f.good ()) {
		ec = errc::file_read_error;
		return chunk();
	}
	
	std::vector<char> buffer;
	buffer.resize(fileSize);
	
	f.read(buffer.data(), fileSize);
	MemoryReader reader(buffer.data(), buffer.size ());
	
	int retVal = lua_load(L, &MemoryReader::read_func, &reader, fname.c_str(), "bt");
	ec = static_cast<errc> (retVal);
	if( retVal == 0 )
		return chunk(L);

	STIR_LOG( warning ).printf("Loading '%s' failed\n", fname.c_str());
	return chunk();
}

path_track::path_track()
{
}
	
//	lua::ctx(L).name (L, "myfun").call(100, "hello");
//	lua::ctx(L).call(L, "myfun", 100, 30);
//	lua::ctx(L) [this] [cclosure (&path_track::include_func, 1)].setglobal();

void path_track::bind( lua_State* L )
{
	lua_pushlightuserdata(L, this);
	lua_pushcclosure(L, &path_track::include_func, 1);
	lua_setglobal(L, "include");
}

void path_track::release( lua_State* L )
{
	lua_pushnil(L);
	lua_setglobal(L, "include");
}

bool path_track::include( path const& filename)
{
	if (filename.has_root_path())
		return m_files.push_back(filename), true;
		
	// look in the file's that's including directory first
	if (m_files.empty()) {
		if (fs::path_exists(filename))
			return m_files.push_back(filename), true;
	} else {
		path curDir = m_files.back ().parent ();
		path fromCur = curDir / filename;
		if (fs::path_exists(fromCur)) {
			m_files.push_back(fromCur);
			return true;
		}
	}
	
	// if not found, go though system dirs
	for (path const& sys : m_system)
	{
		path fromSys = sys / filename;
		if (fs::path_exists(fromSys))
		{
			m_files.push_back(fromSys);
			return true;
		}
	}
	
	return false;
}

void path_track::back()
{
	m_files.pop_back();
}

int path_track::include_func( lua_State* L )
{
	char buf[128];
	path_track* self = static_cast<path_track*>(lua_touserdata(L, lua_upvalueindex(1)));

	// get params
	int n = lua_gettop( L );
	if( n < 1 || lua_type( L, 1 ) != LUA_TSTRING )
	{
		snprintf (buf, sizeof(buf), "include requires one string argument, got %d args", n);
		lua_pushstring(L, buf );
		lua_error(L);
	}

	const char *fname = luaL_optstring(L, 1, NULL);

	if (!self->include(fname))
	{
		snprintf (buf, sizeof(buf), "unable to locate file '%s'", fname ? fname : "<null>");
		lua_pushstring( L, buf );
		lua_error( L );
	}
	int result = 0;

	std::error_code ec;
	chunk inclChunk = load_chunk(L, self->current_file(), ec);
	if (ec) {
		self->back();
		lua_pushfstring( L, "loading lua chunk failed: (%d) %s\n", ec.value(), ec.message().c_str() );
		lua_error( L );
	}
	
	inclChunk.call(ec, NULL);
	
	result = lua_gettop(L) - 1; // -1 because there is string before calling "include" function
	
	self->back();

	return result;
}

/*const char * block_reader( lua_State *L, void *data, size_t *size )
{
	block_reader_state* rs = static_cast<block_reader_state*>( data );
	if( rs->m_offset < rs->m_range.size() )
	{
		char const* p = (char const*)(rs->m_range.begin() + rs->m_offset);
		*size = rs->m_range.size() - rs->m_offset;
		rs->m_offset += *size;
		return p;
	}
	else
		return 0;
}*/
	
char const* MemoryReader::read_func(lua_State *L, void *data, size_t *size )
{
	MemoryReader* me = (MemoryReader*)data;
	if (me->m_ptr >= me->m_end)
		return nullptr;
	else
	{
		char* p = me->m_ptr;
		*size = me->m_end - me->m_ptr;
		me->m_ptr += *size;
		return p;
	}
}

//
chunk::chunk( lua_State* L )
:	m_L( L )
{
	m_chunkref.ref( m_L );
}

chunk::chunk( chunk const& r )
:	m_L( r.m_L )
{
	if (r.m_chunkref.is_valid()) {
		r.m_chunkref.rawgeti( m_L );
		m_chunkref.ref(m_L);
	}
}

chunk& chunk::operator=( chunk const& r )
{
	chunk tmp( r );
	tmp.swap( *this );
	return *this;
}

void chunk::swap( chunk& a )
{
	std::swap( m_L, a.m_L );
	m_chunkref.swap( a.m_chunkref );
}

chunk::~chunk()
{
	if (m_chunkref.is_valid())
		m_chunkref.unref( m_L );
}
	
void chunk::push()
{
	m_chunkref.rawgeti(m_L);
}

void chunk::call(std::error_code& ec, std::string* errorString)
{
	m_chunkref.rawgeti( m_L );
	int result = lua_pcall( m_L, 0, 1, 0 );
	ec = static_cast<errc>(result);
	if (result != 0 && errorString)
		 *errorString = lua_tostring( m_L, -1 );
	lua_pop(m_L,1);
}

//
context::context( bool loadBaseLibraries )
{
	m_lua_state = ::luaL_newstate();
//	if( loadBaseLibraries )
//	{
// for now we load all the libs anyways so that package.loaders gets created
// in case we'll want to strip this down, load only `package' lib and skip the rest
	luaL_openlibs( m_lua_state );
//	}
	
	register_reader(m_lua_state);
	
	m_path_track.bind( m_lua_state );
}

context::~context()
{
	m_path_track.release( m_lua_state );
	::lua_close( m_lua_state );
}
	
void context::register_reader(lua_State* L)
{
	lua_getglobal(L, "package");
	if (lua_isnil(L, -1)) {
		STIR_LOG(error).printf("Table `package' couldn't be found\n");
		return;
	}
	
	lua_getfield(L, -1, "searchers");
	if (lua_isnil(L, -1)) {
		STIR_LOG(error).printf("Unable to find `loaders' in `package' table\n");
		return;
	}

	lua_len(L, -1);
	int searchersSize = (int)lua_tointeger(L, -1);
	lua_pop(L, 1);
	
	lua_pushlightuserdata(L, this);
	lua_pushcclosure(L, &context::package_loader, 1);
    lua_rawseti(L, -2, searchersSize+1);
}
	
int context::package_loader(lua_State* L)
{
	char const* filename = lua_tostring(L, -1);
	
	stir::path fp = stir::path(filename) + ".lua";
	
	STIR_LOG(script).printf ("Loading '%s'\n", filename);
	
	char buf[128];
	context* me = static_cast<context*>(lua_touserdata(L, lua_upvalueindex(1)));
	me->m_path_track.include(fp);
//	{
//		snprintf (buf, sizeof(buf), "can't locate '%s'", fp.c_str());
//		lua_pushstring(L, buf );
//		return 1;
//	}
	
	std::error_code ec;
	chunk c = load_chunk(me->m_lua_state, me->m_path_track.current_file(), ec);
	
	if (ec)
	{
		snprintf (buf, sizeof(buf), "failed to load '%s' (err:%s)", me->m_path_track.current_file().c_str(), ec.message().c_str());
		me->m_path_track.back();
		lua_pushstring(L, buf );
		return 1;
	}
	
	me->m_path_track.back();
	c.push();
	
	return 1;
}

chunk context::load( stir::string const& filename, std::error_code& ec)
{
	m_path_track.include(filename);
	chunk c = load_chunk (m_lua_state, m_path_track.current_file(), ec);
	m_path_track.back();
	
	return c;
}

void display_stack( lua_State* L, std::ostream& os )
{
	int num = lua_gettop(L);
	os << "stack-top: " << num << ": ";
	if( num < 0 )
		os << "<empty>";
	for( int i=1; i<=num; ++i )
	{
		int tp = lua_type( L, i );
		os << i << ":" << lua_typename( L, tp ) << ";";
	}
	os << "\r\n";
}

}}
