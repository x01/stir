#ifndef STIR_LUA_UTILITY_HPP_
#define STIR_LUA_UTILITY_HPP_

#include <string>
#include <vector>
#include <cassert>
#include <system_error>

#include <boost/noncopyable.hpp>
#include <stir/lua/lua_fwd.hpp>
#include <stir/path.hpp>

#include <string>
#include <stack>

namespace stir { namespace lua {
	
class context;

class error : public std::runtime_error
{
public:
	error( std::string const& err ) : std::runtime_error(err) {}
};

class parse_error : public lua::error
{
public:
	parse_error( std::string const& err ) : error(err) {}
};

class execute_error : public lua::error
{
public:
	execute_error( std::string const& err, int errCode ) : error(err), m_error_code(errCode) {}

	int error_code() const { return m_error_code; }

private:
	int	m_error_code;
};

struct reference
{
	reference() : m_reference( LUA_NOREF ) {}

	bool is_valid() const { return m_reference != LUA_NOREF; }

	void ref( lua_State* L)
	{	m_reference = luaL_ref( L, LUA_REGISTRYINDEX ); }
	void unref( lua_State* L )
	{	luaL_unref( L, LUA_REGISTRYINDEX, m_reference ); m_reference = LUA_NOREF; }

	void push( lua_State* L ) const
	{	lua_pushnumber( L, m_reference ); }
	int get() const
	{	return m_reference; }
	
	void rawgeti( lua_State* L ) const
	{	lua_rawgeti( L, LUA_REGISTRYINDEX, m_reference ); } // registry[m_ref] >> {top}
	void rawseti (lua_State* L, int n) const // registry[n] = {top}
	{	lua_rawseti(L, LUA_REGISTRYINDEX, n); }
	void rawsetp (lua_State* L, void* p) const // registry[p] = {top}
	{	lua_rawsetp(L, LUA_REGISTRYINDEX, p); }

	void swap( reference& a )
	{	std::swap( m_reference, a.m_reference ); }

private:
	int		m_reference;
};

class chunk
{
public:
	chunk() : m_L(nullptr) {}
	
	chunk( lua_State* L );
	chunk( chunk const& r );
	chunk& operator=( chunk const& r );
	~chunk();

	void call(std::error_code& ec, std::string* errorString);
	void swap( chunk& a );
	
	void push(); // chunk -> {top}

private:
	lua_State*	m_L;
	reference	m_chunkref;
};


class path_track
{
	typedef std::vector<path> path_cont;
public:
	struct auto_back {
		path_track& pt;
		auto_back(path_track& pt_, path const& inc) : pt(pt_) { pt.include (inc); }
		~auto_back() { pt.back(); }
	};
	
	friend class context;
	
	path_track();
	
	void add_system_include(path const& p) { m_system.push_back (p); }

	void bind( lua_State* L );
	void release( lua_State* L );

	bool include( path const& filename );
	void back();

	path const& current_file() const { return m_files.back(); }

private:
	static int include_func( lua_State* L );
	
	reference m_refL;
	path_cont m_system;
	path_cont m_files;
};

/*struct block_reader_state
{
	block_reader_state( block::range const& r )
	:	m_range(r), m_offset(0) {}

	size_t			m_offset;
	block::range	m_range;
};*/

const char * block_reader( lua_State *L, void *data, size_t *size );
	
class MemoryReader
{
	char* m_ptr, *m_end;
	
public:
	MemoryReader (char* p, size_t s) : m_ptr(p), m_end(p + s) {}
	
	static char const* read_func(lua_State *L, void *data, size_t *size );
};

class context : boost::noncopyable
{
public:
	context( bool loadBaseLibraries = true );
	~context();
	
	

	lua_State* state() const	{	assert(m_lua_state);	return m_lua_state; }

	chunk load( stir::string const& fname, std::error_code& ec);
	
	path_track& get_path_track() { return m_path_track; }

private:
	static int package_loader(lua_State* L);
	void register_reader(lua_State* L);
	
	lua_State*		m_lua_state;
//	luabind::object	m_function;
	path_track		m_path_track;
};

//
void set_global_func( lua_State* L, char const* name, int (*pf)( lua_State* L ) );
void display_stack( lua_State* L, std::ostream& os );

}}

#endif
