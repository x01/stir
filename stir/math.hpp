#ifndef STIR_MATH_HPP_
#define STIR_MATH_HPP_

#include <stir/platform.hpp>
#include <stir/math_types.hpp>
#include <stir/constants.hpp>
#include <cmath>

namespace stir { namespace math {

namespace detail
{
	void matrix_from_quat( matrix& o, quaternion const& q );
}
	
inline float lerp (float a, float b, float t) {
	return a * (1 - t) + b * t;
}

// SIGMOID FUNCTIONS: -∞:0 -- +∞:1, pivot at x = 0
inline float sigmoid_exp (float x, float k = 6.0f) {
	return 1 - 2 * (1 + exp(k * x));
}

inline float sigmoid_sq (float x, float k = 0.05f) {
	return x / sqrt (k + x * x);
}
	
// PLANE
vector3 project( plane const& pl, vector3 const& thisone );

#if STIR_DIRECTX_MATH
//
// QUATERNION
//
inline quaternion from_axis_angle( vector3 const& v, float a )
{	quaternion q; D3DXQuaternionRotationAxis( &q, &v, a ); return q; }
inline quaternion from_axis_angle( float x, float y, float z, float a )
{	return from_axis_angle( vector3(x,y,z), a ); }

inline float to_axis_angle( quaternion const& q, vector3& v )
{	float f; D3DXQuaternionToAxisAngle( &q, &v, &f ); return f; }

inline void quaternion_from_matrix( matrix const& a, quaternion& o )
{	D3DXQuaternionRotationMatrix( &o, &a ); }

#endif

/*inline quaternion make_quaternion( vector3 const& v )
{	return quaternion( v.x, v.y, v.z, 0.0f ); }

// q.w = (q * q*) / 2

inline quaternion conjugate( quaternion const& q )
{	return quaternion( -q.x, -q.y, -q.z, q.w ); }

inline float sq_length( quaternion const& q )
{	return q.x * q.x + q.y * q.y + q.z * q.z + q.w * q.w; }

inline float length( quaternion const& q )
{	return std::sqrt( sq_length( q ) ); }

inline quaternion normalized( quaternion const& q )
{	return q / length( q ); }

inline void normalize( quaternion& a, float len )
{	a.x /= len; a.y /= len; a.z /= len; a.w /= len; }

inline void normalize( quaternion& a )
{	normalize( a, math::length( a ) ); }

inline quaternion inverse( quaternion const& q )
{	return conjugate( q ) / length( q ); }

inline void conjugate( quaternion const& a, quaternion& o )
{	o.x = -a.x;	o.y = -a.y;	o.z = -a.z;	o.w = a.w; }

	
inline float dot( quaternion const& a, quaternion const& b )
{	return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w; }

inline quaternion cross( quaternion const& p, quaternion const& q )
{	return (p*q - q*p) / 2; }


#if STIR_OWN_MATH
inline quaternion from_axis_angle (float x, float y, float z, float a)
{
	float sa = sin (a * 0.5f);
	float ca = cos (a * 0.5f);
	return quaternion (x * ca, y * ca, z * ca, sa);
}
	
inline quaternion from_axis_angle( vector3 const& v, float a )
{
	return from_axis_angle (v.x, v.y, v.z, a);
}

inline float to_axis_angle( quaternion const& q, vector3& v )
{
	float angle = 2.0f * acos (q.w);
	float sn = sqrt (1.0f - q.w*q.w);
	if (angle <= 0.001f) {
		v.x = v.y = v.z = 0;
	} else {		
		v.x = q.x/sn;
		v.y = q.y/sn;
		v.z = q.z/sn;
	}
	return angle;
}

inline void quaternion_from_matrix( matrix const& a, quaternion& o )
{ }
#endif
*/

	/*
//
// VECTOR2
//
inline float sq_length( vector2 const& v )
{	return v.x * v.x + v.y * v.y; }

inline float length( vector2 const& v )
{	return std::sqrt( sq_length( v ) ); }

inline float dot( vector2 const& a, vector2 const& b )
{	return a.x * b.x + a.y * b.y; }

inline vector2 project( vector2 const& onthis, vector2 const& thisone )
{	return onthis * math::dot( onthis, thisone ) / math::length( onthis ); }

inline vector2 project_on_normalized( vector2 const& onthisn, vector2 const& thisone )
{	return onthisn * math::dot( onthisn, thisone ); }

template<int D>
inline vector2 perpendicular( vector2 const& a )
{	return D ? vector2( a.y, -a.x ) : vector2( -a.y, a.x ); }

inline vector2 normalized( vector2 const& v )
{	return v / length( v ); }
inline vector2 normalized( vector2 const& v, float len )
{	return v / len; }

inline vector2 reflect_vector( vector2 const& in, vector2 const& n )
{	return in - n * 2.0f * dot( in, n ); }
inline void normalize( vector2& v )
{	v /= length( v ); }
inline void normalize( vector2& v, float len )
{	v /= len; }


// rotates counterclockwise, assuming x+ to the left, y+ to the top
inline vector2 rotate_vec2( float x, float y, float a_rad )
{
	float sa = sin(a_rad), ca = cos(a_rad);

	float xx = x * ca + y * sa;
	float yy =-x * sa + y * ca;

	return vector2( xx, yy );
}

//
// VECTOR3
//
inline vector3 const& normal( plane const& p )
{	return *(vector3 const*)&p.a; }

inline float dot( plane const& p, stir::vector3 const& v )
{	return p.a * v.x + p.b * v.y + p.c * v.z; }

inline float dist_to_point( plane const& p, stir::vector3 const& v )
{	return p.a * v.x + p.b * v.y + p.c * v.z + p.d; }

inline vector3 make_vector( quaternion const& q )
{	return vector3( q.x, q.y, q.z ); }

inline vector3 orient_vector( vector3 const& v, quaternion const& q )
{	return make_vector( q * make_quaternion( v ) * conjugate( q ) ); }

inline float sq_length( vector3 const& v )
{	return v.x * v.x + v.y * v.y + v.z * v.z; }

inline float length( vector3 const& v )
{	return std::sqrt( sq_length( v ) ); }

inline vector3 normalized( vector3 const& v )
{	return v / length( v ); }
inline vector3 normalized( vector3 const& v, float len )
{	return v / len; }

inline void normalize( vector3& v )
{	v /= length( v ); }
inline void normalize( vector3& v, float len )
{	v /= len; }

inline float dot( vector3 const& a, vector3 const& b )
{	return a.x * b.x + a.y * b.y + a.z * b.z; }

inline vector3 cross( vector3 const& a, vector3 const& b )
{	return vector3( a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x ); }

// incoming_dir - outward_normal * 2.0f * (incoming_dir|outward_normal);
inline vector3 reflect_vector( vector3 const& in, vector3 const& n )
{	return in - n * 2.0f * dot( in, n ); }

inline vector3 to_vector3( vector4 const& a )
{	return vector3( a.x, a.y, a.z ); }

inline vector3 rotate_around_z( vector3 const& v, float a )
{
	float sina = sin(a), cosa = cos(a);
	return vector3( v.x * cosa + v.y * sina, cosa * v.y - sina * v.x, v.z );
}

// [xyz] * MATRIX-3x3
inline vector3 transform_3x3( vector3 const& v, matrix const& m )
{
	return vector3(
		m(0,0)*v.x + m(1,0)*v.y + m(2,0)*v.z,
		m(0,1)*v.x + m(1,1)*v.y + m(2,1)*v.z,
		m(0,2)*v.x + m(1,2)*v.y + m(2,2)*v.z
	);
}

inline vector4 to_vector4( color const& a )
{	return vector4( a.r, a.g, a.b, a.a ); }

// util
inline vector3 spherical_to_cartesian( float theta, float phi )
{	return vector3( sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta) ); }

inline float deg_to_rad( float degrees )
{	return degrees * PI / 180.0f; }
inline float rad_to_deg( float radians )
{	return radians / 180.0f * PI; }

#if STIR_DIRECTX_MATH
inline vector3 transform_coord( vector3 const& va, matrix const& m )
{	vector3 tv; D3DXVec3TransformCoord( &tv, &va, &m ); return tv; }

inline vector3 transform_normal( vector3 const& va, matrix const& m )
{	vector3 tv; D3DXVec3TransformNormal( &tv, &va, &m ); return tv; }

inline vector4 transform_vector( vector4 const& va, matrix const& m )
{	vector4 tv; D3DXVec4Transform( &tv, &va, &m ); return tv; }
#endif

//
// MATRIX
//
#if STIR_DIRECTX_MATH
matrix const& matrix_identity();

inline void matrix_identify( matrix& m )
{	D3DXMatrixIdentity( &m ); }

inline float inverse( matrix const& a, matrix& o )
{	float det = 0; D3DXMatrixInverse( &o, &det, &a ); return det; }

inline void matrix_translation( matrix& o, vector3 const& t )
{	D3DXMatrixTranslation( &o, t.x, t.y, t.z ); }

inline void matrix_translation( matrix& o, float x, float y, float z )
{	D3DXMatrixTranslation( &o, x, y, z ); }

inline void matrix_rotation( quaternion const& q, matrix& o )
{	D3DXMatrixRotationQuaternion( &o, &q ); }
//	{	detail::matrix_from_quat( o, q ); }
inline void matrix_rotation_x( float a, matrix& o )
{	D3DXMatrixRotationX( &o, a ); }
inline void matrix_rotation_y( float a, matrix& o )
{	D3DXMatrixRotationY( &o, a ); }
inline void matrix_rotation_z( float a, matrix& o )
{	D3DXMatrixRotationZ( &o, a ); }

inline void matrix_scaling( matrix& o, vector3 const& s )
{	D3DXMatrixScaling( &o, s.x, s.y, s.z ); }

inline void matrix_scaling( matrix& o, float x, float y, float z )
{	D3DXMatrixScaling( &o, x, y, z ); }

#endif

inline float transform_coord_z_nonhomogenuous( vector3 const& v, matrix const& m )
{	return v.x * m(0,2) + v.y * m(1,2) + v.z * m(2,2) + m(3,2); }

inline float transform_coord_z_homogenuous( vector3 const& v, matrix const& m )
{	return (v.x * m(0,2) + v.y * m(1,2) + v.z * m(2,2) + m(3,2) ) / m(3,3); }

void matrix_orient_translate( matrix& o, vector3 const& offset, quaternion const& rot );
*/
	
/*
// builds matrix, so that geometry in area of (0,0)..(scr_size) will be mapped to viewport (-1,-1)..(1,1)
inline void build_2d_matrix( matrix& o, vector2 const& scr_size, float dz = 0.0f )
{
	matrix tm_before, tm_after, scale_m;
	matrix_scaling( scale_m, 2.0f / float( scr_size.x ), -2.0f / float( scr_size.y ), 1.0f );
	matrix_translation( tm_after, -1.0f, 1.0f, dz );
	matrix_translation( tm_before, -0.5f, -0.5f, 0.0f );

	o = tm_before * scale_m * tm_after;
}

inline vector3 const& get3_x( matrix const& a )		{	return *(vector3 const*)&a._11; }
inline vector3 const& get3_y( matrix const& a )		{	return *(vector3 const*)&a._21; }
inline vector3 const& get3_z( matrix const& a )		{	return *(vector3 const*)&a._31; }
inline vector3 const& get3_offset( matrix const& a ){	return *(vector3 const*)&a._41; }

inline vector3& access3_x( matrix& a )	{	return *(vector3*)&a._11; }
inline vector3& access3_y( matrix& a )	{	return *(vector3*)&a._21; }
inline vector3& access3_z( matrix& a )	{	return *(vector3*)&a._31; }
inline vector3& access3_w( matrix& a )	{	return *(vector3*)&a._41; }

inline vector4& access4_x( matrix& a )	{	return *(vector4*)&a._11; }
inline vector4& access4_y( matrix& a )	{	return *(vector4*)&a._21; }
inline vector4& access4_z( matrix& a )	{	return *(vector4*)&a._31; }
inline vector4& access4_w( matrix& a )	{	return *(vector4*)&a._41; }

inline vector3 extract_scale( matrix const& m )
{	return vector3( math::length( get3_x( m ) ), math::length( get3_y( m ) ), math::length( get3_z( m ) ) ); }

void build_orient_matrix( matrix& o, vector3 const& normalized_front_or_z, vector3 const& normalized_up_or_y = vector3(0,1,0) );

inline void build_matrix( vector3 const& x, vector3 const& y, vector3 const& z, matrix& o )
{
	o._11 = x.x;	o._12 = x.y;	o._13 = x.z;	o._14 = 0.0f;
	o._21 = y.x;	o._22 = y.y;	o._23 = y.z;	o._24 = 0.0f;
	o._31 = z.x;	o._32 = z.y;	o._33 = z.z;	o._34 = 0.0f;
	o._41 = 0.0f;	o._42 = 0.0f;	o._43 = 0.0f;	o._44 = 1.0f;
}

inline void build_matrix_transpose( vector3 const& x, vector3 const& y, vector3 const& z, matrix& o )
{
	o._11 = x.x;	o._12 = y.x;	o._13 = z.x;	o._14 = 0.0f;
	o._21 = x.y;	o._22 = y.y;	o._23 = z.y;	o._24 = 0.0f;
	o._31 = x.z;	o._32 = y.z;	o._33 = z.z;	o._34 = 0.0f;
	o._41 = 0.0f;	o._42 = 0.0f;	o._43 = 0.0f;	o._44 = 1.0f;
}

inline void build_matrix( vector3 const& x, vector3 const& y, vector3 const& z, vector3 const& p, matrix& o )
{
	o._11 = x.x;	o._12 = x.y;	o._13 = x.z;	o._14 = 0.0f;
	o._21 = y.x;	o._22 = y.y;	o._23 = y.z;	o._24 = 0.0f;
	o._31 = z.x;	o._32 = z.y;	o._33 = z.z;	o._34 = 0.0f;
	o._41 = p.x;	o._42 = p.y;	o._43 = p.z;	o._44 = 1.0f;
}

inline void build_matrix_transpose( vector3 const& x, vector3 const& y, vector3 const& z, vector3 const& p, matrix& o )
{
	o._11 = x.x;	o._12 = y.x;	o._13 = z.x;	o._14 = 0.0f;
	o._21 = x.y;	o._22 = y.y;	o._23 = z.y;	o._24 = 0.0f;
	o._31 = x.z;	o._32 = y.z;	o._33 = z.z;	o._34 = 0.0f;
	o._41 = p.x;	o._42 = p.y;	o._43 = p.z;	o._44 = 1.0f;
}*/

}} // stir::math

#endif
