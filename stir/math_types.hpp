#ifndef STIR_MATH_TYPES_HPP_
#define STIR_MATH_TYPES_HPP_

#include <stir/platform.hpp>
#include <pulmotor/pulmotor_fwd.hpp>

#if STIR_WINDOWS

#define NOMINMAX
#include <d3dx9math.h>

namespace stir
{
	typedef float 			float_;
	typedef D3DXVECTOR2 	vector2;
	typedef D3DXVECTOR3 	vector3;
	typedef D3DXVECTOR4 	vector4;
	typedef D3DXCOLOR		color;
	typedef DWORD			packed_color;
	typedef DWORD			color8;
	typedef D3DXQUATERNION	quaternion;
	typedef D3DXMATRIX 	matrix;
	typedef D3DXPLANE 		plane;	
}

#elif STIR_MACOSX || STIR_IPHONE

#include <stir/basic_types.hpp>
//#include <mtxlib.h>
#include <stir/stir_vecmath.hpp>

namespace stir {
	
#if 1
	typedef stir::Vector3 vector3;
	typedef stir::Vector4 vector4;
	typedef stir::Point3 point3;
	typedef stir::Matrix3 matrix3;
	typedef stir::Matrix4 matrix;
	typedef stir::Matrix4 matrix4;
	typedef stir::Transform3 transform;
	typedef stir::Quat quaternion;
	
#else
	typedef mtxlib::vector2 vector2;
	typedef mtxlib::vector3 vector3;
	typedef mtxlib::vector4 vector4;
	typedef mtxlib::matrix44 matrix;
	typedef mtxlib::matrix33 matrix3;
	typedef mtxlib::quaternion quaternion;
#endif
	
#define STIR_GET_8B(x, bits) (((x) >> (bits)) & 0xff)
#define STIR_GET_8B1(x, bits) (float(STIR_GET_8B(x,bits)) / 255.0f)
	
	// This macro takes ARGB and converts into value that is 'RGBA' when stored to memory
	// So on LE it's ABGR, on BE it's RGBA
#define um(x,m) ((u32)(x) & m)
#define STIR_ARGB(x) (um(x, 0xff00ff00u) | (um(x, 0x00ff0000) >> 16) | (um(x, 0x000000ff) << 16))

	constexpr u32 mm(u32 x, u32 m) { return x & m; }
	constexpr u32 argb (u32 x) { return mm(x, 0xff00ff00u) | (mm(x, 0x00ff0000) >> 16) | (mm(x, 0x000000ff) << 16); }
	
	struct color8_argb_t {};

	static const color8_argb_t color8_argb = {};
	struct color8
	{
		union {
			u32 value;
			struct {
				u8 r, g, b, a;
			};
		};
	
		color8 () {}
		color8 (u32 argb, color8_argb_t) : value(STIR_ARGB(argb)) { }
		
		static color8 abgr (u32 v) { return color8(v); }
		static color8 argb (u32 v) { return color8(v, color8_argb); }
		
		static color8 white (u8 a = 0xff) { return color8 (STIR_ARGB((a<<24) | 0x00ffffff)); }
		static color8 black (u8 a = 0xff) { return color8 (STIR_ARGB((a<<24) | 0x00000000)); }
		static color8 red (u8 a = 0xff) { return color8 (STIR_ARGB((a<<24) | 0x00ff0000)); }
		static color8 green (u8 a = 0xff) { return color8 (STIR_ARGB((a<<24) | 0x0000ff00)); }
		static color8 blue (u8 a = 0xff) { return color8 (STIR_ARGB((a<<24) | 0x000000ff)); }
		
		static color8 yellow (u8 a = 0xff) { return color8 (STIR_ARGB((a<<24) | 0x00ffff00)); }
		static color8 magenta (u8 a = 0xff) { return color8 (STIR_ARGB((a<<24) | 0x00ff00ff)); }
		static color8 cyan (u8 a = 0xff) { return color8 (STIR_ARGB((a<<24) | 0x0000ffff)); }
	private:
		color8 (u32 val) : value(val) {}
	};
#undef um

	struct plane {
		enum { version = pulmotor::version_dont_track };
		
		float a, b, c, d;
		template<class ArchiveT> void serialize (ArchiveT& ar, unsigned version) { ar | a | b | c | d; }
	};
	
	struct color {
		
		enum { version = pulmotor::version_dont_track };
		
		color () {}
		color (float r_, float g_, float b_, float a_) : r(r_), g(g_), b(b_), a(a_) {}
//		color (color8 c) : r(STIR_GET_8B1(c,16)), g(STIR_GET_8B1(c,8)), b(STIR_GET_8B1(c,0)), a(STIR_GET_8B1(c,24)) {}

		float r, g, b, a;
		
		float* ptr() { return &r; }
		float const* ptr() const { return &r; }
		
		color8 compact () const
		{
			u32 a1 = a * 255.0f;
			u32 r1 = r * 255.0f;
			u32 g1 = g * 255.0f;
			u32 b1 = b * 255.0f;
			
			// This makes RGBA on LittleEndian machines
			return color8::abgr((a1 << 24) | (b1 << 16) | (g1 << 8) | r1);
		}
		
		static color white (float alpha = 1.0f) { return color (1.0f, 1.0f, 1.0f, alpha); }
		static color black (float alpha = 1.0f) { return color (0, 0, 0, alpha); }
		static color red (float alpha = 1.0f) { return color (1.0f, 0, 0, alpha); }
		static color green (float alpha = 1.0f) { return color (0, 1.0f, 0, alpha); }
		static color blue (float alpha = 1.0f) { return color (0, 0, 1.0f, alpha); }
		
		static color yellow (float alpha = 1.0f) { return color (1.0f, 1.0f, 0, alpha); }
		
		template<class ArchiveT> void serialize (ArchiveT& ar, unsigned version) { ar | r | g | b | a; }
	};
}

//PULMOTOR_ARCHIVE_VER(stir::vector2, pulmotor::version_dont_track);
PULMOTOR_ARCHIVE_VER(stir::vector3, pulmotor::version_dont_track);
PULMOTOR_ARCHIVE_VER(stir::vector4, pulmotor::version_dont_track);
PULMOTOR_ARCHIVE_VER(stir::matrix, pulmotor::version_dont_track);
PULMOTOR_ARCHIVE_VER(stir::matrix3, pulmotor::version_dont_track);
PULMOTOR_ARCHIVE_VER(stir::quaternion, pulmotor::version_dont_track);


#else

#error "define math types for this platform"

#endif

#endif
