#include "stdafx.hpp"

#include "mdump.h"
#include <tchar.h>

#define DUMP_ASK_USER 0

namespace stir
{

TCHAR MiniDumper::m_szAppName[128];

MiniDumper::MiniDumper( LPCTSTR szAppName )
{
	// if this assert fires then you have two instances of MiniDumper
	// which is not allowed
	assert( m_szAppName[0]==0 );

	TCHAR const* pname = _T("Application");
	if( szAppName )
		pname = szAppName;

#if _MSC_VER >= 1400
	wcsncpy_s( m_szAppName, szAppName, 128 );
#else
	_tcsncpy( m_szAppName, szAppName, 128 );
#endif
	::SetUnhandledExceptionFilter( TopLevelFilter );
}

LONG MiniDumper::TopLevelFilter( struct _EXCEPTION_POINTERS *pExceptionInfo )
{
	LONG retval = EXCEPTION_CONTINUE_SEARCH;
	HWND hParent = NULL;						// find a better value for your app

	// firstly see if dbghelp.dll is around and has the function we need
	// look next to the EXE first, as the one in System32 might be old 
	// (e.g. Windows 2000)
	HMODULE hDll = NULL;
	TCHAR szDbgHelpPath[_MAX_PATH];

	if (GetModuleFileName( NULL, szDbgHelpPath, _MAX_PATH ))
	{
		TCHAR *pSlash = _tcsrchr( szDbgHelpPath, _T('\\') );
		if (pSlash)
		{
			_tcscpy_s( pSlash+1, _MAX_PATH - _tcslen(szDbgHelpPath), _T("DBGHELP.DLL") );
			hDll = ::LoadLibrary( szDbgHelpPath );
		}
	}

	if (hDll==NULL)
	{
		// load any version we can
		hDll = ::LoadLibrary( _T("DBGHELP.DLL") );
	}

	LPCTSTR szResult = NULL;

	if (hDll)
	{
		MINIDUMPWRITEDUMP pDump = (MINIDUMPWRITEDUMP)::GetProcAddress( hDll, "MiniDumpWriteDump" );
		if (pDump)
		{
			SYSTEMTIME st;
			GetSystemTime( &st );

			TCHAR szDateString[256];
			wsprintf( szDateString, _T("-crash-%02d_%02d_%02d-%02d_%02d-%02d"),
				st.wYear % 100, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond );

			TCHAR szDumpPath[_MAX_PATH+256];
			TCHAR szScratch [_MAX_PATH];

			// work out a good place for the dump file
			if( !GetModuleFileName( GetModuleHandle(0), szDumpPath, _MAX_PATH ) )
			{
				if (!GetTempPath( _MAX_PATH, szDumpPath ))
					_tcscpy_s( szDumpPath, _T("c:\\temp\\") );
			} else
				*( _tcsrchr( szDumpPath, _T('\\') ) + 1) = 0;

			_tcscat_s( szDumpPath, m_szAppName );
			_tcscat_s( szDumpPath, szDateString );
			_tcscat_s( szDumpPath, _T(".dmp") );

			// ask the user if they want to save a dump file
#if DUMP_ASK_USER
			if (::MessageBox( NULL, _T("Something bad happened in your program, would you like to save a diagnostic file?"), m_szAppName, MB_YESNO )==IDYES)
#endif
			{
				// create the file
				HANDLE hFile = ::CreateFile( szDumpPath, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS,
											FILE_ATTRIBUTE_NORMAL, NULL );

				if (hFile!=INVALID_HANDLE_VALUE)
				{
					_MINIDUMP_EXCEPTION_INFORMATION ExInfo;

					ExInfo.ThreadId = ::GetCurrentThreadId();
					ExInfo.ExceptionPointers = pExceptionInfo;
					ExInfo.ClientPointers = NULL;

					// write the dump
					BOOL bOK = pDump( GetCurrentProcess(), GetCurrentProcessId(), hFile, MiniDumpNormal, &ExInfo, NULL, NULL );
					if (bOK)
					{
						wsprintf( szScratch, _T("Saved dump file to '%s'"), szDumpPath );
						szResult = szScratch;
						retval = EXCEPTION_EXECUTE_HANDLER;
					}
					else
					{
						wsprintf( szScratch, _T("Failed to save dump file to '%s' (error %d)"), szDumpPath, GetLastError() );
						szResult = szScratch;
					}
					::CloseHandle(hFile);
				}
				else
				{
					wsprintf( szScratch, _T("Failed to create dump file '%s' (error %d)"), szDumpPath, GetLastError() );
					szResult = szScratch;
				}
			}
		}
		else
		{
			szResult = _T("DBGHELP.DLL too old");
		}
	}
	else
	{
		szResult = _T("DBGHELP.DLL not found");
	}

#if DUMP_ASK_USER
	if (szResult)
		::MessageBox( NULL, szResult, m_szAppName, MB_OK );
#endif

	return retval;
}

}
