/*
 *	Header file:		MulDiv64.h
 *	Author:				Richard van der Wal
 *	Contact:			R.vdWal@xs4all.nl
 *
 *	Description:
 *		Header file with prototype for 64 bit scaling functions
 *
 *	$Log: $
 * 
 */

/******************************************************************************/
#pragma once

#ifndef STIR_FULL_MULDIV64_
#define STIR_FULL_MULDIV64_

#include "platform.hpp"
#include "basic_types.hpp"

#if STIR_IPHONE || STIR_MACOSX
#include "stir/stir_assembly.h"
#endif

namespace stir { namespace utility
{

#if STIR_IPHONE || STIR_MACOSX
inline uint64_t muldiv64 (int64_t num, int64_t multiplier, int64_t divider)
{
	return stir::muldiv64 (num, multiplier, divider);
}
#else
/*
 * MulDiv64
 * Multiplies an operant by a multiplier and divides the result by a divider
 * Used for scaling 64 bit integer values
 *     Xscaled = Xstart * Multiplier / Divider
 * Uses 128 bit intermediate result
 */
int64_t muldiv64(int64_t operant, int64_t multiplier, int64_t divider);


/*
 * MulShr64
 * Multiplies an operant by a multiplier and right shifts the result rshift times
 * Used for scaling 64 bit integer values
 *     Xscaled = (Xstart * Multiplier) SHR rshift
 * Uses 128 bit intermediate result
 */
int64_t mulshr64(int64_t operant, int64_t multiplier, unsigned char rshift);
#endif
	
} }

#endif // STIR_FULL_MULDIV64_
