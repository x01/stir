SECTION .text

global _stir_muldiv64
_stir_muldiv64:

%define _mres(x) ebp-16+x*4
%define _temp(x) (ebp-24+x*4)

%define _argl (ebp+8)
%define _argh (ebp+12)
%define _mull (ebp+16)
%define _mulh (ebp+20)
%define _divl (ebp+24)
%define _divh (ebp+28)

	push ebp
	mov ebp, esp
	sub esp, 24
	
	push esi
	push edi
	
;	mov esi, [_divl]
;	mov edi, [_divh]
	
	; init
	xor edx, edx
	mov dword [_mres(0)], edx
	mov dword [_mres(1)], edx
	mov dword [_mres(2)], edx
	mov dword [_mres(3)], edx
	
	; mul
	mov eax, [_argl]
	mul dword [_mull]
	mov [_mres(0)], eax
	mov [_mres(1)], edx
	
	mov eax, [_argh]
	mul dword [_mull]
	add [_mres(1)], eax
	adc [_mres(2)], edx
	
	mov eax, [_argl]
	mul dword [_mulh]
	add [_mres(1)], eax
	adc [_mres(2)], edx
	
	mov eax, [_argh]
	mul dword [_mulh]
	add [_mres(2)], eax
	adc [_mres(3)], edx
	
	; divide
	
	; loop:ecx
	; quotient-low:[edx eax]
	; remainder:[_temp(0) _temp(1) edi esi]
	
	mov ecx, 128

.optimize	
	bsr eax, [_mres(3)]
	jnz .start
	
	; 'shift' by 32 bits
	sub ecx, 32
	mov eax, dword [_mres(2)]
	mov edx, dword [_mres(1)]
	mov esi, dword [_mres(0)]
	
	mov dword [_mres(3)], eax
	mov dword [_mres(2)], edx
	mov dword [_mres(1)], esi
	mov dword [_mres(0)], 0
	jmp .optimize

.start:
	
	xor eax, eax
	xor edx, edx
	mov esi, eax
	mov edi, eax
	mov [_temp(0)], eax
	mov [_temp(1)], eax

.divide:

	shl dword [_mres(0)], 1
	rcl dword [_mres(1)], 1
	rcl dword [_mres(2)], 1
	rcl dword [_mres(3)], 1
	
	rcl esi, 1
	rcl edi, 1
	rcl dword [_temp(0)], 1
	rcl dword [_temp(1)], 1
	
	; if temp is bigger than the divisor, subtract divisor and roll in bit-1 to quotient
	cmp dword [_temp(1)], 0
	jnz .subtract
	
	cmp dword [_temp(0)], 0
	jnz .subtract
	
	cmp edi, [_divh]
	ja .subtract
	jb .toosmall
	
	cmp esi, [_divl]
	jb .toosmall

.subtract:

	sub esi, dword [_divl]
	sbb edi, dword [_divh]
	sbb dword [_temp(0)], 0
	sbb dword [_temp(1)], 0
	
	stc
	rcl eax, 1
	rcl edx, 1

	loop .divide
	
	jmp .end

.toosmall:
	clc
	rcl eax, 1
	rcl edx, 1

	loop .divide

.end
	pop edi
	pop esi

	leave
	ret
