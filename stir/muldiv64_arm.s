#include "TargetConditionals.h"

#define TARGET_ARM (TARGET_OS_IPHONE && !TARGET_IPHONE_SIMULATOR)

#if TARGET_ARM
.text
.code 32

.align 4
.globl _stir_muldiv64_arm


// r0 - num, r1 - mul, r2 - div

// temp: r4,5,6,7
// result: r8,9,10,11
// r12,13,14,15
 
_stir_muldiv64_arm:
	stmfd sp!, {r4, r5, r6, r7}
	
	
	

	ldmfd sp!, {r4, r5, r6, r7}
	bx lr
	
#endif
