#include "openal_audio.hpp"
#include <stir/language_util.hpp>
#include <stir/stir_logs.hpp>

namespace stir {
	
static struct { int code; char const* msg; } errorStrings [] =
{
	{ AL_NO_ERROR, "No error" },
	{ AL_INVALID_NAME, "Invalind name" },
	{ AL_INVALID_ENUM, "Invalid enum" },
	{ AL_INVALID_VALUE, "Invalid value" },
	{ AL_INVALID_OPERATION, "Invalid operation" },
	{ AL_OUT_OF_MEMORY, "Out of memory" }
};

void check_aol_error(char const* reason)
{
	ALenum result = alGetError ();
	if (result != AL_NO_ERROR) {
		char const* msg = NULL;
		for (int i=0; i<sizeof(errorStrings)/sizeof(errorStrings[0]); ++i)
			if (errorStrings[i].code == result)
				msg = errorStrings[i].msg;
		
		STIR_LOG(audio).printf ("OpenAL error: (%04x=%s) %s\n", result, msg, reason);
	}
}
	
openal_context::openal_context()
:	dev_(nullptr), ctx_(nullptr)
{
	dev_ = alcOpenDevice (nullptr);
	CHECK_OAL_ERROR("open device");
	
	if (!dev_) {
		STIR_LOG (audio) << "Failed to create OpenAL device\n";
		return;
	}
	
	ctx_ = alcCreateContext (dev_, nullptr);
	CHECK_OAL_ERROR("create context");
	if (!ctx_) {
		STIR_LOG (audio) << "Failed to create OpenAL context\n";
		return;
	}
	
	if (!alcMakeContextCurrent (ctx_)) {
		STIR_LOG (audio) << "Failed to make OpenAL context current\n";
	}
		
	CHECK_OAL_ERROR("make oal context current");
}

openal_context::~openal_context()
{
	ALboolean result = alcCloseDevice (dev_);
	CHECK_OAL_ERROR("close device");
	STIR_LOG (audio) << "Close OpenAL device: " << result << log::endl;
	alcDestroyContext (ctx_);
}


/*openal_audio::openal_audio ()
{
	alGenSources (1, &source_);
}

openal_audio::~openal_audio ()
{
	alDeleteSources (1, &source_);
}

	void alSourcePlayv( ALsizei ns, const ALuint *sids );
	void alSourceStopv( ALsizei ns, const ALuint *sids );
	void alSourceRewindv( ALsizei ns, const ALuint *sids );
	void alSourcePausev( ALsizei ns, const ALuint *sids );
	
*/
	
}