#ifndef STIR_OPTIONAL_HPP
#define STIR_OPTIONAL_HPP

#include <type_traits>

namespace stir {
	
template<class T>
struct optional
{
	typename std::aligned_storage<sizeof(T)>::type m_storage[1];
	bool m_init = false;
	
	optional()
 	{}
	
	optional (optional const& a)
	:	m_init(a.m_init) {
		if (m_init)
			new (m_storage) T (a.get());
	}
	
	optional (optional&& a)
	:	m_init(a.m_init) {
		if (m_init) {
			new (m_storage) T (std::move(a.get()));
			a.m_init = false;
		}
	}
	
	~optional () {
		if (m_init)
			get().~T();
	}
	
	template<class... Args>
	void emplace(Args&&... args)
	{
		new (m_storage) T (std::forward<Args>(args)...);
		m_init = true;
	}
	
	operator bool() const { return m_init; }
	
	T* operator->() { return (T*)m_storage; }
	T const* operator->() const { return (T*)m_storage; }
	
	T& get() & { return (T&)m_storage[0]; }
	T&& get() && { return (T&&)m_storage[0]; }
};

}


#endif
