#ifndef stir_partition_hpp
#define stir_partition_hpp

#include <array>

namespace stir {
	
template<class S, int N>
struct partition {
	
	std::array<S, N> m_part;
	size_t m_parts = 0;
	size_t m_left;
	
	partition (size_t left) : m_left(left) {
		m_part[0].start = m_part[0].count = 0;
	}
	
	bool empty() const { return m_part[0].count == 0; }
	size_t size() const { return m_parts + (m_part[m_parts].count != 0); }
	S const* begin() const { return m_part.data(); }
	S const* end() const { return m_part.data() + size(); }
	
	void restart (size_t left) {
		m_left = left;
		m_parts = 0;
		m_part[0].start = m_part[0].count = 0;
	}
	
	size_t occupied () const { return m_part[m_parts].start + m_part[m_parts].count; }
	
	// return true if `count' elements were added, otherwise a flush is needed and insertion should be retried
	template<class... Args>
	bool part (int count, Args&&... args) {
		
		if (m_left < count) {
			assert (size() != 0);
			return false;
		}
		m_left -= count;
		
		if (m_part[0].count == 0)
			m_part[m_parts].init (0, args...);
		else if (!m_part[m_parts].compatible (args...)) {
			// if no more space if left, return false to indicate that request can not be satisfied
			if (m_parts >= N-1)
				return false;
			size_t occ = occupied();
			++m_parts;
			m_part[m_parts].init (occ, args...);
		}
		
		m_part[m_parts].count += count;
		
		return true;
	}
};
	
	
}


#endif
