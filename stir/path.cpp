#include <stir/platform.hpp>
#include "path.hpp"

namespace stir {

size_t utf8_codepoints (char const* arg)
{
	typedef char value_type;

	size_t multi = 0;
	value_type const* p = arg;
	while (*p != 0)
	{
		value_type c = *p++;
		if ((c & 0x80))
		{
			while (c & 0x40) {
				if (!*p)
					break;
				multi++;
				p++;
				c <<= 1;
			}
		}
	}

	return p - arg - multi;
}

size_t STIR_ATTR_DLL utf16_codepoints (wchar_t const* arg)
{
	wchar_t const* p = arg;
	for (; *p != 0x0000; ++p)
		;
	return p - arg;
}

string STIR_ATTR_DLL forward_slashes( stir::string const& path )
{
	string s = path;
	for( size_t i=0; i<s.size(); ++i )
		if( s[i] == L'\\' )
			s[i] = L'/';
	return s;
}

string STIR_ATTR_DLL back_slashes( stir::string const& path )
{
	string s = path;
	for( size_t i=0; i<s.size(); ++i )
		if( s[i] == L'/' )
			s[i] = L'\\';
	return s;
}

}