#ifndef STIR_PATH_HPP_
#define STIR_PATH_HPP_

#include "platform.hpp"
#include <stir/basic_types.hpp>
#include <stir/complex_types.hpp>

#include "utf8/utf8.h"

#include <cstring>
#include <string>
#include <iosfwd>

#define STIR_USE_BOOST_PATH 0

#if STIR_USE_BOOST_PATH
#include <boost/filesystem/path.hpp>
#endif

namespace stir {

#if STIR_USE_BOOST_PATH
using namespace fs = boost::filesystem;
using fs::path;
	
#else
	
namespace log {
	struct log;
};

// operations required:
// 1) construct from [char, wchar, ucs32]
// 2) keep at different compile-time chosen format
// 3) extract substring at internal (cheapest, for other paths) and any formats (char, wchar, etc.)

#if USE_STIR_UTF8
template<class OctetIterator>
uint32_t utf8_decode (OctetIterator& p, OctetIterator end)
{
	char c = *p++;

	if ((c & 0xc0) == 0xc0)
	{
		// one trailing. 110y yyxx 10xx xxxx
		if ((c & 0xe0) == 0xc0) {
			return ((c & 0x1f) << 6) | (*p++ & 0x3f);
		} else // two trailing. 1110 yyyy 10yy yyxx 10xx xxxx
			if ((c & 0xf0) == 0xe0) {
				return ((c&0x0f) << 12) | ((*p++ & 0x3f) << 6) | (*p++ & 0x3f); 
			} else // three trailing. 1111 0zzz 10zz yyyy 10yy yyxx 10xx xxxx
				if ((c & 0xf0) == 0xe0) {
					return ((c&0x3f) << 18) | ((*p++ & 0x3f) << 12) | ((*p++ & 0x3f) << 6) | (*p++ & 0x3f); 
				}
	}

	return c;
}

template<class Iterator>
Iterator utf8_encode (uint32_t code, Iterator dest)
{
	assert (!"Not implemented");
}

#else

template<class ByteIterator>
uint32_t utf8_decode (ByteIterator& p, ByteIterator end)
{
	return utf8::unchecked::next (p);
}

template<class Iterator>
Iterator utf8_encode (uint32_t code, Iterator dest)
{
	return utf8::unchecked::append (code, dest);
}

#endif

STIR_ATTR_DLL size_t utf8_codepoints (char const* arg);
STIR_ATTR_DLL size_t utf16_codepoints (wchar_t const* arg);

template<class Allocator = std::allocator<char> >
struct utf8_traits
{
	typedef char value_type;
	typedef std::string string_type;

//	typedef std::basic_string<value_type, std::char_traits<value_type>, Allocator> string_type;

	static string_type encode (value_type const* a) {
		return string_type (a);
	}

	static string_type encode (value_type const* a, size_t size) {
		return string_type (a, size);
	}

	static string_type encode (wchar_t const* a) {
		return encode (a, wcslen (a));
	}

	static string_type encode (wchar_t const* a, size_t size) {
		string_type r;
		size_t len = utf16_codepoints(a);
		r.reserve (len);
		char buf[4];
		for (size_t i = 0; i < len; ++i) {
			char* e = utf8_encode (*a++, buf);
			for (int q=0, l = e - buf; q<l; ++q)
				r.push_back (buf[q]);
		}
		return r;
	}
};

template<class Allocator = std::allocator<wchar_t> >
struct utf16_traits
{
	typedef wchar_t value_type;
	typedef std::basic_string<value_type, std::char_traits<value_type>, Allocator> string_type;

	static string_type encode (value_type const* a) {
		return string_type (a, wcslen (a));
	}

	static string_type encode (value_type const* a, size_t size) {
		return string_type (a, size);
	}

	static string_type encode (char const* a) {
		return encode (a, strlen (a));
	}

	static string_type encode (char const* a, size_t size) {
		string_type r;
		size_t len = utf8_codepoints(a);
		r.resize (len);
		int i = 0;
		while (unsigned code = utf8_decode(a, a + size))
			r [i++] = code;
		return r;
	}
};


template<class Traits>
class path_t
{
public:
	typedef Traits path_traits;
	typedef typename Traits::value_type value_type;
	typedef typename Traits::string_type string_type;

	static const value_type separator = '/';
	static const value_type ext_separator = '.';

	path_t ()
	{}
	
	template<class CharT>
	path_t (CharT const* a, size_t size)
	:	m_data (path_traits::encode (a, size))
	{}
	
	template<class CharT>
	path_t (CharT const* a)
	:	m_data (path_traits::encode (a))
	{}
	
	template<class CharT, class TraitsT, class AllocatorT>
	path_t (std::basic_string<CharT, TraitsT, AllocatorT> const& a)
	:	m_data (path_traits::encode (a.c_str(), a.size()))
	{}
	
	path_t const& operator= (path_t p) {
		swap (m_data, p.m_data);
		return *this;
	}

	template<class CharT>
	path_t const& operator= (CharT const* arg) {
		path_t temp (arg);
		swap (m_data, temp.m_data);
		return *this;
	}
	
	template<class CharT, class TraitsT, class AllocatorT>
	path_t const& operator= (std::basic_string<CharT, TraitsT, AllocatorT> const& arg) {
		path_t temp (arg);
		swap (m_data, temp.m_data);
		return *this;
	}
	
	size_t size () const {
		return m_data.size ();
	}

	path_t operator /(path_t const& a) const {
		if (empty ())
			return a;

		path_t p;
		p.m_data.resize (size () + a.size () + 1);
		typename string_type::iterator d = std::copy (m_data.begin (), m_data.end (), p.m_data.begin ());
		*d++ = separator;
		std::copy (a.m_data.begin (), a.m_data.end (), d);
		return p;
	}
	
	template<class T>
	path_t& operator+= (T const* arg) {
		string_type s = path_traits::encode (arg);
		m_data += s;
		return *this;
	}

	template<class T>
	path_t operator+ (T const* arg) const {
		return path_t (m_data + path_traits::encode (arg));
	}

	path_t operator+ (path_t const& arg) const {
		return path_t (m_data + arg.m_data);
	}

	string_type const& string () const {
		return m_data;
	}
	
	value_type const* c_str () const {
		return m_data.c_str();
	}

	path_t parent () const {
		size_t p = m_data.find_last_of (separator);
		if (p == string_type::npos)
			return path_t ();
		else if (p == 0) // only a separator
			return path_t ("/", 1);
		else
			return path_t (m_data.c_str (), p);
	}

	path_t filename () const {
		size_t p = m_data.find_last_of (separator);
		if (p == string_type::npos)
			return *this;
		else
		{
			return path_t (m_data.c_str () + p + 1, m_data.size() - p - 1);
		}
	}
	
	// return name without extension of the last path component
	path_t stem () const {
		size_t fpos = m_data.find_last_of (separator);
		size_t p = m_data.find_last_of (ext_separator);
		if (p == string_type::npos)
		{
			if (fpos == string_type::npos)
				return *this;
			else
				return path_t (m_data.c_str() + fpos + 1);
		}
		else
		{
			if (fpos == string_type::npos)
				return path_t (m_data.c_str(), p);
			else
				return path_t (m_data.c_str() + fpos + 1, p - fpos - 1);
		}
	}
	
	size_t extension_pos() const
	{
		size_t p = m_data.find_last_of (ext_separator);
		if (p == string_type::npos)
			return string_type::npos;
			
		size_t last_comp = m_data.find_first_of (separator, p);
		if (last_comp != string_type::npos && last_comp > p)
			return string_type::npos;
		else
			return p;
	}

	// return last path component extension
	path_t extension () const {
//		size_t fpos = m_data.find_last_of (separator);
//		if (fpos == string_type::npos)
//			fpos = 0;
//
//		fpos += 1;
//		size_t p = m_data.find_last_of (ext_separator, fpos);
		
		size_t extP = extension_pos();
		if (extP == string_type::npos)
			return path_t ();
		else
			return path_t (m_data.c_str() + extP, m_data.size() - extP);
	}
	
	path_t& replace_extension(path_t const& p = path_t()) {
		size_t extP = extension_pos();
		if (extP != string_type::npos)
		{
			if (!p.empty()) {
				if (p.m_data[0] == ext_separator)
					m_data.replace (extP, string_type::npos, p.string());
				else
					m_data.replace (extP + 1, string_type::npos, p.string());
			}
			else
				m_data.erase (extP);
		}
		else
		{
			if (m_data.back() != ext_separator) {
				m_data.reserve (p.size () + 1);
				m_data += ext_separator;
			}
			m_data += p.m_data;
		}
		
		return *this;
	}
	
	path_t root () const {
		if (!m_data.empty () && m_data[0] == separator)
			return path_t ("/");
		
		return path_t ();
	}
	
	bool has_root_path () const {
#if STIR_WINDOWS
		assert ("not implemented");
#else
		return !m_data.empty () && m_data[0] == '/';
#endif
	}
	
	bool empty () const {
		return m_data.empty ();
	}

	bool operator == (path_t const& r) const {
		return m_data == r.m_data;
	}

	bool operator != (path_t const& r) const {
		return m_data != r.m_data;
	}

private:
	string_type	m_data;
};

string STIR_ATTR_DLL forward_slashes( stir::string const& path );
string STIR_ATTR_DLL back_slashes( stir::string const& path );
template<class Traits>
path_t<Traits> STIR_ATTR_DLL replace_extension(path_t<Traits> const& p, path_t<Traits> const& ext) {
	path_t<Traits> pp = p;
	pp.replace_extension (ext);
	return pp;
}

typedef path_t<utf8_traits<> > utf8_path;
typedef path_t<utf16_traits<> > utf16_path;

#if STIR_WINDOWS
typedef utf16_path path;
#else
typedef utf8_path path;
#endif

//template<class Traits>
//inline log::log& operator<<(log::log& l, path<Traits> const& p) {
//	l.printf (p.c_str());
//	return l;
//}

template<class T, class PathTraits>
inline std::basic_ostream<T>& operator <<(std::basic_ostream<T>& os, path_t<PathTraits> const& p)
{
	os << p.c_str ();
	return os;
}
#endif
	
}

#endif

