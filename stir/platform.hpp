#ifndef STIR_PLATFORM_HPP_
#define STIR_PLATFORM_HPP_

#if defined(__APPLE__)
#	define STIR_OWN_MATH 1
#	define STIR_OPENGL 1
#	define STIR_GCC 1
#	include "TargetConditionals.h"
#	define STIR_GLES1 0
#	define STIR_GLES2 1
#	if TARGET_OS_IPHONE
#		define STIR_IPHONE 1
#		define STIR_ARM 1
//TARGET_IPHONE_SIMULATOR
#	else
#		define STIR_MACOSX 1
#		define STIR_X86 1
#	endif
#	define STIR_EXCEPTIONS defined(__EXCEPTIONS)
#endif

#ifdef _WIN32
#warning ("defined (_WIN32)")
#	define STIR_WINDOWS 1
#	define STIR_DIRECTX 1
#	define STIR_DIRECTX_MATH 1
#	define STIR_OWN_MATH 0
#	define STIR_X86 1
#	define STIR_MSVC 1
#endif

#if STIR_WINDOWS
#	if STIR_BUILD_DLL
#		define STIR_ATTR_DLL __declspec(dllexport)
//#		pragma message ("STIR_ATTR_DLL = __declspec(dllexport)")
#	elif STIR_USE_DLL
#		define STIR_ATTR_DLL __declspec(dllimport)
//#		pragma message ("STIR_ATTR_DLL = __declspec(dllimport)")
#	else
#		define STIR_ATTR_DLL
//#		pragma message ("STIR_ATTR_DLL = ''")
#	endif

#pragma warning (disable: 4275 4251)

#elif (STIR_IPHONE || STIR_MACOSX)

#define STIR_ATTR_DLL

#endif

#define STIR_WIDE_STRING (STIR_WINDOWS)

#ifndef STIR_OPENGL
#define STIR_OPENGL 0
#endif

#endif
