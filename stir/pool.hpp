#ifndef stir_pool_hpp
#define stir_pool_hpp

#include <memory>
#include <stir/chain.hpp>
#include <stir/single_link.hpp>

namespace stir
{
	template<class T>
	class pool
	{
		typedef typename std::aligned_storage<sizeof(T)>::type cont_t;
		union block {
			block() : next_free() {}
			single_link next_free;
			cont_t storage[1];
		};
		block* m_free_area, *m_end, *m_storage;
		block* m_free_blocks = nullptr;
		size_t m_used = 0;
		
		enum : size_t { s_diff = offsetof(block, storage) };
		
	public:
		pool (size_t maxSize) {
			m_storage = m_free_area = new block [maxSize];
			m_end = m_storage + maxSize;
		}
		
		~pool () {
			delete []m_storage;
		}
		
		template<class... Args>
		T* construct (Args&&... args) {
			block* d;
			if (m_free_blocks != nullptr) {
				d = m_free_blocks;
				m_free_blocks = reinterpret_cast<block*>( unlink_first (&m_free_blocks->next_free) );
			} else if (m_free_area != m_end) {
				d = m_free_area++;
			} else // no more space left
				return nullptr;
			new (d->storage) T (std::forward<Args&&...>(args...));
			m_used++;
			return reinterpret_cast<T*> (d->storage);
		}
		
		void destroy (T* p) {
			block* b = reinterpret_cast<block*> (p);
			m_used--;
			reinterpret_cast<T*> (b->storage)->~T();
			new (&b->next_free) stir::single_link ();
			m_free_blocks = reinterpret_cast<block*>( prepend(&m_free_blocks->next_free, &b->next_free) );
		}
		
		size_t size() const { return m_used; }
		size_t capacity() const { return m_end - m_storage; }
		
	};
}


#endif
