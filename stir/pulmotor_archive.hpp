#ifndef STIR_PULMOTOR_ARCHIVE_HPP_
#define STIR_PULMOTOR_ARCHIVE_HPP_

#include <pulmotor/archive.hpp>

#include "basic_types.hpp"
#include "math_types.hpp"
#include "flat_types.hpp"

namespace stir
{

template<class ArchiveT>
void archive (ArchiveT& ar, color& v, unsigned version)
{
	ar & v.r & v.g & v.b & v.a;
}

template<class ArchiveT, class T>
void archive (ArchiveT& ar, vector2_tag<T>& v, unsigned version)
{
	ar & v.x & v.y;
}
	
/*template<class ArchiveT, class T>
void archive (ArchiveT& ar, vector3_tag<T>& v, unsigned version)
{
	ar & v.x & v.y & v.z;
}*/
	
template<class ArchiveT, class T>
void archive (ArchiveT& ar, circle_tag<T>& v, unsigned version)
{
	ar & v.center_ & v.radius_;
}

template<class ArchiveT, class T>
void archive (ArchiveT& ar, rect_tag<T>& v, unsigned version)
{
	ar & v.p & v.s;
}
	
template<class ArchiveT, class T>
void archive (ArchiveT& ar, area_tag<T>& v, unsigned version)
{
	ar & v.p0 & v.p1;
}
	
/*	// dynamic vector
template<class ArchiveT, class T, class AllocatorT>
void archive (ArchiveT& ar, dynamic_array<T, AllocatorT>& v, unsigned version)
{
	u32 sz = (u32)v.size ();
	ar | sz;
	
	if (ArchiveT::is_reading)
		v.resize (sz);
	
	if (std::is_fundamental<T>::value)
		ar & pulmotor::memblock (&*v.begin(), sz);
	else
		for (size_t i=0; i<sz; ++i)
			ar | v[i];
}*/

}

#endif
