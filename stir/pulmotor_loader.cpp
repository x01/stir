#include "pulmotor_loader.hpp"
#include <pulmotor/pulmotor_fwd.hpp>
#include "stir_logs.hpp"

namespace stir {
		
pulmotor_loader_impl::pulmotor_loader_impl(path const& prefix, path const& suffix)
:	prefix_(prefix)
,	suffix_(suffix)
{
}

pulmotor_loader_impl::~pulmotor_loader_impl ()
{
}

void* pulmotor_loader_impl::load_impl (resource_id const& id, int profile)
{
	path pathname = prefix_ / id.c_str () + "." + suffix_;
	std::auto_ptr<pulmotor::basic_input_buffer> input = pulmotor::create_plain_input (pathname);
	
	STIR_LOG (resource) << "Loading data file '" << pathname.c_str() << "'";
	
	if (input.get())
	{
		pulmotor::file_size_t fsize = pulmotor::get_file_size (pathname);
		if (fsize > 0)
		{
			u8* data = new u8 [(size_t)fsize];
			
			std::error_code ec;
			size_t wasRead = input->read (data, (size_t)fsize, ec);

			STIR_LOG(resource) << ", result: " << ec.value() << ", read: " << wasRead << log::endl;
			
			assert (0);
			//			pulmotor::util::fixup_pointers<void*> (data);
			
#ifdef _DEBUG
			m_loaded.insert (data);
#endif
			
			return data;
		}
	}
	
	STIR_LOG (resource) << " - FAILED" << log::endl;
	
	return NULL;
}

void pulmotor_loader_impl::unload_impl( void* resource, int profile )
{
#ifdef _DEBUG
	std::set<void*>::iterator it = m_loaded.find (resource);
	if (it == m_loaded.end ())
		STIR_LOG (resource).printf("Attempting to unload a pulmotor-resource that was not loaded! (%p)\n", resource);
	else
		m_loaded.erase (it);
#endif
	delete[] (u8*)resource;
}

}
