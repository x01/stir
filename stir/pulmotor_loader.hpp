#ifndef STIR_PULMOTOR_LOADER_HPP_
#define STIR_PULMOTOR_LOADER_HPP_

#include <stir/platform.hpp>
#include <stir/resource_id.hpp>
#include <stir/resource_loader.hpp>
#include <stir/path.hpp>

#ifdef _DEBUG
#include <set>
#endif

namespace stir {

class STIR_ATTR_DLL pulmotor_loader_impl
{
public:
	pulmotor_loader_impl (path const& prefix, path const& suffix);
	~pulmotor_loader_impl ();
	
	void* load_impl( resource_id const& id, int profile );
	void unload_impl( void* resource, int profile );
	
private:	
	path	prefix_;
	path	suffix_;
	
#ifdef _DEBUG
	std::set<void*>	m_loaded;
#endif
};

template<class T>
class STIR_ATTR_DLL pulmotor_loader : public loader<T>, pulmotor_loader_impl
{
public:
	pulmotor_loader	(path const& prefix, path const& suffix)
	:	pulmotor_loader_impl (prefix, suffix)
	{}
	
	~pulmotor_loader ()
	{}
	
	T load( resource_id const& id, int profile ) {
		void* p = load_impl (id, profile);
		return T (p);
	}
	void unload(T resource, int profile ) {
		void* p = resource.start ();
		return unload_impl (p, profile);
	}
};

//namespace storage { namespace image_atlas {	class atlas; }}
//	
//class STIR_ATTR_DLL atlas_loader : pulmotor_loader
//{
//public:
//	atlas_loader (path const& prefix, path const& suffix)
//	:	pulmotor_loader (prefix, suffix)
//	{}
//	
//	virtual storage::image_atlas::atlas* load( resource_id const& id, int profile ) {
//		return (storage::image_atlas::atlas*)load (id, profile);
//	}
//	virtual void unload( storage::image_atlas::atlas* resource, int profile ) {
//	}
//	
//};
	
}

#endif