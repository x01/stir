#include "resmgr.hpp"
#include "r_atlas.hpp"
#include "texture.hpp"

#include <pulmotor/archive.hpp>
#include <pulmotor/stream.hpp>
#include <stir/pulmotor_archive.hpp>
#include <stir/filesystem.hpp>

namespace stir {

index_manager<atlas&, loader<atlas*> > r_atlas;
	
namespace sa = storage::image_atlas;
	
atlas_loader::atlas_loader (path const& prefix)
:	prefix_(prefix)
,	err_atlas_(nullptr)
{
}
	
atlas_loader::~atlas_loader ()
{
	if (err_atlas_)
	{
		for (auto& p : err_atlas_->pages)
			r_texture.free(p.tex);
		
		delete err_atlas_;
	}
}
		
atlas* atlas_loader::load( resource_id const& id, int profile)
{
	path p = prefix_ / id.get_id() + ".atlas";
	std::error_code ec;
	filesystem::input_buffer in(p, ec);
	if (ec)
		return get_err_object();

	sa::atlas r_a;
	pulmotor::input_archive inA (in);
//	pulmotor::debug_archive<pulmotor::input_archive> inAD (inA, std::cout);
	inA | r_a;
	if (inA.ec)
		return get_err_object();

	atlas* a = new atlas();
	a->sections.assign (r_a.sections.begin (), r_a.sections.end ());
	a->pages.reserve(r_a.pages.size());
	for (auto& r_p : r_a.pages)
	{
		atlas::page p;
		p.name = r_p->name;
		p.pixel_size = r_p->pixel_size;
		
		if (r_p->texture) {
#if STIR_IPHONE
			stir::texture tex = stir::gles::create_texture(*r_p->texture);
#else
			stir::texture tex = nullptr;
#endif
			p.tex = r_texture.reg(tex);
		}
		
		a->pages.push_back(p);
	}
	
	return a;
}
	
void atlas_loader::unload(atlas* a, int profile)
{
	if (!err_atlas_ || a != get_err_object ())
	{
		for (auto& p : a->pages)
			r_texture.free(p.tex);
		
		delete a;
	}
}

atlas* atlas_loader::get_err_object()
{
	if (!err_atlas_)
		err_atlas_ = new atlas;
	
	return err_atlas_;
}
	
	
}
