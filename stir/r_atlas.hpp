#ifndef STIR_ATLAS_HPP_
#define STIR_ATLAS_HPP_

#include <stir/resfwd.hpp>
#include <vector>

#include <stir/platform.hpp>

#include <stir/resource_id.hpp>
#include <stir/resource_loader.hpp>

#include <stir/atlas/storage-atlas.hpp>
#include <stir/atlas/storage-atlas_util.hpp>
#include <stir/texture.hpp>
#include <stir/basic_util.hpp>

namespace stir {
	
	namespace storage { namespace image_atlas {
		struct atlas;
	} }
	
struct atlas
{
	struct page
	{
		castring name;
		vector2f pixel_size;
		rindex_t tex;
	};
	
	typedef storage::image_atlas::section section;
	
	std::vector<page> pages;
	std::vector<section> sections;
	
	section const* get_section (char const* section_name, int group = 0) const {
		auto key = std::make_pair (section_name, group);
		auto it = utility::binary_find (sections.begin (), sections.end (), key, storage::image_atlas::less_by_name_and_group());
		return it == sections.end () ? NULL : &*it;
	}

};

class STIR_ATTR_DLL atlas_loader : public loader<atlas*>
{
public:
	atlas_loader (path const& prefix);
	~atlas_loader ();
	
	virtual atlas* load( resource_id const& id, int profile);
	virtual void unload(atlas* resource, int profile);
	
private:
	atlas* get_err_object();
	
	atlas*	err_atlas_;
	path	prefix_;
};
	
	
extern index_manager<atlas&, loader<atlas*> > r_atlas;
typedef index_wrap<atlas> i_atlas;

}

#endif