#include "resmgr.hpp"
#include "r_fontdata.hpp"

namespace stir {
	index_manager<r_fontdata_dataptr, loader<r_fontdata_dataptr> > r_fontdata;
}
