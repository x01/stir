#ifndef STIR_R_FONT_HPP_
#define STIR_R_FONT_HPP_

#include <stir/resfwd.hpp>

namespace stir {
	namespace storage {
		class font_data;
	}
	
	typedef pulmotor::data_ptr<storage::font_data> r_fontdata_dataptr;
	typedef pulmotor_loader<r_fontdata_dataptr> r_fontdata_loader_t;
	
	extern index_manager<r_fontdata_dataptr, loader<r_fontdata_dataptr> > r_fontdata;
	typedef index_wrap<r_fontdata_dataptr> i_fontdata;
}

#endif // STIR_R_FONT_HPP_
