#include "resmgr.hpp"
#include "r_gpuprog.hpp"
#include "gles_shader.hpp"

#include <stir/filesystem.hpp>

namespace stir {
	
	
index_manager<gpu_program&, loader<gpu_program*>, gpuprog_data> r_gpuprog;

gpuprog_loader::gpuprog_loader (path const& prefix)
:	prefix_(prefix)
,	err_gpuprog_(nullptr)
{
}

gpuprog_loader::~gpuprog_loader ()
{
	delete err_gpuprog_;
}

static bool parse_name(char const* name, path& vsP, path& psP)
{
	enum { none, vs, ps };
	int state = none;
	char const* batch = name, *p = name;
	do {
		char c = *p;
		if (c == ':') {
			if (strncmp ("vs", batch, p - batch) == 0) {
				state = vs;
			} else if(strncmp ("ps", batch, p - batch) == 0) {
				state = ps;
			} else
				state = none;
			batch = p + 1;			
		} if (c == '|' || c == 0/*eos*/) {
			 if (state == vs) {
				 vsP = std::string(batch, p);
			 } else if (state == ps) {
				 psP = std::string(batch, p);
			 } else
				 break;
			batch = p + 1;
		}
	} while (*p++);
	
	if (vsP.empty () && psP.empty())
	{
		vsP = std::string(name) + ".vert";
		psP = std::string(name) + ".frag";
	}
	
	if (!vsP.empty () && !psP.empty())
		return true;
	
	return false;
}
	
gpu_program* gpuprog_loader::load( resource_id const& id, int profile)
{
	path vsP, psP;
	
	if (!parse_name (id.c_str(), vsP, psP))
	{
		STIR_LOG(debug).printf("Can't parse out gpuprog name: %s\n", id.c_str());
		return get_err_object();
	}
	
	std::error_code ec;
	
	char temp[64];
	snprintf(temp, 64, "%s + %s", vsP.c_str(), psP.c_str());

	gpu_program* p = new gpu_program(temp);
	gpu_program::loader loader (*p);
	vsP = prefix_ / vsP;
	psP = prefix_ / psP;
	bool success = loader.load_file(vsP.c_str(), psP.c_str(), ec);
	
	if (!success) {
		delete p;
		STIR_LOG(debug).printf("Can't load gpu program: %s / %s (%s)\n", vsP.c_str(), psP.c_str(), ec.message().c_str());
	}

	// loading may be successful, but an info message (a warning) may be present still
	for (int i=0; i<gpu_program::loader::Shader::count; ++i) {
		if (!loader.msg((gpu_program::loader::Shader)i).empty()) {
			STIR_LOG(debug).printf("gpu program object(%d) %s:\n%s\n", i, loader.m_state[i] == GL_TRUE ? "SUCCESS" : "FAIL",
								   loader.msg((gpu_program::loader::Shader)i).c_str() );
		}
	}
	
	return success ? p : get_err_object();
}

void gpuprog_loader::unload(gpu_program* p, int profile)
{
	if (err_gpuprog_ == nullptr || p != get_err_object ())
	{
		delete p;
	}
}

gpu_program* gpuprog_loader::get_err_object()
{
	if (!err_gpuprog_)
		err_gpuprog_ = new gpu_program("<error gpuprog>");
	
	return err_gpuprog_;
}

}
