#ifndef STIR_R_GPUPROG_HPP_
#define STIR_R_GPUPROG_HPP_

#include <stir/resmgr.hpp>

#include <stir/platform.hpp>
#include <stir/chain.hpp>

namespace stir {
#if STIR_GLES2
	namespace gl {
		class gpu_program;
	}
	
	typedef gl::gpu_program gpu_program;
	
	class STIR_ATTR_DLL gpuprog_loader : public loader<gpu_program*>
	{
	public:
		gpuprog_loader (path const& prefix);
		~gpuprog_loader ();
		
		// name is in the form: ps:filename|vs:filename
		virtual gpu_program* load(resource_id const& id, int profile);
		virtual void unload(gpu_program* resource, int profile);
		
	private:
		gpu_program* get_err_object();
		
		gpu_program* err_gpuprog_;
		path prefix_;
	};
	
	struct STIR_ATTR_DLL gpuprog_listener : public double_chain<gpuprog_listener>
	{
		// called when a gpuprog is loader or reloaded
		virtual void on_gpuprog_was_reloaded() = 0;
	};
	
#else
#error "which open gl?"
#endif
	
	struct STIR_ATTR_DLL gpuprog_data
	{
		double_chain<gpuprog_listener> m_listeners;
		
		void on_load (rindex_t index, gpu_program* prog) {
			for (double_chain_iterator<gpuprog_listener> it=&m_listeners, e = &m_listeners; it != e; ++it ) {
				it->on_gpuprog_was_reloaded();
			}
		}
		void on_free (rindex_t index, gpu_program* prog) {
		}

		void add_listener (double_chain<gpuprog_listener>& l) { m_listeners.tail_insert(l); }
		void remove_listener (double_chain<gpuprog_listener>& l) { l.unlink_this(); }
		
		void call () {
//			for (double_chain_iterator<gpuprog_listener> it=&m_listeners, e = &m_listeners; it != e; ++it ) {
//				it->on_gpuprog_was_reloaded();
//			}
		}
	};

	class gpuprog_loader;
	
	extern index_manager<gpu_program&, loader<gpu_program*>, gpuprog_data> r_gpuprog;
	typedef index_wrap<gpu_program> i_gpuprog;
}


#endif
