#include "resmgr.hpp"
#include "r_spritesheet.hpp"

#include <stir/filesystem.hpp>
#include <stir/atlas/storage-atlas.hpp>

#include <pulmotor/archive.hpp>
#include <pulmotor/archive_std.hpp>
#include <stir/pulmotor_archive.hpp>

namespace stir {
	
index_manager<sprite_sheet&, loader<sprite_sheet*> > r_spritesheet;
	
namespace sa = storage::image_atlas;

sprite_sheet_loader::sprite_sheet_loader (path const& prefix)
:	m_prefix(prefix)
,	m_err_ss(nullptr)
{
}

sprite_sheet_loader::~sprite_sheet_loader ()
{
	delete m_err_ss;
}

sprite_sheet* sprite_sheet_loader::load (resource_id const& id, int profile)
{
	path p = m_prefix / id.get_id() + ".ss";
	std::error_code ec;
	
	filesystem::input_buffer in(p, ec);
	if (ec)
		return get_err_object();
	
	sprite_sheet* ss = new sprite_sheet;
	pulmotor::input_archive inA (in);
//	pulmotor::debug_archive<pulmotor::input_archive> inAD (inA, std::cout);
	inA | *ss;
	if (inA.ec) {
		delete ss;
		return get_err_object();
	}
	
	return ss;
}

void sprite_sheet_loader::unload (sprite_sheet* a, int profile)
{
	if (!m_err_ss || a != get_err_object ())
		delete a;
}

sprite_sheet* sprite_sheet_loader::get_err_object()
{
	if (!m_err_ss)
		m_err_ss = new sprite_sheet;
	
	return m_err_ss;
}


}
