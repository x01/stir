#ifndef STIR_SPRITESHEET_HPP_
#define STIR_SPRITESHEET_HPP_

#include <stir/resfwd.hpp>

namespace stir {
	namespace storage { namespace image_atlas {
		struct sprite_sheet;
	} }
	
	typedef storage::image_atlas::sprite_sheet sprite_sheet;
	
	class STIR_ATTR_DLL sprite_sheet_loader : public loader<sprite_sheet*>
	{
	public:
		sprite_sheet_loader (path const& prefix);
		~sprite_sheet_loader ();
		
		virtual sprite_sheet* load( resource_id const& id, int profile);
		virtual void unload (sprite_sheet* resource, int profile);
		
	private:
		sprite_sheet* get_err_object();
		
		sprite_sheet* m_err_ss;
		path m_prefix;
	};
	
	extern index_manager<sprite_sheet&, loader<sprite_sheet*> > r_spritesheet;
	typedef index_wrap<sprite_sheet> i_spritesheet;
}

#endif
