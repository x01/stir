#ifndef STIR_RESFWD_HPP_
#define STIR_RESFWD_HPP_

#include "basic_types.hpp"

namespace stir {
	
typedef u16 rindex_t;
	
static const rindex_t rinvalid = -1;
	
struct resource_id;
	
template<class ResourceT>
class loader;

template<class ResourceT>
struct index_wrap
{
	rindex_t index;
	index_wrap () : index(rinvalid) {}
	index_wrap (rindex_t i) : index(i) {}
	
	index_wrap& operator= (rindex_t i) { index = i; return *this; }
	operator rindex_t() const { return index; }
};

	//	if_<std::is_reference<ResourceT>::value, typename std::remove_reference<ResourceT>::type*, ResourceT>
template<class ResourceT, class LoaderT, class DataT = void>
class index_manager;
	
template<class T>
class pulmotor_loader;
	
template<class T, class DataT>
struct index_manager_cb
{
	static void on_load (T resource, DataT& data) {}
	static void on_free (T resource, DataT& data) {}
};

}

#endif
