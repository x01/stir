#ifndef STIR_RESOURCE_MANAGER_HPP_
#define STIR_RESOURCE_MANAGER_HPP_

#include "resource_id.hpp"
#include "resource_loader.hpp"
#include "stir_logs.hpp"
#include "dynamic_array.hpp"
#include "language_util.hpp"
#include "resource_id.hpp"
#include "resfwd.hpp" // need fwd-declaration for default template arguments

#include <vector>
#include <map>
#include <set>
#include <algorithm>

namespace stir
{
	
//struct resource_info
//{
//	void*	resource_;
//	u16		profile_;
//	u8		loader_;
//	u8		reserved_;
//	
//	bool is_empty () const { return resource_ == 0; }
//	
//#if _DEBUG
//	resource_id	id_;
//#endif
//};
	
namespace detail {
	
	struct index_pool_base
 	{
		typedef unsigned bit_t;
		
		static int const kBitCount = sizeof(bit_t) * 8;
		static int const kBitCountLog2 = ct_log2<kBitCount>::value;
		static bit_t const kFull = (bit_t)-1;
		static size_t bitchunk (size_t index) { return index >> kBitCountLog2; }
		
		static int first_unset_generic (bit_t v) {
			for (int i=0; i<kBitCount; ++i) {
				if ((v & ((bit_t)0x01U << (kBitCount-1))) == 0)
					return i;
				v <<= 1;
			}
			return kBitCount;
		}
		
		// TODO: optimize this to special instructions
		// returns index of a first zero bit from the left (most significant)
		static int first_unset (bit_t v) {
#if defined(__arm__)
			// ffs(x) = w − clz(x & (−x)).
			bit_t iv = ~v;
			if (iv == 0)
				return kBitCount;
			int unset = __builtin_clz (iv);
			assert (unset == first_unset_generic(v));
			return unset;
#else
			return first_unset_generic(v);
#endif
		}
		
		size_t do_mod (size_t a) const { return a & (bits_.size ()-1); }
		
		typedef dynamic_array<bit_t> bit_cont_t;
		bit_cont_t	bits_;
		size_t		last_empty_chunk_;
		
		// override this in the derived class and expand your associated data array to bits_ equivalent size when called
		virtual void expand_resource () = 0;
		
		void expand ()
		{
			bits_.resize (bits_.size () << 1);
			expand_resource ();
		}
		
		index_pool_base (size_t reserve)
		:	bits_ (reserve)
		,	last_empty_chunk_ (0)
		{
		}
		
		rindex_t allocate_index ()
		{
			size_t chunkIndex = last_empty_chunk_;
			int freeIdx = first_unset (bits_[chunkIndex]);
			
			if (freeIdx >= kBitCount)
			{
				for (chunkIndex=do_mod (chunkIndex+1); chunkIndex != last_empty_chunk_; ++chunkIndex)
				{
					freeIdx = first_unset (bits_[chunkIndex]);
					if (freeIdx < kBitCount)
					{
						last_empty_chunk_ = chunkIndex;
						goto found_empty;
					}
				}
				
				// no free bits found: allocate
				last_empty_chunk_ = bits_.size ();
				expand ();
				freeIdx = first_unset (bits_[chunkIndex]);
				assert (freeIdx < kBitCount);
			}
		found_empty:
			
			bits_[chunkIndex] |= ((bit_t)0x01U << (kBitCount-1)) >> freeIdx;
			return (chunkIndex << kBitCountLog2) + freeIdx;
		}
		
		void free_index (rindex_t idx)
		{
			size_t chunkIdx = bitchunk (idx);
			size_t bitIdx = (idx & kBitCount-1);
			bits_ [chunkIdx] &= ~(((bit_t)0x01U << (kBitCount-1)) >> bitIdx);
		}
	};
}

template<class ResourceT>
class index_pool : detail::index_pool_base
{
	typedef detail::index_pool_base base_t;	
	typedef dynamic_array<ResourceT> res_cont_t;
	
	res_cont_t	resources_;
	
	void expand_resource ()
	{
		resources_.resize (resources_.size () << 1);
	}
	
public:
	index_pool (size_t reserve = 1024)
	:	base_t (reserve)
	,	resources_ (reserve)
	{
	}
	
	~index_pool ()
	{
	}
	
	rindex_t allocate ()
	{
		return base_t::allocate_index ();
	}
	
	void free (rindex_t idx)
	{
		return base_t::free_index (idx);
	}
	
	ResourceT& operator [] (rindex_t idx)
	{
		assert (idx < resources_.size () );
		return resources_ [idx];
	}
	
	ResourceT& fetch (rindex_t idx)
	{
		assert (idx < resources_.size () );
		return resources_ [idx];
	}
};

namespace detail
{
	enum { no_loader = ~0x0 };
	enum { kLoaderCount = 2 };
	
	template<class T> struct data_holder {
		typedef T& return_type;
		data_holder() : m_data() {}
		T& get_data() { return m_data; }
		
		template<class ResourceT>
		void fwd_on_load (rindex_t index, ResourceT res) { m_data.on_load (index, res); }
		template<class ResourceT>
		void fwd_on_free (rindex_t index, ResourceT res) { m_data.on_free (index, res); }
		
		T m_data;
	};
	template<> struct data_holder<void> {
		typedef void return_type;
		void get_data() { }

		template<class ResourceT>
		void fwd_on_load (rindex_t index, ResourceT res) { }
		template<class ResourceT>
		void fwd_on_free (rindex_t index, ResourceT res) { }
	};
	
	template<class ResourceT>
	struct index_info
	{
		index_info () {}
		
		index_info (ResourceT res, unsigned prof, unsigned ldr)
		:	resource (res), profile (prof), loader_index (ldr) {}
		
		ResourceT	resource;
		u16			profile, loader_index;
#ifdef _DEBUG
		std::string	name;
#endif
	};

	enum { kProfileHighBit = 16 };
	typedef unsigned pl_t;
	
#define STIR_MERGE_PROFILE_LOADER(p, l) (((p) << kProfileHighBit) | (l))

	struct resource_key
	{
		resource_key (resource_id const& rid, unsigned profile, unsigned loaderIdx)
		:	id (rid), profile_loader (STIR_MERGE_PROFILE_LOADER(profile, loaderIdx))
		{
			assert (loaderIdx < kLoaderCount);
			assert (profile < (1 << kProfileHighBit));
		}
		
		resource_id	id;
		pl_t profile_loader;
	};
	
	struct resource_finder
	{
		resource_id const&	id;
		pl_t profile_loader;

		resource_finder (resource_id const& rid, unsigned profile, unsigned loaderIdx)
		:	id (rid), profile_loader (STIR_MERGE_PROFILE_LOADER(profile, loaderIdx))
		{}
	};
	
	struct resource_cache_less
	{
		struct is_transparent {};
		
		typedef bool result_type;
		
		bool operator () (resource_key const& a, resource_key const& b) const
		{
			return a.id < b.id ? true : (a.profile_loader < b.profile_loader);
		}
		
		bool operator () (resource_key const& a, resource_finder const& b) const
		{
			return a.id < b.id ? true : (a.profile_loader < b.profile_loader);
		}
		
		bool operator () (resource_finder const& a, resource_key const& b) const
		{
			return a.id < b.id ? true : (a.profile_loader < b.profile_loader);
		}
	};

	// Resource can be specified as a reference. In that case we return a reference to an object
	// but still must store it by pointer
	template<bool IsRef, class NoRefT> struct holder_type {
		typedef NoRefT* type;
		static NoRefT& r(NoRefT* p) { return *p; }
		static NoRefT* h(NoRefT& p) { return &p; }
	};
	template<class T> struct holder_type<false, T> {
		typedef T type;
		static T r(T p) { return p; }
		static T h(T p) { return p; }
 	};
}

template<class ResourceT, class LoaderT, class DataT>
class index_manager : detail::data_holder<DataT>
{
	enum { kLoaderCount = detail::kLoaderCount };
	
	typedef ResourceT Resource;
	typedef detail::data_holder<DataT> DataHolder;
	typedef typename std::remove_reference<ResourceT>::type ResourceNoRef;
	typedef typename std::remove_pointer<ResourceNoRef>::type ResourceTag;
	static const bool ResourceIsReference = std::is_reference<ResourceT>::value;
	typedef typename detail::holder_type<ResourceIsReference, ResourceNoRef> Holder;
	typedef typename Holder::type HoldingResource;
	typedef LoaderT loader_t;
	typedef detail::index_info<HoldingResource> index_info_t;
	typedef index_pool<index_info_t> index_pool_t;
	
	typedef detail::resource_cache_less CacheLess;
	typedef detail::resource_key Key;
	typedef std::map<Key, rindex_t, CacheLess> Cache;
	
	loader_t*		loaders_[kLoaderCount];
	index_pool_t	pool_;
	Cache			cache_;
	
public:
	
	typedef typename Cache::iterator cached_iterator;
	typedef typename Cache::const_iterator const_cached_iterator;
	
	// TODO: index iterator (to iterate over loaded indices)
	
	index_manager ();
	~index_manager ();
	
	Resource resolve (rindex_t index) { return Holder::r (pool_.fetch (index).resource); }
	Resource operator % (rindex_t index) { return Holder::r (pool_.fetch (index).resource); }
	
	Resource resolve (index_wrap<ResourceTag> wrap) { return Holder::r (pool_.fetch (wrap.index).resource); }
	Resource operator % (index_wrap<ResourceTag> wrap) { return Holder::r (pool_.fetch (wrap.index).resource); }
	
	typename DataHolder::return_type data () { return static_cast<detail::data_holder<DataT>*>(this)->get_data(); }
	
	void set_loader (int idx, loader_t* ldr) { assert (idx<kLoaderCount); loaders_[idx] = ldr; }
	loader_t* get_loader (int idx) { assert (idx<kLoaderCount); return loaders_[idx]; }
	
	rindex_t load (resource_id const& rid, int profile = 0, int loader = 0)
	{
		assert (loaders_ [loader] != 0);
		HoldingResource resource = loaders_ [loader]->load (rid, profile);
		
		rindex_t idx = pool_.allocate ();
		pool_.fetch (idx) = index_info_t (resource, profile, loader);

		//		this->fwd_on_load (idx, resource);
		
#ifdef _DEBUG
		pool_.fetch (idx).name = rid.c_str();
//		STIR_LOG(resource).printf ("index (%s): %d\n", rid.c_str(), idx);
#endif
		
		return idx;
	}
	
	rindex_t reg (Resource resource, int profile = 0)
	{
		rindex_t idx = pool_.allocate ();
		pool_.fetch (idx) = index_info_t (Holder::h (resource), profile, detail::no_loader);
		return idx;
	}
	
	void free (index_wrap<ResourceTag> wrap)
 	{
		free (wrap.index);
	}
		
	void free (rindex_t index)
	{
		index_info_t& ii = pool_.fetch (index);

		//		this->fwd_on_free (index, ii.resource);

		if (ii.loader_index != detail::no_loader)
			loaders_ [ii.loader_index]->unload (ii.resource, ii.profile);
#ifdef _DEBUG
		STIR_LOG(resource).printf ("unloading: %s (idx:%d)\n", ii.name.c_str(), index);
#endif		
		pool_.free (index);
	}
	
	rindex_t load_cached (resource_id const& rid, int profile = 0, int loader = 0)
	{
		auto it = cache_.find (detail::resource_finder (rid, profile, loader));
		
		if (it == cache_.end ()) {
			rindex_t index = load (rid, profile, loader);
			Key key (rid, profile, loader);
			cache_.insert (std::make_pair (key, index));
			return index;
		}
		
		return it->second;
	}
	
	void unload_cached ()
	{
		for (cached_iterator it = cached_begin (), end = cached_end (); it != end; ++it)
		{
//			free (it->first);
		}
	}
	
	cached_iterator cached_begin () { return cache_.begin (); }
	cached_iterator cached_end () { return cache_.end (); }
};

template<class ResourceT, class LoaderT, class DataT>
index_manager<ResourceT, LoaderT, DataT>::index_manager ()
{
	for (int i=0; i<kLoaderCount; ++i)
		loaders_[i] = NULL;
}

template<class ResourceT, class LoaderT, class DataT>
index_manager<ResourceT, LoaderT, DataT>::~index_manager ()
{
}
	
	
}

#endif
