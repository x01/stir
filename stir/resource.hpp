#ifndef STIR_RESOURCE_HPP_
#define STIR_RESOURCE_HPP_

#include <string>
#include <map>
#include <boost/shared_ptr.hpp>

#include "log.hpp"
#include "stir_logs.hpp"
#include "dynamic_array.hpp"
#include "complex_types.hpp"
#include "basic_util.hpp"
//#include "utility.hpp"
#include "resource_id.hpp"
#include "resource_handle.hpp"
#include "resource_loader.hpp"

namespace stir
{

inline log::log& operator<< (log::log& l, resource_id const& s) { l << s.c_str (); return l; }

template<class CharT, class CharTraitsT>
inline std::basic_ostream<CharT,CharTraitsT>& operator<<( std::basic_ostream<CharT,CharTraitsT>& os, resource_id const& r )
{	os << r.get_id(); return os; }

namespace resource_conf
{
	extern bool warn_on_load;
}

template<typename IdT, typename HandleT>
class resource_provider
{
public:
	virtual ~resource_provider ();
	virtual HandleT get( IdT const& id, int profile = 0 ) = 0;
};

template<typename IdT, typename HandleT>
resource_provider<IdT,HandleT>::~resource_provider ()
{
}

template<class ResourceT>
struct unload_resource
{
	typedef void result_type;
	void operator()( ResourceT const& r ) const;
};

template<class IdT>
struct extended_id
{
	extended_id (IdT const& rid, int prof, int loader = 0)
		:	m_id( rid ), m_profile( prof ), m_loader (loader) {}

	bool operator<( extended_id<IdT> const& a ) const
	{
		return m_profile == a.m_profile ? m_id < a.m_id : m_profile < a.m_profile;
	}

	IdT					m_id;
	unsigned short		m_profile;
	unsigned char		m_loader, m_dummy;
};

template<class IdT, class UnderlyingResourceT>
struct pair_pointer_policy
{
	typedef UnderlyingResourceT						underlying_resource;
	typedef IdT										id_type;
	typedef extended_id<IdT>							key_type;
	typedef std::map<key_type, underlying_resource>	container_type;
	typedef typename container_type::value_type*		holder_type;
	
	static stir::string const& get_debug_name(holder_type h )
	{	return h->first.m_id; }
};

// ----------------------------------------------------------
template<class ResourceHandleT>
class resource_registry
	:	public resource_provider<typename ResourceHandleT::id_type, ResourceHandleT>
{
	typedef typename ResourceHandleT::registry_policy	registry_policy;
	typedef typename registry_policy::id_type			id_type;
	typedef ResourceHandleT							handle_type;
	
protected:
	typedef typename registry_policy::underlying_resource			underlying_resource;
	typedef loader<id_type, underlying_resource>					resource_loader;
	typedef typename registry_policy::container_type				resource_container;
	typedef typename resource_container::iterator					iterator;

public:
	typedef resource_provider<id_type, underlying_resource>		provider_type;

private:
	resource_loader*				loader_;
	resource_container				container_;	
//	dynamic_array<resource_loader*>	loaders_;
//	int								active_loader_;

public:
	resource_registry (resource_loader& loader)
	:	loader_(&loader)
	{}

	~resource_registry()
	{
		for(typename resource_container::iterator it = container_.begin(); it != container_.end(); ++it)
			loader_->unload (it->second, it->first.m_profile);
			
		container_.clear();
	}

	handle_type get (id_type const& id, int profile) {
		underlying_resource r = loader_->load (id, profile);
		typename registry_policy::key_type key (id, profile /*, active_loader_*/);
		iterator it = container_.insert (std::make_pair(key, r)).first;
		return handle_type(&*it);
	}

	/*void add_loader( resource_loader& loader, bool activate )
	{
		dynamic_array<resource_loader*>	ldrs (loaders_.size () + 1);
		
		std::copy (loaders_.begin (), loaders_.end (), ldrs.begin ());
		ldrs.back () = &loader;
		ldrs.swap (loaders_);
		
		if (activate)
			active_loader_ = loaders_.size () - 1;
	}

	handle_type get( id_type const& id, int profile = 0 )
	{
		resource_container::iterator it = resources_.find( id_data<id_type>( id, profile, 0 ) );
		if( it == resources_.end() )
		{
			if( resource_conf::warn_on_load )
			{
				STIR_LOG(warning) << L"Loading resource (!)" << id << log::endl;
			}
			it = insert_resource( id, profile );
		}

		return handle_type( registry_policy::get_holder( it ) );
	}
	
	void unload (handle_type handle, int profile = 0)
	{
		STIR_LOG (log_resource) << L"Unloading resource " << id << std::endl;
		
		int ldrindex = registry_policy::get_loader (handle.holder);
		underlying_resource r = registry_policy::remove (handle.holder);

		// TODO: multiple loaders
		unload_resource<underlying_resource> unloader;
		unloader (r);
	}*/

	/*
	void reload()
	{
		unload_resource<underlying_resource> unloader;

		for( resource_container::iterator it = resources.begin(); it != resources.end(); ++it )
		{
			unloader( it->second );
			underlying_resource res = loader->load( get_id(it), get_profile(it) );
			it->second = res;
		}
	}

protected:
	typename resource_container::iterator insert_resource( id_type const& id, int profile )
	{
		resource_container::iterator it = resources_.find( id_data<id_type>( id, profile, 0 ) );
		if( it == resources_.end() )
		{
			underlying_resource res = loaders_[active_loader_]->load( id, profile );
			if( res )
			{
				//if( (size_t)res == 6 ) {
				//	MessageBox( 0, _T("res == 6"), _T(""), MB_OK );
				//	load_failed( id );
				//}

				it = registry_policy::store( resources_, id, profile, active_loader_, res );
			}
			else
				load_failed( id, profile );
		}

		return it;
	}

	void load_failed( id_type const& id, int profile )
	{
		STIR_LOG( resource ) << L"Failed to load resource: " << id << "|" << profile << log::endl;
//		stir::stir_throw( msg.str() );
	}

protected:
	resource_container				resources_;
	*/
};

/*
template<class ResourceHandleT>
class resource_registry
	:	protected ResourceHandleT::registry_policy
	,	public resource_provider<typename ResourceHandleT::registry_policy::id_type, ResourceHandleT>
{
	typedef typename ResourceHandleT::registry_policy	registry_policy;
	typedef typename registry_policy::id_type		id_type;
	typedef ResourceHandleT						handle_type;
	
protected:
	typedef typename registry_policy::resource_type 				underlying_resource;
	typedef loader<id_type, underlying_resource>					resource_loader;
	typedef typename registry_policy::container::type				resource_container;

public:
	typedef resource_provider<id_type, underlying_resource>		provider_type;

	resource_registry( resource_loader& loader )
	:	loaders_(1)
	,	active_loader_ (0)
	{
		loaders_.back () = &loader;
	}

	~resource_registry()
	{
		for( resource_container::iterator it = resources_.begin(); it != resources_.end(); ++it )
		{
			if( it->second )
				unload_resource<underlying_resource>()( it->second );
		}
		resources_.clear();
	}

	void add_loader( resource_loader& loader, bool activate )
	{
		dynamic_array<resource_loader*>	ldrs (loaders_.size () + 1);
		
		std::copy (loaders_.begin (), loaders_.end (), ldrs.begin ());
		ldrs.back () = &loader;
		ldrs.swap (loaders_);
		
		if (activate)
			active_loader_ = loaders_.size () - 1;
	}

	handle_type get( id_type const& id, int profile = 0 )
	{
		resource_container::iterator it = resources_.find( id_data<id_type>( id, profile, 0 ) );
		if( it == resources_.end() )
		{
			if( resource_conf::warn_on_load )
			{
				STIR_LOG(warning) << L"Loading resource (!)" << id << log::endl;
			}
			it = insert_resource( id, profile );
		}

		return handle_type( registry_policy::get_holder( it ) );
	}
	
	void unload (handle_type handle, int profile = 0)
	{
		STIR_LOG (log_resource) << L"Unloading resource " << id << std::endl;
		
		int ldrindex = registry_policy::get_loader (*handle.holder);
		underlying_resource r = registry_policy::remove (resources_, id, profile);

		unload_resource<underlying_resource> unloader;
		unloader (r);
	}

	void reload()
	{
		unload_resource<underlying_resource> unloader;

		for( resource_container::iterator it = resources.begin(); it != resources.end(); ++it )
		{
			unloader( it->second );
			underlying_resource res = loader->load( get_id(it), get_profile(it) );
			it->second = res;
		}
	}

protected:
	typename resource_container::iterator insert_resource( id_type const& id, int profile )
	{
		resource_container::iterator it = resources_.find( id_data<id_type>( id, profile, 0 ) );
		if( it == resources_.end() )
		{
			underlying_resource res = loaders_[active_loader_]->load( id, profile );
			if( res )
			{
				//if( (size_t)res == 6 ) {
				//	MessageBox( 0, _T("res == 6"), _T(""), MB_OK );
				//	load_failed( id );
				//}

				it = registry_policy::store( resources_, id, profile, active_loader_, res );
			}
			else
				load_failed( id, profile );
		}// else
		//	if( !it->second = loader->load( id ) )
		//		load_failed( id );

		return it;
	}

	void load_failed( id_type const& id, int profile )
	{
		STIR_LOG( resource ) << L"Failed to load resource: " << id << "|" << profile << log::endl;
//		stir::stir_throw( msg.str() );
	}

protected:
	resource_container				resources_;
	dynamic_array<resource_loader*>	loaders_;
	int								active_loader_;
};*/

} // stir

#endif // STIR_RESOURCE_HPP_
