#ifndef STIR_RESOURCE_HANDLE_HPP_
#define STIR_RESOURCE_HANDLE_HPP_

#include "complex_types.hpp"

namespace stir
{

// policy:

// release_handle
// acquire_handle
// get_resource (holder)
// is_valid (holder)
// get_debug_name (holder)


// this guy tries to be smart. when underlying resource is a pointer, it tries to make handle also behave as a pointer

template<class PolicyT>
struct resource_handle
{
	template<class P> struct remove_ptr;
	template<class P> struct remove_ptr<P const*>	{	typedef P const type; };
	template<class P> struct remove_ptr<P*>			{	typedef P type; };
	template<class P> struct remove_ptr				{	typedef P type; };
		
	template<class T> T& deref (T* p) { return *p; }
	template<class T> T& deref (T& p) { return p; }

	typedef PolicyT registry_policy;
	typedef typename registry_policy::underlying_resource underlying_resource;
	typedef typename registry_policy::id_type id_type;
	typedef typename remove_ptr<underlying_resource>::type raw_resource;

	raw_resource& operator*() const
	{	return *holder->second; }

	raw_resource* operator->() const
	{	return holder->second; }

	underlying_resource get() const
	{	return holder->second; }

	bool valid() const
	{	return holder != 0; }

	operator bool() const
	{	return valid(); }

	bool operator==( resource_handle const& a ) const { return holder == a.holder; }
	bool operator!=( resource_handle const& a ) const { return holder != a.holder; }
	bool operator<( resource_handle const& a ) const { return holder < a.holder; }

	stir::string get_debug_name() const
	{	return valid () ? registry_policy::get_debug_name( holder ) : stir::string("INVALID RESOURCE"); }

private:
	bool operator<=( resource_handle const& a ) const;
	bool operator>=( resource_handle const& a ) const;

protected:
	typedef typename registry_policy::holder_type holder_type;

	resource_handle() {}
	resource_handle( holder_type holder_ ) : holder( holder_ ) {}
	~resource_handle () {}
	
	template<class ResourceHandleT>
	friend class resource_registry;
//	friend class PolicyT;

private:
	holder_type	holder;
};

}

#endif // STIR_RESOURCE_HANDLE_HPP_
