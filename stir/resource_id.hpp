#ifndef STIR_RESOURCE_ID_HPP_
#define STIR_RESOURCE_ID_HPP_

#include <stir/platform.hpp>
#include <stir/complex_types.hpp>
#include <stir/logfwd.hpp>

namespace stir
{

struct resource_id
{
	typedef bool (resource_id::*bool_type)() const;

	resource_id()
	{}

	resource_id( native_char_t const* id )
	:	m_id( id )
	{}

	explicit resource_id( stir::string const& t )
	:	m_id( t )
	{}

	resource_id const& operator=( native_char_t const* a )
	{	m_id = a; return *this; }
	
	native_char_t const* c_str () const
	{	return m_id.c_str(); }

	stir::string const& get_id() const	{	return m_id; }
	operator bool_type() const			{	return m_id.empty() ? 0 : &resource_id::operator !; }
	bool operator !() const			{	return m_id.empty(); }

	bool operator<( resource_id const& a ) const
	{	return m_id < a.m_id; }
	bool operator==( resource_id const& a ) const
	{	return m_id == a.m_id; }

	template<class ArchiveT>
	void serialize( ArchiveT& ar, unsigned version )
	{	ar & m_id; }

private:
	stir::string	m_id;
};

}

#endif // STIR_RESOURCE_ID_HPP_
