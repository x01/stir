#include "resource_loader.hpp"

#include "filesystem.hpp"

namespace stir { namespace loader_aux {

void STIR_ATTR_DLL tokenize_path (path const& p, native_pchar const* stop_points, std::vector<path>& tokens)
{
	size_t start = 0;
	do {
		size_t colon = p.string().find_first_of (stop_points, start);
		
		if (colon == string::npos)
			colon = p.string().size();
		
		tokens.push_back( p.string ().substr (start, colon-start) );
		start = colon + 1;
	} while (start < p.string().size());
}

path STIR_ATTR_DLL find_available_file (path const& prefix, path const* suffixes, size_t count)
{
	for( unsigned i=0; i<count; ++i )
	{
		size_t sufIdx = prefix.string().rfind( suffixes[i].string() );
		if( sufIdx != stir::string::npos && sufIdx == prefix.size() - suffixes[i].size() )
		{
			return prefix;
		}
	}
	
	// no suffix specified, so try each in order and test if formed file exists. select and return the first one matching.
	for( unsigned i=0; i<count; ++i )
	{
		path p = prefix + "." + suffixes[ i ];
		if( filesystem::is_file(p) )
			return p;
	}
	
	return prefix;
}

}}