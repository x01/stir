#ifndef STIR_RESOURCE_LOADER_HPP_
#define STIR_RESOURCE_LOADER_HPP_

#include "platform.hpp"
#include "path.hpp"
#include "basic_util.hpp"
#include "resfwd.hpp"

#include <vector>

namespace stir {

template<class ResourceT>
class loader
{
public:
	typedef resource_id id_type;
	typedef ResourceT resource_type;

	virtual ~loader() = 0;

	virtual ResourceT load( id_type const& id, int profile ) = 0;
	virtual void unload( ResourceT resource, int profile ) = 0;
};

template<class ResourceT>
loader<ResourceT>::~loader ()
{
}
	
namespace loader_aux {

void STIR_ATTR_DLL tokenize_path (path const& p, native_pchar const* stop_points, std::vector<path>& tokens);
path STIR_ATTR_DLL find_available_file (path const& prefix, path const* suffixes, size_t count);
	
}

}

#endif
