#ifndef STIR_SER_STD_HPP_
#define STIR_SER_STD_HPP_

namespace std {

template<class ArchiveT, typename A, typename B>
void serialize (ArchiveT& ar, std::pair<A, B>& o, unsigned version)
{
	ar	| o.first
		| o.second
		;
}

}

#endif
