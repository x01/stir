#ifndef STIR_WHOLE_SER_HPP_
#define STIR_WHOLE_SER_HPP_

#include "../math_types.hpp"
#include "../basic_types.hpp"

namespace stir {

template<class ArchiveT, typename T>
inline void serialize (ArchiveT& ar, vector2_tag<T>& o, unsigned version) {
	ar	| o.x | o.y ;
}

template<class ArchiveT, typename T>
inline void serialize (ArchiveT& ar, rect_tag<T>& o, unsigned version) {
	ar	| o.p | o.s ;
}

template<class ArchiveT, class AreaT>
inline void serialize (ArchiveT& ar, area_tag<AreaT>& o, unsigned version)
{
	ar	| o.p0 | o.p1 ;
}

} // stir

#if 0
namespace mtxlib {

template<class ArchiveT>
inline void serialize (ArchiveT& ar, stir::vector2& o, unsigned version) {
	ar	| o.x | o.y ;
}

template<class ArchiveT>
inline void serialize (ArchiveT& ar, stir::vector3& o, unsigned version) {
	ar	| o.x | o.y | o.z ;
}

template<class ArchiveT>
inline void serialize (ArchiveT& ar, stir::vector4& o, unsigned version) {
	ar	| o.x | o.y | o.z | o.w ;
}

template<class ArchiveT>
inline void serialize (ArchiveT& ar, stir::quaternion& o, unsigned version) {
	ar	| o.x | o.y | o.z | o.w ;
}

template<class ArchiveT>
inline void serialize (ArchiveT& ar, stir::matrix& o, unsigned version) {
	ar	| o.col ;
}

template<class ArchiveT>
inline void serialize (ArchiveT& ar, stir::matrix3& o, unsigned version) {
	ar	| o.col ;
}

}

#else

namespace mtxlib {
	
	template<class ArchiveT>
	inline void serialize (ArchiveT& ar, stir::vector2& o, unsigned version) {
		ar	| o.x | o.y ;
	}
	
	template<class ArchiveT>
	inline void serialize (ArchiveT& ar, stir::vector3& o, unsigned version) {
		ar	| o.x | o.y | o.z ;
	}
	
	template<class ArchiveT>
	inline void serialize (ArchiveT& ar, stir::vector4& o, unsigned version) {
		ar	| o.x | o.y | o.z | o.w ;
	}
	
	template<class ArchiveT>
	inline void serialize (ArchiveT& ar, stir::quaternion& o, unsigned version) {
		ar	| o.x | o.y | o.z | o.w ;
	}
	
	template<class ArchiveT>
	inline void serialize (ArchiveT& ar, stir::matrix& o, unsigned version) {
		ar	| o.col ;
	}
	
	template<class ArchiveT>
	inline void serialize (ArchiveT& ar, stir::matrix3& o, unsigned version) {
		ar	| o.col ;
	}	
}


#endif

#endif
