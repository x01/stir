#ifndef stir_single_link_hpp
#define stir_single_link_hpp

#include <cassert>

namespace stir
{
	struct single_link
	{
		single_link() : m_next(nullptr) {}
		single_link *m_next;
	};
	
	inline single_link* find_tail (single_link* head)
	{
		single_link* p = head;
		while (head) {
			p = head;
			head = head->m_next;
		}
		
		return p;
	}
	
	inline single_link* prepend_one (single_link* head, single_link* chain)
	{
		assert (chain->m_next == nullptr && "the list being appended is not of single item");
		chain->m_next = head;
		return chain;
	}
	
	inline single_link* prepend (single_link* head, single_link* chain)
	{
		single_link* t = find_tail (chain);
		t->m_next = head;
		
		return chain;
	}
	
	inline single_link* unlink_first(single_link* head)
	{
		assert (head);
		
		single_link* nh = head->m_next;
		head->m_next = nullptr;
		return nh;
	}
	
	inline size_t count_size (single_link* head)
	{
		size_t cnt = 0;
		while (head) {
			head = head->m_next;
			++cnt;
		}
		return cnt;
	}
}
#endif
