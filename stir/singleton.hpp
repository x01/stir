#ifndef STIR_SINGLETON_HPP_
#define STIR_SINGLETON_HPP_

#include <memory>

namespace stir
{

template<class ClassT>
class singleton
{
public:
	static void init( ClassT* p)
	{	instance.reset (p); }
	
	static void reset ()
	{	instance.reset (); }

	static void init( std::auto_ptr<ClassT> p = std::auto_ptr<ClassT>() )
	{	instance = p; }

	static ClassT& get_instance()
	{
//#ifdef _DEBUG
//		if( !instance.get()) 
//			stir_throw( _T("singleton not initialized") );
//#endif
		return *instance;
	}

	static ClassT& get_auto()
	{
		if( !instance.get())
			instance.reset( new ClassT );

		return *instance;
	}

private:
	static std::auto_ptr<ClassT>	instance;
};

template<class ClassT>
std::auto_ptr<ClassT> singleton<ClassT>::instance;

}

#endif // STIR_SINGLETON_HPP_
