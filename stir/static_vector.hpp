#ifndef STIR_STATIC_VECTOR_HPP_
#define STIR_STATIC_VECTOR_HPP_

#include "basic_types.hpp"
#include <cassert>
#include <new>

namespace stir
{

#define STIR_DESTRUCTOR(x,p) (p)->~x()

	template<class T, int MaxN>
	class static_vector
	{
		struct storage
		{
			enum { type_size = sizeof(T) };

			T* address( size_t i )				{	assert(i < MaxN); return (T*)data_ + i; }
			T const* address( size_t i ) const	{	assert(i < MaxN); return (T const*)data_ + i; }
			
			union {
				char	data_ [type_size * MaxN];
//				T		preview_ [MaxN];
				float 	float_;
			};

//			boost::aligned_storage<type_size * MaxN>	buffer;
		};

	public:
		typedef T&				reference;
		typedef T const&		const_reference;
		typedef T*				pointer;
		typedef T const*		const_pointer;
		typedef pointer			iterator;
		typedef const_pointer	const_iterator;

		static_vector()
		:	occupied( 0 )
		{}

		explicit static_vector( size_t s, T const& a = T() )
		:	occupied( 0 )
		{
			resize( s, a );
		}

		~static_vector()
		{
			for( int i=0; i<occupied; ++i )
				destroy( buffer.address( i ) );
		}

		// iterator access
		iterator begin()				{	return buffer.address( 0 ); }
		const_iterator begin()	const	{	return buffer.address( 0 ); }
		iterator end()					{	return buffer.address( occupied ); }
		const_iterator end() const		{	return buffer.address( occupied ); }

		T const& back() const
		{
			assert( occupied > 0 );
			return *buffer.address( occupied - 1 );
		}

		T const& front() const
		{
			assert( occupied > 0 );
			return *buffer.address(0);
		}

		void push_back( T const& a )
		{
			assert( occupied < MaxN );
			construct( buffer.address( occupied++ ), a );
		}

		void pop_back()
		{
			assert( occupied > 0 );
			destroy( buffer.address( --occupied ) );
		}

		T& operator[]( size_t i )
		{
			assert( i < MaxN );
			return *buffer.address( (int)i );
		}

		T const& operator[]( size_t i ) const
		{
			assert( i < MaxN );
			return *buffer.address( i );
		}

		int capacity() const
		{	return MaxN; }

		int size() const
		{	return occupied; }

		bool empty() const
		{	return occupied == 0; }

		void clear()
		{
			for( int i=0; i<occupied; ++i )
				destroy( buffer.address( i ) );

			occupied = 0;
		}

		void resize( int n )
		{	resize( n, T() ); }

		void resize( int n, T const& a )
		{
			assert( n <= MaxN );
			if( n < size() )
			{
				for( int i=n; i<size(); ++i )
					destroy( buffer.address( i ) );
			}
			else
			{
				for( int i=size(); i<n; ++i )
					construct( buffer.address( i ), a );
			}
			occupied = n;
		}

	private:
		template<class A, class B>
		void construct( A* p, const B& v )	{	new ((void*)p) A(v); }

		template<class Tp> inline
		void destroy( Tp* p)				{	STIR_DESTRUCTOR(Tp, p); }

	private:
		storage		buffer;
		int			occupied;
	};

	template<class T, int MaxN>
	inline void swap_erase( static_vector<T,MaxN>& container, typename static_vector<T,MaxN>::iterator where )
	{
		typename static_vector<T,MaxN>::iterator endIt = container.end();
		--endIt;
		*where = *(endIt);
		container.pop_back();
	}

	template<class T, int MaxN>
	inline void swap_erase( static_vector<T,MaxN>& container, T const& value  )
	{
		for( typename static_vector<T,MaxN>::iterator it = container.begin(); it != container.end(); )
		{
			if( *it == value )
				swap_erase( container, it );
			else
				++it;
		}
	}

}

#endif // STIR_STATIC_VECTOR_HPP_
