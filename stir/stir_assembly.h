#ifndef STIR_ASSEMBLY_HPP_
#define STIR_ASSEMBLY_HPP_

#include "platform.hpp"
#include "basic_types.hpp"


namespace stir {

extern "C" uint64_t stir_muldiv64 (uint64_t num, uint64_t mul, uint64_t div);
extern "C" uint64_t stir_muldiv64_arm (uint64_t num, uint64_t mul, uint64_t div);
	
inline uint64_t muldiv64 (uint64_t num, uint64_t mul, uint64_t div) {
#if STIR_IPHONE && STIR_ARM
	return stir_muldiv64_arm (num, mul, div);
#else
	return stir_muldiv64 (num, mul, div);
#endif
}

}

#endif // STIR_ASSEMBLY_HPP_
