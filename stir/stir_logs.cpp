//#define _WIN32 "FUCK!"

#include "stir_logs.hpp"
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>

STIR_DEFINE_LOG_INTERNAL (stir::win, "win");
STIR_DEFINE_LOG_INTERNAL (stir::dx, "dx");
STIR_DEFINE_LOG_INTERNAL (stir::gl, "gl");
STIR_DEFINE_LOG_INTERNAL (stir::font, "font");
STIR_DEFINE_LOG_INTERNAL (stir::audio, "audio");
STIR_DEFINE_LOG_INTERNAL (stir::gfx, "gfx");
STIR_DEFINE_LOG_INTERNAL (stir::resource, "resource");
STIR_DEFINE_LOG_INTERNAL (stir::fs, "fs");
STIR_DEFINE_LOG_INTERNAL (stir::debug, "debug");
STIR_DEFINE_LOG_INTERNAL (stir::game, "game");
STIR_DEFINE_LOG_INTERNAL (stir::script, "script");

STIR_DEFINE_LOG_INTERNAL (stir::warning, "warning");
STIR_DEFINE_LOG_INTERNAL (stir::error, "error");
STIR_DEFINE_LOG_INTERNAL (stir::exception, "exception");

namespace stir
{
	
namespace
{
void write_to_std_output_impl (log::log_name_t const& name, log::log_message_t const& msg)
{
	printf ("%s: %s\n", name.c_str (), msg.c_str ());
}
}

struct file_info
{
	FILE*	m_file;
	
	file_info (path const& p) {
#if STIR_WINDOWS
		m_file = _wfopen (p.c_str(), L"w");
#else
		m_file = fopen (p.c_str(), "w");
#endif
	}
	
	~file_info () {
		if (m_file)
			fclose (m_file);
	}
	
	void write (log::log_name_t const& name, log::log_message_t const& msg) {
		if (m_file) {
			fprintf (m_file, STIR_SPFF ": " STIR_SPFF "\n", name.c_str(), msg.c_str());
			fflush (m_file);
		}
	}
};

log::write_function
write_to_file (path const& p)
{
//	file_info* fi = new file_info (p);
	boost::shared_ptr<file_info> fi (new file_info (p));
	return log::write_function (
		boost::bind (&file_info::write, fi, _1, _2)
	);
}

#if STIR_WINDOWS
#include <stir/win_include.hpp>
void write_to_debug_output_impl (log::log_name_t const& name, log::log_message_t const& msg)
{
	OutputDebugStringA (name.c_str());
	OutputDebugStringW (L": ");
	OutputDebugStringW (msg.c_str());
	OutputDebugStringW (L"\n");
}

log::write_function
write_to_debug_output ()
{
	return &write_to_debug_output_impl;
}
#endif

log::write_function
STIR_ATTR_DLL write_to_std_output ()
{
	return &write_to_std_output_impl;
}

}
