#ifndef STIR_LOGS_HPP_
#define STIR_LOGS_HPP_

#include <stir/platform.hpp>
#include "log.hpp"
#include "path.hpp"

namespace stir
{

STIR_DECLARE_LOG_INTERNAL (win);
STIR_DECLARE_LOG_INTERNAL (dx);

STIR_DECLARE_LOG_INTERNAL (gl);

STIR_DECLARE_LOG_INTERNAL (audio);
STIR_DECLARE_LOG_INTERNAL (gfx);
STIR_DECLARE_LOG_INTERNAL (audio);
STIR_DECLARE_LOG_INTERNAL (font);
STIR_DECLARE_LOG_INTERNAL (debug);
STIR_DECLARE_LOG_INTERNAL (game);
STIR_DECLARE_LOG_INTERNAL (resource);
STIR_DECLARE_LOG_INTERNAL (fs);
STIR_DECLARE_LOG_INTERNAL (script);

STIR_DECLARE_LOG_INTERNAL (warning);
STIR_DECLARE_LOG_INTERNAL (error);
STIR_DECLARE_LOG_INTERNAL (exception);
		
log::write_function STIR_ATTR_DLL write_to_file (path const& p);
log::write_function STIR_ATTR_DLL write_to_debug_output ();
log::write_function STIR_ATTR_DLL write_to_std_output ();
	
}

#endif // STIR_LOGS_HPP_
