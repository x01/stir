#pragma message (">> in stir_pch.hpp")

#include <stir/platform.hpp>

#if STIR_WINDOWS
// WINDOWS (needed, as this is used by stir_types right now)
#include <stir/windows/win_include.hpp>

// directx
#include <d3d9.h>
#include <d3dx9.h>

#elif (STIR_IPHONE || STIR_MACOSX)

//#include <gl/gl.h>

#endif

// C++
#include <iosfwd>
#include <algorithm>
#include <vector>
#include <map>
#include <set>
#include <list>
#include <string>

// C
#include <cassert>
#include <cstring>
#include <ctime>
#include <cstdio>

// boost
#include <boost/bind.hpp>
#include <boost/function.hpp>

