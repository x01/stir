#ifndef STIR_UTF_HPP_
#define STIR_UTF_HPP_

#include <utf8/utf8.h>

namespace stir
{
	typedef utf8::unchecked::iterator<char*> utf8_iterator;
	typedef utf8::unchecked::iterator<char const*> utf8_const_iterator;
}


#endif
