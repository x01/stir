#ifndef stir_stir_vecmath_hpp
#define stir_stir_vecmath_hpp

#include <stir/platform.hpp>

#include <vectormath/scalar/vectormath_aos.h>

namespace stir
{
	using namespace Vectormath::Aos;
}

#endif
