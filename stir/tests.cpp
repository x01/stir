#include "tests.hpp"

#include <stir/pool.hpp>
#include <stir/single_link.hpp>
#include <stir/hash_map.hpp>

#include <gtest/gtest.h>

namespace stir
{

int run_tests(int& argc, char** argv)
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
	
}

TEST(single_link, AppendUnlinkWorks)
{
	stir::single_link a, b, c;
	
	stir::prepend(&a, &b);
	
	ASSERT_EQ(1, stir::count_size (&a));
	ASSERT_EQ(2, stir::count_size (&b));
	ASSERT_EQ(1, stir::count_size (&c));
	
	stir::prepend(&b, &c);
	ASSERT_EQ(1, stir::count_size (&a));
	ASSERT_EQ(2, stir::count_size (&b));
	ASSERT_EQ(3, stir::count_size (&c));
	
	stir::unlink_first(&b);
	ASSERT_EQ(1, stir::count_size (&a));
	ASSERT_EQ(1, stir::count_size (&b));
	ASSERT_EQ(2, stir::count_size (&c));
}



//
// For now, header-only classes have tests right here
//
TEST(double_chain, CanHandleTwoChains)
{
	struct X : stir::double_chain<X> { int v; };
	
	stir::double_chain<X> a, b;

	X v1, v2, v3, v4;
	
	ASSERT_TRUE(a.is_solitary());
	ASSERT_TRUE(b.is_solitary());
	ASSERT_EQ(0, a.count_size ());
	ASSERT_EQ(0, v1.count_size ());
	
	
	a.tail_insert(v1);
	ASSERT_EQ(1, a.count_size ());
	
	v1.unlink_this();
	b.tail_insert(v1);
	ASSERT_TRUE(a.is_solitary());
	ASSERT_EQ(1, b.count_size ());
}

TEST(double_link, AppendUnlinkWorks)
{
	stir::double_link a, b, c;
	
	stir::append(&a, &b);
	
	ASSERT_EQ(2, stir::count_size (&a));
	ASSERT_EQ(2, stir::count_size (&b));
	ASSERT_EQ(1, stir::count_size (&c));
	
	stir::append(&a, &c);
	ASSERT_EQ(3, stir::count_size (&a));
	ASSERT_EQ(3, stir::count_size (&b));
	ASSERT_EQ(3, stir::count_size (&c));

	stir::unlink_first(&a);
	ASSERT_EQ(1, stir::count_size (&a));
	ASSERT_EQ(2, stir::count_size (&b));
	ASSERT_EQ(2, stir::count_size (&c));
}

TEST(pool, CanAddRemove)
{
	stir::pool<int> p (10);
	
	int* o1 = p.construct(1);
	ASSERT_EQ(1, *o1);
	ASSERT_EQ(1, p.size());
	
	int* o2 = p.construct(2);
	ASSERT_EQ(2, *o2);
	ASSERT_EQ(2, p.size());

	p.destroy(o1);

	ASSERT_EQ(1, p.size());
	
	int* o11 = p.construct(11);
	ASSERT_EQ(11, *o11);
	ASSERT_EQ(2, p.size());
	ASSERT_EQ(2, *o2);
	ASSERT_EQ(o1, o11);

	p.destroy(o2);
	ASSERT_EQ(1, p.size());

	int* o21 = p.construct(21);
	ASSERT_EQ(21, *o21);
	ASSERT_EQ(2, p.size());
	ASSERT_EQ(11, *o11);
	ASSERT_EQ(o2, o21);
}

TEST(hash_map, BasicTest)
{
	static const int N = 8;
	struct ident { typedef int value_type; int operator()(int a) const { return a; } };
	
	struct _ {
		static bool eq (stir::hash_map<int, int, ident> const& h, int const (&v)[N]) {
			int i = 0;
			for (auto it = h.lean_begin (); it != h.lean_end(); ++it) {
				if (v[i] == -1) {
					if (!it->empty(h.empty_key()))
						return false;
				} else {
					if (it->empty(h.empty_key()))
						return false;
					if (it->v() != v[i])
						return false;
				}
				++i;
			}
			
			return true;
		}
		
		static void p(stir::hash_map<int, int, ident> const& h) {
			for (auto&& q : h) {
				if (q.empty(h.empty_key()))
					printf("x ");
				else
					printf("%d:%d ", q.key, q.v());
			}
			printf("\n");
		}
	};
	
	stir::hash_map<int,int, ident> h1 (-1, N);
	
	static const int x = -1;
	
	h1.insert (1, 100);
	h1.insert (2, 101);
	ASSERT_TRUE(_::eq(h1, {x, 100, 101, x, x, x, x, x}));
	h1.insert (4, 102);
	h1.insert (5, 300); // 5
	ASSERT_TRUE(_::eq(h1, {x, 100, 101, x, 102, 300, x, x}));
	h1.insert (N+5, 301); // 6
	ASSERT_TRUE(_::eq(h1, {x, 100, 101, x, 102, 300, 301, x}));
	h1.insert (6, 400); // 7
	ASSERT_TRUE(_::eq(h1, {x, 100, 101, x, 102, 300, 301, 400}));

	// remove last
	h1.remove (6); // -7
	ASSERT_TRUE(_::eq(h1, {x, 100, 101, x, 102, 300, 301, x}));
	
	// overflow
	h1.insert (7, 401); // 7
	ASSERT_TRUE(_::eq(h1, {x, 100, 101, x, 102, 300, 301, 401}));
	h1.insert (N+7, 402); // 0
	ASSERT_TRUE(_::eq(h1, {402, 100, 101, x, 102, 300, 301, 401}));
	h1.insert (8, 403); // 3
	ASSERT_TRUE(_::eq(h1, {402, 100, 101, 403, 102, 300, 301, 401}));
	_::p(h1);
	h1.remove (7); // r(401)
	_::p(h1);
	ASSERT_TRUE(_::eq(h1, {403, 100, 101, x, 102, 300, 301, 402}));

	h1.insert (6, 501);
	ASSERT_TRUE(_::eq(h1, {403, 100, 101, 501, 102, 300, 301, 402}));
	_::p(h1);
}

