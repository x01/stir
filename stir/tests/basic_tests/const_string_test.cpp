#include <stir/const_string.hpp>
#include <cassert>
#include <cstdlib>

int main ()
{
	using namespace stir;
	using namespace std;

	const_string<char> z;
	const_string<char> z1 ("");
	
	assert (strcmp (z.c_str (), "") == 0);
	assert (strcmp (z1.c_str (), "") == 0);

	const_string<char> a ("a");
	printf ("%s\n", a.c_str ());

	const_string<char> ab ("ab");
	printf ("%s\n", ab.c_str ());
	
	const_string<char> a6 ("aaaabb");
	printf ("%s\n", a6.c_str ());
	
	const_string<char> a7 ("aaaabbb");
//	printf ("%s\n", a7.c_str ());
	
	return 0;
}