#include <stir/basic_cstring.hpp>
#include <cassert>
#include <cstdlib>

int main ()
{
	using namespace stir;
	using namespace std;

	castring az;
	castring az1 ("");	
	assert (strcmp (az.c_str (), "") == 0);
	assert (strcmp (az1.c_str (), "") == 0);

	cwstring wz;
	cwstring wz1 (L"");	
	assert (wcscmp (wz.c_str (), L"") == 0);
	assert (wcscmp (wz1.c_str (), L"") == 0);

	castring aa ("a");
	printf ("%s\n", aa.c_str ());
	cwstring wa (L"a");
	wprintf (L"%s\n", wa.c_str ());

	return 0;
}