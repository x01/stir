#include <stir/path.hpp>
#include <stir/filesystem.hpp>
#include <cassert>
#include <cstdlib>

#include <boost/detail/lightweight_test.hpp>


int main ()
{
	using namespace stir;
	using namespace std;

	// Construction / destruction
	
	{
		platform_path p (L"a/b/ū");
		
		BOOST_TEST (filesystem::create_directories (p) == true);
		BOOST_TEST (filesystem::is_directory (p) == true);
		BOOST_TEST (filesystem::is_directory (p.filename()) == true);
		BOOST_TEST (filesystem::is_directory (p.filename().filename()) == true);
		BOOST_TEST (filesystem::is_directory (p.filename().filename().filename()) == false);		
	}

	boost::report_errors ();

	return 0;
}
