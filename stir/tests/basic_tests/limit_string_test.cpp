#include <stir/limit_string.hpp>
#include <cassert>
#include <cstdlib>

int main ()
{
	using namespace stir;
	using namespace std;

	limit_string<0, char> sz1;
	limit_string<0, char> sz2 ("");

	assert (strcmp (sz1.c_str (), "") == 0);
	assert (strcmp (sz2.c_str (), "") == 0);

	limit_string<0, wchar_t> wz;
	limit_string<0, wchar_t> wz1 (L"");	
	assert (wcscmp (wz.c_str (), L"") == 0);
	assert (wcscmp (wz1.c_str (), L"") == 0);

	limit_string<1, char> aa ("a");
	limit_string<2, char> ab ("aa");
	limit_string<2, char> ac ("aaa");
	assert ( ab == ac);
	printf ("%s\n", aa.c_str ());
	printf ("%s\n", ab.c_str ());
	printf ("%s\n", ac.c_str ());

	printf ("%d\n", ab < ac);

	return 0;
}
