#include <stir/path.hpp>
#include <cassert>
#include <cstdlib>

#include <boost/detail/lightweight_test.hpp>

void pp (stir::platform_path const& p)
{
	printf ("'%s': filename='%s', parent='%s', root='%s'\n", p.c_str(), p.filename().c_str(), p.parent().c_str(), p.root ().c_str());
}

int main ()
{
	using namespace stir;
	using namespace std;

	// Construction / destruction
	
	{
		path<utf8_traits<> > p;
	
		BOOST_TEST (p == "");
		BOOST_TEST (p.filename ().empty ());
	}

	{
		path<utf8_traits<> > p ("dir");
		BOOST_TEST (p == "dir");
		BOOST_TEST (p.filename () == "dir");

		p = "Žąsis";
		printf ("str: %s\n", p.string ().c_str ());
		BOOST_TEST (p.size () == 7);
		
		p = L"Žąsis";
		printf ("str: %s\n", p.string ().c_str ());
		BOOST_TEST (p.size () == 7);
		BOOST_TEST (p.filename () == "Žąsis");
		BOOST_TEST (p == "Žąsis");

		p = p / "Kiaušinis";
		printf ("str: %s\n", p.string ().c_str () );
		BOOST_TEST (p.parent () == "Žąsis");
		BOOST_TEST (p == "Žąsis/Kiaušinis");
		
		p = "";
		BOOST_TEST (p.empty ());
	}

	{
		platform_path p0 (""), p1 ("/"), p2 ("/moo"), p3 ("moo/"), p4 ("/moo/"), p5 ("/moo/bar"), p6 ("moo/bar");

		pp (p5);
		pp (p6);
		pp (p4);

		pp (p0);
		pp (p1);
		pp (p2);
		pp (p3);

		BOOST_TEST (p0.filename () == "");
		BOOST_TEST (p0.parent () == "");

		BOOST_TEST (p1.filename () == "");
		BOOST_TEST (p1.parent () == "/");
		BOOST_TEST (p1.root () == "/");

		BOOST_TEST (p2.filename () == "moo");
		BOOST_TEST (p2.parent () == "/");
		BOOST_TEST (p2.root () == "/");

		BOOST_TEST (p3.filename () == "");
		BOOST_TEST (p3.parent () == "moo");
		BOOST_TEST (p3.root () == "");

		BOOST_TEST (p4.filename () == "");
		BOOST_TEST (p4.parent () == "/moo");
		BOOST_TEST (p4.root () == "/");

		BOOST_TEST (p5.filename () == "bar");
		BOOST_TEST (p5.parent () == "/moo");
		BOOST_TEST (p5.root () == "/");

		BOOST_TEST (p6.filename () == "bar");
		BOOST_TEST (p6.parent () == "moo");
		BOOST_TEST (p6.root () == "");

	}

	boost::report_errors ();

	return 0;
}
