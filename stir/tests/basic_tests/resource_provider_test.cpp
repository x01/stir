#include <stir/resource.hpp>
#include <iostream>
#include <fstream>
#include <stir/resource_handle.hpp>

struct stream_loader : public stir::loader<int, std::iostream*>
{
	std::iostream* load( int const& id, int profile ) {
		char buf[256];
		snprintf (buf, 256, "stream-%d-%d.stream", id, profile);
		return new std::fstream (buf);
	}

	void unload (std::iostream* p, int profile)
	{
		delete p;
	}
};

struct stream__int_ptr : stir::resource_handle<stir::pair_pointer_policy<int, std::iostream*> >
{
	stream__int_ptr( holder_type holder_ ) : stir::resource_handle<stir::pair_pointer_policy<int, std::iostream*> >( holder_ ) {}
};

/*struct stream__int_iterator : stir::resource_handle<stir::iterator_policy<int, std::iostream*> >
{
	stream__int_iterator( holder_type* holder_ ) : stir::resource_handle<stir::iterator_policy<int, std::iostream*> >( holder_ ) {}
};*/

/*struct stream__int_refcount : stir::resource_handle<stir::ref_count_policy<int, std::iostream*> >
{
	stream__int_refcount () {}
	stream__int_refcount( holder_type& holder_ ) : stir::resource_handle<stir::ref_count_policy<int, std::iostream*> >( holder_ ) {}
};*/

namespace stir {
template<>
struct unload_resource<std::iostream*>
{
	void operator () (std::iostream* s) const { delete s; }
};
}

typedef stir::resource_registry<stream__int_ptr>	stream_registry__int_ptr;
//typedef stir::resource_registry<stream__int_iterator, stir::iterator_policy<int, std::iostream*> >	stream_registry__int_iterator;

//typedef stir::resource_registry<stream__int_refcount> stream_registry__int_refcount;

/*// resource non-pointer, id is string
struct text_loader : public stir::loader<stir::string, std::string>
{
	std::string load( std::string const& id, int profile ) {
		std::stringstream s;
		s << "string: " << to_ascii (id);
		return s.str ();
	}
};

struct text__ptr : public stir::resource_handle<stir::pair_pointer_policy <stir::string, std::string> >
{
	text__ptr (holder_type* h) : stir::resource_handle<stir::pair_pointer_policy <stir::string, std::string> > (h) {}
};

struct text__it : public stir::resource_handle<stir::iterator_policy <stir::string, std::string> >
{
	text__it (holder_type* h) : stir::resource_handle<stir::iterator_policy <stir::string, std::string> > (h) {}
};

typedef resource_registry<text__ptr, stir::cache_policy_pair<stir::string, std::iostream*> >	text_registry__ptr;
typedef resource_registry<text__it, stir::iterator_policy<stir::string, std::iostream*> >	text_registry__iterator;
*/

int main ()
{
	using namespace stir;
	
	{
		stream_loader ldr;
		
		
		/*stream_registry__int_refcount refr (ldr);
		
		stream__int_refcount sr1_1 = refr.get (1, 0);
		stream__int_refcount sr1_2 = refr.get (1, 0);
		stream__int_refcount sr2 = refr.get (2, 0);
		*/

		stream_registry__int_ptr reg_ip (ldr);
//		stream_registry__int_iterator reg_ii (ldr);
		
		stream__int_ptr sip0 = reg_ip.get (0, 0);
		stream__int_ptr sip1 = reg_ip.get (0, 1);
		stream__int_ptr sip2 = reg_ip.get (1, 0);
		
		(*sip0) << "Hello" << std::endl;
		(*sip1) << "world!" << std::endl;
		(*sip2) << "Hello world again!" << std::endl;
		
/*		stream_int_iterator sii0 = reg_ii.get (10, 0);
		stream_int_iterator sii1 = reg_ii.get (10, 1);
		stream_int_iterator sii2 = reg_ii.get (11, 0);
		
		(*sii0) << "Hello" << std::endl;
		(*sii1) << "world!" << std::endl;
		(*sii2) << "Hello world again!" << std::endl;*/
	}
	
/*	{
		text_loader ldr;
	
		text_registry__ptr rp (ldr);
		text_registry__iterator ri (ldr);
		
		text__ptr tp0 = rp.get ("pointer1", 0);
		text__ptr tp1 = rp.get ("pointer2", 0);

		text__iterator ti0 = rp.get ("iterator1", 0);
		text__iterator ti1 = rp.get ("iterator2", 0);
	}*/

	return 0;	
}
