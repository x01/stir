#include <stir/platform.hpp>
#include <stir/wildcard.hpp>

#include <cassert>
#include <cstdlib>

#include <boost/detail/lightweight_test.hpp>

int main ()
{
	using namespace stir;
	using namespace std;
	
	char const* p1 = "a*";
	char const* p2 = "a*b";
	char const* p3 = "?a*";
	char const* p4 = "*a?";
	

	BOOST_TEST (wildcard::match("a", p1) );
	BOOST_TEST (wildcard::match("a1", p1) );
	BOOST_TEST (wildcard::match("aa", p1) );
	BOOST_TEST (wildcard::match("aqwert", p1) );
	
	BOOST_TEST (wildcard::match("qa", p1) == false);


	BOOST_TEST (wildcard::match("ab", p2) );
	BOOST_TEST (wildcard::match("aab", p2) );
	BOOST_TEST (wildcard::match("aabb", p2) );
	BOOST_TEST (wildcard::match("aqwbertb", p2) );

	boost::report_errors ();

	return 0;
}
