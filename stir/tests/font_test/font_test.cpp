#include <stir/commonapp/common_app.hpp>
#include <stir/chain.hpp>
#include <stir/windows/dx9/texture.hpp>
#include <stir/singleton.hpp>
#include <stir/commonapp/common_resource.hpp>
#include <stir/gui/font_data.hpp>
#include <stir/gui/font.hpp>
#include <string>
#include <cstdio>

using namespace stir;

// mimicks auto_ptr
template<class T>
struct blit_ptr
{
	explicit blit_ptr (size_t size) : ptr_ (size == 0 ? 0 : (T*)new uint8_t [size]) {}

	blit_ptr (void* p = NULL) : ptr_ ((T*)p) {}
	blit_ptr (blit_ptr& a) : ptr_ (a.ptr_) { a.ptr_ = 0; }
	blit_ptr& operator= (blit_ptr& p) { ptr_ = p.ptr_; return *this; }
	~blit_ptr () { delete [] (uint8_t*)ptr_; }
	
	T* get () { return ptr_; }
	void reset () { delete [] (uint8_t*)ptr_; ptr_ = 0; }
	
	void swap (blit_ptr& p) { swap (ptr_, p.ptr_); }

private:
	T*	ptr_;
};

template<class T>
blit_ptr<T> virtual_blit (platform_path const& p)
{
	io::virtual_file_input in (p);
	if (in.is_good ())
	{
		blit_ptr<T> ptr ((size_t)in.length());
		in.read (ptr.get (), (size_t)in.length(), NULL);
		return ptr;
	}
	
	return blit_ptr<T>();
} 

class font_test_module : public stir::common_module
{
	blit_ptr<gui::font_data> fdat_;
	
public:
	font_test_module (directx9::device& dev)
		:	common_module (dev)
	{
		fdat_ = virtual_blit<gui::font_data> (native_path ("arial.pulmotor"));
	}
	
	~font_test_module ()
	{
	}
	
	virtual void on_render() {
		texture my = common::get<texture> (L"ball");
	
		
	}
};

namespace stir { namespace common {
	stir::common_resources*	g_common_resources;
}}

int main ()
{
	stir::common_app app (L"texture_get_test");
	app.create_window (GetModuleHandle(0), 400, 300, false, 24, 8);
	
	stir::common::g_common_resources = new stir::common_resources (app.get_device (), native_path(""));

	{
		font_test_module mod (app.get_device());
		app.run (mod);
	}

	delete stir::common::g_common_resources;
	
	app.clean();

	
	return 0;
}
