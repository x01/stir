#include <stir/chain.hpp>
#include <stir/dx9/texture_resource.hpp>
#include <stir/commonapp/common_app.hpp>
#include <stir/commonapp/common_resource.hpp>
#include <iostream>
#include <string>
#include <cstdio>

struct maumas
{
	maumas() {
		printf ("maumas::maumas()");
	}
	
	~maumas() {
		printf ("maumas::~maumas()");
	}	
};

struct tinfo : D3DSURFACE_DESC
{

	/*explicit texture_info (texture tex, int level = 0)
	{
		texture_level_get tlg (level, tex.get());
		tlg.surface()->GetDesc( this );
	}*/

	explicit tinfo (IDirect3DTexture9& tex, int level = 0)
	{
		HRESULT hr = S_OK;
		IDirect3DSurface9* surf = 0;
		
		maumas mm;
		
		//std::string moo ("pedro");
		hr = tex.GetSurfaceLevel (level, &surf);
		//printf ("name=%s\n", moo.c_str());
		surf->GetDesc (this);

//		stir::texture_level_get tlg (level, tex);
//		tlg.surface()->GetDesc( this );
	}
};


class test_module : public stir::common_module
{
	stir::common_resources*	m_resources;
	
public:
	test_module (stir::device_context& dc)
		:	stir::common_module (dc)
	{
		m_resources = new stir::common_resources (dc, L"");
	}
	
	virtual void on_render() {
		stir::texture tex = m_resources->texture_mgr ().get (L"moo");
//		resource::manager<texture> ();
		
		tinfo ti (*tex, 0);
		
		printf ("w: %d, ", ti.Width);
	}
};

int main ()
{
	stir::common_app app (L"texture_get_test");
	app.create_window (GetModuleHandle(0), 400, 300, false, 24, 8 );
	
	{
		test_module mod (app.get_device_context());
		app.run (mod);
	}
	
	app.clean();
	
	return 0;
}
