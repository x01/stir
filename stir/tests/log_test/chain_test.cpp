#include <stir/chain.hpp>
#include <iostream>
#include <string>

struct moo : public stir::utility::double_chain<moo>
{
	moo (char const* a) : m_name (a) {}
	char const*	m_name;
};

void display_link (moo& start)
{
	moo* current = &start;
	while (1)
	{
		std::cout << current->m_name << "->";
		current = current->m_next;
		if (current == &start)
			break;
	}
	
	std::cout << std::endl;
}

int main()
{
	moo	a("a");
	moo	b("b");
	moo	c("c");
	moo	d("d");
	
	display_link (a);

	a.insert_after (b);
	display_link (a);

	a.insert_after (c);
	display_link (a);

	a.insert_after (d);
	display_link (a);
	
	b.unlink_this ();
	display_link (a);
	d.unlink_this ();
	display_link (b);
	c.unlink_this ();
	display_link (d);
	
	return 0;
}