#include <stir/log.hpp>
#include <iostream>
#include <string>

STIR_DECLARE_LOG (debug);
STIR_DEFINE_LOG (video,L"video");
STIR_DEFINE_LOG (debug,L"debug");
STIR_DEFINE_LOG (dbg,L"dbg");


int main()
{
	STIR_LOG (debug).printf (L"10");
	
	stir::log::log_iterator lb = stir::log::log_begin();
	stir::log::log_iterator le = stir::log::log_end();
	
	for (stir::log::log_iterator it = lb; it != le; ++it)
	{
		std::cout << it->get_name() << std::endl;
	}

	return 0;
}