#include <stir/log.hpp>
#include <iostream>

using namespace std;

STIR_DECLARE_LOG (dbg);
STIR_DEFINE_LOG (dbg, L"debug");

STIR_DECLARE_LOG (video1);
STIR_DEFINE_LOG (video1, L"video1");

STIR_DECLARE_LOG (audio1);
STIR_DEFINE_LOG (audio1, L"audio1");

void std_writer ( stir::string const& name, stir::string const& contents )
{
	wcout << name << ": " << contents << endl; 
}

void display_link (stir::log::log& start)
{
	stir::log::log* current = &start;
	while (1)
	{
		std::cout << current->get_name();
		current = current->get_next();
		if (current == &start)
			break;
		std::cout << " -> ";
	}
	
	std::cout << std::endl;
}


int main()
{
	STIR_LOG (dbg).set_write_function (std_writer);
	STIR_LOG (audio1).set_write_function (std_writer);
	STIR_LOG (video1).set_write_function (std_writer);



	STIR_LOG (dbg).printf ( L"Hello log (%d)\n", 100 );
	STIR_LOG (dbg).printf ( L"Hello log (%d)\n", 200 );
	STIR_LOG (dbg).printf ( L"Hello log (%d)\n", 200 );
	STIR_LOG (dbg).printf ( L"Hello log (%d)\n", 200 );

	STIR_LOG (audio1).printf ( L"Hello log (%d)\n", 200 );
	STIR_LOG (video1).printf ( L"Hello log (%d)\n", 200 );

	if (STIR_LOG_ENABLED (video1))
		display_link (STIR_LOG_ACCESS (video1));
}