#include "texture.hpp"
#include "resmgr.hpp"

namespace stir {
	index_manager<texture, loader<texture> > r_texture;
}

#if 0
void test_index_cache ()
{
	using namespace stir;
	rindex_t i0 = r_texture.load ("simple");
	rindex_t i1 = r_texture.load_cached ("simple");
	rindex_t i2 = r_texture.load_cached ("simple");
}
#endif
