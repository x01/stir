#ifndef STIR_TEXTURE_HPP_
#define STIR_TEXTURE_HPP_

#include "platform.hpp"
#include "resfwd.hpp"

#if STIR_WINDOWS

#include "windows/dx9/dx9_texture.hpp"

namespace stir {

typedef dx9::texture texture;
typedef dx9::texture_info texture_info;

}

#elif STIR_IPHONE

#include "gles/gles_texture.hpp"

namespace stir {

typedef gles::gles_texture* texture;
typedef gles::texture_info texture_info;

}

#else

namespace stir {
	
	struct dummy_texture {};
	typedef dummy_texture* texture;
	typedef dummy_texture texture_info;
	
}

#endif

namespace stir {
	extern index_manager<texture, loader<texture> > r_texture;
	typedef index_wrap<texture> i_texture;
}

#endif // STIR_TEXTURE_HPP_
