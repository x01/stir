#ifndef STIR_TOUCH_INPUT_HPP_
#define STIR_TOUCH_INPUT_HPP_

#include <stir/flat_types.hpp>

namespace stir
{

enum touch_phase {
	k_touch_begin,
	k_touch_moved,
	k_touch_stationary,
	k_touch_ended,
	k_touch_cancelled
};
	
struct touch_event
{
	bool is_active() const { return phase == k_touch_begin || phase == k_touch_moved || phase == k_touch_stationary; }
	bool is_passive() const { return phase == k_touch_ended || phase == k_touch_cancelled; }
	
	stir::vector2f pt, prevPt;
	stir::touch_phase phase;
};
	
	
}

#endif // STIR_TOUCH_INPUT_HPP_
