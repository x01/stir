#ifndef STIR_WILDCARD_HPP_
#define STIR_WILDCARD_HPP_

#include <stir/platform.hpp>
#include <string.h>

namespace stir { namespace wildcard {

namespace detail {
	inline size_t string_length (char const* str) {
		return ::strlen (str);
	}
	#if STIR_WINDOWS
	inline size_t string_length (wchar_t const* str) {
		return ::wcslen (str);
	}
	#endif
}

template<class ItT>
bool match (ItT begin, ItT end, ItT patternBegin, ItT patternEnd)
{
	bool star = false;
	ItT p = patternBegin;
	ItT s = begin;
	while (s != end)
	{
		size_t pleft = patternEnd - p;
		size_t strleft = end - s;
		
		if (star && strleft > pleft) {
			++s;
			continue;
		} else
			star = false;
	
		switch (*p)
		{
			case '?':
				++p;
				++s;
				break;
				
			case '*':
				star = true;
				do { ++p; } while (*p == '*' && p != patternEnd);
				break;
				
			default:
				if (*p++ != *s++)
					return false;
				break;
		}
	}
	
	return true;
}

template<class ItT>
bool match (ItT str, ItT pattern)
{
	return match (
		str, str + detail::string_length (str), 
		pattern, pattern + detail::string_length (pattern)
	);
}


}}

#endif // STIR_WILDCARD_HPP_
