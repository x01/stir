#ifndef STIR_COM_PTR
#define STIR_COM_PTR

#include <cassert>

#define STIR_COM_PTR_SAFE_COUNT 0

namespace stir {

template <class T>
class com_ptr
{
public:

#if STIR_COM_PTR_SAFE_COUNT
	template <class T>
	class no_addref_release_on_ptr : public T
	{
		private:
			STDMETHOD_(ULONG, AddRef)()=0;
			STDMETHOD_(ULONG, Release)()=0;
	};
#endif

public:
	com_ptr() : p( 0 )
	{}

	explicit com_ptr( T* lp ) : p( lp )
	{
		if (p != 0)
			p->AddRef();
	}

	com_ptr( com_ptr<T> const& lp )
	:	p( lp.p )
	{
		if (p != 0)
			p->AddRef();
	}

	com_ptr<T>& operator=(T* lp)
	{
		if( lp != 0 )
			lp->AddRef();

		if( p )
			p->Release();

		p = lp;

		return *this;
	}

	com_ptr<T>& operator=( com_ptr<T> const& lp )
	{
		if( lp )
			lp.p->AddRef();

		if( p )
			p->Release();

		p = lp.p;

		return *this;
	}

	typedef T _PtrClass;

	~com_ptr()
	{
		if (p)
			p->Release();
	}

	operator bool() const
	{	return p != 0; }

	T& operator*() const
	{
		assert(p!=NULL);
		return *p;
	}

	//The assert on operator& usually indicates a bug.  If this is really
	//what is needed, however, take the address of the p member explicitly.
	T** operator&()
	{
		assert(p==NULL);
		return &p;
	}

#if STIR_COM_PTR_SAFE_COUNT
	no_addref_release_on_ptr<T>* operator->() const
	{
		assert(p!=NULL);
		return (no_addref_release_on_ptr<T>*)p;
	}
#else
	T* operator->() const
	{
		assert(p!=NULL);
		return p;
	}
#endif

	bool operator!() const
	{	return (p == NULL); }

	bool operator<(T* pT) const
	{	return p < pT; }

	bool operator==(T* pT) const
	{	return p == pT; }

	// Release the interface and set to NULL
	int release()
	{
		T* pTemp = p;
		if (pTemp)
		{
			p = NULL;
			return pTemp->Release();
		}
		return 0;
	}

	// Attach to an existing interface (does not AddRef)
	void attach(T* p2)
	{
		if (p)
			p->Release();
		p = p2;
	}

	struct attach_proxy
	{
		attach_proxy( T*& p ) : ptr_ref(p) {}
		void operator=( T* p ) const	{	ptr_ref = p; }
		T*&	ptr_ref;
	};

	attach_proxy attach()
	{
		assert (p == NULL);
		return attach_proxy( p );
	}

	// Detach the interface (does not Release)
	T* detach()
	{
		T* pt = p;
		p = NULL;
		return pt;
	}

	T* get() const
	{	return p; }

private:
	T* p;
};

}

namespace boost
{

template<class T>
inline T* get_pointer( stir::com_ptr<T> const& p )
{
	return p.get();
}

}

#endif
