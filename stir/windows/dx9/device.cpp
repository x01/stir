#pragma message (">> in device.hpp")
#include "device.hpp"
#include <stir/language_util.hpp>
#include <stir/basic_util.hpp>
#include "dx9_utility.hpp"
#include <stir/foreach.hpp>

#define USE_PERF_HUD 0

namespace stir { namespace directx9 {

lost_reset_interface::~lost_reset_interface()
{
}

bool aux::has_stencil( D3DFORMAT fmt )
{
	if( fmt == D3DFMT_D15S1 || fmt == D3DFMT_D24S8 || fmt == D3DFMT_D24X4S4 || fmt == D3DFMT_D24FS8 )
		return true;

	return false;
}

char const* aux::get_fmt_string( D3DFORMAT fmt )
{
	switch( fmt )
	{
		default:
		case D3DFMT_UNKNOWN: return "D3DFMT_UNKNOWN";

		case D3DFMT_R8G8B8: return "D3DFMT_R8G8B8";
		case D3DFMT_A8R8G8B8: return "D3DFMT_A8R8G8B8";
		case D3DFMT_X8R8G8B8: return "D3DFMT_X8R8G8B8";
		case D3DFMT_R5G6B5: return "D3DFMT_R5G6B5";
		case D3DFMT_X1R5G5B5: return "D3DFMT_X1R5G5B5";
		case D3DFMT_A1R5G5B5: return "D3DFMT_A1R5G5B5";
		case D3DFMT_A4R4G4B4: return "D3DFMT_A4R4G4B4";
		case D3DFMT_R3G3B2: return "D3DFMT_R3G3B2";
		case D3DFMT_A8: return "D3DFMT_A8";
		case D3DFMT_A8R3G3B2: return "D3DFMT_A8R3G3B2";
		case D3DFMT_X4R4G4B4: return "D3DFMT_X4R4G4B4";
		case D3DFMT_A2B10G10R10: return "D3DFMT_A2B10G10R10";
		case D3DFMT_A8B8G8R8: return "D3DFMT_A8B8G8R8";
		case D3DFMT_X8B8G8R8: return "D3DFMT_X8B8G8R8";
		case D3DFMT_G16R16: return "D3DFMT_G16R16";
		case D3DFMT_A2R10G10B10: return "D3DFMT_A2R10G10B10";
		case D3DFMT_A16B16G16R16: return "D3DFMT_A16B16G16R16";

		case D3DFMT_A8P8: return "D3DFMT_A8P8";
		case D3DFMT_P8: return "D3DFMT_P8";

		case D3DFMT_L8: return "D3DFMT_L8";
		case D3DFMT_A8L8: return "D3DFMT_A8L8";
		case D3DFMT_A4L4: return "D3DFMT_A4L4";

		case D3DFMT_V8U8: return "D3DFMT_V8U8";
		case D3DFMT_L6V5U5: return "D3DFMT_L6V5U5";
		case D3DFMT_X8L8V8U8: return "D3DFMT_X8L8V8U8";
		case D3DFMT_Q8W8V8U8: return "D3DFMT_Q8W8V8U8";
		case D3DFMT_V16U16: return "D3DFMT_V16U16";
		case D3DFMT_A2W10V10U10: return "D3DFMT_A2W10V10U10";

		case D3DFMT_UYVY: return "D3DFMT_UYVY";
		case D3DFMT_R8G8_B8G8: return "D3DFMT_R8G8_B8G8";
		case D3DFMT_YUY2: return "D3DFMT_YUY2";
		case D3DFMT_G8R8_G8B8: return "D3DFMT_G8R8_G8B8";
		case D3DFMT_DXT1: return "D3DFMT_DXT1";
		case D3DFMT_DXT2: return "D3DFMT_DXT2";
		case D3DFMT_DXT3: return "D3DFMT_DXT3";
		case D3DFMT_DXT4: return "D3DFMT_DXT4";
		case D3DFMT_DXT5: return "D3DFMT_DXT5";

		case D3DFMT_D16_LOCKABLE: return "D3DFMT_D16_LOCKABLE";
		case D3DFMT_D32: return "D3DFMT_D32";
		case D3DFMT_D15S1: return "D3DFMT_D15S1";
		case D3DFMT_D24S8: return "D3DFMT_D24S8";
		case D3DFMT_D24X8: return "D3DFMT_D24X8";
		case D3DFMT_D24X4S4: return "D3DFMT_D24X4S4";
		case D3DFMT_D16: return "D3DFMT_D16";

		case D3DFMT_D32F_LOCKABLE: return "D3DFMT_D32F_LOCKABLE";
		case D3DFMT_D24FS8: return "D3DFMT_D24FS8";


		case D3DFMT_L16: return "D3DFMT_L16";

		case D3DFMT_VERTEXDATA: return "D3DFMT_VERTEXDATA";
		case D3DFMT_INDEX16: return "D3DFMT_INDEX16";
		case D3DFMT_INDEX32: return "D3DFMT_INDEX32";

		case D3DFMT_Q16W16V16U16: return "D3DFMT_Q16W16V16U16";

		case D3DFMT_MULTI2_ARGB8: return "D3DFMT_MULTI2_ARGB8";

			//Floatingpointsurfaceformats

			//s10e5formats(16-bitsperchannel)
		case D3DFMT_R16F: return "D3DFMT_R16F";
		case D3DFMT_G16R16F: return "D3DFMT_G16R16F";
		case D3DFMT_A16B16G16R16F: return "D3DFMT_A16B16G16R16F";

			//IEEEs23e8formats(32-bitsperchannel)
		case D3DFMT_R32F: return "D3DFMT_R32F";
		case D3DFMT_G32R32F: return "D3DFMT_G32R32F";
		case D3DFMT_A32B32G32R32F: return "D3DFMT_A32B32G32R32F";

		case D3DFMT_CxV8U8: return "D3DFMT_CxV8U8";
	}
}

template<class T>
T& operator<<( T& s, device_factory::display_mode const& mode )
{
	s << mode.Width << "x" << mode.Height << "@" << mode.RefreshRate << ":" << aux::get_fmt_string(mode.Format) << ":";
	return s;
}

static std::string get_behaviour_string( unsigned beh )
{
	std::string msg;

	//
	struct info
	{	unsigned id; char const* desc; }
	in[] =
	{
		{ D3DCREATE_ADAPTERGROUP_DEVICE,			"D3DCREATE_ADAPTERGROUP_DEVICE" },
		{ D3DCREATE_DISABLE_DRIVER_MANAGEMENT,		"D3DCREATE_DISABLE_DRIVER_MANAGEMENT" },
		{ D3DCREATE_DISABLE_DRIVER_MANAGEMENT_EX,	"D3DCREATE_DISABLE_DRIVER_MANAGEMENT_EX" },
		{ D3DCREATE_FPU_PRESERVE,					"D3DCREATE_FPU_PRESERVE" },
		{ D3DCREATE_HARDWARE_VERTEXPROCESSING,		"D3DCREATE_HARDWARE_VERTEXPROCESSING" },
		{ D3DCREATE_MIXED_VERTEXPROCESSING,			"D3DCREATE_MIXED_VERTEXPROCESSING" },
		{ D3DCREATE_MULTITHREADED,					"D3DCREATE_MULTITHREADED" },
		{ D3DCREATE_NOWINDOWCHANGES,				"D3DCREATE_NOWINDOWCHANGES" },
		{ D3DCREATE_PUREDEVICE,						"D3DCREATE_PUREDEVICE" },
		{ D3DCREATE_SOFTWARE_VERTEXPROCESSING,		"D3DCREATE_SOFTWARE_VERTEXPROCESSING" }
	};

	for( size_t i=0; i<stir::array_size(in); ++i )
	{
		if( beh&in[i].id )
		{
			if (!msg.empty ())
				msg += "|";
			msg += in[i].desc;
		}
	}

	return msg;
}

static char const* get_device_type( D3DDEVTYPE type )
{
	switch( type )
	{
	case D3DDEVTYPE_HAL:	return "D3DDEVTYPE_HAL";
	case D3DDEVTYPE_NULLREF:return "D3DDEVTYPE_NULLREF";
	case D3DDEVTYPE_REF:	return "D3DDEVTYPE_REF";
	case D3DDEVTYPE_SW:		return "D3DDEVTYPE_SW";
	default:				return "unknown";
 	}
}

// fullscreen modes
// D3DFMT_A1R5G5B5
// D3DFMT_A2R10G10B10
// D3DFMT_A8R8G8B8
// D3DFMT_R5G6B5
// D3DFMT_X1R5G5B5
// D3DFMT_X8R8G8B8

device_factory::device_factory( IDirect3D9& direct3d, D3DDEVTYPE devtype, int adapter )
:	direct3d_( direct3d ), m_create_fullscreen( false )
,	shader_debug_flags( 0 ), behaviour_flags_( 0 )
,	device_type_( devtype ), allow_mixed_vp(false)
{
	assert( adapter == D3DADAPTER_DEFAULT );
	HRESULT hr = direct3d.GetAdapterDisplayMode( D3DADAPTER_DEFAULT, &desktop_mode );
	if( FAILED( hr ) )
		dx_error( hr, L"Failed to get current display mode");
	enum_adapters( adapter_infos );
	auto_defaults();
}

device_factory::~device_factory()
{
	std::for_each( adapter_infos.begin(), adapter_infos.end(), stir::utility::deleter() );
}

int device_factory::generate_clear_mask() const
{
	return
		( D3DCLEAR_TARGET ) |
		( present_parameters_.EnableAutoDepthStencil ? D3DCLEAR_ZBUFFER : 0 ) |
		( ( present_parameters_.EnableAutoDepthStencil && aux::has_stencil( present_parameters_.AutoDepthStencilFormat ) ) ? D3DCLEAR_STENCIL : 0 )
		;
}

IDirect3DDevice9* device_factory::create( HWND hwnd )
{
	IDirect3DDevice9* dev = 0;
	HRESULT hr = S_OK;

	//
	//hr = direct3d_.CheckDeviceType( D3DADAPTER_DEFAULT,
	//	device_type_,
	//	present_parameters_.BackBufferFormat,
	//	present_parameters_.BackBufferFormat,
	//	present_parameters_.Windowed
	//);
	//if( FAILED( hr ) )
	//	dx_error( hr, L"CheckDeviceType failed" );

	// if fullscreen, remove window caption bar
	if( m_create_fullscreen )
	{
//		assert( ::IsWindow(hwnd) );

		LONG_PTR dwStyle = ::GetWindowLongPtr(hwnd, GWL_STYLE);
		LONG_PTR dwNewStyle = 0;//dwStyle & ~( WS_CAPTION |WS_BORDER | WS_SYSMENU );
		::SetWindowLongPtr( hwnd, GWL_STYLE, (LONG)(LONG_PTR)dwNewStyle );
	} else
	{
		present_parameters_.FullScreen_RefreshRateInHz = 0;
	}

	STIR_LOG( dx )
		<< "Created device: "
		<< "fullscreen:" << m_create_fullscreen << " "
		<< "type:" << get_device_type( device_type_ ) << " "
		<< "hwnd:" << hwnd << " "
		<< "behaviour_flags_:" << get_behaviour_string( behaviour_flags_ )
		<< log::endl;

// Set default settings
	UINT AdapterToUse = D3DADAPTER_DEFAULT;
#if (USE_PERF_HUD == 1) || (!_SHIPBUILD)
	// Look for 'NVIDIA NVPerfHUD' adapter
	// If it is present, override default settings
	for( UINT adapter=0; adapter<direct3d_.GetAdapterCount(); adapter++ )
	{
		D3DADAPTER_IDENTIFIER9 identifier;
		hr = direct3d_.GetAdapterIdentifier( adapter, 0, &identifier );
		if( strcmp(identifier.Description,"NVIDIA NVPerfHUD") == 0 )
		{
			AdapterToUse = adapter;
			device_type_ = D3DDEVTYPE_REF;
			break;
		}
	}
#endif

	present_parameters_.hDeviceWindow = hwnd;
	hr = direct3d_.CreateDevice(
		AdapterToUse,
		device_type_,
		hwnd,
		behaviour_flags_,
		&present_parameters_,
		&dev
	);

	if( FAILED(hr) )
		dx_error( hr, L"Cannot create Direct3D9 device" );

	STIR_LOG( dx ) << "Device created" << log::endl;

	return dev;
}

void device_factory::enable_shader_debugging( int debugflags )
{
	shader_debug_flags = debugflags;

	if( shader_debug_flags & vtx_shader_debug ) {
		device_type_		= D3DDEVTYPE_HAL;
		behaviour_flags_	= D3DCREATE_SOFTWARE_VERTEXPROCESSING;
	}

	if( shader_debug_flags & px_shader_debug )
		device_type_ = D3DDEVTYPE_REF;

	if( !(shader_debug_flags & (px_shader_debug|vtx_shader_debug)) )
		auto_defaults();
}

void device_factory::auto_defaults()
{
	HRESULT hr = S_OK;
	D3DCAPS9 caps;
	hr = direct3d_.GetDeviceCaps( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &caps );
	m_caps = caps;

	device_type_		= D3DDEVTYPE_HAL;
	behaviour_flags_	= D3DCREATE_PUREDEVICE | D3DCREATE_HARDWARE_VERTEXPROCESSING;

	// orlok: caps.VertexShaderVersion needs to be masked w/ 0xffff, as higher-word is != 0
	ZeroMemory( &present_parameters_, sizeof(present_parameters_) );
	set_allow_mixed_vp( false );

	if( m_create_fullscreen )
	{
		present_parameters_.BackBufferWidth			= 800;
		present_parameters_.BackBufferHeight		= 600;
		present_parameters_.BackBufferFormat		= D3DFMT_X8R8G8B8;//X8R8G8B8;
	} else {
		present_parameters_.BackBufferWidth			= 0;
		present_parameters_.BackBufferHeight		= 0;
		present_parameters_.BackBufferFormat		= desktop_mode.Format;//X8R8G8B8;
	}

	present_parameters_.BackBufferCount			= 1;
	present_parameters_.Windowed				= !m_create_fullscreen;

	present_parameters_.SwapEffect				= D3DSWAPEFFECT_DISCARD;
	present_parameters_.PresentationInterval	= D3DPRESENT_INTERVAL_IMMEDIATE;
}

device_factory::adapter_info* device_factory::get_adapter_info( unsigned adapter )
{
	HRESULT hr = S_OK;

	D3DFORMAT fs_modes_fmt[] = 
	{
		D3DFMT_A1R5G5B5,
		D3DFMT_A2R10G10B10,
		D3DFMT_A8R8G8B8,
		D3DFMT_R5G6B5,
		D3DFMT_X1R5G5B5,
		D3DFMT_X8R8G8B8
	};
	unsigned fsmodes_fmt_count = sizeof( fs_modes_fmt ) / sizeof( fs_modes_fmt[0] );

	adapter_info* ainfo = new adapter_info();
	hr = direct3d_.GetAdapterIdentifier( adapter, 0, &ainfo->identifier );
	hr = direct3d_.GetDeviceCaps( adapter, D3DDEVTYPE_HAL, &ainfo->caps );

	for( unsigned fmtidx = 0; fmtidx < fsmodes_fmt_count; ++fmtidx )
	{
		D3DFORMAT fmt = fs_modes_fmt[ fmtidx ];
		unsigned mode_count = direct3d_.GetAdapterModeCount( adapter, fmt );

		std::vector<display_mode> modes;

		for( unsigned m = 0; m < mode_count; ++m )
		{
			display_mode mode;
			direct3d_.EnumAdapterModes( adapter, fmt, m, &mode );
			modes.push_back( mode );
		}

		if( !modes.empty() )
			ainfo->modes.insert( std::make_pair( fmt, modes ) );
	}

	return ainfo;
}

void device_factory::enum_adapters( std::vector<adapter_info*>& adapters )
{
	unsigned adapter_count = direct3d_.GetAdapterCount();

	for( unsigned adapter = 0; adapter < adapter_count; ++adapter )
	{
		adapter_info* ai = get_adapter_info( adapter );
		adapters.push_back( ai );

		DWORD vsVer = ai->caps.VertexShaderVersion;
		DWORD psVer = ai->caps.PixelShaderVersion;

		STIR_LOG( dx ) << "Adapter " << adapter << ": " << ai->identifier.DeviceName << log::endl;
		STIR_LOG( dx ) << "  Description: " << ai->identifier.Description << log::endl;
		STIR_LOG( dx ) << "  Driver: " << ai->identifier.Driver << log::endl;
		STIR_LOG( dx ) << "  Vertex Shader: " << ((vsVer&0xff00) >> 8 ) << "." << (vsVer&0xff) << log::endl;
		STIR_LOG( dx ) << "  Pixel Shader: " << ((psVer&0xff00) >> 8 ) << "." << (psVer&0xff) << log::endl;
		STIR_LOG( dx ) << "  Fullscreen Formats: " << (int)ai->modes.size() << log::endl;

		for( format_modes::const_iterator it = ai->modes.begin(); it != ai->modes.end(); ++it )
		{
			if( STIR_LOG_ENABLED(dx) )
			{
				STIR_LOG( dx ) << aux::get_fmt_string( it->first ) << ":";
				mode_list const& ml = it->second;

				int lastw = -1, lasth = -1;
				for( size_t i=0; i<ml.size(); ++i)
				{
					if (lastw != ml[i].Width || lasth != ml[i].Height) {
						STIR_LOG( dx ).printf ("\n  ");
						lastw = ml[i].Width;
						lasth = ml[i].Height;
					} else
						STIR_LOG( dx ).printf (", ");

					STIR_LOG( dx ).printf ("%d x %d @ %d", ml[i].Width, ml[i].Height, ml[i].RefreshRate);					
				}
				STIR_LOG( dx ).printf ("\n");
			}
		}
	}
}

device_factory::display_mode device_factory::display_mode::unknown()
{
	device_factory::display_mode dm;

	dm.Width		= 0;
	dm.Height		= 0;
	dm.RefreshRate	= 0;
	dm.Format		= D3DFMT_UNKNOWN;

	return dm;
}

struct op_mode_best_fmt
{
	bool operator()( device_factory::display_mode const* a, device_factory::display_mode const* b ) const
	{	return a->Format < b->Format; }
};

struct op_mode_best_refresh_rate
{
	bool operator()( device_factory::display_mode const* a, device_factory::display_mode const* b ) const
	{	return a->RefreshRate > b->RefreshRate; }
};

struct op_mode_best_resolution
{
	bool operator()( device_factory::display_mode const* a, device_factory::display_mode const* b ) const
	{	return a->Width * a->Height > b->Width * b->Height; }
};


//if( a->Format < b->Format )
//	return true;
//else if( a->RefreshRate > b->RefreshRate )
//	return true;
//else if( b->Width > a->Width )
//	return true;
//else if( b->Height > a->Height )
//	return true;
//else
//	return false;

void device_factory::set_allow_mixed_vp( bool allow )
{
	allow_mixed_vp = allow;

	if( !shader_debug_flags )
	{
		if( (m_caps.DevCaps & D3DDEVCAPS_PUREDEVICE) && ((m_caps.VertexShaderVersion & 0xffff) >= 0x0100 ) )
		{
			behaviour_flags_	= D3DCREATE_PUREDEVICE | D3DCREATE_HARDWARE_VERTEXPROCESSING;
		}
		else
		{
			if( allow_mixed_vp && (m_caps.DevCaps&D3DDEVCAPS_HWTRANSFORMANDLIGHT) )
				behaviour_flags_	= D3DCREATE_MIXED_VERTEXPROCESSING;
			else
				behaviour_flags_	= D3DCREATE_SOFTWARE_VERTEXPROCESSING;
		}
	} else
		behaviour_flags_	= D3DCREATE_SOFTWARE_VERTEXPROCESSING;
}

device_factory::display_mode
device_factory::find_mode_by_resolution( unsigned adapter, int width, int height, int hz )
{
	if( adapter >= adapter_infos.size() )
		return display_mode::unknown();

	adapter_info const& ai = *adapter_infos[ adapter ];

	std::vector<display_mode const*> matched_modes;

	for( format_modes::const_iterator fmtIt = ai.modes.begin(); fmtIt != ai.modes.end(); ++fmtIt )
	{
		mode_list const& ml = fmtIt->second;

		for( mode_list::const_iterator modeIt=ml.begin(); modeIt != ml.end(); ++modeIt )
		{
//			assert( modeIt->Format == fmtIt->first );

			HRESULT formatOK = direct3d_.CheckDeviceType( adapter, device_type_, modeIt->Format, modeIt->Format, false );
			if( SUCCEEDED(formatOK) &&
				(modeIt->Width == width ) &&
				(modeIt->Height == height ) &&
				((int)modeIt->RefreshRate <= hz || hz == -1 ) )
			{
				matched_modes.push_back( &*modeIt );
			}
		}
	}

	// sort by hz
	STIR_LOG( dx ) << "directx9::device_factory::find_format_by_resolution(" << adapter << "): "
		<< width << " x " << height << " @ " << hz << " : " << (int)matched_modes.size() << log::endl;

	if( !matched_modes.empty() )
	{
#ifdef _DEBUG
		//STIR_LOG( debug ) << "FS modes before sort: " << log::endl;
		//for( std::vector<display_mode const*>::iterator it = matched_modes.begin(); it != matched_modes.end(); ++it )
		//	STIR_LOG( debug ) << "   " << **it << log::endl;
#endif
		typedef std::vector<display_mode const*>::iterator dmit_t;

		std::sort( matched_modes.begin(), matched_modes.end(), op_mode_best_fmt() );

		std::pair<dmit_t,dmit_t> sameFormatRgn = std::equal_range( matched_modes.begin(), matched_modes.end(), matched_modes.front(), op_mode_best_fmt() );
		std::sort( sameFormatRgn.first, sameFormatRgn.second, op_mode_best_refresh_rate() );

		std::pair<dmit_t,dmit_t> sameRefreshRgn = std::equal_range( sameFormatRgn.first, sameFormatRgn.second, matched_modes.front(), op_mode_best_refresh_rate() );
		std::sort( sameRefreshRgn.first, sameRefreshRgn.second, op_mode_best_resolution() );

#ifdef _DEBUG
		//STIR_LOG( debug ) << "FS modes AFTER sort: " << log::endl;
		//for( std::vector<display_mode const*>::iterator it = matched_modes.begin(); it != matched_modes.end(); ++it )
		//	STIR_LOG( debug ) << "   " << **it << log::endl;
#endif

		return *matched_modes.front();
	} else
		return display_mode::unknown();
}

bool device_factory::request_windowed()
{
	m_create_fullscreen = false;
	present_parameters_.BackBufferWidth			= 0;
	present_parameters_.BackBufferHeight		= 0;
	present_parameters_.BackBufferFormat		= desktop_mode.Format;//X8R8G8B8;
	HRESULT hr = direct3d_.CheckDeviceType( D3DADAPTER_DEFAULT, device_type_, desktop_mode.Format, desktop_mode.Format, true );
	if( FAILED(hr) )
		STIR_LOG( dx ) << std::string( aux::get_fmt_string(desktop_mode.Format) ) + " not supported for windowed" << log::endl;
	present_parameters_.FullScreen_RefreshRateInHz = 0;
	present_parameters_.Windowed				= true;

	return true;
}

bool device_factory::request_fullscreen( int width, int height, int hz_or_minus_one )
{
	m_create_fullscreen = true;
	display_mode mode = find_mode_by_resolution( D3DADAPTER_DEFAULT, width, height, hz_or_minus_one );

	if( mode.Format == D3DFMT_UNKNOWN )
		return false;
	else
	{
		present_parameters_.BackBufferFormat			= mode.Format;
		present_parameters_.BackBufferWidth				= mode.Width;
		present_parameters_.BackBufferHeight			= mode.Height;
		present_parameters_.FullScreen_RefreshRateInHz	= mode.RefreshRate;
		present_parameters_.Windowed					= false;

		STIR_LOG( dx ) << "Fullscreen selected: " << mode << log::endl;
		return true;
	}
}

bool device_factory::request_zstencil_creation( int minzbits, int minstencilbits )
{
	D3DFORMAT fmt = find_depth_stencil_format(
		present_parameters_.BackBufferFormat, present_parameters_.BackBufferFormat, minzbits, minstencilbits );

	if( fmt == D3DFMT_UNKNOWN )
	{
		present_parameters_.EnableAutoDepthStencil = false;
		return false;
	}

	STIR_LOG( dx ) << "Z/Stencil selected: " << aux::get_fmt_string( fmt ) << log::endl;
    present_parameters_.AutoDepthStencilFormat = fmt;
	present_parameters_.EnableAutoDepthStencil = true;

	return true;
}

struct fmt_desc
{
	D3DFORMAT	format;
	unsigned	depth;
	unsigned	stencil;
};

struct op_sort_fmt_desc
{
	typedef bool result_type;

	bool operator()( fmt_desc const& a, fmt_desc const& b ) const
	{
		if( a.depth == b.depth )
			return a.stencil < b.stencil;
		else
			return a.depth < b.depth;
	}
};

D3DFORMAT device_factory::find_depth_stencil_format( D3DFORMAT adapterfmt, D3DFORMAT rendertargetfmt,
	int minzbits, int minstencilbits )
{
	fmt_desc zs_fmts[] =
	{
		{ D3DFMT_D16_LOCKABLE, 16, 0 },
		{ D3DFMT_D32, 32, 0 },
		{ D3DFMT_D15S1, 15, 1 },
		{ D3DFMT_D24S8, 24, 8 },
		{ D3DFMT_D24X8, 24, 0 },
		{ D3DFMT_D24X4S4, 24, 4 },
//		{ D3DFMT_D32F_LOCKABLE, 32, 0 },
		{ D3DFMT_D24FS8, 24, 8 },
		{ D3DFMT_D16, 16, 0 }
	};
	unsigned const zs_fmts_size = sizeof( zs_fmts ) / sizeof( zs_fmts[0] );

	std::vector<fmt_desc> supported;

	for( int i=0; i<zs_fmts_size; ++i )
	{
		HRESULT res = direct3d_.CheckDeviceFormat(
			D3DADAPTER_DEFAULT, device_type_, adapterfmt,
			D3DUSAGE_DEPTHSTENCIL, D3DRTYPE_SURFACE, zs_fmts[ i ].format );
		if( FAILED( res ) )
			continue;

		res = direct3d_.CheckDepthStencilMatch( D3DADAPTER_DEFAULT,
			device_type_, adapterfmt, rendertargetfmt, zs_fmts[ i ].format );
		if( SUCCEEDED( res ) )
			supported.push_back( zs_fmts[ i ] );
	}

	std::sort( supported.begin(), supported.end(), op_sort_fmt_desc() );
	std::reverse( supported.begin(), supported.end() );

	{
		STIR_LOG( dx ) << "Z/Stencil available:\n  ";
		for( unsigned i=0; i<supported.size(); ++i )
			STIR_LOG( dx ) << aux::get_fmt_string( supported[ i ].format ) << " ";
		STIR_LOG( dx ) << log::endl;
	}

	// try find direct match
	std::vector<fmt_desc> zmatchonly;

	bool zmatch = false;
	for( unsigned i=0; i<supported.size(); ++i )
	{
		if( supported[ i ].depth == minzbits )
		{
			if( supported[ i ].stencil == minstencilbits )
				return supported[ i ].format;
			else
				zmatchonly.push_back( supported[i] );
		}
	}

	// should be sorted by stencil bits
	for( unsigned i=0; i<zmatchonly.size(); ++i )
	{
		if( (int)zmatchonly[i].stencil >= minstencilbits )
			return supported[ i ].format;
	}

	return D3DFMT_UNKNOWN;
}

//
device::device( IDirect3DDevice9& dev, device_factory& devfactory )
:	device_( dev ), device_factory_( devfactory ), is_lost_( false ), m_mixed_vp( (devfactory.behaviour_flags()&D3DCREATE_MIXED_VERTEXPROCESSING) != 0)
{
	//if( is_mixed_vp() )
	//{
	//	device_.SetSoftwareVertexProcessing( true );
	//}

	opt_shader_debug = device_factory_.shader_debug_enabled();
	HRESULT hr = get_dev().GetViewport( &viewport_ );
}

void device::register_listener( lost_reset_interface& l )
{
	dev_state_listeners_.insert( &l );
	if( !is_lost_ )
		l.device_reset();
}

void device::remove_listener( lost_reset_interface& l )
{
	dev_state_listeners_.erase( &l );
	if( !is_lost_ )
		l.device_lost();
}

void device::process_cooperative_level()
{
	// if device is lost, generate on lost. then on restore
	HRESULT hr = S_OK;
	hr = device_.TestCooperativeLevel();
    if( hr == D3DERR_DEVICELOST && !is_lost_ )
	{
		STIR_LOG( dx ) << "Device lost" << log::endl;
		lose_device();
	}

	if( is_lost_ && (hr = device_.TestCooperativeLevel()) == D3DERR_DEVICENOTRESET )
	{
		STIR_LOG( dx ) << "Reseting device" << log::endl;
		reset_device();
		STIR_LOG( dx ) << "Device restored" << log::endl;
	}
}

void device::lose_device()
{
	HRESULT hr = S_OK;
	is_lost_ = true;
	stir_foreach (lost_reset_interface* l, dev_state_listeners_)
		l->device_lost ();
}

void device::reset_device()
{
	HRESULT hr = S_OK;

//	assert( device_factory_.present_parameters().BackBufferCount >= 1 );
//	assert( device_factory_.present_parameters().BackBufferWidth >= 320 );
//	assert( device_factory_.present_parameters().BackBufferHeight >= 240 );

	hr = device_.Reset( &device_factory_.present_parameters() );
	if( hr != S_OK )
		dx_error( hr, L"Cannot reset device" );

	if( !is_fullscreen() )
	{
		::SetWindowLongPtr( device_factory_.present_parameters().hDeviceWindow, GWL_EXSTYLE, 0 );

        // Also restore the z-order of window to previous state
        HWND hWndInsertAfter = HWND_NOTOPMOST;
        SetWindowPos( device_factory_.present_parameters().hDeviceWindow, hWndInsertAfter, 0, 0, 0, 0, SWP_NOMOVE|SWP_NOREDRAW|SWP_NOSIZE );
	}

	hr = get_dev().GetViewport( &viewport_ );

	STIR_LOG( dx ) << "reset_device: viewport = {"
		<< get_viewport().X << ", " << get_viewport().Y << ", "
		<< get_viewport().Width << ", " << get_viewport().Height << ", "
		<< get_viewport().MinZ << ", " << get_viewport().MaxZ << "}"
		<< log::endl
		;
		
	stir_foreach ( lost_reset_interface* listener, dev_state_listeners_)
		listener->device_reset ();

//	for_each( dev_state_listeners_.begin(), dev_state_listeners_.end(),
//		boost::bind( &lost_reset_interface::device_reset, _1 ) );
	
	STIR_LOG( dx ).printf ( "after for_each::device_reset: ex-style: %x\n",
		::GetWindowLongPtr(device_factory_.present_parameters().hDeviceWindow, GWL_EXSTYLE));
		
	is_lost_ = false;
}

void device::resize_buffers( int width, int height )
{
	HRESULT hr = S_OK;

	if( device_factory_.present_parameters().BackBufferWidth == width &&
		device_factory_.present_parameters().BackBufferHeight == height )
	{
		STIR_LOG( dx ) << "resize rejected, backbuffer is (" << 
			device_factory_.present_parameters().BackBufferWidth << ", " <<
			device_factory_.present_parameters().BackBufferHeight << ")" << log::endl;
		return;
	}


	hr = device_factory_.present_parameters().BackBufferWidth = 0;
	hr = device_factory_.present_parameters().BackBufferHeight = 0;

	lose_device();
	reset_device();

		/*
	IDirect3DSurface9* backbuf = 0;
	IDirect3DSurface9* zstencilbuf = 0;

	hr = device_.CreateRenderTarget(
		width, height,
		device_factory_.present_parameters().BackBufferFormat,
		device_factory_.present_parameters().MultiSampleType,
		device_factory_.present_parameters().MultiSampleQuality,
		false, &backbuf, 0 );

	if( FAILED( hr ) )
		stir::dx_error( hr, L"resize_buffers: CreateRenderTarget failed" );

	hr = device_.CreateDepthStencilSurface(
		width, height,
		D3DFMT_D24S8,
//		device_factory_.present_parameters().AutoDepthStencilFormat,
		device_factory_.present_parameters().MultiSampleType,
		device_factory_.present_parameters().MultiSampleQuality,
		TRUE, &zstencilbuf, 0 );

	if( FAILED( hr ) ) {
		backbuf->Release();
		stir::dx_error( hr, L"resize_buffers: CreateDepthStencilSurface failed" );
	}

	hr = device_.SetRenderTarget( 0, backbuf );
	if( FAILED( hr ) )
		stir::dx_error( hr, L"resize_buffers: Cannot SetRenderTarget" );

	backbuf->Release();

	hr = device_.SetDepthStencilSurface( zstencilbuf );
	if( FAILED( hr ) )
		stir::dx_error( hr, L"resize_buffers: Cannot SetDepthStencilSurface" );
	zstencilbuf->Release();
	*/
}

} }
