#ifndef STIR_DX9_DEVICE_H_
#define STIR_DX9_DEVICE_H_

#include <stir/platform.hpp>

#include <set>
#include <map>
#include <vector>

#include <boost/bind.hpp>
#include <boost/noncopyable.hpp>

//#include "../bautility.hpp"
#include "dx9_utility.hpp"
#include "../../stir_logs.hpp"

namespace stir { namespace directx9 {

class STIR_ATTR_DLL device_factory
{
public:
	struct display_mode : public D3DDISPLAYMODE
	{
		static display_mode unknown();
	};

	typedef std::vector<display_mode> mode_list;
	typedef std::map<D3DFORMAT,mode_list> format_modes;

	struct adapter_info
	{
		D3DCAPS9				caps;
		// maps fullscreen modes supported by specific back-buffer format
		format_modes			modes;
		D3DADAPTER_IDENTIFIER9	identifier;
	};

	enum
	{
		vtx_shader_debug = 0x01,
		px_shader_debug = 0x02
	};

	device_factory( IDirect3D9& direct3d, D3DDEVTYPE devtype, int adapter = D3DADAPTER_DEFAULT );
	~device_factory();

	IDirect3DDevice9* create( HWND hwnd );

	D3DPRESENT_PARAMETERS& present_parameters()	{	return present_parameters_; }
	DWORD& behaviour_flags()					{	return behaviour_flags_; }
	D3DCAPS9 const& caps() const				{	return m_caps; }
	display_mode const& get_desktop_mode() const { return desktop_mode; }
	int generate_clear_mask() const;

	// util functions
	void enable_shader_debugging( int debugflags );
	bool shader_debug_enabled() const			{	return shader_debug_flags != 0; }

	bool is_fullscreen() const					{	return m_create_fullscreen; }

	bool request_windowed();
	bool request_fullscreen( int width, int height, int hz_or_minus_one );
	bool request_zstencil_creation( int minzbits, int minstencilbits );
	void set_allow_mixed_vp( bool allow );

	D3DFORMAT find_depth_stencil_format( D3DFORMAT adapterfmt, D3DFORMAT rendertargetfmt, int minzbits, int minstencilbits );

	format_modes::const_iterator fmt_begin() const	{ return adapter_infos[ 0 ]->modes.begin(); }
	format_modes::const_iterator fmt_end() const	{ return adapter_infos[ 0 ]->modes.end(); }

protected:
	void auto_defaults();

	void enum_adapters( std::vector<adapter_info*>& adapters );
	adapter_info* get_adapter_info( unsigned adapter );

	display_mode find_mode_by_resolution( unsigned adapter, int width, int height, int hz = -1 );

private:
	IDirect3D9&					direct3d_;
	D3DDEVTYPE					device_type_;
	D3DPRESENT_PARAMETERS		present_parameters_;
	DWORD						behaviour_flags_;
	D3DCAPS9					m_caps;

	std::vector<adapter_info*>	adapter_infos;
	display_mode				desktop_mode;
	//
	int							shader_debug_flags;
	bool						m_create_fullscreen, allow_mixed_vp;
};

struct STIR_ATTR_DLL lost_reset_interface
{
	virtual ~lost_reset_interface() = 0;

	virtual void device_lost() = 0;
	virtual void device_reset() = 0;
};

class STIR_ATTR_DLL device : public boost::noncopyable
{
	friend class device_factory;

public:
	typedef std::set<lost_reset_interface*> t_device_state_listeners;

	device( IDirect3DDevice9& dev, device_factory& devfactory );

	void register_listener( lost_reset_interface& l );
	void remove_listener( lost_reset_interface& l );

	void process_cooperative_level();

	IDirect3DDevice9& get_dev() const	{	return device_; }
	unsigned clear_mask() const		{	return device_factory_.generate_clear_mask(); }

	bool is_fullscreen() const		{	return factory().is_fullscreen(); }
	bool is_lost() const			{	return is_lost_; }
	bool is_shader_debug() const	{	return opt_shader_debug; }
	bool is_mixed_vp() const		{	return m_mixed_vp; }
	float ascpect_ratio() const		{	return float(viewport_.Width) / float(viewport_.Height); }

	int get_max_blend_matrices() const { return device_factory_.caps().MaxVertexBlendMatrices; }

	viewport_description const& get_viewport() {	return viewport_; }

	int viewport_width() {	return viewport_.Width; }
	int viewport_height() {	return viewport_.Height; }

	void resize_buffers( int width, int height );

	// 
	device_factory& factory() const	{	return device_factory_; }

//protected:
	void lose_device();
	void reset_device();

private:
	t_device_state_listeners	dev_state_listeners_;
	device_factory&				device_factory_;
	bool						is_lost_, m_mixed_vp;
	IDirect3DDevice9&			device_;
	viewport_description		viewport_;

	// options
	bool						opt_shader_debug;
};

struct swvp_state_saver
{
	swvp_state_saver( IDirect3DDevice9& dev )
		:	m_dev(dev), m_isswvp( dev.GetSoftwareVertexProcessing() )
	{}

	~swvp_state_saver()
	{	m_dev.SetSoftwareVertexProcessing( m_isswvp ); }

	IDirect3DDevice9&	m_dev;
	BOOL				m_isswvp;
};

namespace aux
{
	STIR_ATTR_DLL char const* get_fmt_string( D3DFORMAT fmt );
	STIR_ATTR_DLL bool has_stencil( D3DFORMAT fmt );
}

} // directx9

//!std::ostream& operator<<( std::ostream& os, stir::directx9::device_factory::display_mode const& mode );
//!stir::ostream& operator<<( stir::ostream& os, stir::directx9::device_factory::display_mode const& mode );

} // stir

#endif // STIR_DX9_DEVICE_H_
