#ifndef STIR_DX9_EFFECT_HPP_
#define STIR_DX9_EFFECT_HPP_

#include "../../basic_types.hpp"
#include "../../math_types.hpp"
#include "dx9_include.hpp"

namespace stir { namespace dx9 {

struct effect
{
	typedef D3DXHANDLE handle;
	
	effect () : impl_(0) {}
	effect (ID3DXEffect* efx) : impl_(efx) {}

	void set_matrix (handle name, matrix const& value) { impl_->SetMatrix (name, &value); }
	void set_vector (handle name, vector4 const& value) { impl_->SetVector (name, &value); }
	void set_float (handle name, float value) { impl_->SetFloat (name, value); }
	void set_bool (handle name, bool value) { impl_->SetBool (name, value); }

	ID3DXEffect*	impl_;
};

}}

#endif // STIR_DX9_EFFECT_HPP_
