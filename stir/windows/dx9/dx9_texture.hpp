#ifndef STIR_DX9_TEXTURE_HPP_
#define STIR_DX9_TEXTURE_HPP_

#include "dx9_include.hpp"

namespace stir { namespace dx9 {

typedef IDirect3DTexture9* texture;

struct texture_info : D3DSURFACE_DESC
{
	explicit texture_info (texture tex, int level = 0)
	{
		tex->GetLevelDesc (level, this);
	}

	size_t width () const { return Width; }
	size_t height () const { return Height; }
};

}}

#endif // STIR_DX9_TEXTURE_HPP_
