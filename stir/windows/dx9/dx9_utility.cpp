#include <stir/platform.hpp>
#include "dx9_utility.hpp"
#include <stir/basic_util.hpp>
#include <stir/flat_types.hpp>
#include "device.hpp"
//#include "dx9_effect.hpp"

namespace stir { namespace directx9 {

void dx_error( HRESULT hr, stir::string const& mymsg )
{
	stir::string s =
		mymsg +
		L" (" + utility::to_stir( DXGetErrorString9( hr ) ) +
		L": " + utility::to_stir( DXGetErrorDescription9( hr ) ) +
		L")"
		;
		
	STIR_LOG (error) << s << log::endl;
	utility::stir_fatal( s );
}

static char const* declTypeS[] =
{
	"D3DDECLTYPE_FLOAT1",
	"D3DDECLTYPE_FLOAT2",
	"D3DDECLTYPE_FLOAT3",
	"D3DDECLTYPE_FLOAT4",
	"D3DDECLTYPE_D3DCOLOR",
	"D3DDECLTYPE_UBYTE4",
	"D3DDECLTYPE_SHORT2",
	"D3DDECLTYPE_SHORT4",
	"D3DDECLTYPE_UBYTE4N",
	"D3DDECLTYPE_SHORT2N",
	"D3DDECLTYPE_SHORT4N",
	"D3DDECLTYPE_USHORT2N",
	"D3DDECLTYPE_USHORT4N",
	"D3DDECLTYPE_UDEC3",
	"D3DDECLTYPE_DEC3N",
	"D3DDECLTYPE_FLOAT16_2",
	"D3DDECLTYPE_FLOAT16_4",
	"D3DDECLTYPE_UNUSED"
};

static char const* declMethodS[] =
{
	"D3DDECLMETHOD_DEFAULT",
	"D3DDECLMETHOD_PARTIALU",
	"D3DDECLMETHOD_PARTIALV",
	"D3DDECLMETHOD_CROSSUV",
	"D3DDECLMETHOD_UV",
	"D3DDECLMETHOD_LOOKUP",
	"D3DDECLMETHOD_LOOKUPPRESAMPLED"
};

static char const* declUsageS[] =
{
	"D3DDECLUSAGE_POSITION",
	"D3DDECLUSAGE_BLENDWEIGHT",
	"D3DDECLUSAGE_BLENDINDICES",
	"D3DDECLUSAGE_NORMAL",
	"D3DDECLUSAGE_PSIZE",
	"D3DDECLUSAGE_TEXCOORD",
	"D3DDECLUSAGE_TANGENT",
	"D3DDECLUSAGE_BINORMAL",
	"D3DDECLUSAGE_TESSFACTOR",
	"D3DDECLUSAGE_POSITIONT",
	"D3DDECLUSAGE_COLOR",
	"D3DDECLUSAGE_FOG",
	"D3DDECLUSAGE_DEPTH",
	"D3DDECLUSAGE_SAMPLE"
};

template<class StreamT>
void dump_element( D3DVERTEXELEMENT9 const& el, StreamT& os )
{
	os
		<< el.Stream
		<< ", "
		<< el.Offset
		<< ", "
		<< declTypeS[ el.Type ]
		<< ", "
		<< declMethodS[ el.Method ]
		<< ", "
		<< declUsageS[ el.Usage ]
		<< ", "
		<< el.UsageIndex
		;
}

template<class StreamT>
void dump_declaration( D3DVERTEXELEMENT9 const* el, StreamT& os )
{
	while( el->Stream != 0xff )
	{
		os << "{ ";
		dump_element( *el, os );
		os << " }";

		++el;
		if( el->Stream != 0xff )
			os << " },\n";
	}
}

template<class StreamT>
void dump_declaration( ID3DXMesh& mesh, StreamT& os )
{
	D3DVERTEXELEMENT9 vEl[ MAXD3DDECLLENGTH ];
	HRESULT hr = mesh.GetDeclaration( vEl );
	stir::dump_declaration( vEl, os );
}

namespace aux
{

static short int rect_indices[6] = { 0, 1, 3, 1, 2, 3 };

static void draw_rect_impl( IDirect3DDevice9& dev, unsigned fvf, void const* vtxptr, size_t vtxsize )
{
	dev.SetFVF( fvf );
	HRESULT hr = dev.DrawIndexedPrimitiveUP( D3DPT_TRIANGLELIST,
		0, 4, 2, rect_indices, D3DFMT_INDEX16, vtxptr, vtxsize ); 
	if( FAILED(hr) )
		dx_error( hr, L"Failed to draw indexed primitive UP" );
}

void STIR_ATTR_DLL draw_rect( IDirect3DDevice9& dev, stir::flat::recti const& rc )
{
#pragma pack(push, 1)
	struct vertex
	{
		float	x, y, z;
	}	v[ 4 ] =
	{
		{	(float)rc.p.x,					(float)rc.p.y,					0 },
		{	(float)rc.p.x + (float)rc.s.x,	(float)rc.p.y,					0 },
		{	(float)rc.p.x + (float)rc.s.x,	(float)rc.p.y + (float)rc.s.y,	0 },
		{	(float)rc.p.x,					(float)rc.p.y + (float)rc.s.y,	0 }
	};
#pragma pack(pop)

	draw_rect_impl( dev, D3DFVF_XYZ, v, sizeof(vertex) );
}

void STIR_ATTR_DLL draw_rect_uv( IDirect3DDevice9& dev, stir::flat::recti const& rc, stir::rectangle const& uv )
{
#pragma pack(push, 1)
	struct vertex
	{
		float	x, y, z;
		float	u, v;
	}	v[ 4 ] =
	{
		{	(float)rc.p.x,					(float)rc.p.y,					0, uv.p0.x, uv.p0.y },
		{	(float)rc.p.x + (float)rc.s.x,	(float)rc.p.y,					0, uv.p1.x, uv.p0.y },
		{	(float)rc.p.x + (float)rc.s.x,	(float)rc.p.y + (float)rc.s.y,	0, uv.p1.x, uv.p1.y },
		{	(float)rc.p.x,					(float)rc.p.y + (float)rc.s.y,	0, uv.p0.x, uv.p1.y }
	};
#pragma pack(pop)

	draw_rect_impl( dev, D3DFVF_XYZ|D3DFVF_TEX1, v, sizeof(vertex) );
}

vector4 STIR_ATTR_DLL calc_screen_params( flat::vector2f const& size )
{
	return vector4(
		2.0f / size.x,
		-2.0f / size.y,
		-1.0f - 1.0f / size.x,
		1.0f + 1.0f / size.y
	);
}

/*
void draw_z_rect( device_context& devctx, flat::rect const& rc, effect efx, float z )
{
	vector4 screen_params = calc_screen_params( devctx.get_viewport().get_size() );

	efx->SetFloat( "z_value", z );
	efx->SetVector( "screen_params", &screen_params );

	BOOL swVP = 0;
	if( devctx.is_mixed_vp() )
	{
		swVP = devctx.device().GetSoftwareVertexProcessing();
		devctx.device().SetSoftwareVertexProcessing( true );
	}

	{
		effect::named_technique tech( "t_write_z", efx );

		for( effect::pass p( efx ); p.valid(); ++p )
		{
			draw_rect( devctx.device(), rc );
		}
	}

	if( devctx.is_mixed_vp() )
		devctx.device().SetSoftwareVertexProcessing( swVP );
}*/

} // aux

} // directx9

} // stir

