#ifndef STIR_DX9_UTILITY_H_
#define STIR_DX9_UTILITY_H_

#include <d3dx9.h>
#include <dxerr9.h>
//#include <boost/iterator/iterator_facade.hpp>

#include "../../basic_types.hpp"
#include "../../flat_types.hpp"
#include "../../stir_logs.hpp"
#include "../com_ptr.hpp"
//#include <stir/basic_types.hpp>

namespace stir { namespace directx9 {

class device;

struct viewport_description : public D3DVIEWPORT9
{
	flat::vector2i get_size() const { return flat::vector2i(Width, Height); }
	flat::recti get_rect() const { return flat::recti( X, Y, Width, Height ); }
};

void STIR_ATTR_DLL dx_error( HRESULT hr, stir::string const& mymsg );

inline IDirect3DVertexBuffer9*
create_dynamic_vertex_buffer( IDirect3DDevice9& device, size_t size_in_bytes, DWORD fvf )
{
	IDirect3DVertexBuffer9* vb = 0;

	HRESULT hr = device.CreateVertexBuffer(
		(UINT)size_in_bytes,
		D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY,
		fvf,
		D3DPOOL_DEFAULT,
		&vb,
		0
	);

	if( FAILED( hr ) )
		dx_error( hr, L"create_dynamic_vertex_buffer: cannot create buffer");

	return vb;
}

inline IDirect3DVertexBuffer9*
create_vertex_buffer( IDirect3DDevice9& device, size_t size_in_bytes, DWORD fvf, DWORD usage, D3DPOOL pool )
{
	IDirect3DVertexBuffer9* vb = 0;

	HRESULT hr = device.CreateVertexBuffer(
		(UINT)size_in_bytes,
		usage,
		fvf,
		pool,
		&vb,
		0
	);

	if( FAILED( hr ) )
		dx_error( hr, L"create_vertex_buffer: cannot create buffer");

	return vb;
}

inline IDirect3DVertexDeclaration9*
create_vertex_declaration( IDirect3DDevice9& device, D3DVERTEXELEMENT9 const* vel )
{
	IDirect3DVertexDeclaration9* decl = 0;
	HRESULT hr = device.CreateVertexDeclaration( vel, &decl );
	if( FAILED(hr) )
		dx_error( hr, L"create_vertex_declaration: failed to create vertex declaration" );
	return decl;
}

struct vb_lock : boost::noncopyable
{
	vb_lock( IDirect3DVertexBuffer9& buf, unsigned OffsetToLock = 0, unsigned SizeToLock = 0, unsigned Flags = D3DLOCK_DISCARD ) : buffer( buf )
	{
		HRESULT hr = buffer.Lock( OffsetToLock, SizeToLock, &data, Flags );
		if( FAILED( hr ) )
			dx_error( hr, L"vb_lock: failed to lock vertex buffer" );
	}
	~vb_lock()
	{	buffer.Unlock(); }

	template<class T>
	T* get_ptr() const { return static_cast<T*>( data ); }

private:
	IDirect3DVertexBuffer9& buffer;
	void*					data;
};

struct ib_lock : boost::noncopyable
{
	ib_lock( IDirect3DIndexBuffer9& buf, unsigned offset = 0, unsigned size_in_bytes = 0, DWORD flags = D3DLOCK_DISCARD ) : buffer( buf ), ptr( 0 )
	{
		HRESULT hr = buffer.Lock( offset, size_in_bytes, &ptr, flags );
		if( FAILED( hr ) )
			dx_error( hr, L"ib_lock: Failed to lock index buffer" );
	}
	~ib_lock()
	{	buffer.Unlock(); }

	template<class T>
	T* get_ptr() const {	return static_cast<T*>( ptr ); }

private:
	IDirect3DIndexBuffer9&	buffer;
	void*					ptr;
};

inline IDirect3DIndexBuffer9*
create_static_index_buffer( IDirect3DDevice9& device, size_t size_in_elements, D3DFORMAT format = D3DFMT_INDEX16 )
{
	IDirect3DIndexBuffer9* ib = 0;

	HRESULT hr = device.CreateIndexBuffer(
		(UINT)(size_in_elements * ( format == D3DFMT_INDEX16 ? sizeof( unsigned short ) : sizeof( unsigned int ) ) ),
		D3DUSAGE_WRITEONLY,
		format,
		D3DPOOL_DEFAULT,
		&ib,
		0
	);

	if( FAILED( hr ) )
		dx_error( hr, L"create_dynamic_index_buffer: cannot create buffer" );

	return ib;
}

inline IDirect3DIndexBuffer9*
create_index_buffer( IDirect3DDevice9& device, size_t size_in_elements, D3DFORMAT format, unsigned usage, D3DPOOL pool )
{
	IDirect3DIndexBuffer9* ib = 0;

	HRESULT hr = device.CreateIndexBuffer(
		(UINT)(size_in_elements * ( format == D3DFMT_INDEX16 ? sizeof( unsigned short ) : sizeof( unsigned int ) ) ),
		usage,
		format,
		pool,
		&ib,
		0
	);

	if( FAILED( hr ) )
		dx_error( hr, L"create_dynamic_index_buffer: cannot create buffer" );

	return ib;
}

inline IDirect3DSurface9*
create_render_target( IDirect3DDevice9& dev, unsigned width, unsigned height, D3DFORMAT format, D3DMULTISAMPLE_TYPE msType, unsigned msQuality, bool lockable )
{
	IDirect3DSurface9* back_buffer = 0;

	HRESULT hr = dev.CreateRenderTarget(
		width, height, format,
		msType, msQuality,
		lockable,
		&back_buffer, 0 );
	if( FAILED( hr ) )
		dx_error( hr, L"Failed to create render target" );

	return back_buffer;
}

inline IDirect3DTexture9*
create_texture(
	IDirect3DDevice9& dev, unsigned width, unsigned height, unsigned miplevels, unsigned usage, D3DFORMAT format, D3DPOOL pool )
{
	IDirect3DTexture9* tex = 0;
	HRESULT hr = dev.CreateTexture( width, height, miplevels, usage, format, pool, &tex, 0 );
	if( FAILED( hr ) )
		dx_error( hr, L"Failed to create texture" );
	return tex;
}

inline IDirect3DCubeTexture9*
create_cube_texture(
	IDirect3DDevice9& dev, unsigned len, unsigned miplevels, unsigned usage, D3DFORMAT format, D3DPOOL pool )
{
	IDirect3DCubeTexture9* tex = 0;
	HRESULT hr = dev.CreateCubeTexture( len, miplevels, usage, format, pool, &tex, 0 );
	if( FAILED( hr ) )
		dx_error( hr, L"Failed to create cube texture" );
	return tex;
}

inline IDirect3DTexture9*
create_render_target_texture( IDirect3DDevice9& dev, unsigned width, unsigned height, D3DFORMAT format )
{	return create_texture( dev, width, height, 1, D3DUSAGE_RENDERTARGET, format, D3DPOOL_DEFAULT ); }

inline IDirect3DSurface9*
create_depth_stencil( IDirect3DDevice9& dev, unsigned width, unsigned height, D3DFORMAT format, bool discard )
{
	IDirect3DSurface9* surface = 0;
	HRESULT hr = dev.CreateDepthStencilSurface(
		width, height, format, D3DMULTISAMPLE_NONE, 0, discard, &surface, 0 );

	if( FAILED( hr ) )
		dx_error( hr, L"Failed to create depth/stencil" );

	return surface;
}

inline
com_ptr<IDirect3DSurface9>
get_surface_level( IDirect3DTexture9& tex, unsigned level )
{
   com_ptr<IDirect3DSurface9> surf;
   HRESULT hr = tex.GetSurfaceLevel( level, &surf );
   return surf;
}

struct surface_desc : public D3DSURFACE_DESC
{
   surface_desc()
   {}

   surface_desc( IDirect3DSurface9* surface )
   {   surface->GetDesc( this ); }

   flat::vector2i get_size() const
   {	return flat::vector2i( Width, Height ); }
};

//com_ptr<IDirect3DSurface9>
//lock_surface_level();

struct texture_level_get
{
	texture_level_get( unsigned idx, IDirect3DTexture9& tex )
	{
		HRESULT hr = S_OK;
		hr = tex.GetSurfaceLevel( idx, &surf );
		if( FAILED( hr ) )
			dx_error( hr, L"Failed to obtain surface level" );
	}

	~texture_level_get()
	{
		assert( surf );
		surf->Release();
	}

	IDirect3DSurface9* surface() const
	{	return surf; }

private:
	IDirect3DSurface9*	surf;
};

struct surface_scoped_lock
{
	// flags
	//D3DLOCK_DISCARD
	//D3DLOCK_DONOTWAIT
	//D3DLOCK_NO_DIRTY_UPDATE
	//D3DLOCK_NOSYSLOCK
	//D3DLOCK_READONLY

	surface_scoped_lock( IDirect3DSurface9* surf, unsigned flags = 0 )
	:	surface( surf )
	{
		HRESULT hr = surface->LockRect( &lr, 0, flags );
		if( FAILED( hr ) )
			dx_error( hr, L"surface_scoped_lock: whole surface lock failed" );
	}

	~surface_scoped_lock()
	{
		HRESULT hr = surface->UnlockRect();
	}

	void* get_ptr() const
	{	return lr.pBits; }
	template<class T>
	T* get_ptr_as() const
	{	return static_cast<T*>( get_ptr() ); }

	unsigned get_pitch() const
	{	return lr.Pitch; }
	template<class T>
	unsigned get_pitch_for() const
	{	return lr.Pitch / sizeof( T ); }

private:
	D3DLOCKED_RECT		lr;
	IDirect3DSurface9*	surface;
};

struct cube_surface_accessor
{
	cube_surface_accessor( IDirect3DCubeTexture9& tex ) : m_tex(tex) {}

	com_ptr<IDirect3DSurface9> get( int i, int level )
	{
		assert( i <= D3DCUBEMAP_FACE_NEGATIVE_Z );

		com_ptr<IDirect3DSurface9> surf;
		HRESULT hr = m_tex.GetCubeMapSurface( (D3DCUBEMAP_FACES)i, level, &surf );
		if( FAILED( hr ) )
			dx_error( hr, L"Failed to get cube surface" );

		return surf;
	}

private:
	IDirect3DCubeTexture9&	m_tex;
};

/* too much dependencies
template<class ElementT>
class stride_iterator : public boost::iterator_facade<stride_iterator<ElementT>, ElementT, boost::random_access_traversal_tag>
{
	ElementT*	m_el;
	int			stride;

	typedef boost::iterator_facade<stride_iterator, ElementT, boost::random_access_traversal_tag> facade_t;

public:
	explicit stride_iterator( ElementT* p, int stride_size )
	:	m_el(p), stride( stride_size )
	{}

private:
	friend class boost::iterator_core_access;

	void advance( typename facade_t::difference_type n )
	{ m_el = (ElementT*)(void*)((char*)(void*)m_el + stride * n); }
	bool equal( stride_iterator const& other ) const
	{ return m_el == other.m_el; }
	ElementT& dereference() const
	{ return *m_el; }
	void increment()
	{ advance(1); }
	void decrement()
	{ advance(-1); }
	difference_type distance_to( stride_iterator const& b ) const
	{ return b.m_el - m_el; }
};

template<class ElementT>
inline stride_iterator<ElementT> make_stride_iterator( void* p, int stride, int offset )
{
	return stride_iterator<ElementT>( (ElementT*)(void*)((char*)p + offset), stride );
}
*/


class vertex_declaration
{
	D3DVERTEXELEMENT9*	decl;
//	int					count;
public:
	vertex_declaration( D3DVERTEXELEMENT9* vdecl )
	:	decl( vdecl )//, count( 0 )
	{
		//while( vdecl[ count ].Stream != 0xff )
		//	++count;
	}

	int get_position_offset( int stream = 0, int usage_index = 0 )
	{	return get_offset( D3DDECLTYPE_FLOAT3, D3DDECLUSAGE_POSITION, stream, usage_index, D3DDECLMETHOD_DEFAULT ); }

	int get_offset( D3DDECLTYPE type, D3DDECLUSAGE usage, int stream = 0, int usage_index = 0, D3DDECLMETHOD method = D3DDECLMETHOD_DEFAULT ) const
	{
		int i=0;
		while( decl[ i ].Stream != 0xff && i < MAX_FVF_DECL_SIZE )
		{
			D3DVERTEXELEMENT9 const& el = decl[ i ];
			if( el.Usage == usage && el.Type == type && el.Stream == stream && el.Method == method && el.UsageIndex == usage_index )
				return el.Offset;

			++i;
		}

		return -1;
	}
};

namespace aux
{
	vector4 STIR_ATTR_DLL calc_screen_params( flat::vector2i const& size );
	// draw rect directly passing rect coords
	void STIR_ATTR_DLL draw_rect( IDirect3DDevice9& dev, stir::flat::rectf const& rc );
	void STIR_ATTR_DLL draw_rect_uv( IDirect3DDevice9& dev, flat::rectf const& rc, flat::rectf const& uv = flat::rectf(0,0,1,1) );
//!	void draw_z_rect( device& dev, flat::rectf const& rc, effect efx, float z );
}

//!void dump_element( D3DVERTEXELEMENT9 const& el, stir::ostream& os );
//!void dump_declaration( D3DVERTEXELEMENT9 const* el, stir::ostream& os );
//!void dump_declaration( ID3DXMesh& mesh, stir::ostream& os );

} } // stir :: directx9

#endif // STIR_DX9_UTILITY_H_
