#include <stir/platform.hpp>
#include "effect_aux.hpp"
#include "dx9_utility.hpp"

namespace stir { namespace directx9 {

pass::pass( ID3DXEffect& efx_ )
:	m_efx( &efx_ )
,	current_pass( 0 )
{
	HRESULT hr = S_OK;

//	hr = m_efx->Begin( &passes, D3DXFX_DONOTSAVESAMPLERSTATE | D3DXFX_DONOTSAVESTATE | D3DXFX_DONOTSAVESHADERSTATE );
	hr = m_efx->Begin( &passes, 0 );//D3DXFX_DONOTSAVESHADERSTATE );
	if( FAILED( hr ) )
		dx_error( hr, L"Effect::Begin failed" );
	hr = m_efx->BeginPass( current_pass );
	if( FAILED( hr ) )
		dx_error( hr, L"Effect::BeginPass failed" );
}

pass::~pass()
{
	HRESULT hr = S_OK;
	if( current_pass < passes )
	{
		hr = m_efx->EndPass();
		if( FAILED( hr ) )
			dx_error( hr, L"Effect::EndPass failed" );
	}
	hr = m_efx->End();
	if( FAILED( hr ) )
		dx_error( hr, L"Effect::End failed" );
}

pass& pass::operator++()
{
	assert( current_pass < passes );
	HRESULT hr = S_OK;

	hr = m_efx->EndPass();
	if( FAILED( hr ) )
		dx_error( hr, L"Effect::EndPass failed" );
	current_pass++;
	if( current_pass < passes )
	{
		hr = m_efx->BeginPass( current_pass );
		if( FAILED( hr ) )
			dx_error( hr, L"Effect::BeginPass failed" );
	}
	return *this;
}

}}