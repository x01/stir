#ifndef STIR_DX9_EFFECT_AUX_HPP_
#define STIR_DX9_EFFECT_AUX_HPP_

#include <stir/platform.hpp>

#include <boost/noncopyable.hpp>

#include "dx9_include.hpp"

namespace stir { namespace directx9 {

struct STIR_ATTR_DLL pass : boost::noncopyable
{
	pass( ID3DXEffect& efx_ );
	~pass();

	bool valid() const
	{	return current_pass < passes; }

	unsigned get_current_pass() const
	{	return current_pass; }

	pass& operator++();
//		pass& operator--();

//		void restart();

private:
	ID3DXEffect*	m_efx;
	unsigned	current_pass;
	unsigned	passes;
};

}}

#endif