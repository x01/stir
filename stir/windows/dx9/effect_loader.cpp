#include <stir/platform.hpp>
#include <stir/filesystem.hpp>
#include <stir/io/virtual_file.hpp>
#include <stir/stir_logs.hpp>
#include <stir/basic_util.hpp>
#include <stir/language_util.hpp>
#include "effect_loader.hpp"

namespace stir { namespace directx9 {

struct virtual_file_include : public ID3DXInclude
{
	struct info
	{
		platform_path	m_directory;
		char*			m_data;
		size_t			m_size;	
	};

	typedef std::map<void const*, info>	info_container;

	virtual_file_include( platform_path const& systemDir = native_path("") ) : m_system(systemDir) {}
	virtual ~virtual_file_include() {}

	HRESULT __stdcall Open( D3DXINCLUDE_TYPE IncludeType, LPCSTR pFileName, LPCVOID pParentData, LPCVOID * ppData, UINT * pBytes )
	{
		STIR_LOG( dx ) << "ID3DXInclude::Open: " << pFileName << ": " << IncludeType << log::endl;

		info_container::iterator parentIt = m_loaded.find( pParentData );
		platform_path prefix;
		if( IncludeType == D3DXINC_SYSTEM )
			prefix = m_system;
		else
		if( parentIt != m_loaded.end() )
			prefix = parentIt->second.m_directory;

		platform_path wFilePath = pFileName;
		platform_path fullFilePath = prefix / wFilePath;
		
//		STIR_LOG( debug ) << "ID3DXInclude::Open: loading " << fullFilePath << log::endl;
		
		io::virtual_file_input in (fullFilePath);
		
		if( !in.is_good() )
			return D3DERR_NOTFOUND;
			
		info i;		
		i.m_data = new char [static_cast<size_t>(in.length ())];
		i.m_size = static_cast<size_t>(in.length ());
		i.m_directory = prefix / wFilePath.basename ();

		if (in.read (i.m_data, (size_t)in.length (), &i.m_size) != stir::io::k_ok)
			return D3DERR_NOTFOUND;

		*ppData = i.m_data;
		*pBytes = (unsigned)i.m_size;

		m_loaded.insert( std::make_pair(*ppData,i) );

		return S_OK;
	}

	HRESULT __stdcall Close( LPCVOID pData )
	{
//		STIR_LOG( debug ) << "ID3DXInclude::Close: " << log::endl;

		info_container::iterator it = m_loaded.find( pData );
		if( it != m_loaded.end() ) {
			delete it->second.m_data;
			m_loaded.erase( it );
		}

		return S_OK;
	}

	platform_path		m_system;
	info_container		m_loaded;
};

effect_loader_file::effect_loader_file( device& devctx, platform_path const& prefix, platform_path const& default_suffix, bool enablePool )
:	device_( devctx ), prefix_( prefix ), suffix_( default_suffix ), flags_( 0 )
{
	HRESULT hr;

	if( device_.is_shader_debug() )
	{
		flags_ |= D3DXSHADER_DEBUG | D3DXSHADER_SKIPOPTIMIZATION;
	}

	if( enablePool )
	{
		STIR_LOG( dx ) << "Creating pool to share effect params" << log::endl;

		hr = D3DXCreateEffectPool( &m_effect_pool );
		if( FAILED(hr) )
			STIR_LOG( dx ) << "Failed to create effect pool" << log::endl;
	}

	m_include = new virtual_file_include( L"" );
}

effect_loader_file::~effect_loader_file()
{
	m_include.reset();
}

#define ERR_EFX_USE_PS 0

static char err_efx[] =

"matrix wvp_matrix;"

"float4 vs( float4 xyzw : POSITION ) : POSITION"
"{	return mul( xyzw, wvp_matrix ); }"

"float4 ps() : COLOR"
"{	return float4( 0.7, 0.2, 0.2, 1.0 ); }"

"technique t0 {"
"	pass p0 {"
"		ZWriteEnable		= true;"
"		ZEnable				= true;"
"		CullMode			= CCW;"

"		VertexShader		= compile vs_1_1 vs();"
#if ERR_EFX_USE_PS
"		PixelShader			= compile ps_1_1 ps();"
#else
"		TextureFactor		= 0xffc03030;"
"		ColorOp[0]		= SelectArg1;"
"		ColorArg1[0]	= TFactor;"
"		ColorArg2[0]	= Current;"
"		AlphaOp[0]		= SelectArg1;"
"		ColorArg1[0]	= TFactor;"
"		ColorArg2[0]	= Current;"
"		ColorOp[1]		= Disable;"
"		AlphaOp[1]		= Disable;"
#endif
"	}"
"	pass p1 {"
"		VertexShader		= 0;"
"		PixelShader			= 0;"
"	}"
"}"
;

ID3DXEffect* effect_loader_file::create_err_effect()
{
	com_ptr<ID3DXBuffer> errors;
	com_ptr<ID3DXEffect> efx;

	HRESULT hr = S_OK;
	hr = D3DXCreateEffect(
		&device_.get_dev(),
		err_efx,
		(UINT)array_size( err_efx ),
		0, 0, flags_ | D3DXFX_NOT_CLONEABLE,
		0,
		&efx,
		&errors
	);

	if( FAILED(hr) )
	{
		char* buff = (char*)alloca (errors->GetBufferSize() + 1);
		if( errors ) {
			memcpy (buff, (char const*)errors->GetBufferPointer(), errors->GetBufferSize());
			buff [errors->GetBufferSize()] = 0;
		} else
			buff [0] = 0;
		STIR_LOG( dx ) << "Failed to compile err_effect: " << buff << log::endl;
		return 0;
	}

	return efx.detach();
}

ID3DXEffect* effect_loader_file::load( resource_id const& id, int profile )
{
	HRESULT hr = S_OK;

	com_ptr<ID3DXEffect> efx;
	com_ptr<ID3DXBuffer> errors;
	std::vector<D3DXMACRO> macro_ptr;

	platform_path path = prefix_ / id.get_id() + suffix_;

	STIR_LOG( resource ) << "Loading effect `" << path.c_str() << "', " << log::endl;
	
	if( macros_.get_macro_count() != 0 )
	{
		macro_ptr.reserve( macros_.get_macro_count() );
		macros_.create_pointers( std::back_inserter( macro_ptr ) );
	}

	bool retErrEfx = false;

	// 0x01 - allow software vertex processing effects
	if( (profile&0x01) && device_.is_mixed_vp() )
		device_.get_dev().SetSoftwareVertexProcessing( true );

	hr = D3DXCreateEffectFromFile(
		&device_.get_dev(),
		path.c_str(),
		macro_ptr.empty() ? 0 : &*macro_ptr.begin(),
		m_include.get(),
		flags_ | D3DXFX_NOT_CLONEABLE,
		m_effect_pool.get(),
		&efx,
		&errors
	);

	if( FAILED(hr) )
	{
		STIR_LOG (dx).printf ("failed to load effect: ");
		if( errors && errors->GetBufferSize() )
		{
			std::string errstr( (char*)errors->GetBufferPointer(), errors->GetBufferSize() );
			STIR_LOG (dx) << errstr << log::endl;
		} else
			STIR_LOG (dx) << "error: (" << DXGetErrorString9(hr) << ") " << DXGetErrorDescription9(hr) << log::endl;

		retErrEfx = true;
	} else
	{
		D3DXHANDLE tech;
		hr = efx->FindNextValidTechnique( NULL, &tech );
		if( FAILED( hr ) || tech == 0 )
		{
			STIR_LOG( dx ).printf ("no valid techniques found.\n");
			retErrEfx = true;
		} else
		{
			hr = efx->SetTechnique( tech );
			if( FAILED( hr ) )
			{
				STIR_LOG( dx ).printf ("failed to set technique.\n");
				retErrEfx = true;
			}
		}
	}

	if( retErrEfx )
	{
		STIR_LOG( dx ) << " Returning err_effect." << log::endl;
		if( !m_err_effect )
			m_err_effect.attach( create_err_effect() );

		efx = m_err_effect;
	}

	if( (profile&0x01) && device_.is_mixed_vp() )
		device_.get_dev().SetSoftwareVertexProcessing( false );

	return efx.detach();
}

void effect_loader_file::unload( ID3DXEffect* efx, int profile )
{
	assert (efx);
	efx->Release ();
}

}}
