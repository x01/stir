#ifndef STIR_EFFECT_LOADER_HPP_
#define STIR_EFFECT_LOADER_HPP_

#include <stir/platform.hpp>
#include <stir/resource_id.hpp>
#include <stir/resource_loader.hpp>
#include <stir/windows/dx9/dx9_include.hpp>
#include <stir/windows/dx9/device.hpp>

#include <stir/complex_types.hpp>
#include <stir/basic_util.hpp>
#include <vector>

namespace stir { namespace directx9 {

struct virtual_file_include;

class STIR_ATTR_DLL effect_loader_file : public stir::loader<resource_id,ID3DXEffect*>
{
public:
	struct macro_container
	{
		typedef std::vector<std::pair<std::string,std::string> >	macro_def_t;

		void add( char const* name, char const* value )
		{	macros.push_back( std::make_pair( name, value ) ); }

		template<class ItT>
		void create_pointers( ItT out )
		{
			for( macro_def_t::const_iterator it = macros.begin(); it != macros.end(); ++it )
			{
				D3DXMACRO m;

				m.Name		= it->first.c_str();
				m.Definition= it->second.c_str();

				*out = m;
				++out;
			}

			D3DXMACRO m;

			m.Name		= 0;
			m.Definition= 0;
			*out = m;
		}

		size_t get_macro_count() const
		{	return macros.size(); }

	private:
		macro_def_t		macros;
	};

	effect_loader_file( device& devctx, platform_path const& prefix_, platform_path const& default_suffix_, bool enablePool );
	~effect_loader_file();

	virtual ID3DXEffect* load( resource_id const& id, int profile );
	virtual void unload( ID3DXEffect* efx, int profile );
	
	macro_container& get_macros()
	{	return macros_; }

private:
	ID3DXEffect* create_err_effect();

private:
	device&						device_;
	platform_path				prefix_, suffix_;
	unsigned					flags_;
	macro_container				macros_;
	com_ptr<ID3DXEffect>		m_err_effect;
	com_ptr<ID3DXEffectPool>	m_effect_pool;
	del_ptr<virtual_file_include>		m_include;
};

}}

#endif