#include "effect_params.hpp"

namespace stir { namespace directx9 {

namespace effect_param {

char const* matrix_world				= "world_matrix";
char const* matrix_view					= "view_matrix";
char const* matrix_tex0					= "tex0_matrix";
char const* matrix_tex1					= "tex1_matrix";
char const* matrix_projection			= "projection_matrix";
char const* matrix_view_projection		= "view_projection_matrix";
char const* matrix_world_view_projection= "world_view_projection_matrix";
char const* matrix_light_0				= "light_matrix";

char const* tex_0 = "tex_0";
char const* tex_1 = "tex_1";
char const* tex_2 = "tex_2";

}

}}
