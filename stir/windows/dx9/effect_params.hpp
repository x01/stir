#ifndef STIR_EFFECT_PARAMS_HPP_
#define STIR_EFFECT_PARAMS_HPP_

namespace stir { namespace directx9 {

namespace effect_param {

extern char const* matrix_world;
extern char const* matrix_view;
extern char const* matrix_tex0;
extern char const* matrix_tex1;
extern char const* matrix_projection;
extern char const* matrix_view_projection;
extern char const* matrix_world_view_projection;
extern char const* matrix_light_0;

extern char const* tex_0;
extern char const* tex_1;
extern char const* tex_2;

}

}}

#endif // STIR_EFFECT_PARAMS_HPP_
