#ifndef STIR_LOCK_UTIL_HPP_
#define STIR_LOCK_UTIL_HPP_

#include <stir/platform.hpp>

namespace stir { namespace dx9 {

struct vb_desc : public D3DVERTEXBUFFER_DESC {
	vb_desc () {}
	vb_desc (IDirect3DVertexBuffer9& vb) {
		vb.GetDesc (this);
	}
};

template<class BufferT>
class lock_util
{
	BufferT&	b_;

public:
	vb_desc		desc_;
	size_t		current_;

	lock_util (BufferT& buf)
		:	desc_(buf), b_(buf), current_(0)
	{}

	size_t lock_data (size_t locksize, void** ptr_)
	{
		HRESULT hr;
		size_t can_lock = std::min (locksize, desc_.Size);

		assert (locksize <= desc_.Size);

		if (current_ + can_lock > desc_.Size) {
			current_ = 0;
			hr = b_.Lock (current_, can_lock, ptr_, D3DLOCK_DISCARD);
		} else
			hr = b_.Lock (current_, can_lock, ptr_, D3DLOCK_NOOVERWRITE);

		current_ += can_lock;
		return can_lock;
	}

	void unlock ()
	{
		HRESULT hr = b_.Unlock ();
	}
};

template<class BufferT>
class lock_iterator : protected lock_util<BufferT>
{
public:
	void*	lptr;
	size_t	lsize;

	lock_iterator (BufferT& buf)
	:	locku_(buf)
	{}

	bool lock (size_t total) {
		if (current_ >= total)
			return false;
		else {
			lsize = lock_data (total, lptr);
			return true;
		}
	}

	using lock_util<BufferT>::unlock;


	lock_iterator& operator++ ()
	{
		return *this;
	}
};

// example
#if 0
	size_t vtxcount = 45;
	size_t datasize = vtxcount * sizeof (vertex);
	for (lock_iterator<IDirect3DVertexBuffer9> li (*buffer); li.lock (datasize); ++li)
	{
		// iterate over locked vertex data

		li.unlock ();
	}
#endif




}}

#endif // STIR_LOCK_UTIL_HPP_
