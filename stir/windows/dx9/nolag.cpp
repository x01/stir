#include <stir/platform.hpp>
#include "nolag.hpp"

namespace stir { namespace directx9 {

nolag::nolag( device& dc ) : m_devctx(dc), m_cur_render(0)
{
	m_devctx.register_listener( *this );
}

nolag::~nolag()
{
	m_devctx.remove_listener( *this );
}

void nolag::device_reset()
{
	m_t[0] = create_render_target( m_devctx.get_dev(), 2, 2, D3DFMT_A8R8G8B8, D3DMULTISAMPLE_NONE, 0, true );
	m_t[1] = create_render_target( m_devctx.get_dev(), 2, 2, D3DFMT_A8R8G8B8, D3DMULTISAMPLE_NONE, 0, true );
}

void nolag::device_lost()
{
	m_t[0]->Release(); m_t[0] = 0;
	m_t[1]->Release(); m_t[1] = 0;
}

void nolag::rotate( bool beginScene )
{
	com_ptr<IDirect3DSurface9> zs, bs;
	HRESULT hr = S_OK;

	D3DVECTOR tri[3] = 
	{
		{ -1, 1, 0.5 },
		{  1, 1, 0.5 },
		{ -1, -1, 0.5}
	};

	IDirect3DDevice9& d = m_devctx.get_dev();
	
	// render (m_cur_rend)
	hr = d.GetDepthStencilSurface( &zs );
	hr = d.GetRenderTarget( 0, &bs );
	
	if( beginScene )
	{
		hr = d.BeginScene();
	}
	
	hr = d.SetRenderTarget( 0, m_t[m_cur_render] );
	hr = d.SetDepthStencilSurface( 0 );

	hr = d.SetTexture( 0, 0 );
	hr = d.SetFVF( D3DFVF_XYZ );
	hr = d.DrawPrimitiveUP( D3DPT_TRIANGLELIST, 1, tri, sizeof(tri[0]) );

	if( beginScene )
	{
		hr = d.EndScene();
	}
	
	hr = d.SetDepthStencilSurface( zs.get() );
	hr = d.SetRenderTarget( 0, bs.get() );
	
	// lock/read (1-m_cur_rend)
	{
		surface_scoped_lock lock( m_t[1-m_cur_render], D3DLOCK_READONLY );
		
		(void)*(volatile int*)lock.get_ptr_as<volatile int>();
	}
	
	m_cur_render = 1-m_cur_render;
}

}}
