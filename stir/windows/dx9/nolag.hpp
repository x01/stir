#ifndef STIR_NO_LAG_HPP_
#define STIR_NO_LAG_HPP_

#include "device.hpp"

namespace stir { namespace directx9 {

class nolag : boost::noncopyable, lost_reset_interface
{
public:
	nolag( device& dc );
	~nolag();
	
	virtual void device_reset();
	virtual void device_lost();
	
	void rotate( bool beginScene );

private:
	int					m_cur_render;
	device&				m_devctx;
	IDirect3DSurface9	*m_t[2];
};

}}

#endif // STIR_NO_LAG_HPP_
