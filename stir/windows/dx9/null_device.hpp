#include <d3dx9.h>

#include "../win/com_ptr.hpp"

namespace stir { namespace utility {

bool create_null_device( com_ptr<IDirect3D9>& d3d, com_ptr<IDirect3DDevice9>& dev );

}}