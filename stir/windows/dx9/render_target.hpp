#ifndef STIR_RENDER_TARGET_HPP_
#define STIR_RENDER_TARGET_HPP_

namespace stir
{

class render_target : public stir::lost_reset_interface
{
public:
	struct apply;
	friend struct apply;

	struct apply
	{
		apply( unsigned idx, unsigned level, render_target& tgt )
			:	m_devctx( tgt.dev_context ), m_prevrt( 0 ), m_prevdepthstencil( 0 ), m_rtindex( idx )
			,	m_thisrt( level, *tgt.back_buffer )
		{
			save_and_set( *rt_surface(), tgt.depth_stencil() );
		}

		apply( device_context& devctx, unsigned idx, unsigned level, IDirect3DTexture9& newBB, IDirect3DSurface9* newDS )
			:	m_devctx( devctx ), m_prevrt( 0 ), m_prevdepthstencil( 0 ), m_rtindex( idx )
			,	m_thisrt( level, newBB )
		{
			save_and_set( *rt_surface(), newDS );
		}

		~apply()
		{
			HRESULT hr = S_OK;
			hr = m_devctx.device().SetRenderTarget( m_rtindex, m_prevrt );
			m_prevrt->Release();
			
			hr = m_devctx.device().SetDepthStencilSurface( m_prevdepthstencil );
			if( m_prevdepthstencil )
				m_prevdepthstencil->Release();
		}

		IDirect3DSurface9* rt_surface() const
		{	return m_thisrt.surface(); }

	private:
		void save_and_set( IDirect3DSurface9& newBB, IDirect3DSurface9* newDS )
		{
			HRESULT hr = S_OK;

			// todo: support for iSwapChain & backbuffer
			hr = m_devctx.device().GetRenderTarget( m_rtindex, &m_prevrt );
			hr = m_devctx.device().GetDepthStencilSurface( &m_prevdepthstencil );

			hr = m_devctx.device().SetRenderTarget( m_rtindex, &newBB );
			hr = m_devctx.device().SetDepthStencilSurface( newDS );
		}

	private:
		device_context&		m_devctx;
//		render_target&		target;
		unsigned			m_rtindex;
		IDirect3DSurface9	*m_prevrt, *m_prevdepthstencil;
		texture_level_get	m_thisrt;
	};

	render_target( device_context& dctx, unsigned width_, unsigned height_, D3DFORMAT fmt, D3DFORMAT depthfmt )
		:	dev_context( dctx ), back_buffer( 0 ), depth_buffer( 0 )
		,	width( width_ ), height( height_ ), format( fmt ), depth_format( depthfmt )
	{
		dev_context.register_listener( *this );
	}

	~render_target()
	{
		dev_context.remove_listener( *this );
	}

	virtual void device_reset()
	{
		assert( !back_buffer );
		back_buffer = create_render_target_texture( dev_context.device(), width, height, format );

		assert( !depth_buffer );
		depth_buffer = create_depth_stencil( dev_context.device(), width, height, depth_format, true );
	}

	virtual void device_lost()
	{
		assert( back_buffer );
		assert( depth_buffer );
		back_buffer->Release();		back_buffer = 0;
		depth_buffer->Release();	depth_buffer = 0;
	}

	IDirect3DTexture9* texture() const
	{	return back_buffer; }
	IDirect3DSurface9* depth_stencil() const
	{	return depth_buffer; }

private:
	IDirect3DTexture9	*back_buffer;
	IDirect3DSurface9	*depth_buffer;
	device_context&		dev_context;
	unsigned			width, height;
	D3DFORMAT			format, depth_format;
};

}

#endif // STIR_RENDER_TARGET_HPP_
