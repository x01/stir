#include <stir/platform.hpp>
#include <stir/filesystem.hpp>
#include <stir/io/virtual_file.hpp>
#include "texture.hpp"
#include "texture_loader.hpp"
//#include "../string_utils.hpp"
//#include <stir/iblock/input.hpp>

namespace stir { namespace directx9 {

//
texture_manager::texture_manager( device& dev, texture_manager::resource_loader& loader )
	:	registry_type( loader )
	,	device_( dev )
#ifdef _DEBUG
	,	lost_( true )
#endif
{
	device_.register_listener( *this );
}

texture_manager::~texture_manager()
{
	device_.remove_listener( *this );
}

void texture_manager::device_lost()
{
#ifdef _DEBUG
	assert( !lost_ );
	lost_ = true;
#endif
//	unload_resource<underlying_resource> unloader;
//!	for( resource_container::iterator it = container_.begin(); it != resources.end(); ++it ) {
//!		loaders_ [get_loader (it)] -> unload (it->second);
}

void texture_manager::device_reset()
{
#ifdef _DEBUG
	assert( lost_ );
	lost_ = false;
#endif
//!	for( resource_container::iterator it = resources.begin(); it != resources.end(); ++it )
//!		if( !(it->second = loaders_ [get_loader(it)] ->load( get_id(it), get_profile(it) ) ) )
//!			load_failed( get_id(it), get_profile(it) );
}


} } // stir :: directx9
