#ifndef STIR_TEXTURE_RESOURCE_HPP_
#define STIR_TEXTURE_RESOURCE_HPP_

#include "../../platform.hpp"
#include "../../resource_handle.hpp"
#include "../../resource.hpp"
#include "../../path.hpp"
#include "device.hpp"

namespace stir { namespace directx9 {

struct texture : public resource_handle<pair_pointer_policy<resource_id, IDirect3DTexture9*> >
{
	typedef pair_pointer_policy<resource_id, IDirect3DTexture9*> policy_type;
	typedef policy_type::holder_type holder_type;

	texture( holder_type p = 0 )
		:	resource_handle<policy_type>( p )
	{
		assert( (size_t)p == 0 || (size_t)p > 0x00010000 );
	}

#ifdef __STIR_SHOW_IN_EDITOR
	IDirect3DTexture9* operator->() const;
#endif
};

class texture_manager
:	public resource_registry<texture>
,	public lost_reset_interface
{
	typedef resource_registry<texture> registry_type;

	device&	device_;

public:
	typedef registry_type::provider_type provider;

	texture_manager( device& dev, registry_type::resource_loader& loader );
	~texture_manager();

	void device_reset();
	void device_lost();

private:
#ifdef _DEBUG
	bool	lost_;
#endif
};

typedef texture_manager::provider_type texture_provider;

}

// need to specialize in the same (stir) namespace
template<>
struct unload_resource<IDirect3DTexture9*>
{
	typedef void result_type;
	void operator()( IDirect3DTexture9*& r ) const
	{	assert( r ); r->Release(); r=0; }
};

} // stir :: directx9

#endif // STIR_MESH_RESOURCE_HPP_
