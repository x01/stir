#pragma message (">> in texture_loader.cpp")
#include <stir/platform.hpp>
#include <stir/filesystem.hpp>
#include <stir/io/virtual_file.hpp>
#include <stir/stir_logs.hpp>
#include <stir/basic_util.hpp>
#include "texture_loader.hpp"
//#include "../string_utils.hpp"
//#include <stir/iblock/input.hpp>

namespace stir { namespace directx9 {

texture_loader_file::texture_loader_file( device& devctx, platform_path const& prefix, platform_path const& suffix )
:	device_( devctx )
,	prefix_( prefix )
,	options_( 0 )
{
	loader_aux::tokenize_path (suffix, native_path(":|"), suffixes_);
	device_.register_listener( *this );
}

texture_loader_file::~texture_loader_file()
{
	device_.remove_listener( *this );
}

IDirect3DTexture9* texture_loader_file::load( resource_id const& id, int profile )
{
	HRESULT hr = S_OK;

	platform_path path = loader_aux::find_available_file (prefix_ / id.get_id(), &*suffixes_.begin (), suffixes_.size ());
	
	STIR_LOG( resource ) << "Loading texture '" << path.c_str() << "',";

	D3DXIMAGE_INFO imginfo;
	LPDIRECT3DTEXTURE9 texture = 0;
	
	stir::io::virtual_file_input reader (path);
	
	uint8_t* temp = 0;
	if (reader.is_good ())
	{
		temp = new uint8_t [(size_t)reader.length ()];
		if (reader.read (temp, (size_t)reader.length(), 0) == io::k_ok)
		{
			hr = D3DXCreateTextureFromFileInMemoryEx(
				&device_.get_dev(),
				temp,
				(size_t)reader.length (),
				0,
				0,
				0,
				0, // Usage
				D3DFMT_UNKNOWN,
				D3DPOOL_MANAGED,
				D3DX_DEFAULT,
				D3DX_DEFAULT,
				0,
				&imginfo,
				0,
				&texture
			);
		} 
		else
			hr = E_FAIL;

		delete [] temp;
	}
	else
		hr = E_FAIL;

	if( FAILED( hr ) )
	{
		if( !err_texture_ )
			err_texture_ = create_err_texture( 32 );

		err_texture_.get()->AddRef();
		STIR_LOG( resource ).printf (" not found. Returning err_texture\n");
		return err_texture_.get();
	}

	STIR_LOG( resource ).printf (" loaded: %d x %d x %d @ %s(%d)\n",
		imginfo.Width, imginfo.Height, imginfo.Depth, aux::get_fmt_string(imginfo.Format), imginfo.ImageFileFormat);

	return texture;
}

void texture_loader_file::unload( IDirect3DTexture9* resource, int profile )
{
	resource->Release ();
}

void texture_loader_file::device_lost()
{
	int d = err_texture_.release();
}

void texture_loader_file::device_reset()
{
}

com_ptr<IDirect3DTexture9> texture_loader_file::create_err_texture( int size )
{
	com_ptr<IDirect3DTexture9> defaultTexture;
	defaultTexture.attach(
		create_texture( device_.get_dev(), size, size, 1, 0, D3DFMT_A8R8G8B8, D3DPOOL_MANAGED )
	);

	if( defaultTexture )
	{
        texture_level_get texLevel( 0, *defaultTexture );
		surface_desc desc( texLevel.surface() );

		surface_scoped_lock l( texLevel.surface(), 0 );
		for( int i=0; i<size; ++i )
		{
			for( int j=0; j<size; j += 4 )
			{
			int q = i >> 2;
			int w = j >> 2;
			int a = (i >> 3) ^ (j >> 3);
			unsigned color =
				( (q&0x01) ^ (w&0x01) ) ? 0x00ff8080 : 0x00000000;
			unsigned alpha =
				(a&0x01) ? 0xff000000 : 0x80000000;

			unsigned col = color | alpha;

			std::fill(
				l.get_ptr_as<unsigned>() + l.get_pitch_for<unsigned>() * i + j,
				l.get_ptr_as<unsigned>() + l.get_pitch_for<unsigned>() * i + j + 4,
				col );
			}
		}
	}

   return defaultTexture;
}

}} // stir::directx9
