#ifndef STIR_TEXTURE_LOADER_HPP_
#define STIR_TEXTURE_LOADER_HPP_

#include <stir/platform.hpp>
#include <stir/resource_id.hpp>
#include <stir/resource_loader.hpp>
#include <stir/windows/dx9/dx9_include.hpp>
#include <stir/windows/dx9/device.hpp>

namespace stir { namespace directx9 {

class STIR_ATTR_DLL texture_loader_file : public
	loader<resource_id, IDirect3DTexture9*>,
	lost_reset_interface
{
public:
	texture_loader_file( device& devctx, platform_path const& prefix_, platform_path const& suffix );
	~texture_loader_file();

	virtual IDirect3DTexture9* load( resource_id const& id, int profile );
	virtual void unload( IDirect3DTexture9* resource, int profile );

protected:
	com_ptr<IDirect3DTexture9> create_err_texture( int size );

	virtual void device_lost();
	virtual void device_reset();

private:
	com_ptr<IDirect3DTexture9>	err_texture_;
	device&						device_;
	platform_path				prefix_;
	std::vector<platform_path>	suffixes_;
	unsigned					options_;
};

}}

#endif