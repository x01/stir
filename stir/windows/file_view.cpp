#include "file_view.hpp"

#include "win_include.hpp"

namespace stir
{

// from boost.spirit
// boost/spirit/iterator/impl/file_iterator.ipp

file_view::file_view( stir::string const& fileName )
:	ptr( 0 ), file_size( 0 )
{
	HANDLE hFile = ::CreateFile(
		fileName.c_str(),
		GENERIC_READ,
		FILE_SHARE_READ,
		NULL,
		OPEN_EXISTING,
		FILE_FLAG_SEQUENTIAL_SCAN,
		NULL
	);

	if (hFile == INVALID_HANDLE_VALUE)
		return;

	// Store the size of the file, it's used to construct
	//  the end iterator
	file_size = ::GetFileSize(hFile, NULL);

	HANDLE hMap = ::CreateFileMapping(
		hFile,
		NULL,
		PAGE_READONLY,
		0, 0,
		NULL
	);

	if (hMap == NULL)
	{
		::CloseHandle(hFile);
		return;
	}

	LPVOID pMem = ::MapViewOfFile(
		hMap,
		FILE_MAP_READ,
		0, 0, 0
	);

	if (pMem == NULL)
	{
		::CloseHandle(hMap);
		::CloseHandle(hFile);
		return;
	}

	// We hold both the file handle and the memory pointer.
	// We can close the hMap handle now because Windows holds internally
	//  a reference to it since there is a view mapped.
	::CloseHandle(hMap);

	// It seems like we can close the file handle as well (because
	//  a reference is hold by the filemap object).
	::CloseHandle(hFile);
 
	// Store the handles inside the shared_ptr (with the custom destructors)
	ptr = pMem;
}

file_view::~file_view()
{
	if( ptr )
		::UnmapViewOfFile( ptr );
}

}
