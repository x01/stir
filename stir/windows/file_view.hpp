#ifndef STIR_FILE_VIEW_HPP_
#define STIR_FILE_VIEW_HPP_

#include "../complex_types.hpp"
#include <boost/noncopyable.hpp>

namespace stir
{

// from boost.spirit
// boost/spirit/iterator/impl/file_iterator.ipp

class file_view : boost::noncopyable
{
public:
	file_view( stir::string const& fileName );
	~file_view();

	void const* get_ptr() const
	{	return ptr; }

	size_t get_size() const
	{	return file_size; }
	
	template<class T>
	T const* begin() const
	{	return static_cast<T const*>( ptr ); }
	template<class T>
	T const* end() const
	{	return static_cast<T const*>( (char const*)ptr + file_size ); }

private:
	void const*	ptr;
	size_t		file_size;
};

}

#endif // STIR_FILE_VIEW_HPP_
