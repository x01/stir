#ifndef STIR_MINIDUMP_H_
#define STIR_MINIDUMP_H_

#include <stir/platform.hpp>

#if _MSC_VER < 1300
#error "Visual C++ version not supported by mdump"
#else
#include "dbghelp.h"
#endif

typedef BOOL (WINAPI *MINIDUMPWRITEDUMP)(HANDLE hProcess, DWORD dwPid, HANDLE hFile, MINIDUMP_TYPE DumpType,
									CONST PMINIDUMP_EXCEPTION_INFORMATION ExceptionParam,
									CONST PMINIDUMP_USER_STREAM_INFORMATION UserStreamParam,
									CONST PMINIDUMP_CALLBACK_INFORMATION CallbackParam
									);

namespace stir
{

class STIR_ATTR_DLL MiniDumper
{
private:
	static TCHAR m_szAppName[128];

	static LONG WINAPI TopLevelFilter( struct _EXCEPTION_POINTERS *pExceptionInfo );

public:
	MiniDumper( LPCTSTR szAppName );
};

}

#endif
