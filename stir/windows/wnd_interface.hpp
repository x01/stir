#ifndef STIR_WND_INTERFACE_HPP_
#define STIR_WND_INTERFACE_HPP_

#include "win_include.hpp"
#include <stir/basic_types.hpp>

namespace stir
{
	class render_interface
	{
	public:
		virtual void on_render() = 0;
		virtual void on_resize( int width, int height ) = 0;
		virtual bool is_idle() const { return false; }
	};

	class mouse_listener_interface
	{
	public:
		virtual ~mouse_listener_interface() = 0 {}
		virtual void move( int x, int y ) = 0;
		virtual void button_change( int buttonindex, bool pressed ) = 0;
	};

	class keyboard_listener_interface
	{
	public:
		virtual ~keyboard_listener_interface() = 0 {}
		virtual void key_change( unsigned long key, unsigned long lparam, bool pressed ) = 0;
	};

	// msgProcessed equals to false by default, callee must set it to true to stop message processing
	class window_message_listener
	{
	public:
		virtual ~window_message_listener() = 0 {}
		virtual LRESULT window_message( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam, bool& msgProcessed ) = 0;
	};
}

#endif // STIR_WND_INTERFACE_HPP_
