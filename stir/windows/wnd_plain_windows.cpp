#include "wnd_plain_windows.hpp"

#include <stir/windows/wnd_interface.hpp>
#include <stir/stir_logs.hpp>

namespace stir {

LRESULT WINAPI app_wnd::msg_proc_helper( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	if( msg == WM_CREATE ) {
		//GWL_USERDATA
		CREATESTRUCT* cs = (CREATESTRUCT*)lParam;
		SetWindowLongPtr( hWnd, 0, LONG(LONG_PTR(cs->lpCreateParams)) );
		return ((app_wnd*)LONG_PTR(cs->lpCreateParams))->msg_proc( hWnd, msg, wParam, lParam );
	} else
	{
		ULONG_PTR ptr = ULONG_PTR( GetWindowLongPtr( hWnd, 0 ) );
		if( ptr ) {
			app_wnd* p = (app_wnd*)ptr;
			return p->msg_proc( hWnd, msg, wParam, lParam );
		} else
			return DefWindowProc( hWnd, msg, wParam, lParam );
	}
}

app_wnd::app_wnd( unsigned style ) 
:	window_handle( 0 ), wndclass( 0 ), hinstance( 0 ), event_listener( 0 ), mouse_listener( 0 ), message_listener( 0 )
,	sizing( false ), accel( 0 ), m_fwdmsg( true ), m_style( style ), m_has_stored_rect(false)
{}

bool app_wnd::create( HINSTANCE hinst, stir::string const& title_, int x, int y, int w, int h, bool fullscreenStyle )
{
	hinstance = hinst;
	title = title_;

	WNDCLASSEX wc = {
		sizeof(WNDCLASSEX),
		CS_DBLCLKS|CS_CLASSDC,
		&app_wnd::msg_proc_helper,
		0L, // clsextra
		sizeof( INT_PTR ), // wndextra
		hinstance,
		NULL, // icon
		CopyCursor( LoadCursor( NULL, IDC_ARROW ) ),
//				NULL,//LoadCursor( 0, MAKEINTRESOURCE( IDC_ARROW ) ),
		NULL, NULL,
		L"stir_app_wnd",
		NULL
	};
	wndclass = RegisterClassEx( &wc );

	// Create the application's window
	DWORD exflags = 0;
//#ifndef _DEBUG
//	exflags = WS_EX_TOPMOST;
//#endif
	window_handle = CreateWindowEx( exflags, (LPCTSTR)wndclass, title_.c_str(),
		fullscreenStyle ? 0 : (WS_OVERLAPPEDWINDOW & ~WS_MAXIMIZEBOX & ~WS_THICKFRAME),
		x, y, w, h, 0, NULL, hinstance, (LPVOID)this );

	return true;
}

void app_wnd::resize_client_rect( flat::vector2i const& size )
{
	::SetWindowPos( window_handle, HWND_NOTOPMOST, 0, 0, size.x, size.y, SWP_NOZORDER );
	adjust_to_fit( size.x, size.y );
}

void app_wnd::set_current_style( unsigned style )
{
	::SetWindowLongPtr( window_handle, GWL_STYLE, (LONG)(LONG_PTR)style );
	STIR_LOG( debug ).printf( L"set_style: 0x%0x\n", style);
}

unsigned app_wnd::get_current_style() const
{
	LONG_PTR dwStyle = ::GetWindowLongPtr(window_handle, GWL_STYLE);
	return dwStyle;
}

void app_wnd::set_current_exstyle( unsigned style )
{
	::SetWindowLongPtr( window_handle, GWL_EXSTYLE, (LONG)(LONG_PTR)style );
	STIR_LOG( debug ).printf (L"set_exstyle: 0x%0x", style);
}

unsigned app_wnd::get_current_exstyle() const
{
	LONG_PTR dwStyle = ::GetWindowLongPtr(window_handle, GWL_EXSTYLE);
	return dwStyle;
}

void app_wnd::set_mouse_listener( mouse_listener_interface* ml )
{
	mouse_listener = ml;
}

void app_wnd::set_keyboard_listener( keyboard_listener_interface* kl )
{
	keyboard_listener = kl;
}

void app_wnd::set_message_listener( window_message_listener* msgl )
{
	message_listener = msgl;
}

void app_wnd::show_window( unsigned cmd )
{
	ShowWindow( window_handle, cmd );
	UpdateWindow( window_handle );
}

void app_wnd::set_icon( HICON icon, bool smallicon )
{
	SetClassLongPtr( window_handle, smallicon ? GCLP_HICONSM : GCLP_HICON, LONG(LONG_PTR(icon)) );
}

HMENU app_wnd::set_menu( HMENU menu )
{
	HMENU prevMenu = GetMenu( window_handle );
	SetMenu( window_handle, menu );
	return prevMenu;
}

void app_wnd::set_accelerator( HACCEL acc )
{
	accel = acc;
}

void app_wnd::destroy()
{
//	assert( ::IsWindow( window_handle ) );
	DestroyWindow( window_handle );
	window_handle = 0;

    UnregisterClass( (LPCTSTR)wndclass, hinstance );
}

void app_wnd::set_title( stir::string const& title_ )
{
	title = title_;
	SetWindowText( window_handle, title.c_str() );
}

unsigned app_wnd::run( stir::render_interface& processor )
{
	event_listener = &processor;

	MSG msg;
	memset( &msg, 0, sizeof(msg) );

	while( msg.message != WM_QUIT )
	{
		if( PeekMessage( &msg, 0, 0U, 0U, PM_REMOVE ) )
		{
			if( accel && TranslateAccelerator( msg.hwnd, accel, &msg ) )
				continue;

			TranslateMessage( &msg );
			DispatchMessage( &msg );
		} else {
			if( !processor.is_idle() )
				processor.on_render();
		}
	}

	event_listener = 0;
	message_listener = 0;

	return 0;
}

void app_wnd::adjust_to_fit( unsigned w, unsigned h )
{
	HMENU hMenu = GetMenu( window_handle );

	RECT wrc;
	GetWindowRect( window_handle, &wrc );

	RECT rc = { 0, 0, w, h };
	if( AdjustWindowRect( &rc, get_current_style(), hMenu != 0 ) )
	{
		MoveWindow( window_handle, wrc.left, wrc.top, rc.right - rc.left, rc.bottom - rc.top, true );
//		SetWindowPos( window_handle, HWND_NOTOPMOST, rc.left, rc.top, rc.right - rc.left, rc.bottom - rc.top, SWP_NOZORDER );
	}
}

//void add_mouse_listener( stir::mouse_listener_interface& listener )
//{
//}

LRESULT WINAPI app_wnd::msg_proc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	if( message_listener && m_fwdmsg )
	{
		bool processed = false;
		LRESULT res = message_listener->window_message( hWnd, msg, wParam, lParam, processed );
		if( processed )
			return res;
	}

	switch( msg )
	{
		case WM_DESTROY:
			{
				STIR_LOG( debug ).printf (L"WM_DESTROY: posting quit message\n");
				PostQuitMessage( 0 );
				return 0;
			}
			break;

		case WM_ENTERSIZEMOVE:
			{
				GetWindowRect( hWnd, &rect_on_sizing_begin );
				int ow = rect_on_sizing_begin.right - rect_on_sizing_begin.left, oh = rect_on_sizing_begin.bottom - rect_on_sizing_begin.top;
				sizing = true;
//				STIR_LOG( debug ) << L"WM_ENTERSIZEMOVE: dimensions (" << ow << ", " << oh << ")" << std::endl;
			}
			break;

		case WM_EXITSIZEMOVE:
			{
				sizing = false;
				if( m_fwdmsg )
					send_resize( hWnd );
//				STIR_LOG( debug ) << "WM_EXITSIZEMOVE: dimensions (" << w << ", " << h << ")" << std::endl;
			}
			break;

		case WM_MOUSEMOVE:
			{
				if( mouse_listener && m_fwdmsg )
					mouse_listener->move( LOWORD( lParam ), HIWORD( lParam ) );
			}
			break;

		case WM_LBUTTONDOWN:
			{
				if( mouse_listener && m_fwdmsg )
					mouse_listener->button_change( 1, true );
			}
			break;

		case WM_LBUTTONUP:
			{
				if( mouse_listener && m_fwdmsg )
					mouse_listener->button_change( 1, false );
			}
			break;

		case WM_RBUTTONDOWN:
			{
				if( mouse_listener && m_fwdmsg )
					mouse_listener->button_change( 2, true );
			}
			break;

		case WM_RBUTTONUP:
			{
				if( mouse_listener && m_fwdmsg )
					mouse_listener->button_change( 2, false );
			}
			break;

		case WM_MBUTTONDOWN:
			{
				if( mouse_listener && m_fwdmsg )
					mouse_listener->button_change( 3, true );
			}
			break;

		case WM_MBUTTONUP:
			{
				if( mouse_listener && m_fwdmsg )
					mouse_listener->button_change( 3, false );
			}
			break;

		case WM_KEYDOWN:
			{
				if( keyboard_listener && m_fwdmsg )
					keyboard_listener->key_change( wParam, lParam, true );
			}
			break;

		case WM_KEYUP:
			{
				if( keyboard_listener && m_fwdmsg )
					keyboard_listener->key_change( wParam, lParam, false );
			}
			break;

/*		case WM_SETCURSOR:
			if( m_fwdmsg )
			{
				if( event_listener )
				{
					if( event_listener->on_command_message( wndmsg_setcursor, wParam, lParam ) )
						return true;
				}
				else
					return FALSE;
			}
			break;

		case WM_ACTIVATE:
			if( event_listener && event_listener->on_command_message( wndmsg_activate, wParam, lParam ) && m_fwdmsg )
				return 0;
			break;

		case WM_SIZE:
			if( event_listener && m_fwdmsg )
			{
				if( !sizing && (wParam != SIZE_MINIMIZED || wParam == SIZE_RESTORED ) )//&& (wParam == SIZE_MAXIMIZED || wParam == SIZE_RESTORED) )
					send_resize( hWnd );

				if( event_listener->on_command_message( wndmsg_size, wParam, lParam ) )
				{
					return 0;
				}
				
				return 0;
			}
			break;*/
	}

	return DefWindowProc( hWnd, msg, wParam, lParam );
}

void app_wnd::send_resize( HWND hWnd )
{
	RECT rc;
	GetWindowRect( hWnd, &rc );

	STIR_LOG( game ).printf (L"before-resize exstyle: 0x%08x\n", get_current_exstyle());
	int w = rc.right - rc.left, h = rc.bottom - rc.top;
	int ow = rect_on_sizing_begin.right - rect_on_sizing_begin.left, oh = rect_on_sizing_begin.bottom - rect_on_sizing_begin.top;

	if( w != ow || h != oh )
	{
		STIR_LOG( debug ).printf (L"send_resize: dimensions (%d, %d)\n");

		if( event_listener )
			event_listener->on_resize( w, h );
	}

	STIR_LOG( game ).printf (L"after-resize exstyle: 0x%08x\n", get_current_exstyle());
}

void app_wnd::store_rect()
{
	GetWindowRect( window_handle, &m_rect_stored );
	m_has_stored_rect = true;
}

void app_wnd::restore_rect()
{
	if( m_has_stored_rect )
	{
		SetWindowPos( window_handle, HWND_NOTOPMOST,
			m_rect_stored.left, m_rect_stored.top,
			m_rect_stored.right - m_rect_stored.left, m_rect_stored.bottom - m_rect_stored.top,
			SWP_SHOWWINDOW );
	}
}

} // stir
