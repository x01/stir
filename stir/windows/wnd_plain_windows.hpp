#ifndef STIR_WND_PLAIN_WINDOWS_HPP_
#define STIR_WND_PLAIN_WINDOWS_HPP_

#include "win_include.hpp"
#include "wnd_interface.hpp"

#include <stir/basic_types.hpp>
#include <stir/flat_types.hpp>
#include <stir/stir_logs.hpp>

namespace stir
{
	class STIR_ATTR_DLL app_wnd
	{
		static LRESULT WINAPI msg_proc_helper( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam );

	public:
		static const unsigned default_style = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX;
	
		app_wnd( unsigned style );

		bool create( HINSTANCE hinst, stir::string const& title_, int x, int y, int w, int h, bool fullscreenStyle );
		void destroy();
		void stop_msg_forwarding() { m_fwdmsg = false; }
		
		void store_rect();
		void restore_rect();

		void set_mouse_listener( mouse_listener_interface* ml );
		void set_keyboard_listener( keyboard_listener_interface* kl );
		void set_message_listener( window_message_listener* msgl );

		void set_accelerator( HACCEL acc );
		void set_icon( HICON icon, bool smallicon );
		void set_title( stir::string const& title_ );
		unsigned get_style() const { return m_style; }

		void set_current_style( unsigned style );
		unsigned get_current_style() const;
		void set_current_exstyle( unsigned style );
		unsigned get_current_exstyle() const;

		HMENU set_menu( HMENU menu );
		void resize_client_rect( flat::vector2i const& size );

		void show_window( unsigned cmd = SW_SHOWDEFAULT );

		HWND get_hwnd()
		{	return window_handle; }

		unsigned run( stir::render_interface& processor );

		void adjust_to_fit( unsigned w, unsigned h );

	protected:
		LRESULT WINAPI msg_proc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam );
		void send_resize( HWND hWnd );

	private:
		stir::render_interface*	event_listener;
		stir::mouse_listener_interface*	mouse_listener;
		stir::keyboard_listener_interface* keyboard_listener;
		stir::window_message_listener* message_listener;
		stir::string			title;
		HWND					window_handle;
		ATOM					wndclass;
		HINSTANCE				hinstance;
		HACCEL                  accel;
		RECT					rect_on_sizing_begin, m_rect_stored; // store window rect when e.g. switching to fullscreen to be able to restore it later
		bool					sizing, m_fwdmsg, m_has_stored_rect;
		int						m_style;
	};
}

#endif // STIR_WND_PLAIN_WINDOWS_HPP_
