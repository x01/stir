#include <CoreFoundation/CoreFoundation.h>
#include <CoreServices/CoreServices.h>
#include <QuickLook/QuickLook.h>

#include <stir/asset/texture_asset.hpp>
#include <pulmotor/archive.hpp>
#include <stir/pulmotor_archive.hpp>
#include <stir/path.hpp>

#import <AppKit/NSImage.h>
#import <AppKit/NSGraphics.h>
#import <AppKit/NSGraphicsContext.h>

#include <iostream>

extern "C" OSStatus GeneratePreviewForURL(void *thisInterface, QLPreviewRequestRef preview, CFURLRef url, CFStringRef contentTypeUTI, CFDictionaryRef options);
extern "C" void CancelPreviewGeneration(void *thisInterface, QLPreviewRequestRef preview);

/* -----------------------------------------------------------------------------
   Generate a preview for file

   This function's job is to create preview for designated file
   ----------------------------------------------------------------------------- */

std::string cf2std(CFStringRef cfStr)
{
	if (char const *p = CFStringGetCStringPtr(cfStr, kCFStringEncodingUTF8))
		return p;
	else
	{
		size_t len = CFStringGetLength(cfStr);
		char* buffer = (char*)alloca (len*4);
		if (CFStringGetCString(cfStr, buffer, len * 4, kCFStringEncodingUTF8))
			return buffer;
		else
			return "";
	}
}

OSStatus GeneratePreviewForURL(void *thisInterface, QLPreviewRequestRef preview, CFURLRef url, CFStringRef contentTypeUTI, CFDictionaryRef options)
{
	using namespace stir;
	
	CFStringRef cfpath = CFURLCopyFileSystemPath(url, kCFURLPOSIXPathStyle);
	path path = cf2std (cfpath);
	CFRelease(cfpath);
	
    // To complete your generator please implement the function GeneratePreviewForURL in GeneratePreviewForURL.c
	asset::texture_asset texA;
	std::error_code ec;
	pulmotor::cfile_input_buffer in (path.c_str(), ec);
	if (ec)
		return noErr;
	
	pulmotor::input_archive inA (in);
//	pulmotor::debug_archive<pulmotor::input_archive> inAD (inA, std::cout);
	pulmotor::archive(inA, texA);
	if (inA.ec)
		return noErr;
	
	CGSize canvasSize = { static_cast<CGFloat>(texA.width), static_cast<CGFloat>(texA.height) };
	
	CGContextRef cgContext = QLPreviewRequestCreateContext(preview, *(CGSize *)&canvasSize, false, NULL);
	if(cgContext) {
		NSGraphicsContext* context = [NSGraphicsContext graphicsContextWithGraphicsPort:(void *)cgContext flipped:YES];
		if(context) {
			
			CGColorSpaceRef colorSpace  = CGColorSpaceCreateDeviceRGB();
			
			std::vector<CGImageRef> levels;
			for (auto inImg : texA.levels)
			{
				CGDataProviderRef provider  = CGDataProviderCreateWithData(NULL, inImg.pixels, inImg.size_bytes(), NULL);
				CGImageRef img = CGImageCreate(inImg.width, inImg.height, 8, inImg.comp << 3, inImg.pitch,
											   colorSpace, kCGBitmapByteOrderDefault, provider,
											   NULL, FALSE, kCGRenderingIntentDefault);
				levels.push_back (img);
				CGDataProviderRelease(provider);
			}
			CGColorSpaceRelease(colorSpace);
			
			[NSGraphicsContext setCurrentContext:context];
			for (auto img : levels)
			{
				NSImage* nsImg = [[NSImage alloc] initWithCGImage:img size: NSZeroSize];
				[nsImg drawAtPoint:NSZeroPoint
						  fromRect:NSMakeRect(0,0,[nsImg size].width, [nsImg size].height)
						 operation:NSCompositeSourceOver
						  fraction:1.0];
			}
		}
		QLPreviewRequestFlushContext(preview, cgContext);
		CFRelease(cgContext);
	}
	
    return noErr;
}

void CancelPreviewGeneration(void *thisInterface, QLPreviewRequestRef preview)
{
    // Implement only if supported
}
