#include <string>
#include <string.h>
#include <cassert>
#include <fstream>

#include <stir/log.hpp>
STIR_DEFINE_LOG (info, "info");
STIR_DEFINE_LOG (error, "error");

#include <boost/any.hpp>
#include <stir/dynamic_array.hpp>

//#include <stir/atlas/image_atlas.hpp>
#include <stir/atlas/img_atlas.hpp>
#include <stir/atlas/atlas_common.hpp>
#include <stir/atlas/storage-atlas.hpp>
#include <stir/atlas/storage_access-atlas.hpp>
#include <stir/basic_util.hpp>
#include <stir/filesystem.hpp>
#include <stir/wildcard.hpp>

#include <stir/flat_types.hpp>
//#include <stir/ser/stir.hpp>
#include <stir/path.hpp>

#include <pulmotor/archive.hpp>
#include <pulmotor/stream.hpp>
#include <pulmotor/archive_std.hpp>

#include <stb_image.h>
#include <stir/pulmotor_archive.hpp>


using namespace stir;

char const* g_version = "0.1";

void infof( char const* msg, ... )
{
	va_list vl;
	va_start (vl, msg);
	vprintf (msg, vl);
}

void errorf( char const* msg, ... )
{
	va_list vl;
	va_start (vl, msg);
	vprintf (msg, vl);
}

struct options
{
	options ()
		:	atlas_width (1024), atlas_height(1024), border(0)
		,	optimize (true), allow_nonpow2(false), flip_v (false), endianess (0)
	{
	}

	std::string	atlas_name;
	int			atlas_width, atlas_height, border;
	bool		optimize, allow_nonpow2, flip_v;
	int			endianess;
	stir::path outdir, logname;
	std::vector<stir::path>	images;
	int verbose = 0;
	
	void expand_filenames ()
	{
		std::vector<path> img;
		img.swap(images);
		
		images.reserve (img.size());
		for (path const& p : img)
		{
			if (p.filename().string().find("*") == std::string::npos)
			{
				images.push_back (p);
				continue;
			}
			
			for (filesystem::directory_iterator it (p), end; it != end; ++it)
			{
				if (p == "." || p == "..")
					continue;
				
				path const& entry = *it;
				if (wildcard::match(entry.c_str(), p.c_str()))
					images.push_back (entry);
			}
		}
	}
};

bool handle_bool (char const* str)
{
	using namespace std;
	if (strcmp (str, "yes") == 0)
		return true;
	if (strcmp (str, "true") == 0)
		return true;
	if (strcmp (str, "1") == 0)
		return true;

	return false;
}

int parse_options (options& opts, int count, char* input[])
{
	for (int i=1; i<count;)
	{
		std::string arg = input [i++];
		std::string opt;

		if (arg.find_first_of ('-') == 0)
		   opt = arg.substr (1);
		else if (arg.find ("--") == 0)
		   opt = arg.substr (2);

		if (opt == "width" || opt == "w") {
			if (i == count)
				return 1;
			opts.atlas_width = atoi (input[i++]);
		} else if (opt == "help") {
			printf(
				"Usage:\n"
			   	"   atlas_tool [-v] [-w N] [-h N] [-border N] [-nonpow2 bool]\n"
				"              [-outdir path] [-log path] [-image path] [-name string] <path> [path]...\n"
				"\n"
				"Atlas tool. 2009-2013 (C) x01\n"
				"\n"
				"-w, -width      max atlas width (2048)\n"
				"-h, -height     max atlas height (2048)\n"
				"-b, -border     pad each image with a border of specified width\n"
				"    -nonpow2    allow non power of two final size\n"
				"-o, -outdir     name output directory\n"
				"-l, -log        write all created file to a log\n"
				"    -image      specify image (in case name starts with -)\n"
//				"-e, -endianess  0 - little endian (default), 1 - big endian\n"
				"    -name       name of the atlas (default = atlas)\n"
				"    -fv         flip V of the UVs\n"
				"    -v          print diagnostics (1: +atlas info, 2: +serialization)\n"
			);
			return 2;

		} else if (opt == "height" || opt == "h") {
			if (i == count)
				return 1;
			opts.atlas_height = atoi (input[i++]);
		} else if (opt == "border" || opt == "b") {
			if (i == count)
				return 1;
			opts.border = atoi (input[i++]);
		} else if (opt == "nonpow2") {
			if (i == count)
				return 1;
			opts.allow_nonpow2 = handle_bool (input[i++]);
		} else if (opt == "name") {
			if (i == count)
				return 1;
			opts.atlas_name = input[i++];
		} else if (opt == "outdir" || opt == "o") {
			if (i == count)
				return 1;
			opts.outdir = input[i++];
		} else if (opt == "log") {
			if (i == count)
				return 1;
			opts.logname = input[i++];
		} else if (opt == "image") {
			if (i == count)
				return 1;
			opts.images.push_back (input[i++]);
		} else if (opt == "endian" || opt == "e") {
			if (i == count)
				return 1;
			opts.endianess = atoi (input[i++]);
		} else if (opt == "fv") {
			opts.flip_v = true;
		} else if (opt == "v" || opt == "verbose") {
			opts.verbose++;
		} else if (opt.empty()) {
			opts.images.push_back (arg);
		} else
		{
			infof ("Invalid commad line near '%s'\n", arg.c_str());
			return 1;
		}
	}

	return 0;
}

void lfun (stir::log::log_name_t const& name, stir::log::log_message_t const& msg)
{
	infof ((name + ": " + msg + "\n").c_str());
}

struct original_info
{
	stir::path name;
};

size_t serialize_atlas (stir::storage::image_atlas::atlas& st_atlas, path const& desc_pathname, std::error_code& ec, options const& opts)
{
	pulmotor::cfile_output_buffer outBuf (desc_pathname.c_str(), ec);
	if (ec)
		return 0;
	
	pulmotor::output_archive outA (outBuf);
	if (opts.verbose >= 2) {
		pulmotor::debug_archive<pulmotor::output_archive> outAD (outA, std::cout);
		pulmotor::archive (outAD, st_atlas);
	} else
		pulmotor::archive (outA, st_atlas);
	
	ec = outA.ec;
	return outA.offset();
}

int main (int argc, char** argv)
{
	STIR_LOG_ACCESS (stir::atlas_info).add_write_function (lfun);
	STIR_LOG_ACCESS (stir::atlas_error).add_write_function (lfun);

	options opts;
	int result = parse_options (opts, argc, argv);
	switch (result) {
		case 0:
			if (opts.images.empty())
			{
				errorf ("No images provided. Use -help to see available options\n");
				return 1;
			}

			if (opts.atlas_name.empty ())
				opts.atlas_name = "atlas";
			
			break;
		case 1:
			errorf ("Program is unable to continue. Use -help to see available options\n");
			return 1;
		case 2:
			return 0;
			break;
	}
	
	opts.expand_filenames();

	std::list<stir::image> loaded_imgs;
	stir::image_atlas atlas (opts.atlas_width, opts.atlas_height, opts.border);
	
	// load images
	if (opts.verbose >= 1)
		infof ("Atlasing %d files\n", opts.images.size ());
	
	for (size_t i=0; i<opts.images.size (); ++i)
	{
		stir::path const& p = opts.images[i];
	
		int x, y, comp;
		//		infof (" - loading '%s' (%04.1f %s)\n", p.c_str(), (float)i / opts.images.size () * 100.0f, "%");
		stbi_uc* pix = stbi_load (p.c_str (), &x, &y, &comp, 4);

		if (!pix)
		{
			infof (" Loading %s (%d/%d) FAILED\n", p.c_str(), i, (int)opts.images.size());
			continue;
		}
		
		loaded_imgs.emplace_back(x, y, 4);
		loaded_imgs.back().put(0, 0, stir::image_ref(x, y, pix, 4));
		
		original_info oi;
		oi.name = p.stem().string();
		atlas.add(loaded_imgs.back(), boost::any (oi));
	}
	
	atlas.build_atlas();
	
	// write images to disk
	//	infof ("Writing files out\n");

	if (!opts.outdir.empty ())
	{
		if (!stir::filesystem::create_directories (opts.outdir))
		{
			errorf ("Failed to create %s for output\n", opts.outdir.c_str());
			return 1;
		}
	}
	
	storage::image_atlas::atlas st_atlas;
	st_atlas.pages.resize(atlas.pages.size());
	
	std::vector<storage::image_atlas::section> tmpSec;
	
	for (size_t i=0; i<atlas.pages.size(); ++i)
	{
		atlas_page& p = *atlas.pages[i];
		
		path page_name = opts.atlas_name + "_" + std::to_string(p.index);
//		path page_filepath = opts.outdir / page_name + ".tga";
//		
//		stir::save_image(page_filepath, *p.img);
		
		float px = 1.0f / p.img->width, py = 1.0f / p.img->height;
		
		st_atlas.pages[i] = new storage::image_atlas::page();
		st_atlas.pages[i]->name = page_name.c_str();
		st_atlas.pages[i]->pixel_size = vector2f(px, py);
		st_atlas.pages[i]->texture = new asset::texture_asset();
		asset::texture_builder builder (*st_atlas.pages[i]->texture, p.img->width, p.img->height, asset::rgba8);
		builder.align(16);
		builder.add_level(*p.img);
		builder.finish();
		
		for (auto node : *p.maker)
		{
			if (!node.used)
				continue;
			
			image_info const& ii = *node.get_load<image_info*> ();
			
			storage::image_atlas::section sec;
			
			sec.area = ii.area(px, py);
			sec.page_index = p.index;
			sec.group_index = 0;
			sec.original_name = boost::any_cast<original_info>(ii.attr).name.c_str();
			tmpSec.push_back (sec);
		}
	}
	
	st_atlas.sections.assign(tmpSec.data(), tmpSec.size());

	// make sure the sections are sorted so that storage::find_section works
	std::sort (st_atlas.sections.begin (), st_atlas.sections.end (),
		   storage::image_atlas::less_by_name_and_group () );

	if (opts.verbose >= 1)
	{
		for (size_t i=0; i<st_atlas.sections.size (); ++i)
		{
			storage::image_atlas::section const& s = st_atlas.sections[i];

			infof (" [%3d / %2d]  {%6.3f,%6.3f, %6.3f,%6.3f} %20s (%d)\n", i, s.group_index,
				s.area.p0.x, s.area.p0.y, s.area.p1.x, s.area.p1.y,
				s.original_name.c_str(),
				s.page_index
			);
		}
	}
	
	path desc_pathname = opts.outdir / opts.atlas_name + ".atlas";
	
//	infof ("Writing atlas description '%s'\n", desc_pathname.c_str());
	
	std::error_code ec;
	size_t written = serialize_atlas (st_atlas, desc_pathname, ec, opts);
	if (ec)
		printf ("Failed to write %s, error %d (%s)\n", desc_pathname.c_str(), ec.value(), ec.message().c_str());
	else
		printf ("File '%s' written, %d bytes\n", desc_pathname.c_str(), (int)written);

	return 0;
}
