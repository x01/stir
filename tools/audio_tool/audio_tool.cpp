#include <iostream>
#include <iterator>
#include <vector>

#include <boost/scoped_array.hpp>

#include <stir/language_util.hpp>
#include <stir/asset/audio_asset.hpp>
#include <stir/audio/wave_reader.hpp>
#include <stir/path.hpp>
#include <pulmotor/stream.hpp>
#include <pulmotor/archive.hpp>
#include <pulmotor/stream.hpp>

using namespace stir;

struct op_copy_16bit
{
	typedef u16 output_type;
	static output_type convert (void const* p) {
		return *(u16*)p;
	}
};

struct op_s2u_16bit
{
	typedef u16 output_type;
	static output_type convert (void const* p) {
		return *(u16*)p + 32768;
	}
};

struct op_s16_to_float
{
	typedef float output_type;
	static output_type convert (void const* p) {
		s16 in = *(s16*)p;
		return float(in + 32768) / 65536.0f;
	}
};

template<class SampleOp>
void copy_interleaved(void* dest, size_t destStride, void const* in_data, size_t inStride, size_t sampleCount, size_t channelCount)
{
	typedef typename SampleOp::output_type o_t;
	void const* cur = in_data;

	for (unsigned i=0; i<sampleCount; ++i)
	{
		for (unsigned c=0; c<channelCount; ++c)
		{
			o_t v = SampleOp::convert (cur);
			*(o_t*)dest = v;
			dest = (u8*)dest + destStride;
			cur = (u8 const*)cur + inStride;
		}
	}
}

struct options
{
	options ()
		:	ofmt (stir::asset::wave_u16)
		,	quality(0)
	{}
	stir::asset::audio_format	ofmt;
	stir::path opath;
	stir::path ipath;
	int quality;
};

int main (int narg, char** parg)
{
	using namespace stir;
	using namespace std;

	bool showhelp = false;
	options opts;
	char** input = parg;
	int count = narg;
	for (int i=1; i<count; ) {
		std::string arg = input[i++];
		std::string opt;
		if (arg.find_first_of ('-') == 0)
		   opt = arg.substr (1);
		else if (arg.find ("--") == 0)
		   opt = arg.substr (2);
		else {
			opts.ipath = arg;
			continue;
		}

		if (opt == "fmt") {
			if (i == count)
				return 1;

			string v (input[i++]);

			struct { char const* fmt; asset::audio_format id; asset::audio_compression cmpr; } fmts[3] = {
				{ "wave_u16", asset::wave_u16, asset::audio_compression_none },
				{ "wave_s16", asset::wave_s16, asset::audio_compression_none },
				{ "celt", asset::wave_compressed, asset::audio_compression_celt },
			};

			for (size_t i=0; i<sizeof(fmts)/sizeof(fmts[0]); ++i)
				if (v == fmts[i].fmt) {
					opts.ofmt = fmts[i].id;
					break;
				}
		} else if (opt == "o" || opt == "output") {
			if (i == count)
				return 1;
			opts.opath = input[i++];
		} else if (opt == "quality") {
			if (i == count)
				return 1;
			opts.quality = atoi (input[i++]);
		} else if (opt == "help" || opt == "h") {
			showhelp = true;
			break;
		}
	}

	if (narg < 2 || showhelp)
	{
		printf ("Usage: audio_tool [-fmt wave_s16|wave_u16|celt] [-quality N] [-o output-name] <input-wave>\n");
		return 1;
	}

	if (opts.opath.empty())
		opts.opath = opts.ipath.stem () + ".saud";

	// LOAD THE INPUT and CONVERT to OUTPUT
//	filesystem::handle inWaveH (opts.ipath);
//	if (!inWaveH.good ()) {
//		printf ("Failed to read '" STIR_SPFF "'\n", opts.ipath.c_str());
//		return 1;
//	}

	std::error_code ec;
	std::vector<u8> inWave;
	pulmotor::util::read_file(opts.ipath.c_str(), inWave, ec);
	if (ec) {
		printf ("Failed to read '" STIR_SPFF "'\n", opts.ipath.c_str());
		return 1;
	}

	asset::wave::fmt_chunk const* w_fmt = 0;
	asset::wave::data_chunk const* w_data = 0;

	asset::wave::wave_result parseResult = asset::wave::parse_header (inWave.data (), inWave.size(), &w_fmt, &w_data);
	if (parseResult != asset::wave::status_ok) {
		printf ("Unsupported WAVE file, error code: %d\n", parseResult);
		return 1;
	}

	if (w_fmt->audio_format != 1) {
		printf ("Unsupported waveform compression file (uncompressed only), format: %d\n", w_fmt->audio_format);
		return 1;
	}

	if (w_fmt->bits_per_sample != 16) {
		printf ("Only 16-bit audio is supported, in file: %d\n", w_fmt->bits_per_sample);
		return 1;
	}

	unsigned channelCount = w_fmt->channel_count;
	unsigned inSampleSize = w_fmt->bits_per_sample;
	unsigned sampleCount = w_data->size / (inSampleSize >> 3);

	stir::asset::audio_asset a;
	a.version = 0;
	a.format = opts.ofmt;
	a.compression = stir::asset::audio_compression_none;
	a.channel_count = channelCount;
	a.flags = stir::asset::audio_flag_channels_interleaved;
	a.sample_size = w_fmt->bits_per_sample;
	a.sampling_rate = w_fmt->sample_rate;
	memset (&a.reserved, 0, sizeof(a.reserved));

	a.channel_data.resize (1);

	asset::audio_channel& chan = a.channel_data[0];

	size_t outSampleSize =
		a.format == asset::wave_s16 || a.format == asset::wave_u16 ? 16
	   	: 32;
	size_t outSize = sampleCount * channelCount * outSampleSize >> 3;
	chan.data.resize (outSize, 0);
	void* outPtr = &*chan.data.begin ();
	chan.duration = sampleCount;

	switch (a.format)
	{
		case asset::wave_s16:
			copy_interleaved<op_copy_16bit> (outPtr, 2, w_data->data_ptr (), 2, sampleCount, channelCount);
			break;

		case asset::wave_float:
			copy_interleaved<op_s16_to_float> (outPtr, 4, w_data->data_ptr (), 2, sampleCount, channelCount);
			break;

		case asset::wave_compressed:
			break;
	}
	
	pulmotor::memory_archive_container_t bytes;
	pulmotor::memory_write_archive archO (bytes);
	pulmotor::archive (archO, a);
	size_t written = pulmotor::util::write_file(opts.opath.c_str(), bytes.data(), bytes.size());
	
		
//	size_t written = pulmotor::util::write_file (opts.opath.c_str(), a, pulmotor::target_traits::current, 8);
	printf ("file '%s' written, %lu bytes (%.2f s, format:%s, %d channel(s), %d bits/sample)\n",
			opts.opath.c_str(), written, a.seconds (), asset::format_name((asset::audio_format)a.format), a.channel_count, a.sample_size);
	
	return 0;
}
