#include <ft2build.h>
#include FT_FREETYPE_H

#include <string>
#include <string.h>
#include <cassert>
#include <fstream>
#include <sstream>

#include <stir/log.hpp>
STIR_DEFINE_LOG (info, "info");
STIR_DEFINE_LOG (error, "error");

#include <boost/any.hpp>

#include <stir/atlas/image_atlas.hpp>
#include <stir/atlas/atlas_common.hpp>
#include <stir/atlas/font_image_loader.hpp>
#include <stir/basic_util.hpp>

#include <stir/flat_types.hpp>
#include <stir/ser/stir.hpp>
#include <stir/gui/font_data.hpp>
#include <stir/path.hpp>
#include <stir/filesystem.hpp>

#include <pulmotor/ser.hpp>

#include <stb_image.h>

FT_Library    library;
FT_Face       face;

char const* g_version = "0.9";
bool g_write = true;

void infof(char const* msg, ... )
{
	if (g_write)
	{
		va_list vl;
		va_start (vl, msg);
		
		vprintf (msg, vl);
	}
}

void errorf(char const* msg, ... )
{
	if (g_write)
	{
		va_list vl;
		va_start (vl, msg);
		
		vprintf (msg, vl);
	}
}

std::string to8bit (std::wstring const& a)
{
	std::string ret;
	ret.reserve (a.size());
	for (unsigned i=0; i<a.size(); ++i)
		if (a[i] < 32 || a[i]>127)
			ret += '_';
		else
			ret += a[i] & 0x7f;
	return ret;
}

struct options
{
	enum {
		kFmtA8,
		kFmtRGBA32,
		kFmtDXT1,
		kFmtDXT5
	};
	
	struct group {
		group() {}
		group(std::string const& n, int b, int e)
		:	name(n), begin_code(b), end_code(e)
		{}
		
		std::string	name;
		int	begin_code, end_code;
	};
	
	typedef std::vector<group> group_container;
	
	int					font_size, format;
	stir::platform_path	font_name, outdir;
	stir::platform_path	atlas_name;
	group_container		groups;
	
	bool				optimize, allow_nonpow2, include_kerning, verbose, dump_text;
	
	int					atlas_width, atlas_height, border, dfield;

	options ()
	:	font_size (18), format (kFmtA8), optimize (true), allow_nonpow2 (false), include_kerning (false), verbose (false), dump_text (false)
	,	atlas_width (1024), atlas_height (1024), border (0), dfield (0)
	{}
};

stir::string to_string (int n) {
	stir::stringstream ss;
	ss << n;
	return ss.str();
}

typedef std::map<int,std::vector<stir::platform_path> > group_name_map_t;

void write_to_disk (stir::platform_path const& outdir, stir::platform_path const& filename, stir::image_atlas::atlas_images_container& images, group_name_map_t& groupnames, options const& opts)
{
	stir::platform_path extension = ".tga";
	std::map<int,int> image_indexes;
	int imgcount = 0, imgsuccess = 0;
	for (stir::image_atlas::atlas_images_container::iterator it = images.begin(); it != images.end(); ++it)
	{
		size_t imagegroup = it->group;
		
		stir::platform_path suffix = imagegroup >= opts.groups.size()
			? to_string(imagegroup)
			: opts.groups[imagegroup].name;
		
		stir::platform_path filename = opts.atlas_name + "_" + suffix + "_" + to_string(image_indexes [it->group]);
		
		groupnames [imagegroup].push_back (filename);
		image_indexes [imagegroup] ++;
		
		stir::platform_path completename = outdir / filename + extension;
		infof ("    writing image: '%s'...", completename.c_str());
		
		int saveresult = stbi_write_tga(
										const_cast<char*>( completename.c_str() ),
										it->page->width,
										it->page->height,
										it->page->components,
										it->page->data);
		imgcount++;
		
		if (saveresult) {
			infof ("OK\n");
			imgsuccess++;
		} else
			errorf ("Failed\n");
	}
	
	infof ("  %d out of %d successfully images written\n", imgsuccess, imgcount);
}

inline int iequal (std::string const& a, char const* b)
{
	char const* pa = a.c_str ();
	while (*pa && *b)
	{
		int diff = tolower(*pa++) - tolower(*b++);
		if (diff != 0)
			return diff;
	}
	return 0;
}

int str2num (std::string const& a)
{
	char const* endptr = a.c_str() + a.size();
	int res = strtol (a.c_str(), (char**)&endptr, 0);
	return res;
}

int parse_options (options& opts, int count, char** input)
{
	for (int i=1; i<count; )
	{
		std::string arg = input[i++];
		std::string opt;
		if (arg.find_first_of ('-') == 0)
		   opt = arg.substr (1);
		else if (arg.find ("--") == 0)
		   opt = arg.substr (2);

		if (opt == "font")
		{
			if (i == count)
				return 1;
			opts.font_name = input [i++];
		} else if (opt == "h" || opt == "help")
		{
			printf(
				"Font builder. (C) 2007-2011 x01\n"
				"Usage:\n"
				"  font_builder <[-font] tff-pathname> [-height N] [-format X]\n"
				"               [-group name:first:last] [-text] [-dfield N]\n"
				"               [-nonpot] [-atlas_height N] [-atlas_width N] [-atlas atlas_name]\n"
				"               [-border N] [-kerning] [-v] [-o path]\n"
				"\n"
				"-h, -help    This help message.\n"
				"    -font    Path name to a font file (ttf).\n"
				"    -height  Font height in pixels.\n"
				"    -format  Pixel format of the images written (a8|rgba32)\n"
				"    -kerning Include kerning information if available\n"
				"    -dfield  Generate distance field images (subsampling size, off by default)\n"
				"-v, -verbose Log serialization gather and write\n"
				"-o, -outdir  Destinatin folder\n"
			);
			return 2;
		} else if (opt == "g" || opt == "group")
		{
			if (i == count)
				return 1;

			std::string v = input[i++], gn, gf, gl;
			size_t col1p = v.find_first_of (':', 0);
			if (col1p == std::string::npos)
				return 1;

			size_t col2p = v.find_first_of (':', col1p + 1);
			if (col2p == std::string::npos)
				return 1;

			options::group g;
			g.name = v.substr (0, col1p);
			g.begin_code = str2num (v.substr (col1p + 1, col2p - col1p - 1));
			g.end_code = str2num (v.substr (col2p + 1));
			opts.groups.push_back (g);
		} else if (opt == "height")
		{
			if (i == count)
				return 1;
			opts.font_size = str2num (input[i++]);
		} else if (opt == "dfield")
		{
			if (i == count)
				return 1;
			opts.dfield = str2num (input[i++]);
			if (opts.dfield > 32) {
				printf ("Distance field subsampling value should not exceed 32\n");
				return 1;
			}
		} else if (opt == "o" || opt == "outdir")
		{
			if (i == count)
				return 1;
			opts.outdir = input[i++];
		} else if (opt == "f" || opt == "format")
		{
			if (i == count)
				return 1;
			std::string fmt = input[i++];
			if (iequal(fmt,"rgba32") == 0)
				opts.format = options::kFmtRGBA32;
			else
				opts.format = options::kFmtA8;
		} else if (opt == "v" || opt == "verbose")
		{
			opts.verbose = true;
		} else if (opt == "nonpot")
		{
			opts.allow_nonpow2 = true;
		} else if (opt == "kerning")
		{
			opts.include_kerning = true;
		} else if (opt == "text")
		{
			opts.dump_text = true;
		} else if (opt == "aw" || opt == "atlas_width")
		{
			if (i == count)
				return 1;
			opts.atlas_width = str2num (input[i++]);
		} else if (opt == "ah" || opt == "atlas_height")
		{
			if (i == count)
				return 1;
			opts.atlas_height = str2num (input[i++]);
		} else if (opt == "atlas" || opt == "atlas_name")
		{
			if (i == count)
				return 1;
			opts.atlas_name = input[i++];
		} else if (opt == "border")
		{
			if (i == count)
				return 1;
			opts.border = str2num (input[i++]);
		}
	}
	
	return 0;
}

void lfun (stir::log::log_name_t const& name, stir::log::log_message_t const& msg)
{
	infof ((name + ": " + msg + "\n").c_str());
}

stir::image_atlas::atlas_image const& find_atlas_image (stir::image_atlas::atlas_images_container const& atlas_images, stir::atlas_maker::node const* node)
{
	for (stir::image_atlas::atlas_images_container::const_iterator it = atlas_images.begin (), end = atlas_images.end (); it != end; ++it)
	{
		stir::image_atlas::page_info const& pi = *it->page;
		
		stir::image_atlas::page_info::node_container_t::const_iterator nit = std::find (pi.nodes_.begin (), pi.nodes_.end (), node);
		if (nit != pi.nodes_.end())
			return *it;
		
	}
	
	throw std::runtime_error ("Unable to find built atlas image containing requested source image. Integrity error");
}


float const ftfp = 1.0f / 64.0f;

int main (int argc, char** argv)
{
	try {
		
		options opts;
		switch (parse_options (opts, argc, argv))
		{
			case 0:
				{
					bool cont = true;
					if (opts.font_name.empty ())
						cont = false;

					if (!cont)
					{
						errorf ("Program is unable to continue. Use -h to see available options\n");
						return 1;
					}
					
					if (opts.groups.empty ())
					{
						infof ("No groups specified, adding default - ascii:32:255\n");
						opts.groups.push_back (options::group("ascii", 32, 255));
					}
					
					if (opts.atlas_name.empty ())
					{
						opts.atlas_name = opts.font_name.stem ();
					}
				}
				break;
			case 1:
				errorf ("Invalid option on the command line. Use -h to see available options\n");
				return 0;
			case 2:
				return 0;
		}
		
		if (opts.verbose)
		{
			STIR_LOG_ACCESS (stir::atlas_info).add_write_function (lfun);
			STIR_LOG_ACCESS (stir::atlas_error).add_write_function (lfun);
			//g_write=true;
		}
		
		//
		// Initialize FT2 and create glyphs
		//
		FT_Error error;
		
		error = FT_Init_FreeType( &library );
		if (error)
			throw stir::atlas_detail::ft_error (error, "Failed to initialize FreeType");
		
		stir::image_atlas ta (opts.atlas_width, opts.atlas_height, opts.border);
		
		// load images
		infof ("Font pathname: '%s'\n", opts.font_name.c_str() );
		infof ("  loading images (generating font glyphs)\n");
		
		std::vector<int> allglyphs;
		stir::atlas_detail::ftloader::font_info fontinfo;
		stir::atlas_detail::ftloader::font_image_generator fontgen (library, opts.font_name.c_str(), opts.font_size);
		
		for (size_t grpi=0; grpi<opts.groups.size(); ++grpi)
		{
			options::group const& group = opts.groups[grpi];
			infof ("  generating group [%d] '%s', %d-%d\n", grpi, group.name.c_str(), group.begin_code, group.end_code);
			
			fontgen.get_font_info (fontinfo);
			
			for (int i=group.begin_code; i<=group.end_code; ++i)
			{
				int b = opts.border;
				try
				{
					stir::atlas_detail::ftloader::glyph g;
					fontgen.load_glyph (g, i);

					// make a distance field
					
					stir::atlas_detail::image const& img = *g.image;

					allglyphs.push_back (g.char_code);
					
					stir::storage::font_data::glyph fg;
					
					fg.code_	= g.char_code;
					fg.texture_	= 0; // fill-in later
					fg.uv_		= stir::areaf (0,0,0,0); // fill-in later
					fg.size_	= stir::vector2f (g.width * ftfp + b*2, g.height * ftfp + b*2);
					fg.bearing_	= stir::vector2f (g.bearingx * ftfp + b, g.bearingy * ftfp + b);
					fg.advance_	= g.advance * ftfp;
					
					ta.add_image (grpi, img.width, img.height, img.pixels,
								  img.components == 1 ? true : false,
								  boost::any(fg) );
					
				} catch (stir::atlas_detail::ft_error const& e) {
					errorf ("  Exception while loading %d (0x%03x) symbol; '%s', error code: %d\n", i, i, e.what(), e.error);
				}
			}
		}

		stir::image_atlas::group_container groups;
		ta.order_atlases (groups);
		ta.optimize_atlas (groups);
		
		stir::image_atlas::atlas_images_container atlas_images;
		ta.build_atlas_images (atlas_images, groups, 1, opts.allow_nonpow2, false);
		
		infof ("Built: %d atlas image(s)\n", atlas_images.size());
		
		// write images to disk
		infof ("Writing files out\n");
		if (stir::filesystem::is_directory (opts.outdir))
			stir::filesystem::create_directories (opts.outdir);
		group_name_map_t group_tex_names;
		write_to_disk (opts.outdir, opts.atlas_name, atlas_images, group_tex_names, opts);
	
		// prapare info structure	
		using namespace stir;
		std::vector<storage::font_data::glyph> glyphs;
		std::vector<storage::font_data::kern> kern_pairs;
		std::vector<stir::castring>				tex_names;
		
		// get kerning for all chars
		if (opts.include_kerning)
		{
			for (size_t i=0; i<allglyphs.size(); ++i)
			{
				for (size_t j=0; j<allglyphs.size(); ++j)
				{
					vector2i ki = fontgen.get_kerning (i, j);
					if (ki.x != 0)
					{
						stir::storage::font_data::kern k;
						k.code_a_ = allglyphs[i];
						k.code_b_ = allglyphs[j];
						k.kerning_ = ki.x / 64.0f;
						kern_pairs.push_back (k);
						infof ("    kerning: %lc, %lc -> %f\n", allglyphs[i], allglyphs[j], k.kerning_);
					}
				}
			}
		}
		
		for (group_name_map_t::iterator it = group_tex_names.begin(), end = group_tex_names.end(); it != end; ++it)
			for (int j=0, sz = it->second.size(); j<sz; ++j)
				tex_names.push_back (stir::castring(it->second[j].c_str()));
		
		for (stir::image_atlas::group_container::iterator groupIt = groups.begin(); groupIt != groups.end(); ++groupIt)
		{
			stir::image_atlas::page_container& pages = groupIt->second;
			for (stir::image_atlas::page_container::iterator pageIt = pages.begin(), pageEndIt = pages.end(); pageIt != pageEndIt; ++pageIt)
			{
				stir::atlas_maker& am = **pageIt;
				// size_t pageIndex = std::distance (pages.begin(), pageIt);
				
				for (stir::atlas_maker::iterator imgIt = am.begin(), imgEndIt = am.end(); imgIt != imgEndIt; ++imgIt)
				{
					stir::atlas_maker::node& node = *imgIt;
					if (!node.has_load())
						continue;
					
					stir::image_atlas::image_info const& ii = *node.get_load<stir::image_atlas::image_info*>();
					
					storage::font_data::glyph gi = boost::any_cast<storage::font_data::glyph const&> (ii.attributes);
					
					//					gi.uv_	= stir::recti (node.r.x, node.r.y, node.r.w, node.r.h);
					stir::image_atlas::atlas_image const& ai = find_atlas_image (atlas_images, &node);
					float kx = 1.0f / ai.page->width, ky = 1.0f / ai.page->height;

					float ox = 0.5f * kx;
					float oy = 0.0f;//0.5f * ky;

					int bw = ii.offset_border_x (node.r.w);
					int bh = ii.offset_border_y (node.r.h);
					
					// ogl: 0.5/l + i/l (i:pixel index, l: tex-size)
					gi.uv_.p0.x		= ox + (node.r.x) * kx;
					gi.uv_.p0.y		= oy + (node.r.y) * ky;
					gi.uv_.p1.x		= ox + (node.r.x + node.r.w) * kx;
					gi.uv_.p1.y		= oy + (node.r.y + node.r.h) * ky;				

//					gi.uv_.p0.x		= ox + (node.r.x + bw) * kx;
//					gi.uv_.p0.y		= oy + (node.r.y + bh) * ky;
//					gi.uv_.p1.x		= ox + (node.r.x + bw + ii.width) * kx;
//					gi.uv_.p1.y		= oy + (node.r.y + bh + ii.height) * ky;				
					
					stir::platform_path const& filename = group_tex_names [ii.group_id][ii.atlas_page_index];
					std::vector<stir::castring>::iterator texIt = std::find (tex_names.begin (), tex_names.end (), stir::castring (filename.c_str ()));
					size_t texNameIndex = std::distance (tex_names.begin (), texIt);
					gi.texture_ = texNameIndex;
					
					// add glyph
					glyphs.push_back (gi);
				}
			}
		}
		
		std::sort (glyphs.begin (), glyphs.end (), storage::font_data::glyph_code_less());

		if (opts.dump_text)
		{
			printf ("font info:\n"
					"  size: %d (between lines %5.2f)\n"
					"  ascender: %5.2f\n"
					"  descender: %5.2f\n",
					opts.font_size, fontinfo.text_height * ftfp, fontinfo.ascender * ftfp, fontinfo.descender * ftfp);

			for (size_t i=0; i<tex_names.size (); ++i)
			{
				printf ("tex[%lu]: %s\n", i, tex_names[i].c_str());
			}

			for (size_t i=0; i<glyphs.size(); ++i)
			{
				storage::font_data::glyph const& g = glyphs[i];
				printf ("glyph[%3lu] (%lc): code:%d, sz:%f,%f; bear:%f,%f, adv:%f, uv:%f,%f,%f,%f, tex:%d\n",
						i, g.code_, g.code_,
						g.size_.x, g.size_.y, g.bearing_.x, g.bearing_.y, g.advance_,
						g.uv_.p0.x, g.uv_.p0.y, g.uv_.p1.x, g.uv_.p1.y,
			 			g.texture_
					  );
			}
		}
		
		// create font data
		storage::font_data fd (opts.font_size, fontinfo.text_height * ftfp, fontinfo.ascender * ftfp, fontinfo.descender * ftfp, glyphs.size (), tex_names.size (), kern_pairs.size ());
		
		fd.set_glyphs (&*glyphs.begin (), glyphs.size ());
		if (!kern_pairs.empty ())
			fd.set_kern_pairs (&*kern_pairs.begin (), kern_pairs.size ());
		fd.set_tex_names (&*tex_names.begin (), tex_names.size ());
		
		// write description file
		infof ("  writing out atlas description:\n");
		
		{
			if (!stir::filesystem::create_directories (opts.outdir))
				infof ("Failed to create '%s'\n", opts.outdir.c_str());

			stir::platform_path outname = opts.outdir / opts.atlas_name + ".stirfont";
			std::auto_ptr<pulmotor::basic_output_buffer> po = pulmotor::create_plain_output (outname.string());
			std::vector<unsigned char> buffer;
		
			pulmotor::blit_section bs;
			bs | fd;

			bs.dump_gathered ();
		
			bs.write_out (buffer, pulmotor::target_traits::current);
			
			size_t written = 0;
			po->write (&*buffer.begin (), buffer.size (), &written);
			
			infof ("    font info: %d bytes written\n", written);
		}
		
		/*
		 {
		 std::string filename = opts.atlas_name + ".txt";
		 std::fstream descfile (filename.c_str(), std::ios_base::trunc|std::ios_base::out);
		 
		 stir::atlas_detail::ftloader::font_info fi;
		 
		 descfile << ":groups" << "\n";
		 for (options::group_container::iterator groupIt = opts.groups.begin(); groupIt != opts.groups.end(); ++groupIt)
		 {
		 descfile << groupIt->name << " " << groupIt->begin_code << "\n";
		 }
		 
		 descfile << ":kerning" << "\n";
		 
		 descfile << ":glyphs ";
		 descfile << fontinfo.text_height;
		 descfile << " ";
		 descfile << fontinfo.max_advance;
		 descfile << "\n";
		 
		 descfile << description.str();
		 }*/
		
		FT_Done_FreeType( library );
		
	} catch ( stir::atlas_detail::ft_error& e )
	{
		printf ("error: (%d) %s\n", e.error, e.what());
		printf ("terminating...\n");
	}
	
	return 0;
}
