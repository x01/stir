#include <rapidxml.hpp>

#include <map>
#include <string>
#include <ostream>
#include <iostream>
#include <istream>
#include <fstream>
#include <sstream>
#include <list>
#include <vector>
#include <set>
#include <regex>
#include <unordered_map>

using namespace rapidxml;

int g_verbosity = 1;
void dolog (int level, char const* fmt, ...) {
	if (g_verbosity >= level) {
		va_list vl;
		va_start(vl, fmt);
		char buf[1024];
		vsnprintf(buf, 1024, fmt, vl);
		std::cerr << buf;
		va_end(vl);
	}
}

template<class Cont, typename ExtractString>
std::string join(Cont const& c, char const* sep, ExtractString makeString = [](auto param) -> std::string { return param; }) {
	std::stringstream ss;
	for(auto it = c.begin(), end = c.end(); it != end; ) {
		ss << makeString (*it);
		++it;
		if (it != end)
			ss << sep;
	}
	return ss.str();
}

struct Group
{
	struct Less {
		struct is_transparent {};

		bool operator()(Group const& a, Group const& b) const { return a.m_name < b.m_name; }
		bool operator()(Group const& a, std::string const& b) const { return a.m_name < b; }
		bool operator()(std::string const& a, Group const& b) const { return a < b.m_name; }
	};

	std::string m_name;
	mutable std::set<std::string> m_defined_enums;
	std::vector<std::string> m_aliases;
	bool m_use_group = true;
	bool m_declared_in_enums = false;
	bool m_is_bitfield = false;

	bool is_bitfield() const { return m_is_bitfield; }
	bool use_group() const { return m_use_group; }

	Group () {}
	//Group (bool useGroup, std::string const& name) : m_name(name), m_use_group(useGroup) {}
	Group (std::string const& name, std::initializer_list<char const*> enums) : m_name(name), m_defined_enums(enums.begin(), enums.end()) {}

	template<class ItT>
	std::vector<std::string> filter (ItT begin, ItT end) const {
		std::vector<std::string> result;

		std::set_intersection (
			m_defined_enums.begin (), m_defined_enums.end (), 
			begin, end,
			std::back_inserter (result)
		);

		return std::move (result);
	}

	bool parse (xml_node<>& groupsN);
	static std::string fix_group_name(std::string s) {
		if (!s.empty() && islower(s[0]))
		   	s[0] = toupper(s[0]);
		return s;
	}
};

struct Feature
{
	std::string m_api, m_name, m_version;
	std::vector<std::string> m_types, m_commands, m_enums; // must be sorted

	void sort () {
		std::sort (m_types.begin (), m_types.end());
		std::sort (m_commands.begin (), m_commands.end());
		std::sort (m_enums.begin (), m_enums.end());
	}

	bool parse (xml_node<>& featureN);
	bool parse_require(xml_node<>& reqN);
};

struct Command
{
	// arg can be either simply a value, or an enum
	struct Param
	{
		std::string type;
		std::string group;
		std::string name;
		std::string len;
	};

	struct Less {
		struct is_transparent {};

		bool operator()(Command const& a, Command const& b) const { return a.m_name < b.m_name; }
		bool operator()(Command const& a, std::string const& b) const { return a.m_name < b; }
		bool operator()(std::string const& a, Command const& b) const { return a < b.m_name; }
	};

	std::string processed_name() const {
		if (m_name.find("gl") == 0)
			return m_name.substr (2);
		return m_name;
	}

	std::string full_return_value() const {
		return m_ret_type;
	}

	bool is_void_return() const {
		return m_ret_type == "void";
	}

	std::string m_name;
	std::string m_ret_type;
	std::vector<Param> m_params;
	std::vector<std::string> m_aliases;

	bool parse (xml_node<>& cmdsN);
	bool parse_command (xml_node<>& cmdN);
};

struct Enum
{
	struct Less {
		struct is_transparent {};

		bool operator()(Enum const& a, Enum const& b) const { return a.m_value < b.m_value; }
		bool operator()(Enum const& a, unsigned b) const { return a.m_value < b; }
		bool operator()(unsigned a, Enum const& b) const { return a < b.m_value; }
	};

	unsigned m_value;
	std::string m_name;

	bool parse (xml_node<>& enumN);
};

struct Registry
{
	std::list<Feature> m_features;

	typedef std::unordered_map<std::string, Enum const*> EnumLookupMap;
	typedef std::multiset<Enum, Enum::Less> EnumMultiset;
	EnumMultiset m_enums;
	EnumLookupMap m_enum_lookup;

	std::set<Group, Group::Less> m_groups, m_internal;
	std::set<Group, Group::Less> mutable m_combined; 

	typedef std::vector<Command const*> CmdAliasContainer;
	CmdAliasContainer m_empty_cmd_container;
	std::map<Command const*, CmdAliasContainer> m_cmd_aliases;
	std::set<Command, Command::Less> m_commands;

	Enum const* lookup_enum (std::string const& e) const {
		auto it = m_enum_lookup.find(e);
		return it == m_enum_lookup.end() ? nullptr : it->second;
	}

	// returns 
	CmdAliasContainer const& find_aliases (Command const* cmd) const {
		auto it = m_cmd_aliases.find (cmd);
		if (it == m_cmd_aliases.end()) return m_empty_cmd_container;
		return it->second;
	}

	typedef std::pair<EnumMultiset::iterator, EnumMultiset::iterator> EnumRange;
	EnumRange find_enum (unsigned value) {
		return m_enums.equal_range(value);
	}

	// GL type -> { "c++ type", "code that uses the value' }
	struct ConvertInfo
	{
		std::string m_cpp_type;
		std::string m_unfold_expr;
	};
	std::map<std::string, ConvertInfo> m_convert_types;

	// Contains groups that should be renamed to 'fixed' names
	std::map<std::string, std::string> m_group_rename;

	// Some funcitons are not properly declared for GLenums. So just override here
	struct FixInfo {
		struct Param {
			int m_index;
			std::string m_group;
		};
		std::string m_command;
		std::vector<Param> m_params;
	};

	struct FixLess {
		struct is_transparent {};

		bool operator()(FixInfo const& a, FixInfo const& b) const { return a.m_command < b.m_command; }
		bool operator()(FixInfo const& a, std::string const& b) const { return a.m_command < b; }
		bool operator()(std::string const& a, FixInfo const& b) const { return a < b.m_command; }
	};

	std::set<FixInfo, FixLess> m_fix_info;

	FixInfo const* find_fix_info(std::string const& cmd) const {
		auto it = m_fix_info.find (cmd);
		if (it == m_fix_info.end())
			return nullptr;
		return &*it;
	}

	void disable_group(std::initializer_list<char const*> grps)
	{
		for (auto name : grps){
			Group g;
			g.m_name = name;
			g.m_use_group = false;
			m_internal.insert (g);
		}
	}

	Registry ()
	{
		m_fix_info.insert (FixInfo { "glCreateShader", { FixInfo::Param { 0, "ShaderType" } } });
		m_fix_info.insert (FixInfo { "glBlendEquation", { FixInfo::Param { 0, "BlendEquationMode" } } });
		m_fix_info.insert (FixInfo { "glActiveTexture", { FixInfo::Param { 0, "TextureUnit" } } });
		m_fix_info.insert (FixInfo { "glBindBuffer", { FixInfo::Param { 0, "BufferTarget" } } });
		m_fix_info.insert (FixInfo { "glBindFramebuffer", { FixInfo::Param { 0, "BufferBinding" } } });
		m_fix_info.insert (FixInfo { "glGetActiveAttrib", { FixInfo::Param { 5, "AttributeType" } } });
		m_fix_info.insert (FixInfo { "glGetFramebufferAttachmentParameteriv", { FixInfo::Param { 2, "AttachmentPName" } } });
		m_fix_info.insert (FixInfo { "glVertexAttribPointer", { FixInfo::Param { 2, "VertexAttribPointerType" } } });
		m_fix_info.insert (FixInfo { "glGetShaderiv", { FixInfo::Param { 1, "GetShaderPName" } } });

		m_convert_types.insert (std::make_pair("Boolean", ConvertInfo { "bool", "%s ? GL_TRUE : GL_FALSE" } ));

		disable_group(
			{
				"BufferOffset", "BufferSize", "Texture", "Boolean"
		   	}
		);

		m_internal.insert (Group("VertexAttribPointerType", {	"GL_BYTE", "GL_UNSIGNED_BYTE", "GL_SHORT", "GL_UNSIGNED_SHORT", "GL_FIXED", "GL_FLOAT" }));

		m_internal.insert (Group("TextureTarget",
			{	"GL_TEXTURE_CUBE_MAP",
				"GL_TEXTURE_CUBE_MAP_POSITIVE_X", "GL_TEXTURE_CUBE_MAP_NEGATIVE_X", "GL_TEXTURE_CUBE_MAP_POSITIVE_Y",
			   	"GL_TEXTURE_CUBE_MAP_NEGATIVE_Y", "GL_TEXTURE_CUBE_MAP_POSITIVE_Z", "GL_TEXTURE_CUBE_MAP_NEGATIVE_Z"
			}
		));
		m_internal.insert (Group("BlendEquationMode", { "GL_FUNC_ADD", "GL_FUNC_SUBTRACT", "GL_FUNC_REVERSE_SUBTRACT"}));
		m_group_rename.insert ({"BlendEquationModeEXT", "BlendEquationMode"});

		m_internal.insert (Group("TextureUnit",
		{
		"GL_TEXTURE0", "GL_TEXTURE1", "GL_TEXTURE2", "GL_TEXTURE3", "GL_TEXTURE4",
	   	"GL_TEXTURE5", "GL_TEXTURE6", "GL_TEXTURE7", "GL_TEXTURE8", "GL_TEXTURE9",
		"GL_TEXTURE10", "GL_TEXTURE11", "GL_TEXTURE12", "GL_TEXTURE13", "GL_TEXTURE14",
	   	"GL_TEXTURE15", "GL_TEXTURE16", "GL_TEXTURE17", "GL_TEXTURE18", "GL_TEXTURE19", 
		"GL_TEXTURE20", "GL_TEXTURE21", "GL_TEXTURE22", "GL_TEXTURE23", "GL_TEXTURE24",
	   	"GL_TEXTURE25", "GL_TEXTURE26", "GL_TEXTURE27", "GL_TEXTURE28", "GL_TEXTURE29", 
		"GL_TEXTURE30", "GL_TEXTURE31"
		}));
		m_internal.insert (Group("BufferTarget", { "GL_ARRAY_BUFFER", "GL_ELEMENT_ARRAY_BUFFER" } ));
		m_internal.insert (Group("BufferBinding", { "GL_FRAMEBUFFER", "GL_RENDERBUFFER" } ));
		m_internal.insert (Group("AttributeType", { "GL_FLOAT", "GL_FLOAT_VEC2", "GL_FLOAT_VEC3", "GL_FLOAT_VEC4", "GL_FLOAT_MAT2", "GL_FLOAT_MAT3", "GL_FLOAT_MAT4" } ));

		m_internal.insert (Group("FramebufferTarget", { "GL_FRAMEBUFFER" } ));
		m_internal.insert (Group("FramebufferAttachment", { "GL_COLOR_ATTACHMENT0", "GL_DEPTH_ATTACHMENT", "GL_STENCIL_ATTACHMENT" } ));
		
		m_internal.insert (Group("RenderbufferTarget", { "GL_RENDERBUFFER" } ));
		m_internal.insert (Group("RenderbufferPName", { "GL_RENDERBUFFER_WIDTH", "GL_RENDERBUFFER_HEIGHT", "GL_RENDERBUFFER_INTERNAL_FORMAT",
				   "GL_RENDERBUFFER_RED_SIZE", "GL_RENDERBUFFER_GREEN_SIZE", "GL_RENDERBUFFER_BLUE_SIZE", "GL_RENDERBUFFER_ALPHA_SIZE",
				   "GL_RENDERBUFFER_DEPTH_SIZE", "GL_RENDERBUFFER_STENCIL_SIZE"
			} ));
		m_fix_info.insert (FixInfo { "glGetRenderbufferParameteriv", { FixInfo::Param { 1, "RenderbufferPName" } } });

		m_fix_info.insert (FixInfo { "glRenderbufferStorage", { FixInfo::Param { 1, "InternalFormat" } } });
		

		m_internal.insert (Group("AttachmentPName",
			{
				"GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE",
				"GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME",
				"GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL",
				"GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE"
			}));

		m_internal.insert (Group("DrawElementsType", { "GL_UNSIGNED_BYTE", "GL_UNSIGNED_SHORT" } ));

		m_internal.insert (Group("StencilFaceDirection", { "GL_FRONT", "GL_BACK", "GL_FRON_ANT_BACK" } ));

		m_group_rename.insert ({"PixelInternalFormat", "InternalFormat"});
		m_internal.insert (Group ("InternalFormat", {"GL_RGBA4", "GL_RGB5_A1", "GL_RGB565", "GL_DEPTH_COMPONENT16", "GL_STENCIL_INDEX8"}));

		Group bufferMask("ClearBufferMask", { "GL_COLOR_BUFFER_BIT", "GL_DEPTH_BUFFER_BIT", "GL_STENCIL_BUFFER_BIT" });
		bufferMask.m_is_bitfield = true;
		m_internal.emplace (bufferMask);

		// This conflicts with glStencilOp function binding
		m_group_rename.insert ({"StencilOp", "StencilOper"});
		m_internal.insert (Group ("StencilOper",
			{
				"GL_DECR", "GL_INCR", "GL_INVERT", "GL_KEEP", "GL_REPLACE", "GL_ZERO"
			}));
		
		m_group_rename.insert ({"BlendFuncSeparateParameterEXT", "BlendFuncSeparateParameter"});
		m_internal.insert (Group ("BlendFuncSeparateParameter",
			{
				"GL_ZERO", "GL_ONE",
				"GL_SRC_COLOR", "GL_ONE_MINUS_SRC_COLOR",
				"GL_DST_COLOR", "GL_ONE_MINUS_DST_COLOR",
				"GL_SRC_ALPHA", "GL_ONE_MINUS_SRC_ALPHA",
				"GL_DST_ALPHA", "GL_ONE_MINUS_DST_ALPHA",
				"GL_CONSTANT_COLOR", "GL_ONE_MINUS_CONSTANT_COLOR",
				"GL_CONSTANT_ALPHA", "GL_ONE_MINUS_CONSTANT_ALPHA",
				"GL_SRC_ALPHA_SATURATE1",
			}));

		m_group_rename.insert ({"BufferTargetARB", "BufferTarget"});
		m_group_rename.insert ({"BufferUsageARB", "BufferUsage"});
		m_internal.insert (Group ("BufferUsage", { "GL_STATIC_DRAW", "GL_DYNAMIC_DRAW", "GL_STREAM_DRAW" }));

		m_internal.insert (Group ("GetShaderPName", { "GL_SHADER_TYPE", "GL_DELETE_STATUS", "GL_COMPILE_STATUS", "GL_INFO_LOG_LENGTH", "GL_SHADER_SOURCE_LENGTH" }));

	}

	ConvertInfo const* convert_info(std::string& type) const
	{
		auto it = m_convert_types.find (type);
		if (it != m_convert_types.end()) return &it->second;
		return nullptr;
	}

	Command const* find_command(std::string const& cmdName) const {
		auto it = m_commands.find (cmdName);
		if (it != m_commands.end()) return &*it;
		return nullptr;
	}

	Group const* find_group(std::string groupName) const {

		if (groupName == "FramebufferTarget") 
		{
			dolog(3, "found FramebufferTarget\n");
		}

		// Try to find renamed group
		auto renamed = m_group_rename.find (groupName);
		if (renamed != m_group_rename.end())
			groupName = renamed->second;	

		// Check if it has already been combined/cached
		auto it = m_combined.find (groupName);
		if (it != m_combined.end()) return &*it;

		// Combine...
		auto internal = m_internal.find (groupName);
		auto defined = m_groups.find (groupName);

		// If we have internal, but no defined (in xml), then return internal
		if (internal != m_internal.end()) {
		   if (defined == m_groups.end()) // not defined
		   	   return &*internal;

		   // Otherwise, merge internal values with defined and store in combined
		   Group comb = *defined;
		   comb.m_defined_enums.insert (internal->m_defined_enums.begin (), internal->m_defined_enums.end ());
		   //comb.m_aliases.insert (internal->m_aliases.begin (), internal->m_aliases.end ());
		   comb.m_use_group &= internal->m_use_group;
		   comb.m_is_bitfield |= internal->m_is_bitfield;
		   return &*m_combined.emplace(std::move(comb)).first;
		}
		
		// We don't have internal, return defined if we have one
		if (defined != m_groups.end())
				return &*defined;
		
		// Otherwise it's nothing!
		return nullptr;
	}

	bool parse (xml_node<>& regN);
};

std::ostream& operator<<(std::ostream& os, Command const& cmd)
{
	os	<< cmd.full_return_value() << ""
		<< cmd.processed_name() << "(";

	for (size_t i=0, n = cmd.m_params.size(); i < n; ++i)
	{
		Command::Param const& p = cmd.m_params[i];
		os << p.type << " " << p.name;
		if (i < n-1)
			os << ", ";
	}

	os << ")";

	return os;
}

struct ExtractFeatureTool
{
	Registry const& m_reg;
	std::ostream& m_os;
	
	struct Sig
	{
		Registry const& m_reg;
		Command const* m_cmd;

		// This map holds groups (enums) that are required by this feature.
		// The enums values though, still need to be filtered by 'require' because not all are used by 'feature'
		struct GroupInfo {
			Command const* used_in_command;
		};

		typedef std::set<Group const*> AvailGroups;
		AvailGroups const& m_avail_groups;

		bool is_avail(Group const* group) const {
			return m_avail_groups.find(group) != m_avail_groups.end();
		}

		struct ParamInfo
		{
			std::string m_name;
			// a type specified in registry; cpp_type - type t
			std::string m_type, m_cpp_type;
			std::string m_unfold_expr;
			Group const* m_group = nullptr;
			//std::vector<Group const*> m_aliases;

			std::string get_cpp_arg_type(Group const* paramGroup) const
		   	{
				if (m_cpp_type.empty()) {
					if (paramGroup && paramGroup->use_group())
						return paramGroup->m_name;
					return m_type;
				}

			   	return m_cpp_type;
			}

			// return a string that passes the parameter to actual gl function
			std::string get_use_str(Group const* paramGroup) const {
			   	if (!m_unfold_expr.empty ())
				   	return m_unfold_expr;

				if (paramGroup && paramGroup->use_group()) {
					return "(" + m_type + ")" + m_name;
				} else {
					return m_name;
				}
		   	}
		};

		struct FunctionFix
		{
			Registry::FixInfo const* fixInfo = nullptr;
			Registry const& m_reg;

			FunctionFix (Registry const& reg, Command const& cmd) : m_reg(reg) {
				fixInfo = reg.find_fix_info(cmd.m_name);
			}
			
			void fix (int index, Command::Param& p) const {
				if (fixInfo) {
					for (Registry::FixInfo::Param const& paramFix : fixInfo->m_params) {
						if (paramFix.m_index == index) {
							p.group = paramFix.m_group;
							break;
						}
					}
				}
			}

			void fix (int index, ParamInfo& p) const {
				if (fixInfo) {
					for (Registry::FixInfo::Param const& paramFix : fixInfo->m_params) {
						if (paramFix.m_index == index) {
							p.m_group = m_reg.find_group(paramFix.m_group);
							break;
						}
					}
				}
			}
		};

		std::vector<ParamInfo> m_params;

		Sig (Registry const& reg, Command const& cmd, AvailGroups const& availGroups) : m_reg(reg), m_cmd(&cmd), m_avail_groups(availGroups)
		{
			FunctionFix fix (reg, cmd);

			int index = 0;
			for (auto& p : cmd.m_params) {
				ParamInfo pi = { p.name, p.type };
				if (Group const* g = m_reg.find_group(p.group)) {
					if (is_avail(g))
						pi.m_group = g;
					// for (auto& aliasName : g->m_aliases) {
					//	if (Group const* aG = m_reg.find_group(aliasName))
					//		pi.m_aliases.push_back (aG);
					//}
				}
				
				// Check if we override parameter for this function
				fix.fix (index, pi);

				m_params.push_back (pi);
				++index;
			}
		}

		void transform_params_type()
		{
			Registry::CmdAliasContainer aliases = m_reg.find_aliases(m_cmd);

			for (int i=0; i<m_params.size(); ++i) {

				ParamInfo& pi = m_params[i];

				/*
				Group const* aliasGroup;

				// Go through all aliases of this command for the same parameter index and, in case the parameter has a group,
				// iterate over the group and for each item, gather enum with the same values.
				// Afterwards, choose the shortest (or some other heuristics) name and 
				for (auto const* aliasCmd : aliases) {
					Sig s (m_reg, *aliasCmd, m_avail_groups);
					if (s.m_params[paramIndex].m_group != nullptr) {
						aliasGroups.push_back (s.m_params[paramIndex]);
					}
				}*/
				
				if (Registry::ConvertInfo const* conv = m_reg.convert_info(pi.m_type)) {
					pi.m_cpp_type = conv->m_cpp_type;
					pi.m_unfold_expr = conv->m_unfold_expr;
				}
			}
		}

		void emit (std::ostream& os) const
		{
			// find aliases of this command
			Registry::CmdAliasContainer aliases = m_reg.find_aliases (m_cmd);

			std::list<Sig> aliasSigs;
			for (auto const* a : aliases)
				aliasSigs.push_back (Sig (m_reg, *a, m_avail_groups));

			os << "inline ";
			os << m_cmd->full_return_value() << " " << m_cmd->processed_name();
			os << "(";

			std::stringstream call;

		   	for (size_t i=0, n = m_params.size(); i < n; ++i) {
				ParamInfo const* p = &m_params[i];

				if (p->m_group == nullptr) {
					// In case param is 'raw', search aliases and check if the same param has Group assigned. If so - use that
					for (Sig const& a : aliasSigs) {
						if (a.m_params.size () != m_params.size()) continue;
						if (a.m_params[i].m_type != m_params[i].m_type) continue;
						if (!a.m_params[i].m_group) continue;
						
						p = &a.m_params[i];
						break;
					}
				}

				os << p->get_cpp_arg_type(p->m_group) << " ";
				os << p->m_name;

				call << p->get_use_str(p->m_group);

				if (i != n-1) {
					os << ", ";
					call << ", ";
				}
			}
			os << ")\n";
			os << "{\n";

			os << "\t";
			if (!m_cmd->is_void_return())
				os << m_cmd->full_return_value() << " result = ";

			os << m_cmd->m_name << "(";
			os << call.str();
			os << ");\n";
			os << "\tSTIR_GLRESULT();\n";
			if (!m_cmd->is_void_return())
				os << "\treturn result;\n";
		   	os << "}" << std::endl;
		}
	};

	ExtractFeatureTool(Registry const& r, std::ostream& os) : m_reg(r), m_os(os) {}

	void dump_api(std::string const& api, std::string const& version, std::string const& oneCmd = "")
	{
		// a set of groups that are used for this api (api can include enums/cmds from a 'feature' of previous version)
		for (auto const& f : m_reg.m_features) {
			if (f.m_api == api && f.m_version == version)
			{
				dolog(1, "Processing feature: %s, ver %s\n", f.m_api.c_str(), f.m_version.c_str());
				process_feature (f, oneCmd);
			}
		}

		// enums
		// commands (functions)
	}

	void process_feature(Feature const& feat, std::string const& oneCmd = "")
	{
		Sig::AvailGroups usedGroups;

		std::string id = feat.m_api + "_" + feat.m_version;
		for (char& c : id) {
			if (!isalnum(c))
				c = '_';
			c = toupper(c);
		}

		for (auto const& cmdName : feat.m_commands) {

			Command const* cmd = m_reg.find_command(cmdName);
			if (!cmd) continue;
			if (!oneCmd.empty () && oneCmd != cmdName) continue;

			if (std::count_if (cmd->m_params.begin (), cmd->m_params.end(), [] (auto param) { return !param.group.empty(); }) != 0)
				dolog(3, "Command %s depends on groups:\n", cmd->m_name.c_str());

			Registry::CmdAliasContainer aliases = m_reg.find_aliases (cmd);
			if (!aliases.empty())
				dolog(1, "    aliases: %s\n", join(aliases, " ", [](auto p) { return p->m_name; }).c_str());

			int index = 0;
			for (auto const& p : cmd->m_params) {

				// Get parameter type the way it should have been specified
				Sig::FunctionFix  fix (m_reg, *cmd);
				fix.fix (index, const_cast<Command::Param&> (p));

				// 
				if (!p.group.empty()) {
					dolog(3, "  %s (%s)\n", p.group.c_str(), p.name.c_str() );
					auto* grp = m_reg.find_group(p.group);
					if (!grp) continue;
					if (grp->m_name == "FramebufferTarget")
						dolog(3, "  group %s (%s)\n", grp->m_name.c_str() );
					usedGroups.insert (grp);
				}

				++index;
			}
		}

		// Now we create enums for groups that were used in commands
		dolog (1, "Used groups: %d\n", (int)usedGroups.size());

		// register all enums that have been emitted
		std::set<std::string> usedEnums;

		m_os << "#ifndef STIR_" << id << "_INCLUDED\n";
		m_os << "#define STIR_" << id << "_INCLUDED\n\n";
		m_os << "namespace stir { namespace gl {\n\n";
		
		//assert (std::is_sorted(feat.m_enums));
		std::vector<Group const*> usedGroups1 (usedGroups.begin (), usedGroups.end());
		std::sort (usedGroups1.begin(), usedGroups1.end(), [](auto a, auto b) { return a->m_name < b->m_name; });
		for (Group const* groupP : usedGroups1) {

			Group const& group = *groupP;
					
			dolog(2, "Processing group %s\n", group.m_name.c_str());
		
			std::vector<std::string> enumValues = group.filter (feat.m_enums.begin (), feat.m_enums.end ());
			if (enumValues.empty ()) continue;

			usedEnums.insert (enumValues.begin(), enumValues.end());

			m_os << "enum class " << group.m_name << " : ";
			m_os << (group.is_bitfield() ? "GLbitfield" : "GLenum") << "\n{\n";
			for (auto const& e : enumValues)
			{
				std::string clean = xform_enum(e);
				dolog (3, "  %s (%s)\n", clean.c_str(), e.c_str());

				m_os << "\t" << clean << " = " << e << "," << std::endl;
			}

			m_os << "};\n";

			if (group.is_bitfield()) {
				m_os << "inline " << group.m_name << " operator| (" << group.m_name << " a, " << group.m_name << " b) {\n";
				m_os << "\treturn (" << group.m_name << ") ((GLbitfield)a|(GLbitfield)b);\n}\n";
			}

			m_os << "\n";
		}

		for (auto const& cmdName : feat.m_commands) {

			if (!oneCmd.empty() && cmdName != oneCmd) continue;

			Command const* cmd = m_reg.find_command(cmdName);
			if (!cmd) continue;

			// When generating a prototype, we only convert 'basic type' to enum, if the group has been 'used', that is
			// it was defined either in the registry or locally.
			Sig sig (m_reg, *cmd, usedGroups);

			sig.transform_params_type();
			sig.emit (m_os);
		}

		std::vector<std::string> notEmittedEnums;
		std::set_difference(feat.m_enums.begin(), feat.m_enums.end(), usedEnums.begin(), usedEnums.end(), 
				std::back_inserter(notEmittedEnums));

		if (!notEmittedEnums.empty()) {
			m_os << "// Constants that were not emitted into groups: (" << notEmittedEnums.size() << ")" << std::endl;
			for (auto const& e : notEmittedEnums) {
				m_os << "static const int " << xform_enum(e) << " = " << e << ";" << std::endl;
			}
			m_os << std::endl;
		}

		m_os << "} } // stir :: gl\n\n";
		m_os << "#endif" << std::endl;
	}
	
	std::string remprefix(std::string const& arg, char const* p) {
		size_t gl = arg.find(p);
		if (gl == 0)
			return arg.substr (strlen(p));
		return arg;
	}

	std::string xform_enum(std::string const& arg) { return remprefix (arg, "GL_"); }
	std::string xform_command(std::string const& arg) { return remprefix (arg, "gl"); }

};

std::vector<char> read_file (char const* name) {
	std::ifstream is(name, std::ios::binary | std::ios::ate);
	size_t fileSize = is.tellg();
	is.seekg(0);
	std::vector<char> text;
	text.resize (fileSize);
	is.read (text.data(), fileSize);

	return std::move (text);
}

int main(int argc, char* argv[])
{
	std::string arg_in;
	std::string arg_api = "gles2";
	std::string arg_ver = "2.0";
	std::string arg_oneCmd;
	bool arg_filter = true;

	for (int i=1; i<argc; ) {
		std::string arg = argv[i++];
		if (arg.empty ()) continue;
		if (arg[0]=='-') {
			arg.erase(0,arg.find_first_not_of('-'));
			if (arg == "v") { g_verbosity++; }
			if (arg == "s") { g_verbosity--; }
			if (arg == "api" && i < argc) { arg_api = argv[i++]; }
			if (arg == "version" && i < argc) { arg_ver = argv[i++]; }
			if (arg == "one" && i < argc) { arg_oneCmd = argv[i++]; }
		} else {
			arg_in = arg;
		}
	}

	if (arg_in.empty()) {
		printf("No input xml file specified\n");
		return 1;
	}
	std::vector<char> text = read_file (arg_in.c_str());

	xml_document<> doc;    // character type defaults to char
	doc.parse<0>(text.data());    // 0 means default parse flags
	
	Registry reg;
	if (xml_node<>* node = doc.first_node())
		reg.parse (*node);
	
	dolog (1, "Commands (%d)\n", (int)reg.m_commands.size());
	for (auto const& cmd : reg.m_commands)
		dolog (3, "  %s\n", cmd.m_name.c_str());
	
	dolog (1, "Aliases (%d)\n", (int)reg.m_cmd_aliases.size());
	for (auto const& alias : reg.m_cmd_aliases) {
		dolog (3, "  %s <- ", alias.first->m_name.c_str());
		for (Command const* cmd : alias.second) {
			dolog (3, "  %s ", cmd->m_name.c_str());
		}
		dolog (3, "\n");
	}
	
	dolog (1, "Groups (%d)\n", (int)reg.m_groups.size());
	for (auto const& group : reg.m_groups)
		dolog (3, "  %s%s\n", group.m_name.c_str(), group.m_declared_in_enums ? " (e)" : "");

	dolog (1, "Features (%d)\n", (int)reg.m_features.size());
	for (auto const& feat : reg.m_features)
		dolog (3, "  %s: %s %s\n", feat.m_api.c_str(), feat.m_name.c_str(), feat.m_version.c_str());

	ExtractFeatureTool tool (reg, std::cout);
	tool.dump_api (arg_api, arg_ver, arg_oneCmd);
	//tool.dump_api ("gles1", "1.0");

	return 1;
}


bool Feature::parse (xml_node<>& featureN)
{
	assert (strcmp(featureN.name (), "feature") == 0);

	if (xml_attribute<>* api = featureN.first_attribute("api"))
		m_api = api->value();

	if (xml_attribute<>* name = featureN.first_attribute("name"))
		m_name = name->value();

	if (xml_attribute<>* version = featureN.first_attribute("number"))
		m_version = version->value();

	for (xml_node<>* node = featureN.first_node("require"); node; node = node->next_sibling("require"))
	{
		if (!parse_require(*node))
			return false;
	}

	return true;
}

bool Feature::parse_require(xml_node<>& reqN)
{
	for (xml_node<>* node = reqN.first_node(); node; node = node->next_sibling()) {
		if (strcmp(node->name(), "command") == 0) {
			if (xml_attribute<>* attr = node->first_attribute("name"))
				m_commands.push_back(attr->value());
		} else if (strcmp(node->name(), "type") == 0) {
			if (xml_attribute<>* attr = node->first_attribute("name"))
				m_types.push_back(attr->value());
		} else if (strcmp(node->name(), "enum") == 0) {
			if (xml_attribute<>* attr = node->first_attribute("name"))
				m_enums.push_back(attr->value());
		}
	}

	return true;
}

bool Command::parse (xml_node<>& cmdN)
{
	for (xml_node<>* node = cmdN.first_node(); node; node = node->next_sibling())
	{
		if (strcmp(node->name(), "proto") == 0) {
		
			// <proto group="String">const <ptype>GLubyte</ptype> *<name>glGetString</name></proto>

			for (xml_node<>* content = node->first_node(); content; content = content->next_sibling())
			{
				if (strcmp(content->name(), "ptype") == 0)
					m_ret_type += content->value();
				else if (strcmp(content->name(), "name") == 0) {
					m_name = content->value();
					if (m_name == "glGetAttachedShaders") {
						dolog(2, "parsing %s\n", m_name.c_str());
					}
				} else if (strcmp(content->name(), "") == 0)
					m_ret_type += content->value();
			}

		} else if (strcmp(node->name(), "alias") == 0) {
			if (xml_attribute<>* attr = node->first_attribute("name"))
				m_aliases.push_back (attr->value());
		} else if (strcmp(node->name(), "param") == 0) {

			// <param>const <ptype>GLchar</ptype> *<name>name</name></param>

			Param p;
			if (xml_attribute<>* attr = node->first_attribute("group"))
				p.group = attr->value();
 			
			// len="COMPSIZE(format,type)">const void *<
			if (xml_attribute<>* attr = node->first_attribute("len"))
				p.len = attr->value();
 			
			for (xml_node<>* content = node->first_node(); content; content = content->next_sibling())
			{
				if (strcmp(content->name(), "ptype") == 0)
					p.type += content->value();
				else if (strcmp(content->name(), "name") == 0)
					p.name = content->value();
				else if (strcmp(content->name(), "") == 0)
					p.type += content->value();
			}

			m_params.push_back (p);
		}
	}

	size_t nonSpace = m_ret_type.find_last_not_of(" ");
	if (nonSpace != std::string::npos)
		m_ret_type.erase (nonSpace+1);

	return true;
}

bool Group::parse (xml_node<>& groupsN)
{
	assert (strcmp(groupsN.name (), "group") == 0);

	if (xml_attribute<>* attr = groupsN.first_attribute("name"))
		m_name = attr->value();

	for (xml_node<>* e = groupsN.first_node("enum"); e; e = e->next_sibling("enum")) {
		if (xml_attribute<>* attr = e->first_attribute("name")) {

			m_defined_enums.insert (Group::fix_group_name(attr->value()));
		}
	}

	return true;
} 

bool Enum::parse (xml_node<>& enumN)
{
	if (xml_attribute<>* attr = enumN.first_attribute("value"))
		m_value = strtol(attr->value(), nullptr, 16);

	if (xml_attribute<>* attr = enumN.first_attribute("name"))
		m_name = attr->value();

	return true;
} 

bool Registry::parse (xml_node<>& regN)
{
	for (xml_node<>* enumsN = regN.first_node("enums"); enumsN; enumsN = enumsN->next_sibling("enums"))
	{
		Group g;
		g.m_declared_in_enums = true;
		if (xml_attribute<>* attr = enumsN->first_attribute("group")) // enums may define group
			g.m_name = attr->value();

		for (xml_node<>* node = enumsN->first_node("enum"); node; node = node->next_sibling("enum"))
		{
			Enum e;
			if (e.parse (*node))
			{
				if (!g.m_name.empty())
					g.m_defined_enums.insert (e.m_name);

				m_enums.emplace (std::move(e));
			}
		}

		if (!g.m_name.empty())
			m_groups.insert (g);
	}

	for (auto const& e : m_enums) {
		m_enum_lookup[e.m_name] = &e;
	}

	// Some groups contain only enums that have EXT (and similar) suffixes
	// So find 'normal' enum and add that as well
	std::regex re_ext("(.+)(_EXT|_OES|_ARB|_SGIX)$", std::regex_constants::ECMAScript);
	std::vector<std::string> addEnums;
	addEnums.reserve(4);
	for (Group const& g : m_groups) {
		addEnums.clear();
		for (auto& enumName : g.m_defined_enums) {
			std::smatch m;
			std::regex_search(enumName, m, re_ext);
			if (m.empty()) continue;
			
			dolog(3, "  regex match: %s into [%s][%s]\n", enumName.c_str(), m[1].str().c_str(), m[2].str().c_str());

			std::string regular = m[1];
			
			dolog(3, "  searching for %s from %s\n", regular.c_str(), enumName.c_str());

			Enum const* e = lookup_enum(enumName);
			if (!e) continue;

#if 0
			EnumRange r = find_enum(e->m_value);
			for (auto it = r.first; it != r.second; ++it ) addEnums.push_back (it->m_name);
#else
			addEnums.push_back (e->m_name);
#endif
		}

		dolog(2, "Insert %d enums into %s (%s)\n", (int)addEnums.size(), g.m_name.c_str(), join(addEnums, "/", [](auto p) { return p; } ).c_str());
		for(auto const& e : addEnums)
			g.m_defined_enums.insert (e);
		//g.m_defined_enums.insert (addEnums.begin (), addEnums.end());
	}


	for (xml_node<>* groupsN = regN.first_node("groups"); groupsN; groupsN = groupsN->next_sibling("groups"))
	{
		for (xml_node<>* node = groupsN->first_node("group"); node; node = node->next_sibling("group"))
		{
			Group g;
			if (g.parse (*node))
				m_groups.emplace (std::move(g));
		}
	}

	for (xml_node<>* cmdsN = regN.first_node("commands"); cmdsN; cmdsN = cmdsN->next_sibling("commands"))
	{
		for (xml_node<>* cmdN = cmdsN->first_node("command"); cmdN; cmdN = cmdN->next_sibling("command"))
		{
			Command c;
			if (c.parse (*cmdN))
				m_commands.emplace (std::move(c));
		}
	}

	// construct a map for alias search
	// a command usually/always (?) stores an alias to a NEWEST/LATEST command
	for (auto const& cmd : m_commands) {
		for (auto const& aliasName : cmd.m_aliases) {
			Command const* newer = find_command (aliasName);
			if (!newer) continue;
			m_cmd_aliases[newer].push_back (&cmd);
		}
	}

	for (auto const& alias : m_cmd_aliases) {
		//std::sort (alias.second.begin (), alias.second.end());
		//std::unique (alias.second.begin (), alias.second.end());
	}

	for (xml_node<>* node = regN.first_node("feature"); node; node = node->next_sibling("feature"))
	{
		Feature f;
		if (f.parse (*node))
		{
			f.sort();
			m_features.emplace_back (std::move(f));
		}
	}

	return true;
}

