#include <iostream>
#include <vector>
#include <string>

#include <stir/path.hpp>
#include <stir/atlas/img_atlas.hpp>
#include <stir/atlas/storage-atlas.hpp>
#include <stir/atlas/storage-atlas_util.hpp>
#include <stir/atlas/storage_access-atlas.hpp>
#include <stb_image.h>
#include <pulmotor/ser.hpp>
#include <pulmotor/stream.hpp>
#include <pulmotor/archive.hpp>
#include <stir/pulmotor_archive.hpp>
#include <boost/filesystem.hpp>

#define USE_LUA 1

#if USE_LUA
#include <stir/lua/lua_fwd.hpp>
#include <luabind/luabind.hpp>
#include <luabind/tag_function.hpp>
#include <luabind/adopt_policy.hpp>
#include <luabind/return_reference_to_policy.hpp>
#include <stir/lua/lua_utility.hpp>
#endif

using namespace stir;
namespace fs = boost::filesystem;
namespace ssa = stir::storage::image_atlas;

#define ANIM_SEP '$'
char const* nums = "0123456789";
char const* g_clean = "_ ";

struct anim_def
{
	std::string sprite, anim;
	int start, count;
};

struct filename_info
{
	static int noname;
	
	// name[_ ][seq-numbers][$anim].ext
	std::string sprite, index, anim, ext;
	filename_info (std::string s)
	{
		size_t extS = s.find_last_of('.');
		if (extS != std::string::npos)
		{
			ext = s.substr (extS);
			s.erase(extS);
		}
		
		// now s doesn't contain the extension
		size_t E = std::string::npos;
		
		size_t animSep = s.find_last_of(ANIM_SEP);
		if (animSep != std::string::npos)
		{
			anim = s.substr(animSep+1);
			E = animSep == 0 ? 0 : animSep-1;
		}
		
		size_t nameDirtyE = s.find_last_not_of(nums, E);
		if (nameDirtyE != E && nameDirtyE != s.size()-1) { // if there are some digits
			index = s.substr(nameDirtyE+1, E);
		}
		
		size_t nameE = s.find_last_not_of(g_clean, nameDirtyE);
		if (nameE == std::string::npos) // name is not present
			sprite = std::string("unnamed") + std::to_string(noname++);
		
		sprite = s.substr(0, nameE+1);
	}
};
	
int filename_info::noname = 0;

struct input_image
{
	input_image(fs::path const& p, int m) : path(p), multi(m) {}
	
	fs::path path;
	stir::image_ref img;
	int multi;
	
	std::string anim;
	
	bool operator<(input_image const& a) const {
		return path < a.path;
	}
};

struct sequence : public std::vector<input_image> {
	//	using std::vector<fs::path>::vector<fs::path>;
	
	// sprite name with all additional info stripped
	std::string sprite_name;
};
struct options
{
	struct seq_def
	{
		seq_def(fs::path const& dir, std::string const& sn) : directory(dir), sprite_name(sn) {}
		fs::path directory;
		std::string sprite_name;
	};
	
	struct sheet_def
	{
		sheet_def (fs::path const& pathname, std::string const& name, int w, int h, int count)
		:	file (pathname), sprite_name(name), width(w), height(h), frame_count(count)
		{}
		
		fs::path file;
		std::string sprite_name;
		int width, height, frame_count;
	};
	
	std::vector<anim_def> anims;
	std::vector<seq_def> seqs;
	std::vector<sheet_def> sheets;
	
	fs::path output, lua_config, editor;
	int border = 1;
	bool showhelp = false;
	int verbose = 1;
	
#if USE_LUA
	struct src_def
	{
		options& opts;
		std::string sprite_name;
		src_def(options& o, std::string const& spr) : opts(o), sprite_name(spr) {}
		
		src_def* anim(std::string const& name, int start, int count)
		{
			anim_def ad;
			ad.sprite = sprite_name;
			ad.anim = name;
			ad.start = start;
			ad.count = count;
			
			opts.anims.push_back(ad);
			
			if (opts.verbose > 2)
				printf("  anim> (%s)%s %d+%d\n", sprite_name.c_str(), name.c_str(), start, count);
			return this;
		}
	};
	
	struct ss_def
	{
		options& opts;
		std::string name;
		
		ss_def(options& o, std::string const& name_) : opts(o), name(name_) {
			opts.output = name;
		}
		
		src_def* seq(std::string const& src) {
			fs::path phonyP = opts.lua_config.parent_path() / src;
			opts.seqs.push_back ( options::seq_def { phonyP.parent_path(), phonyP.filename().string() } );
			if (opts.verbose > 2)
				printf(" seq> %s\n", src.c_str());
			
			filename_info fi (phonyP.filename().string());
			
			return new src_def (opts, fi.sprite);
		}
		
		src_def* sheet(std::string const& src, int w, int h, int count) {
			fs::path subP = src;
			fs::path fullP = opts.lua_config.parent_path() / subP;
			
			filename_info fi (subP.stem().string());
			opts.sheets.push_back ( options::sheet_def { fullP, fi.sprite, w, h, count } );

			if (opts.verbose > 2)
				printf(" sheet> %s %dx%d + %d\n", src.c_str(), w, h, count);
			
			return new src_def (opts, fi.sprite);
		}
		int get_border() const { return opts.border; }
		void set_border(int b) { opts.border = b; }
		
		static ss_def* create(options* o, std::string const& name) {
			return new ss_def (*o, name);
		}
	};
	
	static void bind_options(lua_State* L, options* opts)
	{
		using namespace luabind;
		
		module(L)
		[
			def("new_ss", tag_function<ss_def* (std::string)>(boost::bind (&ss_def::create, opts, _1)), adopt(result)),
		 
//			def("new_ss", tag_function<int(std::string)>(boost::bind (&new_ss, opts, _1))),
			class_<ss_def>("sprite_sheet")
			 
//				.def (constructor<std::string>())
//				.def("new_ss", tag_function<int(std::string)>(boost::bind (constructor<options const&, std::string>(), opts, _1)))
				.def ("seq", &ss_def::seq, adopt(result))
				.def ("sheet", &ss_def::sheet, adopt(result))
//				.def ("src", &ss_def::src_name, adopt(result))
				.property("border", &ss_def::get_border, &ss_def::set_border)
			,
			 class_<src_def> ("ss_src_def")
				 .def ("anim", &src_def::anim, return_reference_to(_1))
		 ];
	}
#endif
};

struct SheetBuilder
{
	typedef storage::image_atlas::sprite_animation Animation;
	
	struct Sprite
	{
		std::vector<stir::image_ref> m_frames;
		std::vector<Animation> m_animations;
	};

	struct FrameInfo
	{
		std::string sprite_name;
		int frame_index;
	};
	
private:
	std::map<std::string, Sprite> m_sprites;
	
	std::list<stir::image_ref> m_images;
	
public:
	options& m_opts;
	
	SheetBuilder (options& opts) : m_opts(opts)
	{}
	
	~SheetBuilder ()
	{
		for (auto const& img : m_images)
		{
			delete img.pixels;
		}
	}
	
	void add_frame (std::string const& sprite, stir::image_ref const& img)
	{
		m_sprites[sprite].m_frames.push_back (img);
	}
	
	struct MakeAtlasSheet
	{
		stir::image_atlas& m_atlas;
		std::map<std::string, Sprite>& m_sprites;
		options& m_opts;
		
		MakeAtlasSheet (stir::image_atlas& atlas, SheetBuilder& b)
		:	m_atlas(atlas), m_sprites (b.m_sprites), m_opts(b.m_opts)
		{
			for (auto const& s : b.m_sprites)
			{
				int frameIndex = 0;
				for (auto const& img : s.second.m_frames)
				{
					FrameInfo frameInfo = { s.first, frameIndex++ };
					atlas.add (img, boost::any (frameInfo));
				}
			}
		}
		
		void build_sprite_sheet(storage::image_atlas::sprite_sheet& ss)
		{
			// We need an intermediate step to gather frames of each sprite. This is needed
			// because when iterating nodes, the sprites are scattered and we want to have them ordered
			// according to frame index that is stored in FrameInfo.
			
			// sprite-name : (image-info)
			std::map<std::string, std::vector<stir::image_info const*>> spriteImageInfos;
				
			ss.pages.reserve(m_atlas.pages.size());
			for (size_t i=0; i<m_atlas.pages.size(); ++i)
			{
				image* img = m_atlas.pages[i]->img;
				
				// store texture first
				asset::texture_asset* pageTex = new asset::texture_asset;
				asset::texture_builder builder (*pageTex, img->width, img->height, asset::rgba8);
				builder.compression(asset::compress_none);
				builder.add_level(*img);
				builder.finish();
				
				ss.pages.push_back (new storage::image_atlas::page());
				ss.pages.back()->pixel_size = stir::vector2f (1.0f / pageTex->width, 1.0f / pageTex->height);
				ss.pages.back()->texture = pageTex;
				
				// iterate over nodes
				auto& page = *m_atlas.pages[i];
				for (auto it = page.maker->begin(); it != page.maker->end(); ++it)
				{
					if (!it->is_used())
						continue;
					
					if (!it->has_load())
						continue;
					
					// std::string sprite_name;
					// int frame_index;
					stir::image_info const* ii = it->get_load<stir::image_info const*>();
					FrameInfo const& fi = boost::any_cast<FrameInfo>(ii->attr);
					spriteImageInfos[fi.sprite_name].push_back (ii);
				}
			}
			
			for (auto& sii : spriteImageInfos)
			{
				// Sort image infos by frame index so they're in order
				std::sort (sii.second.begin (), sii.second.end(),
					[](stir::image_info const* a, stir::image_info const* b) {
						FrameInfo const& fi_a = boost::any_cast<FrameInfo const&>(a->attr);
						FrameInfo const& fi_b = boost::any_cast<FrameInfo const&>(b->attr);
						return fi_a.frame_index < fi_b.frame_index;
					});
				
				// Find Sprite description that matches the name that we've pulled from atlas
				auto spriteIt = m_sprites.find (sii.first);
				if (spriteIt == m_sprites.end())
					continue;

				// Create a sprite in sprite sheet and fill info
				ss.sprites.emplace_back(sii.first);
				ssa::sprite& s = ss.sprites.back();
				
				// 1. Copy frames
				for (stir::image_info const* ii : sii.second)
				{
					auto& page = *m_atlas.pages[ii->page_index];
					
					ssa::sprite_frame sf;
					
					sf.area = ii->area(1.0f / page.img->width, 1.0f / page.img->height);
					sf.page_index = ii->page_index;
					sf.group_index = 0;
					s.frames.push_back (sf);
				}
				
				// 2. Add animations for sprite
//				s.anims.push_back (ssa::sprite_animation { "", 0, static_cast<u16>(s.frames.size ()) });
				for (Animation const& a : spriteIt->second.m_animations)
				{
					s.anims.push_back (a);
				}
				
				std::sort (s.anims.begin(), s.anims.end (), ssa::sprite_animation::sort_by_name());
				
				if (m_opts.verbose > 1)
				{
					printf ("Sprite '%s'\n", s.name.c_str());
					
					printf ("  %u frames:\n", (int)s.frames.size());
					int i=0;
					for (auto const& f : s.frames)
					{
						printf ("    %d: (%5.3f, %5.3f : %5.3f, %5.3f) page:%d group:%d\n", i++,
								f.area.p0.x, f.area.p0.y, f.area.p1.x, f.area.p1.y, (int)f.page_index, (int)f.group_index);
					}
					
					printf ("  %u anims:\n", (int)s.anims.size());
					for (auto const& a : s.anims)
					{
						printf ("    '%s': s:%u l:%u frames\n", a.name.c_str(), a.start, a.count);
					}
				}
			}
			
			std::sort (ss.sprites.begin (), ss.sprites.end (), ssa::sprite::sort_by_name());
		}
	};
		
	size_t frame_count (std::string const& sprite) { return m_sprites[sprite].m_frames.size(); }
	
	void animation (std::string const& sprite, std::string const& animName, int start, int count)
	{
		auto& anims = m_sprites[sprite].m_animations;
		auto animIt = std::find_if (anims.begin (), anims.end (),
			[&] (Animation const& a) { return strcmp (a.name.c_str(), animName.c_str()) == 0; });
		if (animIt == anims.end ())
			anims.push_back ( Animation { animName.c_str(), (u16)start, (u16)count } );
		else
			*animIt = Animation { animName.c_str(), (u16)start, (u16)count };
	}
	
	stir::image_ref* load_image(fs::path const& p, std::set<fs::path> const& exts)
	{
		for (auto const& e : exts)
		{
			fs::path fullP = p;
			fullP.replace_extension(e);

			int x, y, comp;
			stbi_uc* pixels = stbi_load (fullP.c_str (), &x, &y, &comp, 4);
			if (!pixels)
				continue;
			
			m_images.emplace_back(x, y, pixels, 4);
			return &m_images.back();
		}
		
		return nullptr;
	}
	
	
	stir::image_ref* load_image(fs::path const& p)
	{
		int x, y, comp;
		stbi_uc* pixels = stbi_load (p.c_str (), &x, &y, &comp, 4);
		
		if (!pixels) {
			printf ("Failed to load an image '%s'\n", p.c_str());
			return nullptr;
		}

		if (m_opts.verbose > 1)
			printf ("Loaded %s: %d x %d\n", p.c_str (), x, y);
		
		m_images.emplace_back(x, y, pixels, 4);
		return &m_images.back();
	}
};

// format "file name XXXX.ext", where XXXX is some number of digits
// or "filename.ext"
// TODO:
// player_001$walk.png
// player_002$walk.png
// player_003$walk.png
// player_004$run.png
// player_005$run.png
// player_006$run.png
struct AnimInfo {
	int start, count;
	static void find_animations (SheetBuilder& builder, std::vector<fs::path> const& animImgs, std::map<std::string, AnimInfo>& anims)
	{
		std::map<std::string, std::vector<int>> frameIndexForAnims;
		for (auto i=0; i<animImgs.size(); ++i) {
			filename_info fi (animImgs[i].string());
			if (!fi.anim.empty())
				frameIndexForAnims[fi.anim].push_back (i);
		}
		
		for (auto const& p : frameIndexForAnims)
		{
			assert (p.second.empty ());
			int start = p.second[0];
			int prev = start;
			for (int i=1; i<p.second.size(); ++i) {
				if (prev != p.second[i]-1) {
					if (builder.m_opts.verbose > 0) {
						printf ("error: frames for animation %s is not in sequence (%d then %d)\n", p.first.c_str(), (int)prev, (int)p.second[i]);
						start = -1;
						break;
					}
				}
				prev = p.second[i];
			}
			
			if (start == -1)
				anims[p.first] = AnimInfo { start, (int)p.second.size() };
		}
	}
};

static std::set<fs::path> s_allowed_exts = { ".png", ".tga", ".psd" };
void trace_sequence (SheetBuilder& builder, fs::path dir, std::string const& spriteName)
{
	filename_info fi (spriteName);
	
	if (dir.empty ())
		dir = ".";
	
	fs::path ext;
	
	std::vector<fs::path> spriteImages;

	for (fs::directory_iterator it (dir), end; it != end; ++it)
	{
		fs::directory_entry const& entry = *it;
		fs::path fn = entry.path().filename();
		
		std::string nameF = fn.string().substr (0, fi.sprite.size());
		if (nameF != fi.sprite)
			continue;
		
		if (!ext.empty () && fn.extension () != ext)
			continue;
		
		if (s_allowed_exts.find (fn.extension()) == s_allowed_exts.end())
			continue;
		
		if (ext.empty ())
			ext = fn.extension ();
		
		spriteImages.push_back (entry.path());
	}
	
	// sort by file name!
	std::sort (spriteImages.begin (), spriteImages.end ());

	// find animations
	std::map<std::string, AnimInfo> anims;
	AnimInfo::find_animations (builder, spriteImages, anims);
	
	// add all the frames
	for (auto const& sprPath : spriteImages)
	{
		if (stir::image_ref* img = builder.load_image (sprPath))
			builder.add_frame(spriteName, *img);
	}
	
	// now submit animations
	for (auto const& animInfo : anims)
	{
		AnimInfo const& ai = animInfo.second;
		if (builder.m_opts.verbose > 1)
			printf ("  Animation: %s (%d, %d frames)\n", animInfo.first.c_str(), (int)ai.start, (int)ai.count);
		
		builder.animation(spriteName, animInfo.first, (int)ai.start, (int)ai.count);
	}
	
//	// load files and add as frames, also create animation
//	for (auto const& animFiles : anims)
//	{
//		// remember animation's start frame
//		size_t start = builder.frame_count(spriteName);
//		
//		for (auto const& fn : animFiles.second)
//		{
//			stir::image_ref* img = builder.load_image (fn);
//			if (img)
//				builder.add_frame(spriteName, *img);
//		}
//		
//		size_t end = builder.frame_count(spriteName);
//		size_t count = end - start;
//		
//		if (builder.m_opts.verbose > 1)
//			printf ("  Animation: %s (%d, %d frames)\n", animFiles.first.c_str(), (int)start, (int)count);
//		
//		builder.animation(spriteName, animFiles.first, (int)start, (int)count);
//	}
	
	// Also create animations that have been specified explicitly
	for (auto const& a : builder.m_opts.anims)
		builder.animation (a.sprite, a.anim, a.start, a.count);
}

bool parse_animdef (std::string const& def, anim_def& r)
{
	size_t col1, col2, col3;
	col1 = def.find_first_of (':');
	if (col1 == std::string::npos) return false;
	col2 = def.find_first_of (':', col1+1);
	if (col2 == std::string::npos) return false;
	col3 = def.find_first_of (':', col2+1);
	if (col3 == std::string::npos) return false;

	r.sprite = def.substr (0, col1);
	r.anim = def.substr (col1+1, col2-col1-1);
	r.start = atoi (def.substr (col2 + 1, col3-col2-1).c_str());
	r.count = atoi (def.substr (col3 + 1).c_str());
	
	return true;
}

int main (int narg, char const** args)
{
	options opts;
	
#if USE_LUA
	stir::lua::context l;
	luabind::open(l.state());
	options::bind_options (l.state(), &opts);
#endif

	for (int i=1; i<narg; ++i)
	{
		if (strcmp(args[i], "-s") == 0 && i < narg-1) {
			std::string src = args[++i], name = "";
			fs::path phonyP = src;
			opts.seqs.push_back(options::seq_def { phonyP.parent_path(), phonyP.filename().string() } );
		} else if (strcmp(args[i], "-S") == 0 && i < narg-2) {
			std::string src = args[++i], name = "";
			size_t numS = src.find_first_of ("0123456789");
			int N = 1;
			if (numS == 0) {
				size_t col = src.find_first_of(':');
				if (col != std::string::npos) {
					std::string num = src.substr(0, col);
					src = src.substr(col+1);
					N = atoi(src.c_str());
				}
			}
			fs::path phonyP = src;
			int C = 0;
			opts.sheets.push_back(options::sheet_def { phonyP.parent_path(), phonyP.filename().string(), N, N, C } );
		} else if (strcmp(args[i], "-b") == 0 && i < narg-1)
			opts.border = atoi(args[++i]);
		else if (strcmp(args[i], "-v") == 0 && i < narg-1)
			opts.verbose++;
		else if (strcmp(args[i], "-o") == 0 && i < narg-1)
			opts.output = args[++i];
		else if (strcmp(args[i], "-e") == 0 && i < narg-1)
			opts.editor = args[++i];
		else if (strcmp(args[i], "-a") == 0 && i < narg-1) {
			std::string def = args[++i];
			anim_def r;
			if (!parse_animdef(def, r))
				printf ("Animation definition isn't valid (%s)\n", def.c_str());
			opts.anims.push_back (r);
			continue;
		} else if (strcmp(args[i], "-h") == 0 || narg <= 1) {
			opts.showhelp = true;
#if USE_LUA
		} else if (strcmp(args[i], "-lua") == 0 && i < narg-1) {
			std::string ls = args[++i];
			printf ("Loading lua config from '%s'\n", ls.c_str());
			
			opts.lua_config = ls;

			std::error_code ec;
			stir::lua::chunk lc = l.load (ls, ec);
			if (!ec) {
				std::string err;
				lc.call(ec, &err);
				if (ec) {
					printf ("Running lua config '%s' failed with error %d:'%s', lua error: '%s'\n",
						ls.c_str(), ec.value(), ec.message().c_str(), err.c_str());
					return 1;
				}
			} else {
				printf ("Loading lua config '%s' failed with error (%d:%s)\n",
						ls.c_str(), ec.value(), ec.message().c_str());
				return 1;
			}
#endif
		} else if (i < narg) {
			printf ("Unknown option '%s'\n", args[i]);
			return 1;
		}
	}

	if (opts.showhelp || narg <= 1)
	{
		printf(
			"Sprite sheet compositor from animation frame sequences. (L) 2013\n"
			"\n"
			"Synopsis:\n"
			"sprite_sheet [-v] [-b N] -S <W:H:N:path>[|name] -s <path>[|name]\n"
			"             [-a <sprite>:<anim>:<start>:<count>] -o <ss-name> [-e editor-sprites]\n"
			"\n"
			"Usage:\n"
			"  -s   x name of a sequence. file's name becomes sprite name. (can be multiple)\n"
			"       file name should follow this pattern: name[_ ][seq-numbers][$anim].ext\n"
			"  -S   image is sprite sheet, W & H define sprite size, 0 for image size; N - number of sprites\n"
			"  -a   define animation. sprite-name:anim-name:startFrame:count\n"
			"       that contains all sprite frames.\n"
			"  -o   name of the resulting sprite-sheet and atlas images\n"
			"  -b   border size (1 by default)\n"
			"  -v   verbose (1: +info, 2: +images)\n"
			"  -e   generate an image for editor (keeping sprites sorted per name)\n"
#if USE_LUA
			"  -lua config.lua to configure sprite_sheet\n"
#else
			"  (-lua is disabled)\n"
#endif
		);
		return 1;
	}
	
	printf ("Working dir: %s\n", fs::current_path().c_str());
	
	SheetBuilder builder (opts);
	
	// seq -> [anim]
	stir::image_atlas atlas (1024, 1024, opts.border);
	
	for (auto const& s : opts.seqs)
	{
		if (opts.verbose > 1)
			printf ("Processing seq '%s'\n", s.sprite_name.c_str());
		
		trace_sequence (builder, s.directory, s.sprite_name);
	}
	
	for (options::sheet_def s : opts.sheets)
	{
		if (opts.verbose > 1)
			printf ("Processing sheet %s ('%s')\n", s.sprite_name.c_str(), s.file.c_str());
		
		stir::image_ref* img = builder.load_image(s.file, s_allowed_exts);
		
		if (!img) {
			printf ("Failed to load image '%s'\n", s.file.c_str());
			continue;
		}
		
		if (img->height < s.height) {
			printf ("WARNING: sheet height is smaller than sprite height!\n");
			s.height = img->height;
		}
		if (img->width < s.width) {
			printf ("WARNING: sheet width is smaller than sprite width!\n");
			s.width = img->width;
		}
		
		int cntH = img->height / s.height;
		int cntW = img->width / s.width;
		
		// Need to generate anim def here and not in options, because if frame_count is 0
		// we need to calculate how many actual frames the sheet contains
		filename_info fi (s.file.stem().string());
		if (!fi.anim.empty() && s.frame_count == 0) {
			printf (">>>>>> add anim: %s %s %d %d\n", fi.sprite.c_str(), fi.anim.c_str(), (int)builder.frame_count (fi.sprite), cntH * cntW);
			opts.anims.push_back(anim_def {fi.sprite, fi.anim, (int)builder.frame_count (fi.sprite), cntH * cntW} );
		}
		
		for (int i=0, index = 0; i<cntH; ++i)
		{
			for (int j=0; j<cntW; ++j)
			{
				++index;
				
				image_ref fr = img->subrect (s.width * j, s.height * i, s.width, s.height);
				builder.add_frame(s.sprite_name, fr);
				
				if (s.frame_count != 0 && index >= s.frame_count)
					break;
			}
			
			if (s.frame_count != 0 && index >= s.frame_count)
				break;
		}
		
		for (auto const& ad : opts.anims)
		{
			if (ad.sprite == s.sprite_name)
			{
				builder.animation(ad.sprite, ad.anim, ad.start, ad.count);
			}
		}
	}

	if (opts.verbose > 1)
		printf("Building atlas...\n");
	
	ssa::sprite_sheet ss;
	SheetBuilder::MakeAtlasSheet makeAtlas (atlas, builder);
	atlas.build_atlas (atlas_page::pot_only|atlas_page::border_dup);
	makeAtlas.build_sprite_sheet(ss);
	
#ifdef _DEBUG
	printf ("SPRITES: %d\n", (int)ss.sprites.size());
	for (auto& s : ss.sprites)
	{
		printf ("  %s: %d frames, %d animations\n", s.name.c_str(), (int)s.frames.size(), (int)s.anims.size());
	}
#endif
	

	pulmotor::memory_archive_container_t bytes;
	pulmotor::memory_write_archive archO (bytes);
	pulmotor::archive (archO, ss);
	size_t written = pulmotor::util::write_file(opts.output.c_str(), bytes.data(), bytes.size());

	//    size_t written = pulmotor::util::write_file (opts.output.c_str(), ss, pulmotor::target_traits::current, 8);
    printf ("file '%s' written, %lu bytes (%d sprites, %d pages)\n",
		opts.output.c_str(), written, (int)ss.sprites.size (), (int)ss.pages.size ());

	if (opts.verbose >= 3)
	{
		for (int i=0; i<atlas.pages.size (); ++i)
		{
			stir::image_ref* img = atlas.pages[i]->img;
			fs::path filename = ss.pages[i]->name.c_str();
			if (filename.empty())
				filename = fs::path(opts.output).stem();
			fs::path out = opts.output.parent_path() / filename;
			out += "_" + std::to_string(i) + ".tga";
			printf ("  %d: %s %dx%d\n", i, out.c_str(), img->width, img->height);
			if (!stbi_write_tga (out.c_str(), img->width, img->height, img->comp, img->pixels))
				printf ("    -- FAILED\n");
		}
	}

	return 0;
}
