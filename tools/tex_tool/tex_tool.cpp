#include <iostream>
#include <iterator>
#include <vector>
#include <fstream>

#include <stir/language_util.hpp>
#include <stir/asset/texture_asset.hpp>
#include <stb_image.h>
#include <stir/path.hpp>
#include <pulmotor/ser.hpp>
#include <pulmotor/archive.hpp>
#include <pulmotor/archive_json.hpp>

#include <rapidjson/filewritestream.h>

using namespace stir;

struct options
{
	options ()
	{}
	stir::asset::texture_format	ofmt = asset::rgba8;
	path opath;
	path ipath;
	int mips = 0;
	int verbose = 0;
	//	bool js = false;
};

int main (int narg, char** parg)
{
	using namespace stir;
	using namespace std;

//	size_t sx = 32, sy = 32;
	bool showhelp = false;
	options opts;
	int stbimg_components = 4;
	char** input = parg;
	int count = narg;
	for (int i=1; i<count; )
	{
		std::string arg = input[i++];
		std::string opt;
		if (arg.find_first_of ('-') == 0)
		   opt = arg.substr (1);
		else if (arg.find ("--") == 0)
		   opt = arg.substr (2);
		else
		{
			opts.ipath = arg;
			continue;
		}

		if (opt == "fmt")
		{
			if (i == count)
				return 1;

			string v (input[i++]);

			struct { char const* fmt; asset::texture_format id; int stbcomp; } fmts[2] = {
				{ "i8", asset::i8, 1 },
		//		{ "r5g6b5", asset::r5g6b5i, 3 },
		//		{ "rgb8", asset::rgb8, 3 },
				{ "rgba8", asset::rgba8, 4 },
		//		{ "dxt1", asset::dxt1, 4 },
		//		{ "dxt3", asset::dxt3, 4 },
		//		{ "dxt5", asset::dxt5, 4 },
			};

			for (size_t i=0; i<sizeof(fmts)/sizeof(fmts[0]); ++i)
				if (v == fmts[i].fmt)
				{
					opts.ofmt = fmts[i].id;
					stbimg_components = fmts[i].stbcomp;
					break;
				}
		} else if (opt == "o" || opt == "output")
		{
			if (i == count)
				return 1;
			opts.opath = input[i++];
		} else if (opt == "mips")
		{
			if (i == count)
				return 1;
			opts.mips = atoi (input[i++]);
//		} else if (opt == "js")
//		{
//			opts.js = true;
		} else if (opt == "help" || opt == "h")
		{
			showhelp = true;
			break;
		} else if (opt == "verbose" || opt == "v")
		{
			opts.verbose++;
		}
	}

	if (narg < 2 || showhelp)
	{
		printf ("Usage: tex_tool [-v] [-fmt rgba8] [-mips N] [-o output-name] <texture>\n");
		return 1;
	}

	if (opts.opath.empty())
		opts.opath = stir::replace_extension (opts.ipath, path(".stex"));
   
	int imgw, imgh, component;
	if (opts.verbose)
		printf ("Loading '%s'\n", opts.ipath.c_str());
	unsigned char* imgdata = stbi_load (opts.ipath.c_str(), &imgw, &imgh, &component, stbimg_components);
	if (!imgdata) {
		printf ("Failed to load %s\n", opts.ipath.c_str());
		return 1;
	}
	
	int imgc = stbimg_components;

	asset::texture_asset a;
	asset::texture_builder builder (a, imgw, imgh, opts.ofmt);
	builder.compression (asset::compress_none);
	builder.add_level (image_ref (imgw, imgh, imgdata, imgc));
//	builder.generate_mips(...);
	builder.finish ();
	
//	std::vector<unsigned char> odata;
//	pulmotor::util::blit_to_container (ms, odata, false);

	size_t written = 0;
	if (1)
	{
#define USE_FILE 1
		
#if USE_FILE
		std::error_code ec;
//		if (opts.js)
//		{
//			using namespace rapidjson;
//			typedef Writer<rapidjson::FileWriteStream> writer_t;
//			
//			path jsonPath(opts.opath + ".json");
//			pulmotor::cfile_output_buffer outF (jsonPath.c_str(), ec);
//			if (ec) {
//				printf("Failed to open %s, error: (%d) %s\n", jsonPath.c_str(), ec.value(), ec.message().c_str());
//				return 1;
//			}
//			
//			std::fstream fo ((opts.opath + "json.txt").c_str(), std::ios_base::out);
//			
//			pulmotor::output_archive wa (outF);
//			pulmotor::debug_archive<pulmotor::output_archive> dwa (wa, fo);
//			pulmotor::archive(dwa, a);
//				
//			/*
//			char buffer[65536];
//			FileWriteStream outS (outF.handle(), buffer, sizeof(buffer));
//			writer_t w (outS);
//			{
//				pulmotor::json_output_archive<writer_t> jsOut (w);
//				pulmotor::archive(jsOut, a);
//			}*/
//		}
//		else
		{
//			std::fstream fo ((opts.opath + ".txt").c_str(), std::ios_base::out);
			pulmotor::cfile_output_buffer outF (opts.opath.c_str(), ec);
			pulmotor::output_archive wa (outF);
			if (opts.verbose) {
				pulmotor::debug_archive<pulmotor::output_archive> dwa (wa, std::cout);
				pulmotor::archive(dwa, a);
				written = dwa.offset();
			}
			else {
				pulmotor::archive(wa, a);
				written = wa.offset();
			}
		}
#else
		pulmotor::memory_archive_container_t arch;
		pulmotor::memory_write_archive wa (arch);
		pulmotor::archive(wa, a);
		written = pulmotor::util::write_file(opts.opath.c_str(), arch.data(), arch.size());
#endif
		
//		for (int i=0; i<a.levels.size(); ++i)
//		{
//			path fn = opts.opath + std::to_string(i) + "_original.tga";
//			stir::save_image(fn, a.levels[i]);
//		}
		
		// lets test reading back
		stir::asset::texture_asset b;
#if USE_FILE
		{
			pulmotor::cfile_input_buffer fin (opts.opath.c_str(), ec);
			pulmotor::input_archive ra (fin);
//			std::fstream fo_reading ((opts.opath + ".reading.txt").c_str(), std::ios_base::out);
//			pulmotor::debug_archive<pulmotor::input_archive> dra (ra, fo_reading);
			pulmotor::archive(ra, b);
		}
#else
		pulmotor::memory_read_archive ra (arch.data(), arch.size());
		pulmotor::archive(ra, b);
#endif
		
//		for (int i=0; i<b.levels.size(); ++i)
//		{
//			path fn = opts.opath + std::to_string(i) + ".tga";
//			stir::save_image(fn, b.levels[i]);
//		}
	}
/*	else
	{
		written = pulmotor::util::write_file (opts.opath.c_str(), a, pulmotor::target_traits::current, 8);
	}
*/
	printf ("file '%s' written, %lu bytes\n", opts.opath.c_str(), written);
	
	stbi_image_free (imgdata);

	return 0;
}
